Monapalette
======
The source code for monapalette.komikikaku.com.

License
------
The code is available under the MIT license. See the `LICENSE` file for the full text of this license.

The images under `web/img/theme/` were provided by the creators exclusively for monapalette.komikikaku.com, so please do not use them elsewhere.

コードはMITライセンスなので `LICENSE` ファイルをご確認ください。`web/img/theme/`以下の画像はモナパレット用に各クリエイターさんたちからご提供いただいたものなので、他所での利用はご遠慮ください。
