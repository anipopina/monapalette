const CACHE_NAME = 'mplt220713';
const deleteCaches = async() => {
    const cacheKeys = await caches.keys();
    for (const key of cacheKeys) await caches.delete(key);
    console.log('cache cleared');
};
const aPutCache = async(request, response) => {
    const clonedResponse = response.clone(); // responseは流用するとダメなので先にclone
    const cache = await caches.open(CACHE_NAME);
    await cache.put(request, clonedResponse);
};
const aFetchPutCache = async(request) => {
    const networkResponse = await fetch(request);
    await aPutCache(request, networkResponse);
};
self.addEventListener('install', () => {
    self.skipWaiting();
});
self.addEventListener('activate', (event) => {
    event.waitUntil((async() => {
        await deleteCaches();
        if ('navigationPreload' in self.registration) await self.registration.navigationPreload.enable();
    })());
    self.clients.claim(); // すぐにこのService Workerによる制御を開始
});
self.addEventListener('fetch', (event) => {
    // HTML: event.request.mode === 'navigate'
    if (event.request.mode === 'navigate' || event.request.url.endsWith('.js') || event.request.url.endsWith('.css')) {
        event.respondWith((async() => {
            const cache = await caches.open(CACHE_NAME);
            const cachedResponse = await cache.match(event.request);
            if (cachedResponse) {
                aFetchPutCache(event.request);
                return cachedResponse;
            }
            if (event.request.mode === 'navigate') {
                const preloadResponse = await event.preloadResponse;
                if (preloadResponse) {
                    aPutCache(event.request, preloadResponse);
                    return preloadResponse;
                }
            }
            const networkResponse = await fetch(event.request);
            aPutCache(event.request, networkResponse);
            return networkResponse;
        })());
    }
});
