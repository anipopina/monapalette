// version: 20211103
const Util = {};
Util.aSleep = (msec) => new Promise(resolve => setTimeout(resolve, msec));
Util.uniq = (array) => Array.from(new Set(array));
Util.round = (num, decimal) => Math.round(num * Math.pow(10, decimal)) / Math.pow(10, decimal);
Util.randomInt = (min, max) => Math.floor(Math.random() * (max + 1 - min)) + min;
Util.reverseStr = (str) => str.split('').reverse().join('');
Util.zeroPadding = (num, length) => ('0'.repeat(length) + num).slice(-length);
Util.saveStorage = Util.saveObjectToLocalStorage = (key, object) => {
    localStorage.setItem(key, JSON.stringify(object));
};
Util.loadStorage = Util.loadObjectFromLocalStorage = (key) => {
    const value = localStorage.getItem(key);
    if (!value) return null;
    try {
        if (value[0] === '%') return JSON.parse(decodeURIComponent(value)); // 旧版との互換用
        else return JSON.parse(value)
    }
    catch (error) { console.error(error); return null }
};
Util.floorLargeInt = (largeInt) => {
    return (largeInt > Number.MAX_SAFE_INTEGER)?
        Math.floor(largeInt * ((Number.MAX_SAFE_INTEGER - 2) / Number.MAX_SAFE_INTEGER)) : largeInt;
};
Util.ref2array = (ref) => {
    if (Array.isArray(ref)) return ref;
    if (!ref) return [];
    return [ref];
};
Util.dictizeListWithKey = (list, key) => {
    const dict = {};
    for (const item of list) item[key] && (dict[item[key]] = item);
    return dict;
};
Util.shuffle = (array) => {
    let n = array.length;
    while (n) {
        const i = Math.floor(Math.random() * n--);
        [array[n], array[i]] = [array[i], array[n]];
    }
};
Util.execCopy = string => {
    const tmp = document.createElement('div');
    const style = tmp.style;
    let success = false;
    tmp.appendChild(document.createElement('pre')).textContent = string;
    style.position = 'fixed';
    style.left = '-100%';
    document.body.appendChild(tmp);
    document.getSelection().selectAllChildren(tmp);
    success = document.execCommand('copy'); // 成功でtrue、失敗or未対応だとfalse
    document.body.removeChild(tmp);
    return success;
};
Util.commafy = (num, { decimal = 8, removeDecimalZero = true, noComma = false } = {}) => {
    num = Number(num);
    if (isNaN(num)) return null;
    const parts = num.toFixed(decimal).split('.');
    if (!noComma) parts[0] = parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
    if (parts[1] && removeDecimalZero) parts[1] = parts[1].replace(/0+$/, '');
    return parts[1] ? parts.join('.') : parts[0];
};
Util.escapeHTML = (str) => {
    return str.replace(/[&'`"<>]/g, match => {
        return { '&': '&amp;', "'": '&#x27;', '`': '&#x60;', '"': '&quot;', '<': '&lt;', '>': '&gt;' }[match];
    });
};
Util.aTimeout = (ms, promise) => {
    return new Promise((resolve, reject) => {
        const timeoutID = setTimeout(() => reject(new Error('timeout by aTimeout()')), ms);
        promise.then(resolve).catch(reject).finally(() => { clearTimeout(timeoutID) });
    });
};
Util.aFetchWithTimeout = async(timeoutMs, url, fetchParams) => {
    return await Util.aTimeout(timeoutMs, fetch(url, fetchParams));
};
Util.aRedundantFetch = async(timeoutMs, urls, fetchParams) => {
    let error;
    for (const url of urls) {
        try {
            const response = await Util.aFetchWithTimeout(timeoutMs, url, fetchParams);
            if (!response.ok) throw new Error(`aRedundantFetch response was not ok: ${url}`);
            return response;
        }
        catch (_error) { console.error(error = _error) }
    }
    throw error;
};
Util.aGetJSON = async(url, useCache = false) => {
    if (!Util.aGetJSON.cacheDict) Util.aGetJSON.cacheDict = {};
    if (useCache && (url in Util.aGetJSON.cacheDict)) return Util.aGetJSON.cacheDict[url];
    try {
        console.log('aGetJSON: ' + url);
        const response = await fetch(url);
        if (!response.ok) throw new Error(`network response was not ok. status: ${response.status}`);
        const json = await response.json();
        if (useCache) Util.aGetJSON.cacheDict[url] = json;
        else delete Util.aGetJSON.cacheDict[url];
        return json;
    }
    catch (error) {
        delete Util.aGetJSON.cacheDict[url];
        throw error;
    }
};
Util.aGetNumber = async(url, useCache = false) => {
    if (!Util.aGetNumber.cacheDict) Util.aGetNumber.cacheDict = {};
    if (useCache && (url in Util.aGetNumber.cacheDict)) return Util.aGetNumber.cacheDict[url];
    try {
        console.log('aGetNumber: ' + url);
        const response = await fetch(url);
        if (!response.ok) throw new Error(`network response was not ok. status: ${response.status}`);
        const number = Number(await response.text());
        if (useCache) Util.aGetNumber.cacheDict[url] = number;
        else delete Util.aGetNumber.cacheDict[url];
        return number;
    }
    catch (error) {
        delete Util.aGetNumber.cacheDict[url];
        throw error;
    }
};
Util.aJsonRPCRequest = async(url, method, params = {}, useCache = false) => {
    if (!Util.aJsonRPCRequest.cacheDict) Util.aJsonRPCRequest.cacheDict = {};
    const headers = { 'Content-Type': 'application/json', 'Accept': 'application/json' };
    const body = JSON.stringify({ id: 1, method, params });
    const cacheKey = `${url},${method},${body}`;
    if (useCache && Util.aJsonRPCRequest.cacheDict[cacheKey]) return Util.aJsonRPCRequest.cacheDict[cacheKey];
    try {
        console.log(`aJsonRPCRequest: ${url} ${method}`);
        const response = await fetch(url, { method: 'POST', headers, body });
        if (!response.ok) throw new Error(`network response was not ok. status: ${response.status}`);
        const parsedResponse = await response.json();
        if (parsedResponse.error) throw parsedResponse.error;
        if (useCache) Util.aJsonRPCRequest.cacheDict[cacheKey] = parsedResponse.result;
        else delete Util.aJsonRPCRequest.cacheDict[cacheKey];
        return parsedResponse.result;
    }
    catch (error) {
        delete Util.aJsonRPCRequest.cacheDict[cacheKey];
        throw error;
    }
};
Util.aOpenContentSwal = (contentDiv, additionalOptions = {}) => {
    if (!contentDiv) return;
    const options = { content: contentDiv, button: false, ...additionalOptions };
    if (window.myswal) return window.myswal(options);
    else return window.swal(options);
};
