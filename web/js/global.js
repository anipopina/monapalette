/*global Util, route, swal, myswal, TrezorConnect */
window.opener = null; // treasureビューアからの親ウィンドウ操作をブロック
const COINS = {
    MONA: 'MONA',
    BTC: 'BTC',
    MONA_TESTNET: 'MONA_TESTNET',
    BTC_TESTNET: 'BTC_TESTNET',
};
const ADDR_TYPE = {
    P2PKH: 'P2PKH',
    P2WPKH: 'P2WPKH',
};
const DISPENSER_STATUS = {
    OPEN: 0,
    CLOSE: 10,
};
const TAGG_TIME = new Date().getTime();
const TagG = {
    /* モナパレットはMonapartyのAPI仕様やCounterwalletのUIを参考に設計されています  https://github.com/monaparty */

    /* MIT License  Copyright (c) 2011-2019 bitcoinjs-lib contributors  https://github.com/bitcoinjs/bitcoinjs-lib/blob/master/LICENSE */
    bitcoinjs: require('bitcoinjs-lib'),
    /* MIT License  Copyright (c) 2015-2016 Bitcoinjs developers  https://github.com/bitcoinjs/bitcoinjs-message/blob/master/LICENSE */
    bitcoinjsMessage: require('bitcoinjs-message'),
    /* MIT License  coininfo https://opensource.org/licenses/mit-license.php */
    coininfo: require('coininfo'),
    /* MIT License  Copyright (c) 2018 Daniel Cousens  https://github.com/bitcoinjs/coinselect/blob/master/LICENSE */
    coinselect: require('coinselect'),
    coinselectSplit: require('coinselect/split'),
    /* MIT License  Copyright (c) 2018 cryptocoinjs  https://github.com/cryptocoinjs/hdkey/blob/master/LICENSE */
    HDKey: require('hdkey'),
    /* ISC License  Copyright (c) 2014, Wei Lu <luwei.here@gmail.com> and Daniel Cousens <email@dcousens.com>  https://github.com/bitcoinjs/bip39/blob/master/LICENSE */
    bip39: require('bip39'),
    /* MIT License  Copyright (c) 2009-2013 Jeff Mott Copyright (c) 2013-2016 Evan Vosberg  https://github.com/brix/crypto-js/blob/develop/LICENSE */
    CryptoJS: require('crypto-js'),
    /* MIT License  Copyright (c) Fedor Indutny, 2014  https://github.com/indutny/hash.js */
    hash: require('hash.js'),
    /* MIT License  Copyright (c) 2018 cryptocoinjs  https://github.com/cryptocoinjs/bs58/blob/master/LICENSE */
    Base58: require('bs58'),
    /* MIT License  Copyright (c) 2017 crypto-browserify  https://github.com/crypto-browserify/randombytes/blob/master/LICENSE */
    randomBytes: require('randombytes'),
    /* MIT License  Copyright (c) 2013 keybase  https://github.com/keybase/more-entropy/blob/master/LICENSE */
    MoreEntropy: require('more-entropy'),
    /* MIT License  Copyright (c) Fedor Indutny, 2017  https://github.com/indutny/hmac-drbg */
    HmacDRBG: require('hmac-drbg'),

    query: route.query() || {},
    body: null,
    bodySpinner: null,
    bodyDispenserPurchase: null,
    bodyDexPurchase: null,
    isReadOnlyMode: false,
    isTrezor: false,
    jp: true,
    confNum: 0,
    addrsData: null,
    addrsDataMaxIndex: 0,
    addrSelectItems: [],
    addrSelectDefaultValue: null,
    firstAddrLabel: null,
    originalDocumentTitle: document.title,
    addr2isMine: {},
    asset2isInMyWallet: {}, // assetNameでもOK
    asset2isMyOwn: {}, // assetNameでもOK
    asset2info: {}, // assetNameでもOK
    asset2extendedInfo: {},
    asset2card: {},
    asset2treasures: {},
    asset2rakugakiTitle: {},
    asset2isAnime: {},
    addrbook: [],
    tokenVaultCacheID: TAGG_TIME,
    rakugakiAsset2cacheID: {},
    dboardReadIndex: 0,
    changedAssets: [],
    windowMessageListeners: [],

    IS_PREVIEW: (location.hostname.indexOf('localhost') !== -1),
    IS_LAZYLOADAVAILABLE: ('loading' in HTMLImageElement.prototype),
    RELATIVE_URL_BASE: '/',
    RELATIVE_LINK_BASE: '/',
    IS_MPURSEEX: (window.MONAPALETTE_IS_MPURSEEX === true),
    QUERYKEY_ENCMNEMONIC: 'ep', // pはパスフレーズ時代の名残り
    QUERYKEY_ADDRTYPE: 'at',
    QUERYKEY_ACCOUNT: 'account',
    QUERYKEY_TREZORPATH: 'trezor_path',
    QUERYKEY_SEARCH: 'search',
    QUERYKEY_TREASUREURL: 'tu',
    MAX_ADDRCOUNT: 20,
    SWAL_TIMER: 2000,
    COIN: window.MONAPALETTE_COIN || COINS.MONA,
    STORAGEKEY_XPUB_FORMAT: 'monapalette_xpub_{HASH}',
    STORAGEKEY_ADDRINDEX_FORMAT: 'monapalette_addrindex_{FIRST_ADDR}',
    STORAGEKEY_LABEL_FORMAT: 'monapalette_label_{ADDR}',
    STORAGEKEY_STYLE_FORMAT: 'monapalette_style_{ADDR}',
    STORAGEKEY_RECENTPAIRS: 'monapalette_recentpairs',
    STORAGEKEY_ADDRBOOK: 'monapalette_addrbook',
    STORAGEKEY_ENDPOINTS_FORMAT: 'monapalette_blockendpoints_{COIN}',
    STORAGEKEY_CONFNUM: 'monapalette_confnum',
    STORAGEKEY_LANG: 'monapalette_lang',
    STORAGEKEY_DBOARDREADINDEX_FORMAT: 'monapalette_dboardreadindex_{FIRST_ADDR}',
    IPFSURL_FORMAT: 'https://mona.party/ipfs/{HASH}',
    TWITTERUSERURL_FORMAT: 'https://twitter.com/{SN}',
    TWEETURL_FORMAT: 'https://twitter.com/{SN}/status/{TWID}',
    OPENTWEETURL_FORMAT: 'https://twitter.com/intent/tweet?text={ENCODEDTXT}',
};
for (const key in TagG.query) TagG.query[key] = TagG.query[key].split('#')[0];
if (TagG.IS_PREVIEW) {
    TagG.RELATIVE_URL_BASE = '';
    TagG.RELATIVE_LINK_BASE = '#';
}
TagG.setMonaParams = () => {
    TagG.BASECHAIN_ASSET = 'MONA';
    TagG.BUILTIN_ASSET = 'XMP';
    TagG.SATOSHI_RATIO = 100000000;
    TagG.SATOSHI_DECIMAL = 8;
    TagG.AVG_BLOCKINTERVAL_MS = 1000 * 90;
    TagG.FEESAT_PERBYTE = 200;
    TagG.MULTISIG_FEESAT_PERBYTE = 250;
    TagG.SENDMIN_SAT = 1000;
    TagG.ISSUEFEE_NAMED = 50;
    TagG.ISSUEFEE_SUB = 25;
    TagG.ISSUEFEE_NFT = 0.25;
    TagG.ISSUEFEE_NUMERIC = 0;
    TagG.DIVIDENDFEE_PER_RECIPIENT = 0.0002;
    TagG.SWEEPFEE_AMOUNT = 0.5;
    TagG.ADDR_PATTERN = '([MP][1-9A-HJ-NP-Za-km-z]{33}|mona1[ac-hj-np-z02-9]{8,87})';
    TagG.INSIGHT_API_ENDPOINT = 'https://mona.insight.monaco-ex.org/insight-api-monacoin/';
    TagG.BLOCKBOOK_API_ENDPOINTS = ['https://blockbook.electrum-mona.org/api/', 'https://blockbook.monacoin.cloud/api/'];
    TagG.ADDR_EXPLORERURL_FORMAT = 'https://blockbook.electrum-mona.org/address/{ADDR}';
    TagG.ADDR_PARTYEXPURL_FORMAT = 'https://mpchain.info/address/{ADDR}';
    TagG.ASSET_PARTYEXPURL_FORMAT = 'https://mpchain.info/asset/{ASSET}';
    TagG.CARD_DETAILURL_FORMAT = 'https://card.mona.jp/explorer/card_detail?asset={ASSETNAME}';
    TagG.TREZOR_COIN = 'MONA';
    TagG.NETWORK = TagG.coininfo('MONA').toBitcoinJS();
    // ここまではMONAとBTCともに設定
    TagG.CARDSERVER_API = 'https://card.mona.jp/api/';
    TagG.TOKENVAULT_API = 'https://5addehsclc.execute-api.ap-northeast-1.amazonaws.com/';
    TagG.TOKENVAULT_ROOT = 'https://tokenvault.s3-ap-northeast-1.amazonaws.com/';
    TagG.TOKENVAULT_VIEWER = 'treasure.html';
    TagG.ANIMELIST_JSON = 'https://monapachan.s3.ap-northeast-1.amazonaws.com/animelist.json';
    TagG.SHOPS_JSON = 'https://monapachan.s3.ap-northeast-1.amazonaws.com/shops.json';
    TagG.SHOPS_IMGROOT = 'https://monapachan-img.s3-ap-northeast-1.amazonaws.com/shop-img/';
    TagG.DBOARD_CACHEJSON = 'https://monapalette.s3.ap-northeast-1.amazonaws.com/dboardcache.json';
    TagG.DISPENSERS_CACHEJSON = 'https://monapalette.s3.ap-northeast-1.amazonaws.com/dispenserscache.json';
    TagG.DEXASSETS_CACHEJSON = 'https://monapalette.s3.ap-northeast-1.amazonaws.com/dexassetscache.json';
    TagG.HOLDERS_CACHEJSON = 'https://monapalette.s3.ap-northeast-1.amazonaws.com/holderscache.json';
    TagG.MONACHARAT_ASSETGROUPS = ['MonaCharaT'];
    TagG.MONACHARAT_IMGROOT = 'https://monacharat-img.komikikaku.com/';
    TagG.MONACHARAT_SITEROOT = 'https://monacharat.komikikaku.com/';
    TagG.RAKUGAKI_ASSETGROUPS = ['Rakugaki'];
    TagG.RAKUGAKI_IMGROOT = 'https://tokenvault.s3-ap-northeast-1.amazonaws.com/rakugaki/';
    TagG.MONACUTE_ASSETGROUPS = ['Monacute'];
    TagG.MONACUTE_URLFORMAT = 'https://monacute.art/monacutes/{ID}';
    TagG.MONACUTE_IMGFORMAT = 'https://image.monacute.art/{DNA}.png';
    if (TagG.IS_PREVIEW) {
        // TagG.TOKENVAULT_API = 'https://gshhmlbpwb.execute-api.ap-northeast-1.amazonaws.com/';
        // TagG.TOKENVAULT_ROOT = 'https://tokenvault-test.s3-ap-northeast-1.amazonaws.com/';
        TagG.TOKENVAULT_VIEWER = 'treasure_preview.html';
        TagG.MONACHARAT_ASSETGROUPS.push('TestCharaT');
        TagG.RAKUGAKI_ASSETGROUPS.push('NFTEST');
    }
};
TagG.setBtcParams = () => {
    TagG.BASECHAIN_ASSET = 'BTC';
    TagG.BUILTIN_ASSET = 'XCP';
    TagG.SATOSHI_RATIO = 100000000;
    TagG.SATOSHI_DECIMAL = 8;
    TagG.AVG_BLOCKINTERVAL_MS = 1000 * 60 * 10;
    TagG.FEESAT_PERBYTE = 20;
    TagG.MULTISIG_FEESAT_PERBYTE = 20;
    TagG.SENDMIN_SAT = 1000;
    TagG.ISSUEFEE_NAMED = 0.5;
    TagG.ISSUEFEE_SUB = 0.25;
    TagG.ISSUEFEE_NFT = 1e10; // unavailable
    TagG.ISSUEFEE_NUMERIC = 0;
    TagG.DIVIDENDFEE_PER_RECIPIENT = 0.0002;
    TagG.SWEEPFEE_AMOUNT = 0.5;
    TagG.ADDR_PATTERN = '([13][1-9A-HJ-NP-Za-km-z]{33}|bc1[ac-hj-np-z02-9]{8,87})';
    TagG.INSIGHT_API_ENDPOINT = null;
    TagG.BLOCKBOOK_API_ENDPOINTS = null;
    TagG.ADDR_EXPLORERURL_FORMAT = 'https://blockstream.info/address/{ADDR}';
    TagG.ADDR_PARTYEXPURL_FORMAT = 'https://xchain.io/address/{ADDR}';
    TagG.ASSET_PARTYEXPURL_FORMAT = 'https://xchain.io/asset/{ASSET}';
    TagG.CARD_DETAILURL_FORMAT = null;
    TagG.TREZOR_COIN = 'BTC';
    TagG.NETWORK = TagG.coininfo('BTC').toBitcoinJS();
    // ここまではMONAとBTCともに設定
    TagG.MONAPARTY_ENDPOINTS = ['https://wallet.counterwallet.io/_api'];
};
switch (TagG.COIN) {
    case COINS.MONA:
        TagG.setMonaParams();
        break;
    case COINS.MONA_TESTNET:
        TagG.setMonaParams();
        TagG.ADDR_PATTERN = '([mnp][1-9A-HJ-NP-Za-km-z]{33}|tmona1[ac-hj-np-z02-9]{8,87})';
        TagG.INSIGHT_API_ENDPOINT = null;
        TagG.BLOCKBOOK_API_ENDPOINTS = null;
        TagG.ADDR_EXPLORERURL_FORMAT = 'https://testnet-blockbook.electrum-mona.org/address/{ADDR}';
        TagG.ADDR_PARTYEXPURL_FORMAT = 'https://testnet.mpchain.info/address/{ADDR}';
        TagG.ASSET_PARTYEXPURL_FORMAT = 'https://testnet.mpchain.info/asset/{ASSET}';
        TagG.CARDSERVER_API = null;
        TagG.TREZOR_COIN = 'MONA_TESTNET';
        TagG.NETWORK = TagG.coininfo('MONA-TEST').toBitcoinJS();
        TagG.MONAPARTY_ENDPOINTS = ['https://testnet-monapa.electrum-mona.org/_t_api', 'https://wallet.monaparty.me/_t_api/'];
        TagG.MONACHARAT_ASSETGROUPS = [];
        break;
    case COINS.BTC:
        TagG.setBtcParams();
        break;
    case COINS.BTC_TESTNET:
        TagG.setBtcParams();
        TagG.ADDR_PATTERN = '([mn2][1-9A-HJ-NP-Za-km-z]{33,34}|tb1[ac-hj-np-z02-9]{8,87})';
        TagG.ADDR_EXPLORERURL_FORMAT = 'https://blockstream.info/testnet/address/{ADDR}';
        TagG.ADDR_PARTYEXPURL_FORMAT = 'https://testnet.xchain.io/address/{ADDR}';
        TagG.ASSET_PARTYEXPURL_FORMAT = 'https://testnet.xchain.io/asset/{ASSET}';
        TagG.TREZOR_COIN = 'TEST';
        TagG.NETWORK = TagG.coininfo('BTC-TEST').toBitcoinJS();
        TagG.MONAPARTY_ENDPOINTS = ['https://wallet.counterwallet.io/_t_api'];
        break;
}
const Monaparty = require('../mymodules/Monaparty.js');
{
    Monaparty.timeout = 30000;
    Monaparty.defaults = {
        serversFile: Monaparty.serversFile,
        fullEndpoints: Monaparty.fullEndpoints,
        nocoindEndpoints: Monaparty.nocoindEndpoints,
    };
    const savedEndpoints = Util.loadStorage(TagG.STORAGEKEY_ENDPOINTS_FORMAT.replace('{COIN}', TagG.COIN));
    if (savedEndpoints || TagG.MONAPARTY_ENDPOINTS) {
        Monaparty.serversFile = null;
        Monaparty.fullEndpoints = savedEndpoints || TagG.MONAPARTY_ENDPOINTS;
        Monaparty.nocoindEndpoints = [];
    }
    Monaparty.aParty = Monaparty.aCounterPartyRequest;
    Monaparty.aBlock = Monaparty.aCounterBlockRequest;
    Monaparty.aPartyTableAll = Monaparty.aCounterPartyRequestTableAll;
}
TagG.aSignedJsonRPCRequest = async(url, method, params, addrData) => {
    const { address } = addrData;
    const paramsToSign = {
        method: method,
        timestamp: new Date().getTime(),
        params: params || {},
    };
    const paramStr = JSON.stringify(paramsToSign);
    const sign = await TagG.aSignMessage(paramStr, addrData);
    return await Util.aJsonRPCRequest(url, method, { sign, address, paramStr }, false);
};
{
    let _mnemonic, _accountXpub, _wif, _addrType, _accountPath, _encMnemonic;
    TagG.setMnemonic = (mnemonic, addrType, accountPath = null, encMnemonic = null) => {
        [_mnemonic, _addrType, _encMnemonic] = [mnemonic, addrType, encMnemonic];
        _accountPath = accountPath || TagG.addrType2defaultAccountPath(addrType);
    };
    TagG.setAccountXpub = (accountXpub, addrType, accountPath, encMnemonic = null) => {
        [_accountXpub, _addrType, _accountPath, _encMnemonic] = [accountXpub, addrType, accountPath, encMnemonic];
        if (encMnemonic) TagG.isReadOnlyMode = true;
    };
    TagG.setWIF = (wif, addrType) => {
        [_wif, _addrType] = [wif, addrType];
    };
    TagG.aUpdateAddrsDataWithUI = async(spinner = null) => {
        try {
            if (spinner) spinner.start();
            const nowTime = new Date().getTime();
            if (TagG.addrsData) {
                if (TagG.aUpdateAddrsDataWithUI.lastTime > nowTime - TagG.AVG_BLOCKINTERVAL_MS / 3) return;
                await aUpdateBalances(TagG.addrsData.slice(0, TagG.addrsDataMaxIndex + 1));
            }
            else {
                TagG.addrsData = await aGetAddrsData();
                if (!TagG.addrsData) return;
                TagG.addrsDataMaxIndex = await aUpdateBalances(TagG.addrsData);
                const indexStoreKey = TagG.STORAGEKEY_ADDRINDEX_FORMAT.replace('{FIRST_ADDR}', TagG.addrsData[0].address);
                const storedIndex = Util.loadStorage(indexStoreKey);
                if (storedIndex > TagG.addrsDataMaxIndex) TagG.addrsDataMaxIndex = storedIndex;
                TagG.addr2isMine = {};
                for (const addrData of TagG.addrsData) TagG.addr2isMine[addrData.address] = true;
                const dboardIndexStoreKey = TagG.STORAGEKEY_DBOARDREADINDEX_FORMAT.replace('{FIRST_ADDR}', TagG.addrsData[0].address);
                TagG.dboardReadIndex = Util.loadStorage(dboardIndexStoreKey) || 0;
                TagG.setAddrSelectItems();
                TagG.updatefirstAddrLabel();
                TagG.aCheckMentions();
            }
            TagG.aUpdateAddrsDataWithUI.lastTime = nowTime;
        }
        catch (error) {
            console.error(error);
            myswal('Error', TagG.jp ? `アドレスデータの取得に失敗しました。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}` : `Failed to fetch address data. hint: ${error}`, 'error');
            return { error };
        }
        finally { if (spinner) spinner.stop() }
    };
    TagG.setAddrSelectItems = () => {
        TagG.addrSelectItems = TagG.addrsData.slice(0, TagG.addrsDataMaxIndex + 1)
            .map((data, i) => ({ value: data.address, label: `${i + 1}: ${data.label || data.address}` }));
        TagG.addrSelectDefaultValue = TagG.addrSelectItems[0].value;
    };
    const aGetAddrsData = async() => {
        if (_mnemonic) return getAddrsDataForMnemonic();
        else if (_accountXpub) return getAddrsDataForAccountXpub();
        else if (_wif) return getAddrsDataForWIF();
        else if (TagG.IS_MPURSEEX && window.mpurse) return await aGetAddrsDataFromMpurse();
        else return null;
    };
    const aUpdateBalances = async(addrsData) => {
        const [baseIndex, assetIndex] = await Promise.all([TagG.aUpdateBaseAssetBalances(addrsData), TagG.aUpdateAssetBalances(addrsData)]);
        const maxIndex = Math.max(baseIndex, assetIndex);
        return maxIndex;
    };
    const getAddrsDataForMnemonic = () => {
        const addrsData = [];
        const seed = TagG.bip39.mnemonicToSeed(_mnemonic);
        const masterHDKey = TagG.HDKey.fromMasterSeed(seed);
        for (let index = 0; index < TagG.MAX_ADDRCOUNT; index++) {
            const path = `${_accountPath}/0/${index}`;
            const { ecPair, addrObj } = getKeyDataForDerivePath(masterHDKey, path, _addrType);
            addrsData.push(getAddrDataFormat({ index, addrObj, ecPair, addrType: _addrType, path }));
        }
        if (_encMnemonic) {
            const accountHDKey = masterHDKey.derive(_accountPath);
            saveAccountXpub(accountHDKey.publicExtendedKey, _accountPath, _encMnemonic);
        }
        return addrsData;
    };
    const getAddrsDataForAccountXpub = () => {
        const addrsData = [];
        const accountHDKey = TagG.HDKey.fromExtendedKey(_accountXpub, TagG.NETWORK.versions.bip32);
        for (let index = 0; index < TagG.MAX_ADDRCOUNT; index++) {
            const path = `m/0/${index}`;
            const fullPath = path.replace('m', _accountPath);
            const { addrObj } = getKeyDataForDerivePath(accountHDKey, path, _addrType);
            addrsData.push(getAddrDataFormat({ index, addrObj, addrType: _addrType, path: fullPath }));
        }
        return addrsData;
    };
    const getAddrsDataForWIF = () => {
        const { ecPair, addrObj } = getKeyDataForWIF(_wif, _addrType);
        const addrData = getAddrDataFormat({ index: 0, addrObj, ecPair, addrType: _addrType });
        return [addrData];
    };
    const aGetAddrsDataFromMpurse = async() => {
        const address = await window.mpurse.getAddress();
        const addrData = getAddrDataFormat({ index: 0, address });
        return [addrData];
    };
    const saveAccountXpub = (accountXpub, accountPath, encMnemonic) => {
        const encMnemonicHash = TagG.hash.sha256().update(encMnemonic).digest('hex');
        const key = TagG.hash.sha256().update(encMnemonicHash + accountPath).digest('hex');
        const encXpub = TagG.CryptoJS.AES.encrypt(accountXpub, encMnemonicHash).toString();
        Util.saveStorage(TagG.STORAGEKEY_XPUB_FORMAT.replace('{HASH}', key), encXpub);
    };
    TagG.loadAccountXpub = (accountPath, encMnemonic) => {
        const encMnemonicHash = TagG.hash.sha256().update(encMnemonic).digest('hex');
        const key = TagG.hash.sha256().update(encMnemonicHash + accountPath).digest('hex');
        const encXpub = Util.loadStorage(TagG.STORAGEKEY_XPUB_FORMAT.replace('{HASH}', key));
        if (!encXpub) return null;
        const accountXpub = TagG.CryptoJS.enc.Utf8.stringify(TagG.CryptoJS.AES.decrypt(encXpub, encMnemonicHash));
        return accountXpub;
    };
    TagG.aUpgradeFromReadOnlyMode = async() => {
        const text = TagG.jp?
        '現在Read Onlyモードでウォレットを開いています。秘密鍵を利用するためにはQuick Access URLに対応するパスワードを入力してください':
        'You are currently opening the wallet in read-only mode. Enter the quick-access password to use private key.';
        const swalContent = TagG.swalContents.getPasswordWin({ title: (TagG.jp ? 'パスワード' : 'Password'), text });
        const isSpinning = TagG.bodySpinner.isSpinning;
        if (isSpinning) TagG.bodySpinner.stop();
        await Util.aOpenContentSwal(swalContent, { isCutin: true });
        if (isSpinning) TagG.bodySpinner.start();
        if (!swalContent.isOK) throw (TagG.jp ? 'パスワードの入力をキャンセルしました' : 'Password input cancelled.');
        const password = swalContent.value;
        let mnemonic;
        try { mnemonic = TagG.CryptoJS.enc.Utf8.stringify(TagG.CryptoJS.AES.decrypt(_encMnemonic, password)) }
        catch (error) { throw 'Invalid Password' }
        if (!TagG.bip39.validateMnemonic(mnemonic)) throw 'Invalid Password';
        _mnemonic = mnemonic;
        const seed = TagG.bip39.mnemonicToSeed(_mnemonic);
        const masterHDKey = TagG.HDKey.fromMasterSeed(seed);
        for (const addrData of TagG.addrsData) {
            const { ecPair, addrObj } = getKeyDataForDerivePath(masterHDKey, addrData.path, addrData.addrType);
            if (addrData.address !== addrObj.address) throw new Error('upgrade address mismatch');
            addrData.ecPair = ecPair;
        }
        TagG.isReadOnlyMode = false;
        TagG.body.update(); // body-bottomfloat を更新
    };
    TagG.aUpdateBaseAssetBalances = async(addrsData) => {
        let maxIndex;
        if (TagG.BLOCKBOOK_API_ENDPOINTS) {
            maxIndex = await aSetUTXOsByBlockbookAPI(addrsData).catch(console.error);
            if (typeof maxIndex === 'number') return maxIndex;
        }
        if (TagG.INSIGHT_API_ENDPOINT && (addrsData[0] && addrsData[0].addrType === ADDR_TYPE.P2PKH)) {
            maxIndex = await aSetUTXOsByInsightAPI(addrsData).catch(console.error);
            if (typeof maxIndex === 'number') return maxIndex;
        }
        return await aSetUTXOsByMonapartyAPI(addrsData);
    };
    TagG.aUpdateAssetBalances = async(addrsData) => {
        try {
            const nowTime = new Date().getTime();
            const addrs = addrsData.map(data => data.address);
            if (!addrsData.some(addrData => (addrData.assetBlock === 0 || addrData.assetTime < nowTime - 1000 * 60 * 20))) {
                const lastHeight = addrsData.reduce((minHeight, data) => (data.assetBlock < minHeight ? data.assetBlock : minHeight), Number.MAX_SAFE_INTEGER);
                const isChanged = await aCheckIsAssetBalanceChanged(addrs, lastHeight);
                if (!isChanged) {
                    await TagG.aLoadTreasures();
                    return TagG.addrsDataMaxIndex;
                }
            }
            let maxIndex = 0;
            aGetBlockHeight().then(height => {
                for (const data of addrsData) data.assetBlock = height;
            }).catch(console.error); // awaitしない
            const addr2data = Util.dictizeListWithKey(addrsData, 'address');
            const balanceFilters = [{ field: 'address', op: 'IN', value: addrs }];
            const issuanceFilters = [{ field: 'issuer', op: 'IN', value: addrs }, { field: 'transfer', op: '==', value: 1 }];
            const dispenserFilters = [{ field: 'source', op: 'IN', value: addrs }, { field: 'status', op: '==', value: DISPENSER_STATUS.OPEN }]; // get_dispensersはstatusパラメータが効かないのでフィルタでstatus指定
            const [balances, transferIssuances, escrs, dispensers] = await Promise.all([
                Monaparty.aPartyTableAll('get_balances', { filters: balanceFilters }),
                Monaparty.aPartyTableAll('get_issuances', { filters: issuanceFilters, status: 'valid', order_by: 'tx_index', order_dir: 'ASC' }),
                Monaparty.aBlock('get_escrowed_balances', { addresses: addrs }),
                Monaparty.aParty('get_dispensers', { filters: dispenserFilters }),
                TagG.aLoadTreasures(),
            ]);
            const assets = [...balances.map(bal => bal.asset), ...transferIssuances.map(iss => iss.asset)];
            if (TagG.changedAssets.length > 0) {
                await TagG.aLoadAssets(TagG.changedAssets, false, false);
                TagG.changedAssets = [];
            }
            await TagG.aLoadAssets(assets, true, true);
            for (const issuance of transferIssuances) { // transferでownerだけ受取るとbalanceに載らないためissuancesから補完
                const assetInfo = TagG.asset2info[issuance.asset];
                if (!addr2data[assetInfo.owner]) continue;
                if (!balances.some(bal => (bal.asset === assetInfo.asset && bal.address === assetInfo.owner)))
                    balances.push({ address: assetInfo.owner, asset: assetInfo.asset, quantity: 0 });
            }
            for (const addrData of addrsData) {
                addrData.builtinSat = 0;
                addrData.builtinUnconfSat = 0;
                addrData.builtinEscrSat = 0;
                addrData.assets = [];
                addrData.dispensers = [];
            }
            for (const dispenser of dispensers) {
                const addrData = addr2data[dispenser.source];
                addrData.dispensers.push(TagG.formatDispenserData(dispenser));
            }
            TagG.asset2isInMyWallet = {};
            TagG.asset2isMyOwn = {};
            for (const balance of balances) {
                const addrData = addr2data[balance.address];
                const assetInfo = TagG.asset2info[balance.asset];
                const dispenser = addrData.dispensers.find(disp => (disp.asset === balance.asset));
                let escrSat = (escrs[balance.address] && escrs[balance.address][balance.asset]) || 0;
                if (dispenser) escrSat += dispenser.remainingSat;
                if (balance.asset === TagG.BUILTIN_ASSET) {
                    addrData.builtinSat = balance.quantity;
                    addrData.builtinUnconfSat = 0; // 未実装
                    addrData.builtinEscrSat = escrSat;
                    if (balance.quantity) TagG.asset2isInMyWallet[TagG.BUILTIN_ASSET] = true;
                }
                else {
                    const asset = assetInfo.asset;
                    const name = assetInfo.name;
                    const cardData = TagG.asset2card[name] || null;
                    const assetData = { // format: assetData
                        ...assetInfo,
                        satoshi: balance.quantity,
                        unconfSat: 0, // 未実装
                        escrSat: escrSat,
                        isOwn: assetInfo.owner === balance.address,
                        imageL: TagG.getAssetImageURL('L', asset),
                        imageM: TagG.getAssetImageURL('M', asset),
                        imageS: TagG.getAssetImageURL('S', asset),
                        imageAuto: TagG.getAssetImageURL('auto', asset),
                        cardData,
                        infoRef: assetInfo,
                    };
                    assetData.searchText = getAssetSearchText(assetData);
                    if (assetData.satoshi || assetData.unconfSat || assetData.escrSat || assetData.isOwn) addrData.assets.push(assetData);
                    if (assetData.satoshi || assetData.escrSat) TagG.asset2isInMyWallet[asset] = TagG.asset2isInMyWallet[name] = true;
                    if (assetData.isOwn) TagG.asset2isMyOwn[asset] = TagG.asset2isMyOwn[name] = true;
                }
                if (addrData.index > maxIndex) maxIndex = addrData.index;
            }
            for (const data of addrsData) data.assetTime = nowTime;
            return maxIndex;
        }
        catch (error) {
            for (const data of addrsData) [data.assetTime, data.assetBlock] = [0, 0];
            throw error;
        }
    };
    const getAssetSearchText = (assetData) => {
        const { asset, name, group, description, isLocked, cardData } = assetData;
        let text = `${name.toLowerCase()} ${name} ${asset} ${description} ${isLocked ? '[LOCKED]' : ''}`;
        if (group) text += ` ${group} [NFT]`;
        if (!cardData) return text;
        text += ` ${cardData.cardName} ${cardData.ownerName} ${cardData.description} ${cardData.tags}`;
        return text;
    };
    const aCheckIsAssetBalanceChanged = async(addrs, lastHeight) => {
        // issuanceでdescriptionを書き換えただけでもXMPが0減るdebitが発生するので捕捉可能
        const params = {
            start_block: lastHeight + 1,
            filters: [{ field: 'address', op: 'IN', value: addrs }],
        };
        const [debits, credits] = await Promise.all([
            Monaparty.aParty('get_debits', params),
            Monaparty.aParty('get_credits', params),
        ]);
        return Boolean(debits.length || credits.length);
    };
    const aGetBlockHeight = async() => {
        const urls = TagG.BLOCKBOOK_API_ENDPOINTS.map(blockbook => `${blockbook}api`);
        let status;
        for (const url of urls) {
            status = await Util.aGetJSON(url, false).catch(console.error);
            if (status) break;
        }
        return status.blockbook.bestHeight;
    };
    TagG.formatDispenserData = (dispenser) => {
        const assetInfo = TagG.asset2info[dispenser.asset];
        const cardData = TagG.asset2card[assetInfo.name] || null;
        const mainDescription = TagG.getMainDescription(assetInfo);
        const [unitBaseSat, unitAssetSat, escrowSat, remainingSat] = [dispenser.satoshirate, dispenser.give_quantity, dispenser.escrow_quantity, dispenser.give_remaining];
        const [unitBaseAmount, unitAssetAmount, escrowAmount, remainingAmount] = [unitBaseSat / TagG.SATOSHI_RATIO, TagG.qty2amount(unitAssetSat, assetInfo.isDivisible), TagG.qty2amount(escrowSat, assetInfo.isDivisible), TagG.qty2amount(remainingSat, assetInfo.isDivisible)];
        const [unitBaseAmountS, unitAssetAmountS, escrowAmountS, remainingAmountS] = [Util.commafy(unitBaseAmount), Util.commafy(unitAssetAmount), Util.commafy(escrowAmount), Util.commafy(remainingAmount)];
        return { // format: dispenserData
            assetInfo,
            txID: dispenser.tx_hash,
            txIndex: dispenser.tx_index,
            address: dispenser.source,
            asset: dispenser.asset,
            assetName: assetInfo.name,
            assetGroup: assetInfo.group,
            isDivisible: assetInfo.isDivisible,
            unitBaseSat, unitBaseAmount, unitBaseAmountS,
            unitAssetSat, unitAssetAmount, unitAssetAmountS,
            escrowSat, escrowAmount, escrowAmountS,
            remainingSat, remainingAmount, remainingAmountS,
            zaikoNum: Math.floor(dispenser.give_remaining / dispenser.give_quantity),
            imageURL: TagG.getAssetImageURL('auto', assetInfo.asset),
            largeImageURL: TagG.getAssetImageURL('L', assetInfo.asset),
            cardData,
            mainDescription,
        };
    };
    TagG.getMainDescription = (assetInfo) => {
        const cardData = TagG.asset2card[assetInfo.name] || null;
        let mainDescription = assetInfo.extendedInfo ? assetInfo.extendedInfo.description : assetInfo.description;
        if (cardData) mainDescription = cardData.description;
        if (TagG.asset2rakugakiTitle[assetInfo.asset]) mainDescription = `${TagG.jp ? 'らくがきNFT' : 'Rakugaki NFT'}「 ${TagG.asset2rakugakiTitle[assetInfo.asset]} 」`;
        return mainDescription;
    };
    const getAddrDataFormat = ({ index, address = null, addrObj = null, ecPair = null, addrType = ADDR_TYPE.P2PKH, path = null }) => {
        address = address || addrObj.address;
        const labelStorageKey = TagG.STORAGEKEY_LABEL_FORMAT.replace('{ADDR}', address);
        const styleStorageKey = TagG.STORAGEKEY_STYLE_FORMAT.replace('{ADDR}', address);
        const addrData = { // format: addrData
            index,
            address,
            ecPair,
            addrType,
            path,
            publicKey: addrObj ? addrObj.pubkey : null,
            script: addrObj ? addrObj.output : TagG.bitcoinjs.address.toOutputScript(address, TagG.NETWORK),
            baseSat: 0,
            baseUnconfSat: 0,
            builtinSat: 0,
            builtinUnconfSat: 0,
            builtinEscrSat: 0,
            assetTime: 0,
            assetBlock: 0,
            utxos: [],
            assets: [],
            label: Util.loadStorage(labelStorageKey) || null,
            labelStorageKey,
            styleStorageKey,
        };
        addrData.getBalanceSat = (asset) => getBalanceSat(addrData, asset);
        addrData.getBalanceAmount = (asset) => getBalanceAmount(addrData, asset);
        return addrData;
    };
    const formatUTXOs = (utxos) => {
        // 各種APIのUTXOを同じ形式にフォーマット
        // coinselectでそのまま使える形式
        return utxos.map(utxo => {
            const value = Number(utxo.value) || Number(utxo.satoshis) || Math.round(Number(utxo.amount) * TagG.SATOSHI_RATIO);
            const amount = value / TagG.SATOSHI_RATIO;
            return { // format: utxo
                txid: utxo.txid,
                vout: utxo.vout,
                value: value,
                amount: amount,
                confirmations: utxo.confirmations,
            };
        });
    };
    const aSetUTXOsByBlockbookAPI = async(addrsData) => {
        let maxIndex = 0;
        const promises = [];
        for (const addrData of addrsData) {
            const endpoints = TagG.BLOCKBOOK_API_ENDPOINTS.slice();
            if (addrData.index % 2) endpoints.reverse();
            const utxoURLs = endpoints.map(endpoint => `${endpoint}v2/utxo/${addrData.address}`);
            const aSet = async() => {
                let rawUTXOs;
                for (const url of utxoURLs) {
                    rawUTXOs = await Util.aGetJSON(url, false).catch(console.error);
                    if (rawUTXOs) break;
                }
                if (!rawUTXOs) throw new Error(`aSetUTXOsByBlockbookAPI failed: ${JSON.stringify(utxoURLs)}`);
                addrData.utxos = formatUTXOs(rawUTXOs);
                setBaseSatsFromUTXOs(addrData);
                if (addrData.utxos.length && addrData.index > maxIndex) maxIndex = addrData.index;
            };
            promises.push(aSet());
        }
        await Promise.all(promises);
        for (let i = maxIndex + 1; i < TagG.MAX_ADDRCOUNT; i++) {
            const addrData = addrsData[i];
            if (!addrData) break;
            const endpoints = TagG.BLOCKBOOK_API_ENDPOINTS.slice();
            if (addrData.index % 2) endpoints.reverse();
            const addrURLs = endpoints.map(endpoint => `${endpoint}v2/address/${addrData.address}?page=1&pageSize=1`);
            let addrInfo;
            for (const url of addrURLs) {
                addrInfo = await Util.aGetJSON(url, false).catch(console.error);
                if (addrInfo && ('totalReceived' in addrInfo)) break;
            }
            if (!(addrInfo && ('totalReceived' in addrInfo))) throw new Error(`aSetUTXOsByBlockbookAPI failed: ${JSON.stringify(addrURLs)}`);
            if (Number(addrInfo.totalReceived) > 0) maxIndex = i;
            else break;
        }
        return maxIndex;
    };
    const aSetUTXOsByInsightAPI = async(addrsData) => {
        let maxIndex = 0;
        const addrs = addrsData.map(data => data.address);
        const utxosURL = `${TagG.INSIGHT_API_ENDPOINT}addrs/${addrs.join(',')}/utxo`;
        const rawAllUTXOs = await Util.aGetJSON(utxosURL, false);
        for (const addrData of addrsData) {
            const rawUTXOs = rawAllUTXOs.filter(rawUTXO => (rawUTXO.address === addrData.address));
            addrData.utxos = formatUTXOs(rawUTXOs);
            setBaseSatsFromUTXOs(addrData);
            if (addrData.utxos.length && addrData.index > maxIndex) maxIndex = addrData.index;
        }
        for (let i = maxIndex + 1; i < TagG.MAX_ADDRCOUNT; i++) {
            const addrData = addrsData[i];
            if (!addrData) break;
            const totalReceivedURL = `${TagG.INSIGHT_API_ENDPOINT}addr/${addrData.address}/totalReceived`;
            const totalReceived = await Util.aGetNumber(totalReceivedURL, false);
            if (totalReceived > 0) maxIndex = i;
            else break;
        }
        return maxIndex;
    };
    const aSetUTXOsByMonapartyAPI = async(addrsData) => {
        let maxIndex = 0;
        const addrs = addrsData.map(data => data.address);
        const params = {
            'addresses': addrs,
            'with_uxtos': true, // なぜかutxosじゃなくてuxtos
            'with_last_txn_hashes': true, // ドキュメントではintだが実際はtrue/falseでしか処理されない
        };
        const chainAddrsInfo = await Monaparty.aBlock('get_chain_address_info', params);
        for (const { addr, last_txns: lastTXs, uxtos: utxos } of chainAddrsInfo) { // なぜかutxosじゃなくてuxtos
            const addrData = addrsData.find(data => (data.address === addr));
            addrData.utxos = formatUTXOs(utxos);
            setBaseSatsFromUTXOs(addrData);
            if (lastTXs.length && addrData.index > maxIndex) maxIndex = addrData.index;
        }
        return maxIndex;
    };
    const setBaseSatsFromUTXOs = (addrData) => {
        addrData.baseSat = addrData.utxos
            .filter(utxo => (utxo.confirmations >= 1))
            .reduce((sum, utxo) => (sum + utxo.value), 0);
        addrData.baseUnconfSat = addrData.utxos
            .filter(utxo => (utxo.confirmations < 1))
            .reduce((sum, utxo) => (sum + utxo.value), 0);
    };
    const getKeyDataForDerivePath = (parentHDKey, derivePath, addrType) => {
        const hdKey = parentHDKey.derive(derivePath);
        const ecPair = hdKey.privateKey ? TagG.bitcoinjs.ECPair.fromPrivateKey(hdKey.privateKey, { network: TagG.NETWORK }) : null;
        const addrObj = (addrType === ADDR_TYPE.P2WPKH)?
            TagG.bitcoinjs.payments.p2wpkh({ pubkey: hdKey.publicKey, network: TagG.NETWORK }):
            TagG.bitcoinjs.payments.p2pkh({ pubkey: hdKey.publicKey, network: TagG.NETWORK });
        return { ecPair, addrObj };
    };
    const getKeyDataForWIF = (wif, addrType) => {
        const ecPair = TagG.bitcoinjs.ECPair.fromWIF(wif, TagG.NETWORK);
        const addrObj = (addrType === ADDR_TYPE.P2WPKH)?
            TagG.bitcoinjs.payments.p2wpkh({ pubkey: ecPair.publicKey, network: TagG.NETWORK }):
            TagG.bitcoinjs.payments.p2pkh({ pubkey: ecPair.publicKey, network: TagG.NETWORK });
        return { ecPair, addrObj };
    };
    const getBalanceSat = (addrData, asset) => {
        if (asset === TagG.BASECHAIN_ASSET) return addrData.baseSat;
        if (asset === TagG.BUILTIN_ASSET) return addrData.builtinSat;
        const assetData = addrData.assets.find(_assetData => (_assetData.asset === asset || _assetData.name === asset));
        return assetData ? assetData.satoshi : 0;
    };
    const getBalanceAmount = (addrData, asset) => {
        const sat = addrData.getBalanceSat(asset);
        if (asset === TagG.BASECHAIN_ASSET || asset === TagG.BUILTIN_ASSET) return TagG.qty2amount(sat, true);
        const assetInfo = TagG.asset2info[asset];
        return assetInfo ? TagG.qty2amount(sat, assetInfo.isDivisible) : 0;
    };
}
{
    TagG.aLoadAssetsInfo = async(assets, useCache = false) => { // assetNameでもOK
        const filteredAssets = useCache ? assets.filter(asset => !TagG.asset2info[asset]) : assets;
        const assetsInfo = await aGetManyAssetsInfo(filteredAssets);
        TagG.addAssetsInfo(assetsInfo);
    };
    TagG.addAssetsInfo = (assetsInfo) => {
        for (const info of assetsInfo) {
            const name = info.asset_longname || info.asset;
            const shortenName = shortenAsset(name);
            const assetgroup = info.assetgroup || info.asset_group || null; // 表記ゆれ対策
            let parsedDesc = null;
            if (info.description.trim()[0] === '{') { // 配列や""で囲われた文字列もparseできてしまうので{}だけに限定
                try { parsedDesc = JSON.parse(info.description) } catch (err) {}
            }
            const appData = {};
            if (parsedDesc?.attr) {
                const attributes = [];
                for (const name in parsedDesc.attr) {
                    const value = parsedDesc.attr[name];
                    if (['string', 'number'].includes(typeof value)) attributes.push({ name, value })
                }
                if (attributes.length) appData.attributes = attributes;
            }
            if (assetgroup) {
                if (TagG.MONACHARAT_ASSETGROUPS?.includes(assetgroup)) appData.monacharat = { // Monacharat
                    url: `${TagG.MONACHARAT_SITEROOT}${info.asset}`,
                    image: `${TagG.MONACHARAT_IMGROOT}${name}.png`,
                    thumb: `${TagG.MONACHARAT_IMGROOT}${name}_160.png`,
                };
                if (TagG.RAKUGAKI_ASSETGROUPS?.includes(assetgroup)) appData.rakugakinft = { // RakugakiNFT
                    image: `${TagG.RAKUGAKI_IMGROOT}${name}`,
                };
                if (TagG.MONACUTE_ASSETGROUPS?.includes(assetgroup) && parsedDesc?.monacute) { // Monacute
                    const monacuteData = parsedDesc.monacute;
                    appData.monacute = {
                        id: monacuteData.id,
                        name: monacuteData.name,
                        dna: monacuteData.dna,
                        model: monacuteData.model,
                        url: TagG.MONACUTE_URLFORMAT.replace('{ID}', monacuteData.id),
                        image: TagG.MONACUTE_IMGFORMAT.replace('{DNA}', monacuteData.dna),
                    };
                }
            }
            Object.assign(info, { // format: assetInfo
                name,
                shortenName,
                assetgroup,
                group: assetgroup,
                easyLabel: assetgroup ? `${assetgroup} (${shortenName})` : shortenName,
                longLabel: assetgroup ? `${name} (${assetgroup})` : name,
                issuedSat: info.supply,
                issuedAmount: TagG.qty2amount(info.supply, info.divisible),
                isDivisible: info.divisible,
                isReassignable: info.reassignable,
                isListed: info.listed,
                isLocked: info.locked,
                parsedDesc,
                explorerURL: TagG.ASSET_PARTYEXPURL_FORMAT.replace('{ASSET}', info.asset),
                extendedInfo: TagG.asset2extendedInfo[info.asset] || null,
                appData,
            });
            TagG.asset2info[info.asset] = TagG.asset2info[info.name] = info;
        }
    };
    const aGetManyAssetsInfo = async(assets) => {
        const ONCE = 250;
        const promises = [];
        for (let i = 0; i < Math.ceil(assets.length / ONCE); i++) {
            const slicedAssets = assets.slice(ONCE * i, ONCE * (i + 1));
            promises.push(Monaparty.aBlock('get_assets_info', { assetsList: slicedAssets }));
        }
        const assetsInfoArray = await Promise.all(promises);
        const assetsInfo = assetsInfoArray.reduce((joined, nextList) => joined.concat(nextList), []);
        return assetsInfo;
    };
    const shortenAsset = (assetName) => (assetName[0] === 'A' ? `${assetName.slice(0, 5)}...${assetName.slice(-4)}` : assetName);
}
{
    const IMG_URL_FORMAT_ORIGINAL = 'https://mcspare.nachatdayo.com/image_server/img/{CID}';
    const IMG_URL_FORMAT_SIZED = 'https://monacard-img.komikikaku.com/{CID}l';
    const IMG_URL_FORMAT_SMALL = 'https://monacard-img.komikikaku.com/{CID}t';
    TagG.cardLoadedMap = {};
    TagG.aLoadCardsData = async(assetNames, useCache = false) => {
        if (!TagG.CARDSERVER_API) { console.log('aLoadCardsData skip'); return }
        const URL = `${TagG.CARDSERVER_API}card_detail_post`;
        const filteredAssetNames = useCache ? assetNames.filter(name => !TagG.cardLoadedMap[name]) : assetNames;
        const fetchParams = {
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/json' },
            body: `assets=${encodeURIComponent(filteredAssetNames.join(','))}`,
        };
        let { details } = await fetch(URL, fetchParams).then(res => res.json());
        TagG.addCardsData(details || []);
        for (const name of filteredAssetNames) TagG.cardLoadedMap[name] = true; // 冗長だけど両方必要 *
    };
    TagG.aPostCardImage = async(imgBlob) => { // Monacard 2.0
        if (!TagG.CARDSERVER_API) throw 'Monacard is not available for this coin';
        console.log('post cardImage');
        const url = `${TagG.CARDSERVER_API}upload_image`;
        const formData = new FormData();
        formData.append('image', imgBlob);
        const response = await fetch(url, { method: 'POST', body: formData });
        const resultText = await response.text();
        if (!response.ok) throw new Error('cardImage upload failed.\n\n', resultText);
        const result = JSON.parse(resultText);
        if (result.success) console.log('cardImage successfully uploaded');
        else throw new Error('cardImage upload failed.\n\n', resultText)
        return result.success.cid;
    };
    TagG.addCardsData = (cardDetails) => {
        for (const detail of cardDetails) {
            TagG.asset2card[detail.asset] = TagG.asset2card[detail.asset_common_name] = { // format: cardData
                asset: detail.asset,
                cardName: unescapeHTML(detail.card_name),
                ownerName: unescapeHTML(detail.owner_name),
                ownerSN: detail.tw_name,
                siteURL: TagG.CARD_DETAILURL_FORMAT.replace('{ASSETNAME}', detail.asset_common_name),
                imgurURL: IMG_URL_FORMAT_ORIGINAL.replace('{CID}', detail.cid), // Thanks to Imgur
                imgurSizedURL: IMG_URL_FORMAT_SIZED.replace('{CID}', detail.cid),
                imgurSmallURL: IMG_URL_FORMAT_SMALL.replace('{CID}', detail.cid),
                description: unescapeHTML(detail.add_description),
                version: detail.ver,
                tags: unescapeHTML(detail.tag),
            };
            TagG.cardLoadedMap[detail.asset_common_name] = true; // 冗長だけど両方必要 *
        }
    };
    const unescapeHTML = (str) => {
        return str.replace(/&(amp|quot|#039|lt|gt);/g, match => {
            return { '&amp;': '&', '&quot;': '"', '&#039;': "'", '&lt;': '<', '&gt;': '>' }[match];
        });
    };
}
TagG.aLoadAssets = async(assets, useInfoCache = false, useCardCache = false) => { // longnameでもOK
    assets = Util.uniq(assets);
    await TagG.aLoadAssetsInfo(assets, useInfoCache);
    const assetNames = assets.map(asset => (TagG.asset2info[asset] || {}).name).filter(val => val);
    await TagG.aLoadCardsData(assetNames, useCardCache).catch(console.error);
};
TagG.aLoadTreasures = async() => {
    const nowTime = new Date().getTime();
    if (TagG.tokenVaultCacheID < nowTime - 1000 * 60 * 30) TagG.tokenVaultCacheID = nowTime;
    const { treasures } = await Util.aGetJSON(`${TagG.TOKENVAULT_ROOT}public/tokenvault.json?${TagG.tokenVaultCacheID}`, false);
    const asset2treasures = {};
    for (const { asset, assetNm, isDiv, qty, memo, hasTmb } of treasures) {
        if (!asset2treasures[asset]) asset2treasures[asset] = [];
        const tmbImage = hasTmb? `${TagG.TOKENVAULT_ROOT}thumbnails/${asset}_${qty}` : `${TagG.RELATIVE_URL_BASE}img/monapalette_notmb.png`;
        asset2treasures[asset].push({ // format: treasureData
            asset,
            assetName: assetNm,
            isDivisible: isDiv,
            qtyN: window.BigInt ? window.BigInt(qty) : Number(qty),
            qtyS: qty,
            amountS: TagG.qtyS2amountS(qty, isDiv),
            memo,
            hasTmb,
            tmbImage,
        });
    }
    TagG.asset2treasures = asset2treasures;
};
TagG.aLoadRakugakiTitles = async() => {
    if (!TagG.TOKENVAULT_ROOT) return;
    const { asset2title } = await Util.aGetJSON(`${TagG.TOKENVAULT_ROOT}public/rakugaki.json?${TagG.getCacheQuery(30)}`, false);
    TagG.asset2rakugakiTitle = asset2title;
};
TagG.aLoadAnimeList = async() => {
    if (!TagG.ANIMELIST_JSON) return;
    const { animeAssets } = await Util.aGetJSON(`${TagG.ANIMELIST_JSON}?${TagG.getCacheQuery(15)}`);
    TagG.asset2isAnime = {};
    for (const asset of animeAssets) TagG.asset2isAnime[asset] = true;
};
TagG.aLoadCachedAssetData = async() => {
    const promises = [];
    // アセット情報は上書きされるので完全更新の頻度が高いやつを後ろに
    if (TagG.DBOARD_CACHEJSON) promises.push(Util.aGetJSON(`${TagG.DBOARD_CACHEJSON}?${TagG.getCacheQuery(10)}`).catch(() => ({})));
    if (TagG.DEXASSETS_CACHEJSON) promises.push(Util.aGetJSON(`${TagG.DEXASSETS_CACHEJSON}?${TagG.getCacheQuery(20)}`).catch(() => ({})));
    if (TagG.DISPENSERS_CACHEJSON) promises.push(Util.aGetJSON(`${TagG.DISPENSERS_CACHEJSON}?${TagG.getCacheQuery(2)}`).catch(() => ({})));
    const cacheJsons = await Promise.all(promises);
    for (const { assetsInfo, monacards, noCards } of cacheJsons) {
        if (!assetsInfo) continue;
        TagG.addAssetsInfo(assetsInfo.filter(info => !TagG.asset2info[info.asset]));
        TagG.addCardsData(monacards.filter(card => !TagG.asset2card[card.asset_common_name]));
        for (const name of noCards) TagG.cardLoadedMap[name] = true;
    }
};
TagG.aGetDispensers = async() => {
    let dispensers;
    try {
        dispensers = (await Util.aGetJSON(`${TagG.DISPENSERS_CACHEJSON}?${TagG.getCacheQuery(2)}`)).dispensers;
    }
    catch (e) {
        const getDispensersParams = {
            'limit': 1000,
            'order_by': 'tx_index',
            'order_dir': 'DESC',
            // get_dispensersはstatusパラメータが効かないのでフィルタでstatus指定
            'filters': [{ field: 'status', op: '==', value: DISPENSER_STATUS.OPEN }],
        };
        dispensers = await Monaparty.aParty('get_dispensers', getDispensersParams);
    }
    const assets = dispensers.map(dispenser => dispenser.asset);
    await TagG.aLoadAssets(assets, true, true);
    dispensers = dispensers.map(TagG.formatDispenserData);
    return dispensers;
};
TagG.aCheckMentions = async() => {
    if (!TagG.DBOARD_CACHEJSON) return;
    const DMESSAGE_PREFIX = '@b.m ';
    const ADDR_REG = new RegExp(`@${TagG.ADDR_PATTERN}`, 'g');
    const HOLDERS_REG = /@holders\([A-Z0-9]{4,}(\.[a-zA-Z0-9.\-_@!]+)?\)/g;
    const ASSET_REG = /@[A-Z0-9]{4,}(\.[a-zA-Z0-9.\-_@!]+)?/g;
    let mentionCount = 0;
    const { messages } = await Util.aGetJSON(`${TagG.DBOARD_CACHEJSON}?${TagG.getCacheQuery(10)}`);
    const isDeletedIndexAddr = {};
    const index2address = {};
    for (const rawMessage of messages) index2address[rawMessage.tx_index] = rawMessage.source;
    for (const rawMessage of messages) {
        const index = rawMessage.tx_index;
        if (index <= TagG.dboardReadIndex) continue;
        try {
            const address = rawMessage.source;
            const rawText = rawMessage.text.slice(DMESSAGE_PREFIX.length);
            const parsedText = JSON.parse(rawText);
            if (isDeletedIndexAddr[`${index}.${address}`]) continue;
            let text = null;
            if (typeof parsedText === 'string') text = parsedText;
            else if (typeof parsedText.d === 'number') isDeletedIndexAddr[`${parsedText.d}.${address}`] = true;
            else if (typeof parsedText.m === 'string') text = parsedText.m;
            if (typeof parsedText.r === 'number') {
                if (TagG.addr2isMine[index2address[parsedText.r]]) { mentionCount++; continue }
            }
            if (!text || !text.includes('@')) continue;
            let matches;
            matches = text.match(ADDR_REG);
            if (matches && matches.map(txt => txt.slice(1)).some(addr => TagG.addr2isMine[addr])) { mentionCount++; continue }
            matches = text.match(HOLDERS_REG);
            if (matches && matches.map(txt => txt.split(/[()]/)[1]).some(asset => TagG.asset2isInMyWallet[asset])) { mentionCount++; continue }
            matches = text.match(ASSET_REG);
            if (matches && matches.map(txt => txt.slice(1)).some(asset => TagG.asset2isMyOwn[asset])) { mentionCount++; continue }
        }
        catch (error) {}
    }
    TagG.bodyMenu.setNotifications({ 'D-Board': mentionCount });
};
TagG.aGetTreasureURL = async(asset, qtyS, addrData = null) => {
    if (qtyS === '0') return `${TagG.TOKENVAULT_ROOT}treasures/${asset}_${qtyS}`;
    if (addrData) {
        const assetData = addrData.assets.find(_asset => (_asset.asset === asset));
        if (!assetData) throw new Error(`aGetTreasureURL failed: ${JSON.stringify({ asset, qtyS })}`);
        const params = { asset, quantity: qtyS, isOwner: assetData.isOwn };
        const data = await TagG.aSignedJsonRPCRequest(TagG.TOKENVAULT_API, 'getTreasure', params, addrData);
        return data.treasureDownloadURL;
    }
};
TagG.aOpenTreasure = async(asset, qtyS, addrData = null) => {
    const treasureDownloadURL = await TagG.aGetTreasureURL(asset, qtyS, addrData);
    const url = `${TagG.RELATIVE_URL_BASE}${TagG.TOKENVAULT_VIEWER}?${TagG.QUERYKEY_TREASUREURL}=${encodeURIComponent(treasureDownloadURL)}`;
    const result = window.open(url, '_blank'); // noreferrerを指定するとresultがundefinedになってしまうのでビューア側で window.opener = null とすることで対応
    if (!result) window.location.href = url;
};
{
    const IPFS_REGEXP = /^ipfs:\/\/([1-9A-HJ-NP-Za-km-z]+)$/;
    const SECUREURL_REGEXP = /^https?:\/\/([\w-]+\.)+[\w-]+(\/[\w-.?%&=]*)?$/;
    const asset2skip = {};
    TagG.aLoadAssetsExtendedInfo = async() => {
        const promises = [];
        for (const asset in TagG.asset2info) {
            if (asset2skip[asset]) continue;
            const assetInfo = TagG.asset2info[asset];
            if (asset !== assetInfo.asset) { asset2skip[asset] = true; continue } // assetがlongnameの場合
            const ipfsMatch = assetInfo.description.match(IPFS_REGEXP);
            if (!ipfsMatch) { asset2skip[asset] = true; continue }
            const jsonHash = ipfsMatch[1];
            const existingExtInfo = TagG.asset2extendedInfo[asset];
            if (existingExtInfo && existingExtInfo.hash === jsonHash) continue;
            promises.push(aLoadExtendedInfo(assetInfo, jsonHash))
        }
        await Promise.all(promises);
        const isUpdated = Boolean(promises.length);
        return isUpdated;
    };
    const aLoadExtendedInfo = async(assetInfo, jsonHash) => {
        const TIMEOUT_MS = 3000;
        try {
            const jsonURL = TagG.IPFSURL_FORMAT.replace('{HASH}', jsonHash);
            const extInfo = { hash: jsonHash };
            const extJson = await Util.aTimeout(TIMEOUT_MS, Util.aGetJSON(jsonURL, true));
            if (![assetInfo.asset, assetInfo.name].includes(extJson.asset)) return;
            for (const key of ['description', 'image', 'website', 'pgpsig']) {
                if ((key in extJson) && (typeof extJson[key] === 'string')) extInfo[key] = extJson[key];
            }
            if (extInfo.image) {
                const ipfsMatch = extInfo.image.match(IPFS_REGEXP);
                if (ipfsMatch) extInfo.imageURL = TagG.IPFSURL_FORMAT.replace('{HASH}', ipfsMatch[1]);
            }
            if (extInfo.website) extInfo.isWebsiteSecure = Boolean(extInfo.website.match(SECUREURL_REGEXP));
            TagG.asset2extendedInfo[assetInfo.asset] = extInfo;
            assetInfo.extendedInfo = extInfo;
        }
        catch (error) {
            console.warn(error);
            asset2skip[assetInfo.asset] = true;
        }
    };
}
{
    TagG.aGenerateStrongMnemonic = async(bits) => {
        const entHex = TagG.randomBytes(bits / 8).toString('hex');
        const moreEntHex = await aGetMoreEntropyHex(bits);
        const drbg = new TagG.HmacDRBG({
            hash: TagG.hash.sha256,
            entropy: entHex,
            nonce: moreEntHex,
            minEntropy: bits,
        });
        const strongEntHex = drbg.generate(bits / 8, 'hex');
        const strongMnemonic = TagG.bip39.entropyToMnemonic(strongEntHex);
        return strongMnemonic;
    };
    const aGetMoreEntropyHex = (minBits) => {
        return new Promise((resolve) => {
            const moreEntropyGenerator = new TagG.MoreEntropy.Generator();
            moreEntropyGenerator.generate(minBits, (entropyInts) => {
                const moreEntropyHex = Buffer.from(entropyInts).toString('hex');
                resolve(moreEntropyHex);
            });
        });
    };
}
TagG.getSizedImgurImageURL = (baseURL, size) => { // sizes ref: https://api.imgur.com/models/image
    if (baseURL.indexOf('i.imgur.com') === -1) return baseURL;
    if (baseURL.match(/\/\d+\./)) return baseURL; // ファイル名が数字のときは別サイズがない
    return baseURL.
    replace('.png', size + '.png').
    replace('.jpg', size + '.jpg').
    replace('.gif', size + '.gif');
};
{
    TagG.aMonapartyTXCreate = async(createCommand, createParams, txData) => {
        const { tx_hex, btc_in, btc_change, btc_fee } = await Monaparty.aParty(createCommand, createParams);
        inspectTXHex(tx_hex, btc_change, createCommand, createParams);
        // format: txData
        txData.txHex = tx_hex;
        txData.inputsSat = btc_in;
        txData.oturiSat = btc_change;
        txData.feeSat = btc_fee;
        txData.dustSat = btc_in - btc_change - btc_fee;
        txData.feeAmount = txData.feeSat / TagG.SATOSHI_RATIO;
        txData.dustAmount = txData.dustSat / TagG.SATOSHI_RATIO;
        txData.feedustAmount = txData.feeAmount + txData.dustAmount;
    };
    const inspectTXHex = (tx_hex, btc_change, createCommand, createParams) => {
        const { source, custom_inputs, dividend_asset } = createParams;
        const txObj = TagG.bitcoinjs.Transaction.fromHex(tx_hex);
        if (custom_inputs) {
            for (const input of txObj.ins) {
                const txID = TagG.reverseBuffer(input.hash).toString('hex');
                if (!custom_inputs.some(cinput => cinput.txid === txID)) throw new Error(TagG.jp ? 'トランザクションの内容が想定と異なる可能性があります' : 'tx may be different from what is expected');
            }
        }
        let change = 0, outgoing = 0;
        for (const output of txObj.outs) {
            try {
                const outputAddr = TagG.bitcoinjs.address.fromOutputScript(output.script, TagG.NETWORK);
                if (outputAddr === source) change += output.value;
                else outgoing += output.value;
            }
            catch (error) {
                outgoing += output.value;
            }
        }
        if (change !== btc_change) throw new Error(TagG.jp ? 'トランザクションの内容が想定と異なる可能性があります' : 'tx may be different from what is expected');
        let isBaseTX = (createCommand === 'create_dividend' && dividend_asset === TagG.BASECHAIN_ASSET);
        if (!isBaseTX && outgoing > TagG.FEESAT_PERBYTE * 1e6) throw new Error(TagG.jp ? 'トランザクションの内容が想定と異なる可能性があります' : 'tx may be different from what is expected');
        console.log('inspectTXHex OK');
    };
}
TagG.aBroadcast = async(signedTXHex) => {
    // return 'XXXXdummytxidXXXX'; // DEBUG: ローカルの挙動だけテストするためにブロードキャストをスキップ
    TagG.aUpdateAddrsDataWithUI.lastTime = 0;
    return await Monaparty.aBlock('broadcast_tx', { 'signed_tx_hex': signedTXHex });
};
TagG.aSignInOut = async(inputs, outputs, addrData) => {
    const { script } = addrData;
    const txBuilder = new TagG.bitcoinjs.TransactionBuilder(TagG.NETWORK);
    for (const input of inputs) txBuilder.addInput(input.txid, input.vout, null, script);
    for (const output of outputs) txBuilder.addOutput(output.address, output.value);
    const txObj = txBuilder.buildIncomplete();
    for (let i = 0; i < txObj.ins.length; i++) txObj.ins[i].script = script; // 上でもscript設定してるので冗長に見えるけど必要あり
    const unsignedTXHex = txObj.toHex();
    const signedTXHex = await TagG.aSignTXHex(unsignedTXHex, addrData, inputs.map(input => input.value));
    return signedTXHex;
};
TagG.aBroadcastInOut = async(inputs, outputs, addrData) => {
    const signedTXHex = await TagG.aSignInOut(inputs, outputs, addrData);
    const txID = await TagG.aBroadcast(signedTXHex);
    return txID;
};
{
    TagG.aSignTXHex = async(unsignedTXHex, addrData, inputValues = null) => {
        const txObj = TagG.bitcoinjs.Transaction.fromHex(unsignedTXHex);
        let signedTXHex;
        if (TagG.isReadOnlyMode) await TagG.aUpgradeFromReadOnlyMode();
        if (addrData.ecPair) signedTXHex = await aSignTXHexWithECPair(txObj, addrData, inputValues);
        else if (TagG.IS_MPURSEEX) signedTXHex = await window.mpurse.signRawTransaction(unsignedTXHex);
        else if (TagG.isTrezor) signedTXHex = await aSignTXHexWithTrezor(txObj, addrData, inputValues);
        const signedTXObj = TagG.bitcoinjs.Transaction.fromHex(signedTXHex);
        checkTxManipulation(txObj, signedTXObj);
        return signedTXHex;
    };
    const aSignTXHexWithECPair = async(txObj, addrData, inputValues = null) => {
        const { ecPair, addrType } = addrData;
        const txBuilder = getTxBuilder(txObj);
        console.log({ ins: txObj.ins, outs: txObj.outs });
        if (addrType === ADDR_TYPE.P2WPKH) {
            let i = 0;
            for (const txIn of txObj.ins) {
                if (inputValues) txBuilder.sign(i, ecPair, null, null, inputValues[i]);
                else txBuilder.sign(i, ecPair, null, null, await aGetInputValue(txIn));
                i++;
            }
        }
        else txObj.ins.forEach((txIn, i) => txBuilder.sign(i, ecPair));
        return txBuilder.build().toHex();
    };
    const aSignTXHexWithTrezor = async(txObj, addrData, inputValues = null) => {
        const addressPathArray = parsePath(addrData.path);
        const inputs = [], outputs = [];
        let i = 0;
        for (const txIn of txObj.ins) {
            let scriptType = 'SPENDADDRESS';
            if (addrData.addrType === ADDR_TYPE.P2WPKH) scriptType = 'SPENDWITNESS';
            const value = inputValues ? inputValues[i] : await aGetInputValue(txIn);
            inputs.push({
                address_n: addressPathArray,
                prev_index: txIn.index,
                prev_hash: TagG.reverseBuffer(txIn.hash).toString('hex'),
                amount: `${value}`,
                script_type: scriptType,
            });
            i++;
        }
        for (const txOut of txObj.outs) {
            try {
                const toAddr = TagG.bitcoinjs.address.fromOutputScript(txOut.script, TagG.NETWORK);
                const isOturi = (addrData.address === toAddr);
                const output = { script_type: 'PAYTOADDRESS', amount: `${txOut.value}` };
                if (isOturi && addrData.addrType === ADDR_TYPE.P2WPKH) output.script_type = 'PAYTOWITNESS';
                if (isOturi) output.address_n = addressPathArray;
                else output.address = toAddr;
                outputs.push(output);
            }
            catch (e) {
                const asm = TagG.bitcoinjs.script.toASM(txOut.script);
                if (asm.indexOf('OP_RETURN ') !== 0) throw TagG.jp?
                'このトランザクションはTrezorで署名ができません。Trezorではデータ量の多いMonapartyトランザクション(Pay to Multisigという古い形式のoutputを含むトランザクション)への署名はサポートしていないので、データ量を減らすかTrezor以外のアドレスを使ってください。もし署名する方法をご存知でしたら情報をお寄せください':
                'Trezor does not support signing of data-heavy Monaparty tx (tx containing old-style output called Pay to Multisig), so you should either reduce the amount of data or use a non-Trezor address.';
                const data = asm.slice(10);
                outputs.push({ script_type: 'PAYTOOPRETURN', op_return_data: data, amount: `${txOut.value}` });
            }
        }
        const trezorResult = await TrezorConnect.signTransaction({ inputs, outputs, coin: TagG.TREZOR_COIN });
        if (!trezorResult.success) throw new Error(TagG.jp ? `エラーが発生しました: ${trezorResult.payload.error}` : `Failed. hint: ${trezorResult.payload.error}`);
        return trezorResult.payload.serializedTx;
    };
    const aGetInputValue = async(txIn) => {
        const utxoTxID = TagG.reverseBuffer(txIn.hash).toString('hex');
        const utxoHex = await Monaparty.aParty('getrawtransaction', { 'tx_hash': utxoTxID });
        const utxoTxObj = TagG.bitcoinjs.Transaction.fromHex(utxoHex);
        const utxoValue = utxoTxObj.outs[txIn.index].value;
        return utxoValue;
    };
    const parsePath = (path) => {
        const pathArray = [];
        const split = path.split('/');
        split.shift();
        for (const str of split) {
            const [numStr, emptyStr] = str.split("'");
            let num = Number(numStr);
            if (emptyStr === '') num = ((num | 0x80000000) >>> 0);
            pathArray.push(num);
        }
        return pathArray;
    };
    const getTxBuilder = (txObj) => {
        const txBuilder = new TagG.bitcoinjs.TransactionBuilder(TagG.NETWORK);
        txBuilder.setVersion(txObj.version);
        txBuilder.setLockTime(txObj.locktime);
        for (const txOut of txObj.outs) txBuilder.addOutput(txOut.script, txOut.value);
        for (const txIn of txObj.ins) txBuilder.addInput(txIn.hash, txIn.index, txIn.sequence, txIn.script);
        return txBuilder;
    };
    const checkTxManipulation = (beforeTxObj, afterTxObj) => {
        const errMessage = TagG.jp ? '署名時にトランザクションが改ざんされた可能性があります' : 'The tx may have been tampered with during signing.';
        for (let i = 0; i < afterTxObj.ins.length; i++) {
            if (afterTxObj.ins[i].hash.toString('hex') !== beforeTxObj.ins[i].hash.toString('hex')) throw new Error(errMessage);
            if (afterTxObj.ins[i].index !== beforeTxObj.ins[i].index) throw new Error(errMessage);
        }
        for (let i = 0; i < afterTxObj.outs.length; i++) {
            if (afterTxObj.outs[i].script.toString('hex') !== beforeTxObj.outs[i].script.toString('hex')) throw new Error(errMessage);
            if (afterTxObj.outs[i].value !== beforeTxObj.outs[i].value) throw new Error(errMessage);
        }
    };
}
{
    TagG.aSignMessage = async(message, addrData) => {
        let signature;
        if (TagG.isReadOnlyMode) await TagG.aUpgradeFromReadOnlyMode();
        if (addrData.ecPair) signature = await aSignMessageWithECPair(message, addrData);
        else if (TagG.IS_MPURSEEX) signature = await window.mpurse.signMessage(message);
        else if (TagG.isTrezor) signature = await aSignMessageWithTrezor(message, addrData);
        const isValid = TagG.bitcoinjsMessage.verify(message, addrData.address, signature, TagG.NETWORK.messagePrefix);
        if (!isValid) throw new Error(TagG.jp ? '署名の検証に失敗しました' : 'Signature verification failed.');
        return signature;
    };
    const aSignMessageWithECPair = async(message, addrData) => {
        const { ecPair, addrType } = addrData;
        const sign = (addrType === ADDR_TYPE.P2WPKH)?
              TagG.bitcoinjsMessage.sign(message, ecPair.privateKey, ecPair.compressed, TagG.NETWORK.messagePrefix, { extraEntropy: TagG.randomBytes(32), segwitType: 'p2wpkh' }):
              TagG.bitcoinjsMessage.sign(message, ecPair.privateKey, ecPair.compressed, TagG.NETWORK.messagePrefix, { extraEntropy: TagG.randomBytes(32) });
        return sign.toString('base64');
    };
    const aSignMessageWithTrezor = async(message, addrData) => {
        const trezorResult = await TrezorConnect.signMessage({ path: addrData.path, message: message, coin: TagG.TREZOR_COIN });
        if (!trezorResult.success) throw new Error(TagG.jp ? `エラーが発生しました: ${trezorResult.payload.error}` : `Failed. hint: ${trezorResult.payload.error}`);
        return trezorResult.payload.signature;
    };
}
TagG.getAssetImageURL = (size, asset) => { // size = 'L'/'M'/'S'/'auto', assetはnameでも可
    const assetInfo = TagG.asset2info[asset];
    if (!assetInfo) return null;
    if (assetInfo.appData.monacharat) {
        switch (size) {
            case 'L': case 'M': case 'auto': return assetInfo.appData.monacharat.image;
            case 'S': return assetInfo.appData.thumb;
        }
    }
    if (assetInfo.appData.rakugakinft) return assetInfo.appData.rakugakinft.image;
    const cardData = TagG.asset2card[assetInfo.name];
    if (cardData) {
        switch (size) {
            case 'L': return cardData.imgurURL;
            case 'M': return cardData.imgurSizedURL;
            case 'S': return cardData.imgurSmallURL;
            case 'auto':
                if (TagG.asset2isAnime[cardData.asset]) return cardData.imgurURL;
                else return cardData.imgurSizedURL;
        }
    }
    return null;
};
TagG.getRakugakiCacheID = (asset, isAnonymous = false) => {
    // anonymousのときは非anonymousのキャッシュが使われるとエラーになるので分ける
    return (TagG.rakugakiAsset2cacheID[asset] || TAGG_TIME) + (isAnonymous ? '_' : '');
};
TagG.updatefirstAddrLabel = () => {
    const newLabel = TagG.addrsData[0].label || TagG.addrsData[0].address.slice(-4);
    if (newLabel === TagG.firstAddrLabel) return;
    TagG.firstAddrLabel = newLabel;
    TagG.body.update(); // body-bottomfloat を更新
    document.title = `${TagG.firstAddrLabel} - ${TagG.originalDocumentTitle}`;
}
TagG.validateAddress = (address) => {
    try {
        TagG.bitcoinjs.address.toOutputScript(address, TagG.NETWORK);
        return true;
    } catch (error) {
        console.warn(error);
        return false;
    }
};
TagG.validateWIF = (wif) => {
    try {
        TagG.bitcoinjs.ECPair.fromWIF(wif, TagG.NETWORK);
        return true;
    } catch (error) {
        console.warn(error);
        return false;
    }
};
TagG.getDefaultAddrLabel = (index = 0) => TagG.IS_MPURSEEX ? 'Mpurse Address' : `Address${Util.zeroPadding(index + 1, 2)}`;
TagG.addrType2purpose = (addrType) => {
    if (addrType === ADDR_TYPE.P2WPKH) return 84;
    return 44;
};
TagG.addrType2defaultAccountPath = (addrType, account = 0) => `m/${TagG.addrType2purpose(addrType)}'/${TagG.NETWORK.versions.bip44}'/${account}'`;
TagG.reverseBuffer = (buffer) => {
    const reversedBuffer = Buffer.allocUnsafe(buffer.length);
    const maxIndex = buffer.length - 1;
    for (let i = 0; i <= maxIndex; i++) reversedBuffer[i] = buffer[maxIndex - i];
    return reversedBuffer;
};
TagG.qtyS2amountS = (qtyS, isDivisible) => {
    if (!isDivisible) return qtyS;
    qtyS = '0'.repeat(TagG.SATOSHI_DECIMAL) + qtyS;
    let [intPart, fracPart] = [qtyS.slice(0, qtyS.length - TagG.SATOSHI_DECIMAL), qtyS.slice(-TagG.SATOSHI_DECIMAL)];
    intPart = intPart.replace(/^0+/, '') || '0';
    fracPart = fracPart.replace(/0+$/, '');
    if (fracPart) return `${intPart}.${fracPart}`;
    else return intPart;
};
TagG.qty2amount = (qty, isDivisible) => (isDivisible ? qty / TagG.SATOSHI_RATIO : qty);
TagG.amount2qty = (amount, isDivisible) => Math.round(isDivisible ? amount * TagG.SATOSHI_RATIO : amount);
TagG.getCacheQuery = (minutes) => Math.floor(new Date().getTime() / (60e3 * minutes));

{
    if (!TagG.IS_PREVIEW) route.base('/');
    if (TagG.IS_MPURSEEX && window.mpurse) {
        window.mpurse.updateEmitter.removeAllListeners().on('addressChanged', () => location.reload());
    }
    TrezorConnect.manifest({ email: 'komikikaku+trezor@gmail.com', appUrl: 'https://monapalette.komikikaku.com/' });
    window.myswal = async(...args) => { // オリジナルパラメータ: isCutin
        if (!myswal.paramsStack) myswal.paramsStack = [];
        let params;
        if (args.length === 1) {
            if (typeof args[0] === 'string') params = { text: args[0], className: 'swalcustom-singlebutton' };
            else {
                params = args[0];
                if (!('className' in params)) {
                    if ('content' in params) params.className = 'swalcustom-content';
                    else if ('icon' in params) params.className = 'swalcustom-icon';
                    else if ('buttons' in params && params.buttons.length === 2) params.className = 'swalcustom-okorcancel';
                    else if (!('buttons' in params)) params.className = 'swalcustom-singlebutton';
                    else if (!('title' in params) && ('buttons' in params) && Object.keys(params.buttons).length === 0) params.className = 'swalcustom-justtext';
                }
            }
        }
        else if (args.length === 2) params = { title: args[0], text: args[1], className: 'swalcustom-singlebutton' };
        else if (args.length === 3) params = { title: args[0], text: args[1], icon: args[2], className: 'swalcustom-icon' };
        else params = args; // このときだけarray
        myswal.paramsStack.push(params);
        let swalReturn;
        if (Array.isArray(params)) swalReturn = await swal(...params);
        else swalReturn = await swal(params);
        // swalの仕様によりswal中にswalされた場合はpromiseがpendingのままになるので、その場合はこの先は実行されない
        myswal.paramsStack.pop();
        const lastParams = myswal.paramsStack.pop();
        if (params.isCutin && lastParams) {
            if (Array.isArray(lastParams)) myswal(...lastParams); // awaitしない
            else myswal(lastParams);
        }
        else myswal.paramsStack = [];
        return swalReturn;
    };
    if (TagG.COIN === COINS.MONA) {
        TagG.asset2card['MONANA'] = {
            cardName: 'MONANA',
            ownerName: 'unknown',
            ownerSN: '',
            siteURL: TagG.CARD_DETAILURL_FORMAT.replace('{ASSETNAME}', 'MONANA'),
            imgurURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA.png`,
            imgurSizedURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA_preview.png`,
            imgurSmallURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA_small.png`,
            description: 'モナカードではありませんが、DEXでよく利用されるトークンなのでモナパレット専用の画像を表示しています',
            version: '1',
            tags: '',
        };
    }
    if (TagG.COIN === COINS.MONA_TESTNET) { // testnetにMonacardがないので表示確認用 // トークンはどれでもいい
        TagG.asset2card['SEGWITOKEN'] = {
            cardName: 'SEGWITOKEN',
            ownerName: 'monapalette',
            ownerSN: '',
            siteURL: TagG.CARD_DETAILURL_FORMAT.replace('{ASSETNAME}', 'SEGWITOKEN'),
            imgurURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA.png`,
            imgurSizedURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA_preview.png`,
            imgurSmallURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA_small.png`,
            description: 'testnetにMonacardがないので表示確認用',
            version: '1',
            tags: '',
        };
    }
    const storedLang = Util.loadStorage(TagG.STORAGEKEY_LANG);
    if (storedLang) TagG.jp = (storedLang === 'JA');
    else TagG.jp = (['ja', 'ja-JP'].includes(navigator.language));
    const confNum = Util.loadStorage(TagG.STORAGEKEY_CONFNUM);
    if (Number.isInteger(confNum)) TagG.confNum = confNum;
    TagG.addrbook = Util.loadStorage(TagG.STORAGEKEY_ADDRBOOK) || [];
    TagG.aLoadTreasures().catch(console.warn);
    TagG.aLoadRakugakiTitles().catch(console.warn);
    TagG.aLoadAnimeList().catch(console.warn);
    TagG.aLoadCachedAssetData().catch(console.warn);
    window.addEventListener('message', (event) => {
        for (const listener of TagG.windowMessageListeners) listener(event);
    });
    window.addEventListener('load', () => {
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.register(`${TagG.RELATIVE_URL_BASE}serviceworker.js`).
            then(() => console.log('Service worker registered')).
            catch((error) => {
                console.warn('Error registering service worker:');
                console.warn(error);
            });
        }
    });
    if (!TagG.IS_LAZYLOADAVAILABLE) myswal({
        title: (TagG.jp ? 'ご注意！' : 'CAUTION!'),
        text: (TagG.jp ? 'このブラウザではモナパレットの一部のリスト表示で件数が制限されるため、他のブラウザを使ったほうがよいです。モダンなブラウザであればだいたい大丈夫ですが、推奨はChromeです。' : 'As this browser can\'t display all items of some list in Monapalette, it is better to use another browser. A modern browser is usually fine, but Chrome is recommended.'),
        icon: 'warning',
        isCutin: true,
    });
}
