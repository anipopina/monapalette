/*global riot, TweenLite, QRCode, mdc, Spinner, Chart, BalloonBlockEditor, BigInt, lightGallery */
// version: 20211103
const Util = {};
Util.aSleep = (msec) => new Promise(resolve => setTimeout(resolve, msec));
Util.uniq = (array) => Array.from(new Set(array));
Util.round = (num, decimal) => Math.round(num * Math.pow(10, decimal)) / Math.pow(10, decimal);
Util.randomInt = (min, max) => Math.floor(Math.random() * (max + 1 - min)) + min;
Util.reverseStr = (str) => str.split('').reverse().join('');
Util.zeroPadding = (num, length) => ('0'.repeat(length) + num).slice(-length);
Util.saveStorage = Util.saveObjectToLocalStorage = (key, object) => {
    localStorage.setItem(key, JSON.stringify(object));
};
Util.loadStorage = Util.loadObjectFromLocalStorage = (key) => {
    const value = localStorage.getItem(key);
    if (!value) return null;
    try {
        if (value[0] === '%') return JSON.parse(decodeURIComponent(value)); // 旧版との互換用
        else return JSON.parse(value)
    }
    catch (error) { console.error(error); return null }
};
Util.floorLargeInt = (largeInt) => {
    return (largeInt > Number.MAX_SAFE_INTEGER)?
        Math.floor(largeInt * ((Number.MAX_SAFE_INTEGER - 2) / Number.MAX_SAFE_INTEGER)) : largeInt;
};
Util.ref2array = (ref) => {
    if (Array.isArray(ref)) return ref;
    if (!ref) return [];
    return [ref];
};
Util.dictizeListWithKey = (list, key) => {
    const dict = {};
    for (const item of list) item[key] && (dict[item[key]] = item);
    return dict;
};
Util.shuffle = (array) => {
    let n = array.length;
    while (n) {
        const i = Math.floor(Math.random() * n--);
        [array[n], array[i]] = [array[i], array[n]];
    }
};
Util.execCopy = string => {
    const tmp = document.createElement('div');
    const style = tmp.style;
    let success = false;
    tmp.appendChild(document.createElement('pre')).textContent = string;
    style.position = 'fixed';
    style.left = '-100%';
    document.body.appendChild(tmp);
    document.getSelection().selectAllChildren(tmp);
    success = document.execCommand('copy'); // 成功でtrue、失敗or未対応だとfalse
    document.body.removeChild(tmp);
    return success;
};
Util.commafy = (num, { decimal = 8, removeDecimalZero = true, noComma = false } = {}) => {
    num = Number(num);
    if (isNaN(num)) return null;
    const parts = num.toFixed(decimal).split('.');
    if (!noComma) parts[0] = parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
    if (parts[1] && removeDecimalZero) parts[1] = parts[1].replace(/0+$/, '');
    return parts[1] ? parts.join('.') : parts[0];
};
Util.escapeHTML = (str) => {
    return str.replace(/[&'`"<>]/g, match => {
        return { '&': '&amp;', "'": '&#x27;', '`': '&#x60;', '"': '&quot;', '<': '&lt;', '>': '&gt;' }[match];
    });
};
Util.aTimeout = (ms, promise) => {
    return new Promise((resolve, reject) => {
        const timeoutID = setTimeout(() => reject(new Error('timeout by aTimeout()')), ms);
        promise.then(resolve).catch(reject).finally(() => { clearTimeout(timeoutID) });
    });
};
Util.aFetchWithTimeout = async(timeoutMs, url, fetchParams) => {
    return await Util.aTimeout(timeoutMs, fetch(url, fetchParams));
};
Util.aRedundantFetch = async(timeoutMs, urls, fetchParams) => {
    let error;
    for (const url of urls) {
        try {
            const response = await Util.aFetchWithTimeout(timeoutMs, url, fetchParams);
            if (!response.ok) throw new Error(`aRedundantFetch response was not ok: ${url}`);
            return response;
        }
        catch (_error) { console.error(error = _error) }
    }
    throw error;
};
Util.aGetJSON = async(url, useCache = false) => {
    if (!Util.aGetJSON.cacheDict) Util.aGetJSON.cacheDict = {};
    if (useCache && (url in Util.aGetJSON.cacheDict)) return Util.aGetJSON.cacheDict[url];
    try {
        console.log('aGetJSON: ' + url);
        const response = await fetch(url);
        if (!response.ok) throw new Error(`network response was not ok. status: ${response.status}`);
        const json = await response.json();
        if (useCache) Util.aGetJSON.cacheDict[url] = json;
        else delete Util.aGetJSON.cacheDict[url];
        return json;
    }
    catch (error) {
        delete Util.aGetJSON.cacheDict[url];
        throw error;
    }
};
Util.aGetNumber = async(url, useCache = false) => {
    if (!Util.aGetNumber.cacheDict) Util.aGetNumber.cacheDict = {};
    if (useCache && (url in Util.aGetNumber.cacheDict)) return Util.aGetNumber.cacheDict[url];
    try {
        console.log('aGetNumber: ' + url);
        const response = await fetch(url);
        if (!response.ok) throw new Error(`network response was not ok. status: ${response.status}`);
        const number = Number(await response.text());
        if (useCache) Util.aGetNumber.cacheDict[url] = number;
        else delete Util.aGetNumber.cacheDict[url];
        return number;
    }
    catch (error) {
        delete Util.aGetNumber.cacheDict[url];
        throw error;
    }
};
Util.aJsonRPCRequest = async(url, method, params = {}, useCache = false) => {
    if (!Util.aJsonRPCRequest.cacheDict) Util.aJsonRPCRequest.cacheDict = {};
    const headers = { 'Content-Type': 'application/json', 'Accept': 'application/json' };
    const body = JSON.stringify({ id: 1, method, params });
    const cacheKey = `${url},${method},${body}`;
    if (useCache && Util.aJsonRPCRequest.cacheDict[cacheKey]) return Util.aJsonRPCRequest.cacheDict[cacheKey];
    try {
        console.log(`aJsonRPCRequest: ${url} ${method}`);
        const response = await fetch(url, { method: 'POST', headers, body });
        if (!response.ok) throw new Error(`network response was not ok. status: ${response.status}`);
        const parsedResponse = await response.json();
        if (parsedResponse.error) throw parsedResponse.error;
        if (useCache) Util.aJsonRPCRequest.cacheDict[cacheKey] = parsedResponse.result;
        else delete Util.aJsonRPCRequest.cacheDict[cacheKey];
        return parsedResponse.result;
    }
    catch (error) {
        delete Util.aJsonRPCRequest.cacheDict[cacheKey];
        throw error;
    }
};
Util.aOpenContentSwal = (contentDiv, additionalOptions = {}) => {
    if (!contentDiv) return;
    const options = { content: contentDiv, button: false, ...additionalOptions };
    if (window.myswal) return window.myswal(options);
    else return window.swal(options);
};
/*global Util, route, swal, myswal, TrezorConnect */
window.opener = null; // treasureビューアからの親ウィンドウ操作をブロック
const COINS = {
    MONA: 'MONA',
    BTC: 'BTC',
    MONA_TESTNET: 'MONA_TESTNET',
    BTC_TESTNET: 'BTC_TESTNET',
};
const ADDR_TYPE = {
    P2PKH: 'P2PKH',
    P2WPKH: 'P2WPKH',
};
const DISPENSER_STATUS = {
    OPEN: 0,
    CLOSE: 10,
};
const TAGG_TIME = new Date().getTime();
const TagG = {
    /* モナパレットはMonapartyのAPI仕様やCounterwalletのUIを参考に設計されています  https://github.com/monaparty */

    /* MIT License  Copyright (c) 2011-2019 bitcoinjs-lib contributors  https://github.com/bitcoinjs/bitcoinjs-lib/blob/master/LICENSE */
    bitcoinjs: require('bitcoinjs-lib'),
    /* MIT License  Copyright (c) 2015-2016 Bitcoinjs developers  https://github.com/bitcoinjs/bitcoinjs-message/blob/master/LICENSE */
    bitcoinjsMessage: require('bitcoinjs-message'),
    /* MIT License  coininfo https://opensource.org/licenses/mit-license.php */
    coininfo: require('coininfo'),
    /* MIT License  Copyright (c) 2018 Daniel Cousens  https://github.com/bitcoinjs/coinselect/blob/master/LICENSE */
    coinselect: require('coinselect'),
    coinselectSplit: require('coinselect/split'),
    /* MIT License  Copyright (c) 2018 cryptocoinjs  https://github.com/cryptocoinjs/hdkey/blob/master/LICENSE */
    HDKey: require('hdkey'),
    /* ISC License  Copyright (c) 2014, Wei Lu <luwei.here@gmail.com> and Daniel Cousens <email@dcousens.com>  https://github.com/bitcoinjs/bip39/blob/master/LICENSE */
    bip39: require('bip39'),
    /* MIT License  Copyright (c) 2009-2013 Jeff Mott Copyright (c) 2013-2016 Evan Vosberg  https://github.com/brix/crypto-js/blob/develop/LICENSE */
    CryptoJS: require('crypto-js'),
    /* MIT License  Copyright (c) Fedor Indutny, 2014  https://github.com/indutny/hash.js */
    hash: require('hash.js'),
    /* MIT License  Copyright (c) 2018 cryptocoinjs  https://github.com/cryptocoinjs/bs58/blob/master/LICENSE */
    Base58: require('bs58'),
    /* MIT License  Copyright (c) 2017 crypto-browserify  https://github.com/crypto-browserify/randombytes/blob/master/LICENSE */
    randomBytes: require('randombytes'),
    /* MIT License  Copyright (c) 2013 keybase  https://github.com/keybase/more-entropy/blob/master/LICENSE */
    MoreEntropy: require('more-entropy'),
    /* MIT License  Copyright (c) Fedor Indutny, 2017  https://github.com/indutny/hmac-drbg */
    HmacDRBG: require('hmac-drbg'),

    query: route.query() || {},
    body: null,
    bodySpinner: null,
    bodyDispenserPurchase: null,
    bodyDexPurchase: null,
    isReadOnlyMode: false,
    isTrezor: false,
    jp: true,
    confNum: 0,
    addrsData: null,
    addrsDataMaxIndex: 0,
    addrSelectItems: [],
    addrSelectDefaultValue: null,
    firstAddrLabel: null,
    originalDocumentTitle: document.title,
    addr2isMine: {},
    asset2isInMyWallet: {}, // assetNameでもOK
    asset2isMyOwn: {}, // assetNameでもOK
    asset2info: {}, // assetNameでもOK
    asset2extendedInfo: {},
    asset2card: {},
    asset2treasures: {},
    asset2rakugakiTitle: {},
    asset2isAnime: {},
    addrbook: [],
    tokenVaultCacheID: TAGG_TIME,
    rakugakiAsset2cacheID: {},
    dboardReadIndex: 0,
    changedAssets: [],
    windowMessageListeners: [],

    IS_PREVIEW: (location.hostname.indexOf('localhost') !== -1),
    IS_LAZYLOADAVAILABLE: ('loading' in HTMLImageElement.prototype),
    RELATIVE_URL_BASE: '/',
    RELATIVE_LINK_BASE: '/',
    IS_MPURSEEX: (window.MONAPALETTE_IS_MPURSEEX === true),
    QUERYKEY_ENCMNEMONIC: 'ep', // pはパスフレーズ時代の名残り
    QUERYKEY_ADDRTYPE: 'at',
    QUERYKEY_ACCOUNT: 'account',
    QUERYKEY_TREZORPATH: 'trezor_path',
    QUERYKEY_SEARCH: 'search',
    QUERYKEY_TREASUREURL: 'tu',
    MAX_ADDRCOUNT: 20,
    SWAL_TIMER: 2000,
    COIN: window.MONAPALETTE_COIN || COINS.MONA,
    STORAGEKEY_XPUB_FORMAT: 'monapalette_xpub_{HASH}',
    STORAGEKEY_ADDRINDEX_FORMAT: 'monapalette_addrindex_{FIRST_ADDR}',
    STORAGEKEY_LABEL_FORMAT: 'monapalette_label_{ADDR}',
    STORAGEKEY_STYLE_FORMAT: 'monapalette_style_{ADDR}',
    STORAGEKEY_RECENTPAIRS: 'monapalette_recentpairs',
    STORAGEKEY_ADDRBOOK: 'monapalette_addrbook',
    STORAGEKEY_ENDPOINTS_FORMAT: 'monapalette_blockendpoints_{COIN}',
    STORAGEKEY_CONFNUM: 'monapalette_confnum',
    STORAGEKEY_LANG: 'monapalette_lang',
    STORAGEKEY_DBOARDREADINDEX_FORMAT: 'monapalette_dboardreadindex_{FIRST_ADDR}',
    IPFSURL_FORMAT: 'https://mona.party/ipfs/{HASH}',
    TWITTERUSERURL_FORMAT: 'https://twitter.com/{SN}',
    TWEETURL_FORMAT: 'https://twitter.com/{SN}/status/{TWID}',
    OPENTWEETURL_FORMAT: 'https://twitter.com/intent/tweet?text={ENCODEDTXT}',
};
for (const key in TagG.query) TagG.query[key] = TagG.query[key].split('#')[0];
if (TagG.IS_PREVIEW) {
    TagG.RELATIVE_URL_BASE = '';
    TagG.RELATIVE_LINK_BASE = '#';
}
TagG.setMonaParams = () => {
    TagG.BASECHAIN_ASSET = 'MONA';
    TagG.BUILTIN_ASSET = 'XMP';
    TagG.SATOSHI_RATIO = 100000000;
    TagG.SATOSHI_DECIMAL = 8;
    TagG.AVG_BLOCKINTERVAL_MS = 1000 * 90;
    TagG.FEESAT_PERBYTE = 200;
    TagG.MULTISIG_FEESAT_PERBYTE = 250;
    TagG.SENDMIN_SAT = 1000;
    TagG.ISSUEFEE_NAMED = 50;
    TagG.ISSUEFEE_SUB = 25;
    TagG.ISSUEFEE_NFT = 0.25;
    TagG.ISSUEFEE_NUMERIC = 0;
    TagG.DIVIDENDFEE_PER_RECIPIENT = 0.0002;
    TagG.SWEEPFEE_AMOUNT = 0.5;
    TagG.ADDR_PATTERN = '([MP][1-9A-HJ-NP-Za-km-z]{33}|mona1[ac-hj-np-z02-9]{8,87})';
    TagG.INSIGHT_API_ENDPOINT = 'https://mona.insight.monaco-ex.org/insight-api-monacoin/';
    TagG.BLOCKBOOK_API_ENDPOINTS = ['https://blockbook.electrum-mona.org/api/', 'https://blockbook.monacoin.cloud/api/'];
    TagG.ADDR_EXPLORERURL_FORMAT = 'https://blockbook.electrum-mona.org/address/{ADDR}';
    TagG.ADDR_PARTYEXPURL_FORMAT = 'https://mpchain.info/address/{ADDR}';
    TagG.ASSET_PARTYEXPURL_FORMAT = 'https://mpchain.info/asset/{ASSET}';
    TagG.CARD_DETAILURL_FORMAT = 'https://card.mona.jp/explorer/card_detail?asset={ASSETNAME}';
    TagG.TREZOR_COIN = 'MONA';
    TagG.NETWORK = TagG.coininfo('MONA').toBitcoinJS();
    // ここまではMONAとBTCともに設定
    TagG.CARDSERVER_API = 'https://card.mona.jp/api/';
    TagG.TOKENVAULT_API = 'https://5addehsclc.execute-api.ap-northeast-1.amazonaws.com/';
    TagG.TOKENVAULT_ROOT = 'https://tokenvault.s3-ap-northeast-1.amazonaws.com/';
    TagG.TOKENVAULT_VIEWER = 'treasure.html';
    TagG.ANIMELIST_JSON = 'https://monapachan.s3.ap-northeast-1.amazonaws.com/animelist.json';
    TagG.SHOPS_JSON = 'https://monapachan.s3.ap-northeast-1.amazonaws.com/shops.json';
    TagG.SHOPS_IMGROOT = 'https://monapachan-img.s3-ap-northeast-1.amazonaws.com/shop-img/';
    TagG.DBOARD_CACHEJSON = 'https://monapalette.s3.ap-northeast-1.amazonaws.com/dboardcache.json';
    TagG.DISPENSERS_CACHEJSON = 'https://monapalette.s3.ap-northeast-1.amazonaws.com/dispenserscache.json';
    TagG.DEXASSETS_CACHEJSON = 'https://monapalette.s3.ap-northeast-1.amazonaws.com/dexassetscache.json';
    TagG.HOLDERS_CACHEJSON = 'https://monapalette.s3.ap-northeast-1.amazonaws.com/holderscache.json';
    TagG.MONACHARAT_ASSETGROUPS = ['MonaCharaT'];
    TagG.MONACHARAT_IMGROOT = 'https://monacharat-img.komikikaku.com/';
    TagG.MONACHARAT_SITEROOT = 'https://monacharat.komikikaku.com/';
    TagG.RAKUGAKI_ASSETGROUPS = ['Rakugaki'];
    TagG.RAKUGAKI_IMGROOT = 'https://tokenvault.s3-ap-northeast-1.amazonaws.com/rakugaki/';
    TagG.MONACUTE_ASSETGROUPS = ['Monacute'];
    TagG.MONACUTE_URLFORMAT = 'https://monacute.art/monacutes/{ID}';
    TagG.MONACUTE_IMGFORMAT = 'https://image.monacute.art/{DNA}.png';
    if (TagG.IS_PREVIEW) {
        // TagG.TOKENVAULT_API = 'https://gshhmlbpwb.execute-api.ap-northeast-1.amazonaws.com/';
        // TagG.TOKENVAULT_ROOT = 'https://tokenvault-test.s3-ap-northeast-1.amazonaws.com/';
        TagG.TOKENVAULT_VIEWER = 'treasure_preview.html';
        TagG.MONACHARAT_ASSETGROUPS.push('TestCharaT');
        TagG.RAKUGAKI_ASSETGROUPS.push('NFTEST');
    }
};
TagG.setBtcParams = () => {
    TagG.BASECHAIN_ASSET = 'BTC';
    TagG.BUILTIN_ASSET = 'XCP';
    TagG.SATOSHI_RATIO = 100000000;
    TagG.SATOSHI_DECIMAL = 8;
    TagG.AVG_BLOCKINTERVAL_MS = 1000 * 60 * 10;
    TagG.FEESAT_PERBYTE = 20;
    TagG.MULTISIG_FEESAT_PERBYTE = 20;
    TagG.SENDMIN_SAT = 1000;
    TagG.ISSUEFEE_NAMED = 0.5;
    TagG.ISSUEFEE_SUB = 0.25;
    TagG.ISSUEFEE_NFT = 1e10; // unavailable
    TagG.ISSUEFEE_NUMERIC = 0;
    TagG.DIVIDENDFEE_PER_RECIPIENT = 0.0002;
    TagG.SWEEPFEE_AMOUNT = 0.5;
    TagG.ADDR_PATTERN = '([13][1-9A-HJ-NP-Za-km-z]{33}|bc1[ac-hj-np-z02-9]{8,87})';
    TagG.INSIGHT_API_ENDPOINT = null;
    TagG.BLOCKBOOK_API_ENDPOINTS = null;
    TagG.ADDR_EXPLORERURL_FORMAT = 'https://blockstream.info/address/{ADDR}';
    TagG.ADDR_PARTYEXPURL_FORMAT = 'https://xchain.io/address/{ADDR}';
    TagG.ASSET_PARTYEXPURL_FORMAT = 'https://xchain.io/asset/{ASSET}';
    TagG.CARD_DETAILURL_FORMAT = null;
    TagG.TREZOR_COIN = 'BTC';
    TagG.NETWORK = TagG.coininfo('BTC').toBitcoinJS();
    // ここまではMONAとBTCともに設定
    TagG.MONAPARTY_ENDPOINTS = ['https://wallet.counterwallet.io/_api'];
};
switch (TagG.COIN) {
    case COINS.MONA:
        TagG.setMonaParams();
        break;
    case COINS.MONA_TESTNET:
        TagG.setMonaParams();
        TagG.ADDR_PATTERN = '([mnp][1-9A-HJ-NP-Za-km-z]{33}|tmona1[ac-hj-np-z02-9]{8,87})';
        TagG.INSIGHT_API_ENDPOINT = null;
        TagG.BLOCKBOOK_API_ENDPOINTS = null;
        TagG.ADDR_EXPLORERURL_FORMAT = 'https://testnet-blockbook.electrum-mona.org/address/{ADDR}';
        TagG.ADDR_PARTYEXPURL_FORMAT = 'https://testnet.mpchain.info/address/{ADDR}';
        TagG.ASSET_PARTYEXPURL_FORMAT = 'https://testnet.mpchain.info/asset/{ASSET}';
        TagG.CARDSERVER_API = null;
        TagG.TREZOR_COIN = 'MONA_TESTNET';
        TagG.NETWORK = TagG.coininfo('MONA-TEST').toBitcoinJS();
        TagG.MONAPARTY_ENDPOINTS = ['https://testnet-monapa.electrum-mona.org/_t_api', 'https://wallet.monaparty.me/_t_api/'];
        TagG.MONACHARAT_ASSETGROUPS = [];
        break;
    case COINS.BTC:
        TagG.setBtcParams();
        break;
    case COINS.BTC_TESTNET:
        TagG.setBtcParams();
        TagG.ADDR_PATTERN = '([mn2][1-9A-HJ-NP-Za-km-z]{33,34}|tb1[ac-hj-np-z02-9]{8,87})';
        TagG.ADDR_EXPLORERURL_FORMAT = 'https://blockstream.info/testnet/address/{ADDR}';
        TagG.ADDR_PARTYEXPURL_FORMAT = 'https://testnet.xchain.io/address/{ADDR}';
        TagG.ASSET_PARTYEXPURL_FORMAT = 'https://testnet.xchain.io/asset/{ASSET}';
        TagG.TREZOR_COIN = 'TEST';
        TagG.NETWORK = TagG.coininfo('BTC-TEST').toBitcoinJS();
        TagG.MONAPARTY_ENDPOINTS = ['https://wallet.counterwallet.io/_t_api'];
        break;
}
const Monaparty = require('../mymodules/Monaparty.js');
{
    Monaparty.timeout = 30000;
    Monaparty.defaults = {
        serversFile: Monaparty.serversFile,
        fullEndpoints: Monaparty.fullEndpoints,
        nocoindEndpoints: Monaparty.nocoindEndpoints,
    };
    const savedEndpoints = Util.loadStorage(TagG.STORAGEKEY_ENDPOINTS_FORMAT.replace('{COIN}', TagG.COIN));
    if (savedEndpoints || TagG.MONAPARTY_ENDPOINTS) {
        Monaparty.serversFile = null;
        Monaparty.fullEndpoints = savedEndpoints || TagG.MONAPARTY_ENDPOINTS;
        Monaparty.nocoindEndpoints = [];
    }
    Monaparty.aParty = Monaparty.aCounterPartyRequest;
    Monaparty.aBlock = Monaparty.aCounterBlockRequest;
    Monaparty.aPartyTableAll = Monaparty.aCounterPartyRequestTableAll;
}
TagG.aSignedJsonRPCRequest = async(url, method, params, addrData) => {
    const { address } = addrData;
    const paramsToSign = {
        method: method,
        timestamp: new Date().getTime(),
        params: params || {},
    };
    const paramStr = JSON.stringify(paramsToSign);
    const sign = await TagG.aSignMessage(paramStr, addrData);
    return await Util.aJsonRPCRequest(url, method, { sign, address, paramStr }, false);
};
{
    let _mnemonic, _accountXpub, _wif, _addrType, _accountPath, _encMnemonic;
    TagG.setMnemonic = (mnemonic, addrType, accountPath = null, encMnemonic = null) => {
        [_mnemonic, _addrType, _encMnemonic] = [mnemonic, addrType, encMnemonic];
        _accountPath = accountPath || TagG.addrType2defaultAccountPath(addrType);
    };
    TagG.setAccountXpub = (accountXpub, addrType, accountPath, encMnemonic = null) => {
        [_accountXpub, _addrType, _accountPath, _encMnemonic] = [accountXpub, addrType, accountPath, encMnemonic];
        if (encMnemonic) TagG.isReadOnlyMode = true;
    };
    TagG.setWIF = (wif, addrType) => {
        [_wif, _addrType] = [wif, addrType];
    };
    TagG.aUpdateAddrsDataWithUI = async(spinner = null) => {
        try {
            if (spinner) spinner.start();
            const nowTime = new Date().getTime();
            if (TagG.addrsData) {
                if (TagG.aUpdateAddrsDataWithUI.lastTime > nowTime - TagG.AVG_BLOCKINTERVAL_MS / 3) return;
                await aUpdateBalances(TagG.addrsData.slice(0, TagG.addrsDataMaxIndex + 1));
            }
            else {
                TagG.addrsData = await aGetAddrsData();
                if (!TagG.addrsData) return;
                TagG.addrsDataMaxIndex = await aUpdateBalances(TagG.addrsData);
                const indexStoreKey = TagG.STORAGEKEY_ADDRINDEX_FORMAT.replace('{FIRST_ADDR}', TagG.addrsData[0].address);
                const storedIndex = Util.loadStorage(indexStoreKey);
                if (storedIndex > TagG.addrsDataMaxIndex) TagG.addrsDataMaxIndex = storedIndex;
                TagG.addr2isMine = {};
                for (const addrData of TagG.addrsData) TagG.addr2isMine[addrData.address] = true;
                const dboardIndexStoreKey = TagG.STORAGEKEY_DBOARDREADINDEX_FORMAT.replace('{FIRST_ADDR}', TagG.addrsData[0].address);
                TagG.dboardReadIndex = Util.loadStorage(dboardIndexStoreKey) || 0;
                TagG.setAddrSelectItems();
                TagG.updatefirstAddrLabel();
                TagG.aCheckMentions();
            }
            TagG.aUpdateAddrsDataWithUI.lastTime = nowTime;
        }
        catch (error) {
            console.error(error);
            myswal('Error', TagG.jp ? `アドレスデータの取得に失敗しました。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}` : `Failed to fetch address data. hint: ${error}`, 'error');
            return { error };
        }
        finally { if (spinner) spinner.stop() }
    };
    TagG.setAddrSelectItems = () => {
        TagG.addrSelectItems = TagG.addrsData.slice(0, TagG.addrsDataMaxIndex + 1)
            .map((data, i) => ({ value: data.address, label: `${i + 1}: ${data.label || data.address}` }));
        TagG.addrSelectDefaultValue = TagG.addrSelectItems[0].value;
    };
    const aGetAddrsData = async() => {
        if (_mnemonic) return getAddrsDataForMnemonic();
        else if (_accountXpub) return getAddrsDataForAccountXpub();
        else if (_wif) return getAddrsDataForWIF();
        else if (TagG.IS_MPURSEEX && window.mpurse) return await aGetAddrsDataFromMpurse();
        else return null;
    };
    const aUpdateBalances = async(addrsData) => {
        const [baseIndex, assetIndex] = await Promise.all([TagG.aUpdateBaseAssetBalances(addrsData), TagG.aUpdateAssetBalances(addrsData)]);
        const maxIndex = Math.max(baseIndex, assetIndex);
        return maxIndex;
    };
    const getAddrsDataForMnemonic = () => {
        const addrsData = [];
        const seed = TagG.bip39.mnemonicToSeed(_mnemonic);
        const masterHDKey = TagG.HDKey.fromMasterSeed(seed);
        for (let index = 0; index < TagG.MAX_ADDRCOUNT; index++) {
            const path = `${_accountPath}/0/${index}`;
            const { ecPair, addrObj } = getKeyDataForDerivePath(masterHDKey, path, _addrType);
            addrsData.push(getAddrDataFormat({ index, addrObj, ecPair, addrType: _addrType, path }));
        }
        if (_encMnemonic) {
            const accountHDKey = masterHDKey.derive(_accountPath);
            saveAccountXpub(accountHDKey.publicExtendedKey, _accountPath, _encMnemonic);
        }
        return addrsData;
    };
    const getAddrsDataForAccountXpub = () => {
        const addrsData = [];
        const accountHDKey = TagG.HDKey.fromExtendedKey(_accountXpub, TagG.NETWORK.versions.bip32);
        for (let index = 0; index < TagG.MAX_ADDRCOUNT; index++) {
            const path = `m/0/${index}`;
            const fullPath = path.replace('m', _accountPath);
            const { addrObj } = getKeyDataForDerivePath(accountHDKey, path, _addrType);
            addrsData.push(getAddrDataFormat({ index, addrObj, addrType: _addrType, path: fullPath }));
        }
        return addrsData;
    };
    const getAddrsDataForWIF = () => {
        const { ecPair, addrObj } = getKeyDataForWIF(_wif, _addrType);
        const addrData = getAddrDataFormat({ index: 0, addrObj, ecPair, addrType: _addrType });
        return [addrData];
    };
    const aGetAddrsDataFromMpurse = async() => {
        const address = await window.mpurse.getAddress();
        const addrData = getAddrDataFormat({ index: 0, address });
        return [addrData];
    };
    const saveAccountXpub = (accountXpub, accountPath, encMnemonic) => {
        const encMnemonicHash = TagG.hash.sha256().update(encMnemonic).digest('hex');
        const key = TagG.hash.sha256().update(encMnemonicHash + accountPath).digest('hex');
        const encXpub = TagG.CryptoJS.AES.encrypt(accountXpub, encMnemonicHash).toString();
        Util.saveStorage(TagG.STORAGEKEY_XPUB_FORMAT.replace('{HASH}', key), encXpub);
    };
    TagG.loadAccountXpub = (accountPath, encMnemonic) => {
        const encMnemonicHash = TagG.hash.sha256().update(encMnemonic).digest('hex');
        const key = TagG.hash.sha256().update(encMnemonicHash + accountPath).digest('hex');
        const encXpub = Util.loadStorage(TagG.STORAGEKEY_XPUB_FORMAT.replace('{HASH}', key));
        if (!encXpub) return null;
        const accountXpub = TagG.CryptoJS.enc.Utf8.stringify(TagG.CryptoJS.AES.decrypt(encXpub, encMnemonicHash));
        return accountXpub;
    };
    TagG.aUpgradeFromReadOnlyMode = async() => {
        const text = TagG.jp?
        '現在Read Onlyモードでウォレットを開いています。秘密鍵を利用するためにはQuick Access URLに対応するパスワードを入力してください':
        'You are currently opening the wallet in read-only mode. Enter the quick-access password to use private key.';
        const swalContent = TagG.swalContents.getPasswordWin({ title: (TagG.jp ? 'パスワード' : 'Password'), text });
        const isSpinning = TagG.bodySpinner.isSpinning;
        if (isSpinning) TagG.bodySpinner.stop();
        await Util.aOpenContentSwal(swalContent, { isCutin: true });
        if (isSpinning) TagG.bodySpinner.start();
        if (!swalContent.isOK) throw (TagG.jp ? 'パスワードの入力をキャンセルしました' : 'Password input cancelled.');
        const password = swalContent.value;
        let mnemonic;
        try { mnemonic = TagG.CryptoJS.enc.Utf8.stringify(TagG.CryptoJS.AES.decrypt(_encMnemonic, password)) }
        catch (error) { throw 'Invalid Password' }
        if (!TagG.bip39.validateMnemonic(mnemonic)) throw 'Invalid Password';
        _mnemonic = mnemonic;
        const seed = TagG.bip39.mnemonicToSeed(_mnemonic);
        const masterHDKey = TagG.HDKey.fromMasterSeed(seed);
        for (const addrData of TagG.addrsData) {
            const { ecPair, addrObj } = getKeyDataForDerivePath(masterHDKey, addrData.path, addrData.addrType);
            if (addrData.address !== addrObj.address) throw new Error('upgrade address mismatch');
            addrData.ecPair = ecPair;
        }
        TagG.isReadOnlyMode = false;
        TagG.body.update(); // body-bottomfloat を更新
    };
    TagG.aUpdateBaseAssetBalances = async(addrsData) => {
        let maxIndex;
        if (TagG.BLOCKBOOK_API_ENDPOINTS) {
            maxIndex = await aSetUTXOsByBlockbookAPI(addrsData).catch(console.error);
            if (typeof maxIndex === 'number') return maxIndex;
        }
        if (TagG.INSIGHT_API_ENDPOINT && (addrsData[0] && addrsData[0].addrType === ADDR_TYPE.P2PKH)) {
            maxIndex = await aSetUTXOsByInsightAPI(addrsData).catch(console.error);
            if (typeof maxIndex === 'number') return maxIndex;
        }
        return await aSetUTXOsByMonapartyAPI(addrsData);
    };
    TagG.aUpdateAssetBalances = async(addrsData) => {
        try {
            const nowTime = new Date().getTime();
            const addrs = addrsData.map(data => data.address);
            if (!addrsData.some(addrData => (addrData.assetBlock === 0 || addrData.assetTime < nowTime - 1000 * 60 * 20))) {
                const lastHeight = addrsData.reduce((minHeight, data) => (data.assetBlock < minHeight ? data.assetBlock : minHeight), Number.MAX_SAFE_INTEGER);
                const isChanged = await aCheckIsAssetBalanceChanged(addrs, lastHeight);
                if (!isChanged) {
                    await TagG.aLoadTreasures();
                    return TagG.addrsDataMaxIndex;
                }
            }
            let maxIndex = 0;
            aGetBlockHeight().then(height => {
                for (const data of addrsData) data.assetBlock = height;
            }).catch(console.error); // awaitしない
            const addr2data = Util.dictizeListWithKey(addrsData, 'address');
            const balanceFilters = [{ field: 'address', op: 'IN', value: addrs }];
            const issuanceFilters = [{ field: 'issuer', op: 'IN', value: addrs }, { field: 'transfer', op: '==', value: 1 }];
            const dispenserFilters = [{ field: 'source', op: 'IN', value: addrs }, { field: 'status', op: '==', value: DISPENSER_STATUS.OPEN }]; // get_dispensersはstatusパラメータが効かないのでフィルタでstatus指定
            const [balances, transferIssuances, escrs, dispensers] = await Promise.all([
                Monaparty.aPartyTableAll('get_balances', { filters: balanceFilters }),
                Monaparty.aPartyTableAll('get_issuances', { filters: issuanceFilters, status: 'valid', order_by: 'tx_index', order_dir: 'ASC' }),
                Monaparty.aBlock('get_escrowed_balances', { addresses: addrs }),
                Monaparty.aParty('get_dispensers', { filters: dispenserFilters }),
                TagG.aLoadTreasures(),
            ]);
            const assets = [...balances.map(bal => bal.asset), ...transferIssuances.map(iss => iss.asset)];
            if (TagG.changedAssets.length > 0) {
                await TagG.aLoadAssets(TagG.changedAssets, false, false);
                TagG.changedAssets = [];
            }
            await TagG.aLoadAssets(assets, true, true);
            for (const issuance of transferIssuances) { // transferでownerだけ受取るとbalanceに載らないためissuancesから補完
                const assetInfo = TagG.asset2info[issuance.asset];
                if (!addr2data[assetInfo.owner]) continue;
                if (!balances.some(bal => (bal.asset === assetInfo.asset && bal.address === assetInfo.owner)))
                    balances.push({ address: assetInfo.owner, asset: assetInfo.asset, quantity: 0 });
            }
            for (const addrData of addrsData) {
                addrData.builtinSat = 0;
                addrData.builtinUnconfSat = 0;
                addrData.builtinEscrSat = 0;
                addrData.assets = [];
                addrData.dispensers = [];
            }
            for (const dispenser of dispensers) {
                const addrData = addr2data[dispenser.source];
                addrData.dispensers.push(TagG.formatDispenserData(dispenser));
            }
            TagG.asset2isInMyWallet = {};
            TagG.asset2isMyOwn = {};
            for (const balance of balances) {
                const addrData = addr2data[balance.address];
                const assetInfo = TagG.asset2info[balance.asset];
                const dispenser = addrData.dispensers.find(disp => (disp.asset === balance.asset));
                let escrSat = (escrs[balance.address] && escrs[balance.address][balance.asset]) || 0;
                if (dispenser) escrSat += dispenser.remainingSat;
                if (balance.asset === TagG.BUILTIN_ASSET) {
                    addrData.builtinSat = balance.quantity;
                    addrData.builtinUnconfSat = 0; // 未実装
                    addrData.builtinEscrSat = escrSat;
                    if (balance.quantity) TagG.asset2isInMyWallet[TagG.BUILTIN_ASSET] = true;
                }
                else {
                    const asset = assetInfo.asset;
                    const name = assetInfo.name;
                    const cardData = TagG.asset2card[name] || null;
                    const assetData = { // format: assetData
                        ...assetInfo,
                        satoshi: balance.quantity,
                        unconfSat: 0, // 未実装
                        escrSat: escrSat,
                        isOwn: assetInfo.owner === balance.address,
                        imageL: TagG.getAssetImageURL('L', asset),
                        imageM: TagG.getAssetImageURL('M', asset),
                        imageS: TagG.getAssetImageURL('S', asset),
                        imageAuto: TagG.getAssetImageURL('auto', asset),
                        cardData,
                        infoRef: assetInfo,
                    };
                    assetData.searchText = getAssetSearchText(assetData);
                    if (assetData.satoshi || assetData.unconfSat || assetData.escrSat || assetData.isOwn) addrData.assets.push(assetData);
                    if (assetData.satoshi || assetData.escrSat) TagG.asset2isInMyWallet[asset] = TagG.asset2isInMyWallet[name] = true;
                    if (assetData.isOwn) TagG.asset2isMyOwn[asset] = TagG.asset2isMyOwn[name] = true;
                }
                if (addrData.index > maxIndex) maxIndex = addrData.index;
            }
            for (const data of addrsData) data.assetTime = nowTime;
            return maxIndex;
        }
        catch (error) {
            for (const data of addrsData) [data.assetTime, data.assetBlock] = [0, 0];
            throw error;
        }
    };
    const getAssetSearchText = (assetData) => {
        const { asset, name, group, description, isLocked, cardData } = assetData;
        let text = `${name.toLowerCase()} ${name} ${asset} ${description} ${isLocked ? '[LOCKED]' : ''}`;
        if (group) text += ` ${group} [NFT]`;
        if (!cardData) return text;
        text += ` ${cardData.cardName} ${cardData.ownerName} ${cardData.description} ${cardData.tags}`;
        return text;
    };
    const aCheckIsAssetBalanceChanged = async(addrs, lastHeight) => {
        // issuanceでdescriptionを書き換えただけでもXMPが0減るdebitが発生するので捕捉可能
        const params = {
            start_block: lastHeight + 1,
            filters: [{ field: 'address', op: 'IN', value: addrs }],
        };
        const [debits, credits] = await Promise.all([
            Monaparty.aParty('get_debits', params),
            Monaparty.aParty('get_credits', params),
        ]);
        return Boolean(debits.length || credits.length);
    };
    const aGetBlockHeight = async() => {
        const urls = TagG.BLOCKBOOK_API_ENDPOINTS.map(blockbook => `${blockbook}api`);
        let status;
        for (const url of urls) {
            status = await Util.aGetJSON(url, false).catch(console.error);
            if (status) break;
        }
        return status.blockbook.bestHeight;
    };
    TagG.formatDispenserData = (dispenser) => {
        const assetInfo = TagG.asset2info[dispenser.asset];
        const cardData = TagG.asset2card[assetInfo.name] || null;
        const mainDescription = TagG.getMainDescription(assetInfo);
        const [unitBaseSat, unitAssetSat, escrowSat, remainingSat] = [dispenser.satoshirate, dispenser.give_quantity, dispenser.escrow_quantity, dispenser.give_remaining];
        const [unitBaseAmount, unitAssetAmount, escrowAmount, remainingAmount] = [unitBaseSat / TagG.SATOSHI_RATIO, TagG.qty2amount(unitAssetSat, assetInfo.isDivisible), TagG.qty2amount(escrowSat, assetInfo.isDivisible), TagG.qty2amount(remainingSat, assetInfo.isDivisible)];
        const [unitBaseAmountS, unitAssetAmountS, escrowAmountS, remainingAmountS] = [Util.commafy(unitBaseAmount), Util.commafy(unitAssetAmount), Util.commafy(escrowAmount), Util.commafy(remainingAmount)];
        return { // format: dispenserData
            assetInfo,
            txID: dispenser.tx_hash,
            txIndex: dispenser.tx_index,
            address: dispenser.source,
            asset: dispenser.asset,
            assetName: assetInfo.name,
            assetGroup: assetInfo.group,
            isDivisible: assetInfo.isDivisible,
            unitBaseSat, unitBaseAmount, unitBaseAmountS,
            unitAssetSat, unitAssetAmount, unitAssetAmountS,
            escrowSat, escrowAmount, escrowAmountS,
            remainingSat, remainingAmount, remainingAmountS,
            zaikoNum: Math.floor(dispenser.give_remaining / dispenser.give_quantity),
            imageURL: TagG.getAssetImageURL('auto', assetInfo.asset),
            largeImageURL: TagG.getAssetImageURL('L', assetInfo.asset),
            cardData,
            mainDescription,
        };
    };
    TagG.getMainDescription = (assetInfo) => {
        const cardData = TagG.asset2card[assetInfo.name] || null;
        let mainDescription = assetInfo.extendedInfo ? assetInfo.extendedInfo.description : assetInfo.description;
        if (cardData) mainDescription = cardData.description;
        if (TagG.asset2rakugakiTitle[assetInfo.asset]) mainDescription = `${TagG.jp ? 'らくがきNFT' : 'Rakugaki NFT'}「 ${TagG.asset2rakugakiTitle[assetInfo.asset]} 」`;
        return mainDescription;
    };
    const getAddrDataFormat = ({ index, address = null, addrObj = null, ecPair = null, addrType = ADDR_TYPE.P2PKH, path = null }) => {
        address = address || addrObj.address;
        const labelStorageKey = TagG.STORAGEKEY_LABEL_FORMAT.replace('{ADDR}', address);
        const styleStorageKey = TagG.STORAGEKEY_STYLE_FORMAT.replace('{ADDR}', address);
        const addrData = { // format: addrData
            index,
            address,
            ecPair,
            addrType,
            path,
            publicKey: addrObj ? addrObj.pubkey : null,
            script: addrObj ? addrObj.output : TagG.bitcoinjs.address.toOutputScript(address, TagG.NETWORK),
            baseSat: 0,
            baseUnconfSat: 0,
            builtinSat: 0,
            builtinUnconfSat: 0,
            builtinEscrSat: 0,
            assetTime: 0,
            assetBlock: 0,
            utxos: [],
            assets: [],
            label: Util.loadStorage(labelStorageKey) || null,
            labelStorageKey,
            styleStorageKey,
        };
        addrData.getBalanceSat = (asset) => getBalanceSat(addrData, asset);
        addrData.getBalanceAmount = (asset) => getBalanceAmount(addrData, asset);
        return addrData;
    };
    const formatUTXOs = (utxos) => {
        // 各種APIのUTXOを同じ形式にフォーマット
        // coinselectでそのまま使える形式
        return utxos.map(utxo => {
            const value = Number(utxo.value) || Number(utxo.satoshis) || Math.round(Number(utxo.amount) * TagG.SATOSHI_RATIO);
            const amount = value / TagG.SATOSHI_RATIO;
            return { // format: utxo
                txid: utxo.txid,
                vout: utxo.vout,
                value: value,
                amount: amount,
                confirmations: utxo.confirmations,
            };
        });
    };
    const aSetUTXOsByBlockbookAPI = async(addrsData) => {
        let maxIndex = 0;
        const promises = [];
        for (const addrData of addrsData) {
            const endpoints = TagG.BLOCKBOOK_API_ENDPOINTS.slice();
            if (addrData.index % 2) endpoints.reverse();
            const utxoURLs = endpoints.map(endpoint => `${endpoint}v2/utxo/${addrData.address}`);
            const aSet = async() => {
                let rawUTXOs;
                for (const url of utxoURLs) {
                    rawUTXOs = await Util.aGetJSON(url, false).catch(console.error);
                    if (rawUTXOs) break;
                }
                if (!rawUTXOs) throw new Error(`aSetUTXOsByBlockbookAPI failed: ${JSON.stringify(utxoURLs)}`);
                addrData.utxos = formatUTXOs(rawUTXOs);
                setBaseSatsFromUTXOs(addrData);
                if (addrData.utxos.length && addrData.index > maxIndex) maxIndex = addrData.index;
            };
            promises.push(aSet());
        }
        await Promise.all(promises);
        for (let i = maxIndex + 1; i < TagG.MAX_ADDRCOUNT; i++) {
            const addrData = addrsData[i];
            if (!addrData) break;
            const endpoints = TagG.BLOCKBOOK_API_ENDPOINTS.slice();
            if (addrData.index % 2) endpoints.reverse();
            const addrURLs = endpoints.map(endpoint => `${endpoint}v2/address/${addrData.address}?page=1&pageSize=1`);
            let addrInfo;
            for (const url of addrURLs) {
                addrInfo = await Util.aGetJSON(url, false).catch(console.error);
                if (addrInfo && ('totalReceived' in addrInfo)) break;
            }
            if (!(addrInfo && ('totalReceived' in addrInfo))) throw new Error(`aSetUTXOsByBlockbookAPI failed: ${JSON.stringify(addrURLs)}`);
            if (Number(addrInfo.totalReceived) > 0) maxIndex = i;
            else break;
        }
        return maxIndex;
    };
    const aSetUTXOsByInsightAPI = async(addrsData) => {
        let maxIndex = 0;
        const addrs = addrsData.map(data => data.address);
        const utxosURL = `${TagG.INSIGHT_API_ENDPOINT}addrs/${addrs.join(',')}/utxo`;
        const rawAllUTXOs = await Util.aGetJSON(utxosURL, false);
        for (const addrData of addrsData) {
            const rawUTXOs = rawAllUTXOs.filter(rawUTXO => (rawUTXO.address === addrData.address));
            addrData.utxos = formatUTXOs(rawUTXOs);
            setBaseSatsFromUTXOs(addrData);
            if (addrData.utxos.length && addrData.index > maxIndex) maxIndex = addrData.index;
        }
        for (let i = maxIndex + 1; i < TagG.MAX_ADDRCOUNT; i++) {
            const addrData = addrsData[i];
            if (!addrData) break;
            const totalReceivedURL = `${TagG.INSIGHT_API_ENDPOINT}addr/${addrData.address}/totalReceived`;
            const totalReceived = await Util.aGetNumber(totalReceivedURL, false);
            if (totalReceived > 0) maxIndex = i;
            else break;
        }
        return maxIndex;
    };
    const aSetUTXOsByMonapartyAPI = async(addrsData) => {
        let maxIndex = 0;
        const addrs = addrsData.map(data => data.address);
        const params = {
            'addresses': addrs,
            'with_uxtos': true, // なぜかutxosじゃなくてuxtos
            'with_last_txn_hashes': true, // ドキュメントではintだが実際はtrue/falseでしか処理されない
        };
        const chainAddrsInfo = await Monaparty.aBlock('get_chain_address_info', params);
        for (const { addr, last_txns: lastTXs, uxtos: utxos } of chainAddrsInfo) { // なぜかutxosじゃなくてuxtos
            const addrData = addrsData.find(data => (data.address === addr));
            addrData.utxos = formatUTXOs(utxos);
            setBaseSatsFromUTXOs(addrData);
            if (lastTXs.length && addrData.index > maxIndex) maxIndex = addrData.index;
        }
        return maxIndex;
    };
    const setBaseSatsFromUTXOs = (addrData) => {
        addrData.baseSat = addrData.utxos
            .filter(utxo => (utxo.confirmations >= 1))
            .reduce((sum, utxo) => (sum + utxo.value), 0);
        addrData.baseUnconfSat = addrData.utxos
            .filter(utxo => (utxo.confirmations < 1))
            .reduce((sum, utxo) => (sum + utxo.value), 0);
    };
    const getKeyDataForDerivePath = (parentHDKey, derivePath, addrType) => {
        const hdKey = parentHDKey.derive(derivePath);
        const ecPair = hdKey.privateKey ? TagG.bitcoinjs.ECPair.fromPrivateKey(hdKey.privateKey, { network: TagG.NETWORK }) : null;
        const addrObj = (addrType === ADDR_TYPE.P2WPKH)?
            TagG.bitcoinjs.payments.p2wpkh({ pubkey: hdKey.publicKey, network: TagG.NETWORK }):
            TagG.bitcoinjs.payments.p2pkh({ pubkey: hdKey.publicKey, network: TagG.NETWORK });
        return { ecPair, addrObj };
    };
    const getKeyDataForWIF = (wif, addrType) => {
        const ecPair = TagG.bitcoinjs.ECPair.fromWIF(wif, TagG.NETWORK);
        const addrObj = (addrType === ADDR_TYPE.P2WPKH)?
            TagG.bitcoinjs.payments.p2wpkh({ pubkey: ecPair.publicKey, network: TagG.NETWORK }):
            TagG.bitcoinjs.payments.p2pkh({ pubkey: ecPair.publicKey, network: TagG.NETWORK });
        return { ecPair, addrObj };
    };
    const getBalanceSat = (addrData, asset) => {
        if (asset === TagG.BASECHAIN_ASSET) return addrData.baseSat;
        if (asset === TagG.BUILTIN_ASSET) return addrData.builtinSat;
        const assetData = addrData.assets.find(_assetData => (_assetData.asset === asset || _assetData.name === asset));
        return assetData ? assetData.satoshi : 0;
    };
    const getBalanceAmount = (addrData, asset) => {
        const sat = addrData.getBalanceSat(asset);
        if (asset === TagG.BASECHAIN_ASSET || asset === TagG.BUILTIN_ASSET) return TagG.qty2amount(sat, true);
        const assetInfo = TagG.asset2info[asset];
        return assetInfo ? TagG.qty2amount(sat, assetInfo.isDivisible) : 0;
    };
}
{
    TagG.aLoadAssetsInfo = async(assets, useCache = false) => { // assetNameでもOK
        const filteredAssets = useCache ? assets.filter(asset => !TagG.asset2info[asset]) : assets;
        const assetsInfo = await aGetManyAssetsInfo(filteredAssets);
        TagG.addAssetsInfo(assetsInfo);
    };
    TagG.addAssetsInfo = (assetsInfo) => {
        for (const info of assetsInfo) {
            const name = info.asset_longname || info.asset;
            const shortenName = shortenAsset(name);
            const assetgroup = info.assetgroup || info.asset_group || null; // 表記ゆれ対策
            let parsedDesc = null;
            if (info.description.trim()[0] === '{') { // 配列や""で囲われた文字列もparseできてしまうので{}だけに限定
                try { parsedDesc = JSON.parse(info.description) } catch (err) {}
            }
            const appData = {};
            if (parsedDesc?.attr) {
                const attributes = [];
                for (const name in parsedDesc.attr) {
                    const value = parsedDesc.attr[name];
                    if (['string', 'number'].includes(typeof value)) attributes.push({ name, value })
                }
                if (attributes.length) appData.attributes = attributes;
            }
            if (assetgroup) {
                if (TagG.MONACHARAT_ASSETGROUPS?.includes(assetgroup)) appData.monacharat = { // Monacharat
                    url: `${TagG.MONACHARAT_SITEROOT}${info.asset}`,
                    image: `${TagG.MONACHARAT_IMGROOT}${name}.png`,
                    thumb: `${TagG.MONACHARAT_IMGROOT}${name}_160.png`,
                };
                if (TagG.RAKUGAKI_ASSETGROUPS?.includes(assetgroup)) appData.rakugakinft = { // RakugakiNFT
                    image: `${TagG.RAKUGAKI_IMGROOT}${name}`,
                };
                if (TagG.MONACUTE_ASSETGROUPS?.includes(assetgroup) && parsedDesc?.monacute) { // Monacute
                    const monacuteData = parsedDesc.monacute;
                    appData.monacute = {
                        id: monacuteData.id,
                        name: monacuteData.name,
                        dna: monacuteData.dna,
                        model: monacuteData.model,
                        url: TagG.MONACUTE_URLFORMAT.replace('{ID}', monacuteData.id),
                        image: TagG.MONACUTE_IMGFORMAT.replace('{DNA}', monacuteData.dna),
                    };
                }
            }
            Object.assign(info, { // format: assetInfo
                name,
                shortenName,
                assetgroup,
                group: assetgroup,
                easyLabel: assetgroup ? `${assetgroup} (${shortenName})` : shortenName,
                longLabel: assetgroup ? `${name} (${assetgroup})` : name,
                issuedSat: info.supply,
                issuedAmount: TagG.qty2amount(info.supply, info.divisible),
                isDivisible: info.divisible,
                isReassignable: info.reassignable,
                isListed: info.listed,
                isLocked: info.locked,
                parsedDesc,
                explorerURL: TagG.ASSET_PARTYEXPURL_FORMAT.replace('{ASSET}', info.asset),
                extendedInfo: TagG.asset2extendedInfo[info.asset] || null,
                appData,
            });
            TagG.asset2info[info.asset] = TagG.asset2info[info.name] = info;
        }
    };
    const aGetManyAssetsInfo = async(assets) => {
        const ONCE = 250;
        const promises = [];
        for (let i = 0; i < Math.ceil(assets.length / ONCE); i++) {
            const slicedAssets = assets.slice(ONCE * i, ONCE * (i + 1));
            promises.push(Monaparty.aBlock('get_assets_info', { assetsList: slicedAssets }));
        }
        const assetsInfoArray = await Promise.all(promises);
        const assetsInfo = assetsInfoArray.reduce((joined, nextList) => joined.concat(nextList), []);
        return assetsInfo;
    };
    const shortenAsset = (assetName) => (assetName[0] === 'A' ? `${assetName.slice(0, 5)}...${assetName.slice(-4)}` : assetName);
}
{
    const IMG_URL_FORMAT_ORIGINAL = 'https://mcspare.nachatdayo.com/image_server/img/{CID}';
    const IMG_URL_FORMAT_SIZED = 'https://monacard-img.komikikaku.com/{CID}l';
    const IMG_URL_FORMAT_SMALL = 'https://monacard-img.komikikaku.com/{CID}t';
    TagG.cardLoadedMap = {};
    TagG.aLoadCardsData = async(assetNames, useCache = false) => {
        if (!TagG.CARDSERVER_API) { console.log('aLoadCardsData skip'); return }
        const URL = `${TagG.CARDSERVER_API}card_detail_post`;
        const filteredAssetNames = useCache ? assetNames.filter(name => !TagG.cardLoadedMap[name]) : assetNames;
        const fetchParams = {
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/json' },
            body: `assets=${encodeURIComponent(filteredAssetNames.join(','))}`,
        };
        let { details } = await fetch(URL, fetchParams).then(res => res.json());
        TagG.addCardsData(details || []);
        for (const name of filteredAssetNames) TagG.cardLoadedMap[name] = true; // 冗長だけど両方必要 *
    };
    TagG.aPostCardImage = async(imgBlob) => { // Monacard 2.0
        if (!TagG.CARDSERVER_API) throw 'Monacard is not available for this coin';
        console.log('post cardImage');
        const url = `${TagG.CARDSERVER_API}upload_image`;
        const formData = new FormData();
        formData.append('image', imgBlob);
        const response = await fetch(url, { method: 'POST', body: formData });
        const resultText = await response.text();
        if (!response.ok) throw new Error('cardImage upload failed.\n\n', resultText);
        const result = JSON.parse(resultText);
        if (result.success) console.log('cardImage successfully uploaded');
        else throw new Error('cardImage upload failed.\n\n', resultText)
        return result.success.cid;
    };
    TagG.addCardsData = (cardDetails) => {
        for (const detail of cardDetails) {
            TagG.asset2card[detail.asset] = TagG.asset2card[detail.asset_common_name] = { // format: cardData
                asset: detail.asset,
                cardName: unescapeHTML(detail.card_name),
                ownerName: unescapeHTML(detail.owner_name),
                ownerSN: detail.tw_name,
                siteURL: TagG.CARD_DETAILURL_FORMAT.replace('{ASSETNAME}', detail.asset_common_name),
                imgurURL: IMG_URL_FORMAT_ORIGINAL.replace('{CID}', detail.cid), // Thanks to Imgur
                imgurSizedURL: IMG_URL_FORMAT_SIZED.replace('{CID}', detail.cid),
                imgurSmallURL: IMG_URL_FORMAT_SMALL.replace('{CID}', detail.cid),
                description: unescapeHTML(detail.add_description),
                version: detail.ver,
                tags: unescapeHTML(detail.tag),
            };
            TagG.cardLoadedMap[detail.asset_common_name] = true; // 冗長だけど両方必要 *
        }
    };
    const unescapeHTML = (str) => {
        return str.replace(/&(amp|quot|#039|lt|gt);/g, match => {
            return { '&amp;': '&', '&quot;': '"', '&#039;': "'", '&lt;': '<', '&gt;': '>' }[match];
        });
    };
}
TagG.aLoadAssets = async(assets, useInfoCache = false, useCardCache = false) => { // longnameでもOK
    assets = Util.uniq(assets);
    await TagG.aLoadAssetsInfo(assets, useInfoCache);
    const assetNames = assets.map(asset => (TagG.asset2info[asset] || {}).name).filter(val => val);
    await TagG.aLoadCardsData(assetNames, useCardCache).catch(console.error);
};
TagG.aLoadTreasures = async() => {
    const nowTime = new Date().getTime();
    if (TagG.tokenVaultCacheID < nowTime - 1000 * 60 * 30) TagG.tokenVaultCacheID = nowTime;
    const { treasures } = await Util.aGetJSON(`${TagG.TOKENVAULT_ROOT}public/tokenvault.json?${TagG.tokenVaultCacheID}`, false);
    const asset2treasures = {};
    for (const { asset, assetNm, isDiv, qty, memo, hasTmb } of treasures) {
        if (!asset2treasures[asset]) asset2treasures[asset] = [];
        const tmbImage = hasTmb? `${TagG.TOKENVAULT_ROOT}thumbnails/${asset}_${qty}` : `${TagG.RELATIVE_URL_BASE}img/monapalette_notmb.png`;
        asset2treasures[asset].push({ // format: treasureData
            asset,
            assetName: assetNm,
            isDivisible: isDiv,
            qtyN: window.BigInt ? window.BigInt(qty) : Number(qty),
            qtyS: qty,
            amountS: TagG.qtyS2amountS(qty, isDiv),
            memo,
            hasTmb,
            tmbImage,
        });
    }
    TagG.asset2treasures = asset2treasures;
};
TagG.aLoadRakugakiTitles = async() => {
    if (!TagG.TOKENVAULT_ROOT) return;
    const { asset2title } = await Util.aGetJSON(`${TagG.TOKENVAULT_ROOT}public/rakugaki.json?${TagG.getCacheQuery(30)}`, false);
    TagG.asset2rakugakiTitle = asset2title;
};
TagG.aLoadAnimeList = async() => {
    if (!TagG.ANIMELIST_JSON) return;
    const { animeAssets } = await Util.aGetJSON(`${TagG.ANIMELIST_JSON}?${TagG.getCacheQuery(15)}`);
    TagG.asset2isAnime = {};
    for (const asset of animeAssets) TagG.asset2isAnime[asset] = true;
};
TagG.aLoadCachedAssetData = async() => {
    const promises = [];
    // アセット情報は上書きされるので完全更新の頻度が高いやつを後ろに
    if (TagG.DBOARD_CACHEJSON) promises.push(Util.aGetJSON(`${TagG.DBOARD_CACHEJSON}?${TagG.getCacheQuery(10)}`).catch(() => ({})));
    if (TagG.DEXASSETS_CACHEJSON) promises.push(Util.aGetJSON(`${TagG.DEXASSETS_CACHEJSON}?${TagG.getCacheQuery(20)}`).catch(() => ({})));
    if (TagG.DISPENSERS_CACHEJSON) promises.push(Util.aGetJSON(`${TagG.DISPENSERS_CACHEJSON}?${TagG.getCacheQuery(2)}`).catch(() => ({})));
    const cacheJsons = await Promise.all(promises);
    for (const { assetsInfo, monacards, noCards } of cacheJsons) {
        if (!assetsInfo) continue;
        TagG.addAssetsInfo(assetsInfo.filter(info => !TagG.asset2info[info.asset]));
        TagG.addCardsData(monacards.filter(card => !TagG.asset2card[card.asset_common_name]));
        for (const name of noCards) TagG.cardLoadedMap[name] = true;
    }
};
TagG.aGetDispensers = async() => {
    let dispensers;
    try {
        dispensers = (await Util.aGetJSON(`${TagG.DISPENSERS_CACHEJSON}?${TagG.getCacheQuery(2)}`)).dispensers;
    }
    catch (e) {
        const getDispensersParams = {
            'limit': 1000,
            'order_by': 'tx_index',
            'order_dir': 'DESC',
            // get_dispensersはstatusパラメータが効かないのでフィルタでstatus指定
            'filters': [{ field: 'status', op: '==', value: DISPENSER_STATUS.OPEN }],
        };
        dispensers = await Monaparty.aParty('get_dispensers', getDispensersParams);
    }
    const assets = dispensers.map(dispenser => dispenser.asset);
    await TagG.aLoadAssets(assets, true, true);
    dispensers = dispensers.map(TagG.formatDispenserData);
    return dispensers;
};
TagG.aCheckMentions = async() => {
    if (!TagG.DBOARD_CACHEJSON) return;
    const DMESSAGE_PREFIX = '@b.m ';
    const ADDR_REG = new RegExp(`@${TagG.ADDR_PATTERN}`, 'g');
    const HOLDERS_REG = /@holders\([A-Z0-9]{4,}(\.[a-zA-Z0-9.\-_@!]+)?\)/g;
    const ASSET_REG = /@[A-Z0-9]{4,}(\.[a-zA-Z0-9.\-_@!]+)?/g;
    let mentionCount = 0;
    const { messages } = await Util.aGetJSON(`${TagG.DBOARD_CACHEJSON}?${TagG.getCacheQuery(10)}`);
    const isDeletedIndexAddr = {};
    const index2address = {};
    for (const rawMessage of messages) index2address[rawMessage.tx_index] = rawMessage.source;
    for (const rawMessage of messages) {
        const index = rawMessage.tx_index;
        if (index <= TagG.dboardReadIndex) continue;
        try {
            const address = rawMessage.source;
            const rawText = rawMessage.text.slice(DMESSAGE_PREFIX.length);
            const parsedText = JSON.parse(rawText);
            if (isDeletedIndexAddr[`${index}.${address}`]) continue;
            let text = null;
            if (typeof parsedText === 'string') text = parsedText;
            else if (typeof parsedText.d === 'number') isDeletedIndexAddr[`${parsedText.d}.${address}`] = true;
            else if (typeof parsedText.m === 'string') text = parsedText.m;
            if (typeof parsedText.r === 'number') {
                if (TagG.addr2isMine[index2address[parsedText.r]]) { mentionCount++; continue }
            }
            if (!text || !text.includes('@')) continue;
            let matches;
            matches = text.match(ADDR_REG);
            if (matches && matches.map(txt => txt.slice(1)).some(addr => TagG.addr2isMine[addr])) { mentionCount++; continue }
            matches = text.match(HOLDERS_REG);
            if (matches && matches.map(txt => txt.split(/[()]/)[1]).some(asset => TagG.asset2isInMyWallet[asset])) { mentionCount++; continue }
            matches = text.match(ASSET_REG);
            if (matches && matches.map(txt => txt.slice(1)).some(asset => TagG.asset2isMyOwn[asset])) { mentionCount++; continue }
        }
        catch (error) {}
    }
    TagG.bodyMenu.setNotifications({ 'D-Board': mentionCount });
};
TagG.aGetTreasureURL = async(asset, qtyS, addrData = null) => {
    if (qtyS === '0') return `${TagG.TOKENVAULT_ROOT}treasures/${asset}_${qtyS}`;
    if (addrData) {
        const assetData = addrData.assets.find(_asset => (_asset.asset === asset));
        if (!assetData) throw new Error(`aGetTreasureURL failed: ${JSON.stringify({ asset, qtyS })}`);
        const params = { asset, quantity: qtyS, isOwner: assetData.isOwn };
        const data = await TagG.aSignedJsonRPCRequest(TagG.TOKENVAULT_API, 'getTreasure', params, addrData);
        return data.treasureDownloadURL;
    }
};
TagG.aOpenTreasure = async(asset, qtyS, addrData = null) => {
    const treasureDownloadURL = await TagG.aGetTreasureURL(asset, qtyS, addrData);
    const url = `${TagG.RELATIVE_URL_BASE}${TagG.TOKENVAULT_VIEWER}?${TagG.QUERYKEY_TREASUREURL}=${encodeURIComponent(treasureDownloadURL)}`;
    const result = window.open(url, '_blank'); // noreferrerを指定するとresultがundefinedになってしまうのでビューア側で window.opener = null とすることで対応
    if (!result) window.location.href = url;
};
{
    const IPFS_REGEXP = /^ipfs:\/\/([1-9A-HJ-NP-Za-km-z]+)$/;
    const SECUREURL_REGEXP = /^https?:\/\/([\w-]+\.)+[\w-]+(\/[\w-.?%&=]*)?$/;
    const asset2skip = {};
    TagG.aLoadAssetsExtendedInfo = async() => {
        const promises = [];
        for (const asset in TagG.asset2info) {
            if (asset2skip[asset]) continue;
            const assetInfo = TagG.asset2info[asset];
            if (asset !== assetInfo.asset) { asset2skip[asset] = true; continue } // assetがlongnameの場合
            const ipfsMatch = assetInfo.description.match(IPFS_REGEXP);
            if (!ipfsMatch) { asset2skip[asset] = true; continue }
            const jsonHash = ipfsMatch[1];
            const existingExtInfo = TagG.asset2extendedInfo[asset];
            if (existingExtInfo && existingExtInfo.hash === jsonHash) continue;
            promises.push(aLoadExtendedInfo(assetInfo, jsonHash))
        }
        await Promise.all(promises);
        const isUpdated = Boolean(promises.length);
        return isUpdated;
    };
    const aLoadExtendedInfo = async(assetInfo, jsonHash) => {
        const TIMEOUT_MS = 3000;
        try {
            const jsonURL = TagG.IPFSURL_FORMAT.replace('{HASH}', jsonHash);
            const extInfo = { hash: jsonHash };
            const extJson = await Util.aTimeout(TIMEOUT_MS, Util.aGetJSON(jsonURL, true));
            if (![assetInfo.asset, assetInfo.name].includes(extJson.asset)) return;
            for (const key of ['description', 'image', 'website', 'pgpsig']) {
                if ((key in extJson) && (typeof extJson[key] === 'string')) extInfo[key] = extJson[key];
            }
            if (extInfo.image) {
                const ipfsMatch = extInfo.image.match(IPFS_REGEXP);
                if (ipfsMatch) extInfo.imageURL = TagG.IPFSURL_FORMAT.replace('{HASH}', ipfsMatch[1]);
            }
            if (extInfo.website) extInfo.isWebsiteSecure = Boolean(extInfo.website.match(SECUREURL_REGEXP));
            TagG.asset2extendedInfo[assetInfo.asset] = extInfo;
            assetInfo.extendedInfo = extInfo;
        }
        catch (error) {
            console.warn(error);
            asset2skip[assetInfo.asset] = true;
        }
    };
}
{
    TagG.aGenerateStrongMnemonic = async(bits) => {
        const entHex = TagG.randomBytes(bits / 8).toString('hex');
        const moreEntHex = await aGetMoreEntropyHex(bits);
        const drbg = new TagG.HmacDRBG({
            hash: TagG.hash.sha256,
            entropy: entHex,
            nonce: moreEntHex,
            minEntropy: bits,
        });
        const strongEntHex = drbg.generate(bits / 8, 'hex');
        const strongMnemonic = TagG.bip39.entropyToMnemonic(strongEntHex);
        return strongMnemonic;
    };
    const aGetMoreEntropyHex = (minBits) => {
        return new Promise((resolve) => {
            const moreEntropyGenerator = new TagG.MoreEntropy.Generator();
            moreEntropyGenerator.generate(minBits, (entropyInts) => {
                const moreEntropyHex = Buffer.from(entropyInts).toString('hex');
                resolve(moreEntropyHex);
            });
        });
    };
}
TagG.getSizedImgurImageURL = (baseURL, size) => { // sizes ref: https://api.imgur.com/models/image
    if (baseURL.indexOf('i.imgur.com') === -1) return baseURL;
    if (baseURL.match(/\/\d+\./)) return baseURL; // ファイル名が数字のときは別サイズがない
    return baseURL.
    replace('.png', size + '.png').
    replace('.jpg', size + '.jpg').
    replace('.gif', size + '.gif');
};
{
    TagG.aMonapartyTXCreate = async(createCommand, createParams, txData) => {
        const { tx_hex, btc_in, btc_change, btc_fee } = await Monaparty.aParty(createCommand, createParams);
        inspectTXHex(tx_hex, btc_change, createCommand, createParams);
        // format: txData
        txData.txHex = tx_hex;
        txData.inputsSat = btc_in;
        txData.oturiSat = btc_change;
        txData.feeSat = btc_fee;
        txData.dustSat = btc_in - btc_change - btc_fee;
        txData.feeAmount = txData.feeSat / TagG.SATOSHI_RATIO;
        txData.dustAmount = txData.dustSat / TagG.SATOSHI_RATIO;
        txData.feedustAmount = txData.feeAmount + txData.dustAmount;
    };
    const inspectTXHex = (tx_hex, btc_change, createCommand, createParams) => {
        const { source, custom_inputs, dividend_asset } = createParams;
        const txObj = TagG.bitcoinjs.Transaction.fromHex(tx_hex);
        if (custom_inputs) {
            for (const input of txObj.ins) {
                const txID = TagG.reverseBuffer(input.hash).toString('hex');
                if (!custom_inputs.some(cinput => cinput.txid === txID)) throw new Error(TagG.jp ? 'トランザクションの内容が想定と異なる可能性があります' : 'tx may be different from what is expected');
            }
        }
        let change = 0, outgoing = 0;
        for (const output of txObj.outs) {
            try {
                const outputAddr = TagG.bitcoinjs.address.fromOutputScript(output.script, TagG.NETWORK);
                if (outputAddr === source) change += output.value;
                else outgoing += output.value;
            }
            catch (error) {
                outgoing += output.value;
            }
        }
        if (change !== btc_change) throw new Error(TagG.jp ? 'トランザクションの内容が想定と異なる可能性があります' : 'tx may be different from what is expected');
        let isBaseTX = (createCommand === 'create_dividend' && dividend_asset === TagG.BASECHAIN_ASSET);
        if (!isBaseTX && outgoing > TagG.FEESAT_PERBYTE * 1e6) throw new Error(TagG.jp ? 'トランザクションの内容が想定と異なる可能性があります' : 'tx may be different from what is expected');
        console.log('inspectTXHex OK');
    };
}
TagG.aBroadcast = async(signedTXHex) => {
    // return 'XXXXdummytxidXXXX'; // DEBUG: ローカルの挙動だけテストするためにブロードキャストをスキップ
    TagG.aUpdateAddrsDataWithUI.lastTime = 0;
    return await Monaparty.aBlock('broadcast_tx', { 'signed_tx_hex': signedTXHex });
};
TagG.aSignInOut = async(inputs, outputs, addrData) => {
    const { script } = addrData;
    const txBuilder = new TagG.bitcoinjs.TransactionBuilder(TagG.NETWORK);
    for (const input of inputs) txBuilder.addInput(input.txid, input.vout, null, script);
    for (const output of outputs) txBuilder.addOutput(output.address, output.value);
    const txObj = txBuilder.buildIncomplete();
    for (let i = 0; i < txObj.ins.length; i++) txObj.ins[i].script = script; // 上でもscript設定してるので冗長に見えるけど必要あり
    const unsignedTXHex = txObj.toHex();
    const signedTXHex = await TagG.aSignTXHex(unsignedTXHex, addrData, inputs.map(input => input.value));
    return signedTXHex;
};
TagG.aBroadcastInOut = async(inputs, outputs, addrData) => {
    const signedTXHex = await TagG.aSignInOut(inputs, outputs, addrData);
    const txID = await TagG.aBroadcast(signedTXHex);
    return txID;
};
{
    TagG.aSignTXHex = async(unsignedTXHex, addrData, inputValues = null) => {
        const txObj = TagG.bitcoinjs.Transaction.fromHex(unsignedTXHex);
        let signedTXHex;
        if (TagG.isReadOnlyMode) await TagG.aUpgradeFromReadOnlyMode();
        if (addrData.ecPair) signedTXHex = await aSignTXHexWithECPair(txObj, addrData, inputValues);
        else if (TagG.IS_MPURSEEX) signedTXHex = await window.mpurse.signRawTransaction(unsignedTXHex);
        else if (TagG.isTrezor) signedTXHex = await aSignTXHexWithTrezor(txObj, addrData, inputValues);
        const signedTXObj = TagG.bitcoinjs.Transaction.fromHex(signedTXHex);
        checkTxManipulation(txObj, signedTXObj);
        return signedTXHex;
    };
    const aSignTXHexWithECPair = async(txObj, addrData, inputValues = null) => {
        const { ecPair, addrType } = addrData;
        const txBuilder = getTxBuilder(txObj);
        console.log({ ins: txObj.ins, outs: txObj.outs });
        if (addrType === ADDR_TYPE.P2WPKH) {
            let i = 0;
            for (const txIn of txObj.ins) {
                if (inputValues) txBuilder.sign(i, ecPair, null, null, inputValues[i]);
                else txBuilder.sign(i, ecPair, null, null, await aGetInputValue(txIn));
                i++;
            }
        }
        else txObj.ins.forEach((txIn, i) => txBuilder.sign(i, ecPair));
        return txBuilder.build().toHex();
    };
    const aSignTXHexWithTrezor = async(txObj, addrData, inputValues = null) => {
        const addressPathArray = parsePath(addrData.path);
        const inputs = [], outputs = [];
        let i = 0;
        for (const txIn of txObj.ins) {
            let scriptType = 'SPENDADDRESS';
            if (addrData.addrType === ADDR_TYPE.P2WPKH) scriptType = 'SPENDWITNESS';
            const value = inputValues ? inputValues[i] : await aGetInputValue(txIn);
            inputs.push({
                address_n: addressPathArray,
                prev_index: txIn.index,
                prev_hash: TagG.reverseBuffer(txIn.hash).toString('hex'),
                amount: `${value}`,
                script_type: scriptType,
            });
            i++;
        }
        for (const txOut of txObj.outs) {
            try {
                const toAddr = TagG.bitcoinjs.address.fromOutputScript(txOut.script, TagG.NETWORK);
                const isOturi = (addrData.address === toAddr);
                const output = { script_type: 'PAYTOADDRESS', amount: `${txOut.value}` };
                if (isOturi && addrData.addrType === ADDR_TYPE.P2WPKH) output.script_type = 'PAYTOWITNESS';
                if (isOturi) output.address_n = addressPathArray;
                else output.address = toAddr;
                outputs.push(output);
            }
            catch (e) {
                const asm = TagG.bitcoinjs.script.toASM(txOut.script);
                if (asm.indexOf('OP_RETURN ') !== 0) throw TagG.jp?
                'このトランザクションはTrezorで署名ができません。Trezorではデータ量の多いMonapartyトランザクション(Pay to Multisigという古い形式のoutputを含むトランザクション)への署名はサポートしていないので、データ量を減らすかTrezor以外のアドレスを使ってください。もし署名する方法をご存知でしたら情報をお寄せください':
                'Trezor does not support signing of data-heavy Monaparty tx (tx containing old-style output called Pay to Multisig), so you should either reduce the amount of data or use a non-Trezor address.';
                const data = asm.slice(10);
                outputs.push({ script_type: 'PAYTOOPRETURN', op_return_data: data, amount: `${txOut.value}` });
            }
        }
        const trezorResult = await TrezorConnect.signTransaction({ inputs, outputs, coin: TagG.TREZOR_COIN });
        if (!trezorResult.success) throw new Error(TagG.jp ? `エラーが発生しました: ${trezorResult.payload.error}` : `Failed. hint: ${trezorResult.payload.error}`);
        return trezorResult.payload.serializedTx;
    };
    const aGetInputValue = async(txIn) => {
        const utxoTxID = TagG.reverseBuffer(txIn.hash).toString('hex');
        const utxoHex = await Monaparty.aParty('getrawtransaction', { 'tx_hash': utxoTxID });
        const utxoTxObj = TagG.bitcoinjs.Transaction.fromHex(utxoHex);
        const utxoValue = utxoTxObj.outs[txIn.index].value;
        return utxoValue;
    };
    const parsePath = (path) => {
        const pathArray = [];
        const split = path.split('/');
        split.shift();
        for (const str of split) {
            const [numStr, emptyStr] = str.split("'");
            let num = Number(numStr);
            if (emptyStr === '') num = ((num | 0x80000000) >>> 0);
            pathArray.push(num);
        }
        return pathArray;
    };
    const getTxBuilder = (txObj) => {
        const txBuilder = new TagG.bitcoinjs.TransactionBuilder(TagG.NETWORK);
        txBuilder.setVersion(txObj.version);
        txBuilder.setLockTime(txObj.locktime);
        for (const txOut of txObj.outs) txBuilder.addOutput(txOut.script, txOut.value);
        for (const txIn of txObj.ins) txBuilder.addInput(txIn.hash, txIn.index, txIn.sequence, txIn.script);
        return txBuilder;
    };
    const checkTxManipulation = (beforeTxObj, afterTxObj) => {
        const errMessage = TagG.jp ? '署名時にトランザクションが改ざんされた可能性があります' : 'The tx may have been tampered with during signing.';
        for (let i = 0; i < afterTxObj.ins.length; i++) {
            if (afterTxObj.ins[i].hash.toString('hex') !== beforeTxObj.ins[i].hash.toString('hex')) throw new Error(errMessage);
            if (afterTxObj.ins[i].index !== beforeTxObj.ins[i].index) throw new Error(errMessage);
        }
        for (let i = 0; i < afterTxObj.outs.length; i++) {
            if (afterTxObj.outs[i].script.toString('hex') !== beforeTxObj.outs[i].script.toString('hex')) throw new Error(errMessage);
            if (afterTxObj.outs[i].value !== beforeTxObj.outs[i].value) throw new Error(errMessage);
        }
    };
}
{
    TagG.aSignMessage = async(message, addrData) => {
        let signature;
        if (TagG.isReadOnlyMode) await TagG.aUpgradeFromReadOnlyMode();
        if (addrData.ecPair) signature = await aSignMessageWithECPair(message, addrData);
        else if (TagG.IS_MPURSEEX) signature = await window.mpurse.signMessage(message);
        else if (TagG.isTrezor) signature = await aSignMessageWithTrezor(message, addrData);
        const isValid = TagG.bitcoinjsMessage.verify(message, addrData.address, signature, TagG.NETWORK.messagePrefix);
        if (!isValid) throw new Error(TagG.jp ? '署名の検証に失敗しました' : 'Signature verification failed.');
        return signature;
    };
    const aSignMessageWithECPair = async(message, addrData) => {
        const { ecPair, addrType } = addrData;
        const sign = (addrType === ADDR_TYPE.P2WPKH)?
              TagG.bitcoinjsMessage.sign(message, ecPair.privateKey, ecPair.compressed, TagG.NETWORK.messagePrefix, { extraEntropy: TagG.randomBytes(32), segwitType: 'p2wpkh' }):
              TagG.bitcoinjsMessage.sign(message, ecPair.privateKey, ecPair.compressed, TagG.NETWORK.messagePrefix, { extraEntropy: TagG.randomBytes(32) });
        return sign.toString('base64');
    };
    const aSignMessageWithTrezor = async(message, addrData) => {
        const trezorResult = await TrezorConnect.signMessage({ path: addrData.path, message: message, coin: TagG.TREZOR_COIN });
        if (!trezorResult.success) throw new Error(TagG.jp ? `エラーが発生しました: ${trezorResult.payload.error}` : `Failed. hint: ${trezorResult.payload.error}`);
        return trezorResult.payload.signature;
    };
}
TagG.getAssetImageURL = (size, asset) => { // size = 'L'/'M'/'S'/'auto', assetはnameでも可
    const assetInfo = TagG.asset2info[asset];
    if (!assetInfo) return null;
    if (assetInfo.appData.monacharat) {
        switch (size) {
            case 'L': case 'M': case 'auto': return assetInfo.appData.monacharat.image;
            case 'S': return assetInfo.appData.thumb;
        }
    }
    if (assetInfo.appData.rakugakinft) return assetInfo.appData.rakugakinft.image;
    const cardData = TagG.asset2card[assetInfo.name];
    if (cardData) {
        switch (size) {
            case 'L': return cardData.imgurURL;
            case 'M': return cardData.imgurSizedURL;
            case 'S': return cardData.imgurSmallURL;
            case 'auto':
                if (TagG.asset2isAnime[cardData.asset]) return cardData.imgurURL;
                else return cardData.imgurSizedURL;
        }
    }
    return null;
};
TagG.getRakugakiCacheID = (asset, isAnonymous = false) => {
    // anonymousのときは非anonymousのキャッシュが使われるとエラーになるので分ける
    return (TagG.rakugakiAsset2cacheID[asset] || TAGG_TIME) + (isAnonymous ? '_' : '');
};
TagG.updatefirstAddrLabel = () => {
    const newLabel = TagG.addrsData[0].label || TagG.addrsData[0].address.slice(-4);
    if (newLabel === TagG.firstAddrLabel) return;
    TagG.firstAddrLabel = newLabel;
    TagG.body.update(); // body-bottomfloat を更新
    document.title = `${TagG.firstAddrLabel} - ${TagG.originalDocumentTitle}`;
}
TagG.validateAddress = (address) => {
    try {
        TagG.bitcoinjs.address.toOutputScript(address, TagG.NETWORK);
        return true;
    } catch (error) {
        console.warn(error);
        return false;
    }
};
TagG.validateWIF = (wif) => {
    try {
        TagG.bitcoinjs.ECPair.fromWIF(wif, TagG.NETWORK);
        return true;
    } catch (error) {
        console.warn(error);
        return false;
    }
};
TagG.getDefaultAddrLabel = (index = 0) => TagG.IS_MPURSEEX ? 'Mpurse Address' : `Address${Util.zeroPadding(index + 1, 2)}`;
TagG.addrType2purpose = (addrType) => {
    if (addrType === ADDR_TYPE.P2WPKH) return 84;
    return 44;
};
TagG.addrType2defaultAccountPath = (addrType, account = 0) => `m/${TagG.addrType2purpose(addrType)}'/${TagG.NETWORK.versions.bip44}'/${account}'`;
TagG.reverseBuffer = (buffer) => {
    const reversedBuffer = Buffer.allocUnsafe(buffer.length);
    const maxIndex = buffer.length - 1;
    for (let i = 0; i <= maxIndex; i++) reversedBuffer[i] = buffer[maxIndex - i];
    return reversedBuffer;
};
TagG.qtyS2amountS = (qtyS, isDivisible) => {
    if (!isDivisible) return qtyS;
    qtyS = '0'.repeat(TagG.SATOSHI_DECIMAL) + qtyS;
    let [intPart, fracPart] = [qtyS.slice(0, qtyS.length - TagG.SATOSHI_DECIMAL), qtyS.slice(-TagG.SATOSHI_DECIMAL)];
    intPart = intPart.replace(/^0+/, '') || '0';
    fracPart = fracPart.replace(/0+$/, '');
    if (fracPart) return `${intPart}.${fracPart}`;
    else return intPart;
};
TagG.qty2amount = (qty, isDivisible) => (isDivisible ? qty / TagG.SATOSHI_RATIO : qty);
TagG.amount2qty = (amount, isDivisible) => Math.round(isDivisible ? amount * TagG.SATOSHI_RATIO : amount);
TagG.getCacheQuery = (minutes) => Math.floor(new Date().getTime() / (60e3 * minutes));

{
    if (!TagG.IS_PREVIEW) route.base('/');
    if (TagG.IS_MPURSEEX && window.mpurse) {
        window.mpurse.updateEmitter.removeAllListeners().on('addressChanged', () => location.reload());
    }
    TrezorConnect.manifest({ email: 'komikikaku+trezor@gmail.com', appUrl: 'https://monapalette.komikikaku.com/' });
    window.myswal = async(...args) => { // オリジナルパラメータ: isCutin
        if (!myswal.paramsStack) myswal.paramsStack = [];
        let params;
        if (args.length === 1) {
            if (typeof args[0] === 'string') params = { text: args[0], className: 'swalcustom-singlebutton' };
            else {
                params = args[0];
                if (!('className' in params)) {
                    if ('content' in params) params.className = 'swalcustom-content';
                    else if ('icon' in params) params.className = 'swalcustom-icon';
                    else if ('buttons' in params && params.buttons.length === 2) params.className = 'swalcustom-okorcancel';
                    else if (!('buttons' in params)) params.className = 'swalcustom-singlebutton';
                    else if (!('title' in params) && ('buttons' in params) && Object.keys(params.buttons).length === 0) params.className = 'swalcustom-justtext';
                }
            }
        }
        else if (args.length === 2) params = { title: args[0], text: args[1], className: 'swalcustom-singlebutton' };
        else if (args.length === 3) params = { title: args[0], text: args[1], icon: args[2], className: 'swalcustom-icon' };
        else params = args; // このときだけarray
        myswal.paramsStack.push(params);
        let swalReturn;
        if (Array.isArray(params)) swalReturn = await swal(...params);
        else swalReturn = await swal(params);
        // swalの仕様によりswal中にswalされた場合はpromiseがpendingのままになるので、その場合はこの先は実行されない
        myswal.paramsStack.pop();
        const lastParams = myswal.paramsStack.pop();
        if (params.isCutin && lastParams) {
            if (Array.isArray(lastParams)) myswal(...lastParams); // awaitしない
            else myswal(lastParams);
        }
        else myswal.paramsStack = [];
        return swalReturn;
    };
    if (TagG.COIN === COINS.MONA) {
        TagG.asset2card['MONANA'] = {
            cardName: 'MONANA',
            ownerName: 'unknown',
            ownerSN: '',
            siteURL: TagG.CARD_DETAILURL_FORMAT.replace('{ASSETNAME}', 'MONANA'),
            imgurURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA.png`,
            imgurSizedURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA_preview.png`,
            imgurSmallURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA_small.png`,
            description: 'モナカードではありませんが、DEXでよく利用されるトークンなのでモナパレット専用の画像を表示しています',
            version: '1',
            tags: '',
        };
    }
    if (TagG.COIN === COINS.MONA_TESTNET) { // testnetにMonacardがないので表示確認用 // トークンはどれでもいい
        TagG.asset2card['SEGWITOKEN'] = {
            cardName: 'SEGWITOKEN',
            ownerName: 'monapalette',
            ownerSN: '',
            siteURL: TagG.CARD_DETAILURL_FORMAT.replace('{ASSETNAME}', 'SEGWITOKEN'),
            imgurURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA.png`,
            imgurSizedURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA_preview.png`,
            imgurSmallURL: `${TagG.RELATIVE_URL_BASE}img/card/MONANA_small.png`,
            description: 'testnetにMonacardがないので表示確認用',
            version: '1',
            tags: '',
        };
    }
    const storedLang = Util.loadStorage(TagG.STORAGEKEY_LANG);
    if (storedLang) TagG.jp = (storedLang === 'JA');
    else TagG.jp = (['ja', 'ja-JP'].includes(navigator.language));
    const confNum = Util.loadStorage(TagG.STORAGEKEY_CONFNUM);
    if (Number.isInteger(confNum)) TagG.confNum = confNum;
    TagG.addrbook = Util.loadStorage(TagG.STORAGEKEY_ADDRBOOK) || [];
    TagG.aLoadTreasures().catch(console.warn);
    TagG.aLoadRakugakiTitles().catch(console.warn);
    TagG.aLoadAnimeList().catch(console.warn);
    TagG.aLoadCachedAssetData().catch(console.warn);
    window.addEventListener('message', (event) => {
        for (const listener of TagG.windowMessageListeners) listener(event);
    });
    window.addEventListener('load', () => {
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.register(`${TagG.RELATIVE_URL_BASE}serviceworker.js`).
            then(() => console.log('Service worker registered')).
            catch((error) => {
                console.warn('Error registering service worker:');
                console.warn(error);
            });
        }
    });
    if (!TagG.IS_LAZYLOADAVAILABLE) myswal({
        title: (TagG.jp ? 'ご注意！' : 'CAUTION!'),
        text: (TagG.jp ? 'このブラウザではモナパレットの一部のリスト表示で件数が制限されるため、他のブラウザを使ったほうがよいです。モダンなブラウザであればだいたい大丈夫ですが、推奨はChromeです。' : 'As this browser can\'t display all items of some list in Monapalette, it is better to use another browser. A modern browser is usually fine, but Chrome is recommended.'),
        icon: 'warning',
        isCutin: true,
    });
}
riot.tag2('about-page', '<div class="app-page"> <div class="page-header"> <i class="material-icons page-icon">info</i> <h2 class="page-title">About</h2> </div> <div class="page-content"> <div if="{!TagG.IS_MPURSEEX}" class="section-frame"> <div class="section-title">{TagG.jp ? \'これはなに\' : \'What is this?\'}</div> <div class="paragraph"> <span if="{TagG.jp}">お手軽に使えるモナコイン＋Monapartyの超高機能総合エンタテイメントウォレットです。</span> <span if="{!TagG.jp}">Monapalette is an easy-to-use ultra-high-functionality entertainment wallet of Monacoin + Monaparty.</span> </div> <div if="{TagG.jp}" class="paragraph"> <span>詳しいことは<a href="https://opaque-toast-7ea.notion.site/fe5a2c309f3a495fa7a3399549cfe50c" target="_blank">モナパレット大全</a>を見てください。</span> </div> </div> <div if="{TagG.IS_MPURSEEX}" class="section-frame"> <div class="section-title">{TagG.jp ? \'これはなに\' : \'What is this?\'}</div> <div class="paragraph"> <span if="{TagG.jp}">Mpurseはとても便利ですが機能はシンプルなので、モナパレットの機能をMpurseのアドレスで使えるようにしてみました。これでMpurseのアドレスからでもトークンを送る以外のMonapartyの操作ができるし、モナカードとの連携もお手軽です。やったね。</span> <span if="{!TagG.jp}">MpurseExte enables you to use Monapalette\'s functions with your Mpurse address.</span> </div> <div class="paragraph"> <span if="{TagG.jp}">本家: <a href="https://monapalette.komikikaku.com/" target="_blank">モナパレット</a></span> <span if="{!TagG.jp}">Powered by <a href="https://monapalette.komikikaku.com/" target="_blank">Monapalette</a></span> </div> </div> <div class="section-frame"> <div class="section-title">{TagG.jp ? \'利用上の注意\' : \'Notes on Use\'}</div> <div class="paragraph"> <span if="{TagG.jp}">モナパレットではウォレットのニーモニックをあなた自身が管理するので、運営は秘密鍵にはノータッチです。つまりニーモニックや秘密鍵を失くしたり盗まれたりしてモナコイン／Monapartyトークンを失ったときはあなた自身の責任です。とくにニーモニックを入力する都合上、フィッシングサイトなどにもご注意ください。利用者側の管理／知識の不足によるいかなるトラブルもモナパレットは関知しません。</span> <span if="{!TagG.jp}">Don\'t Trust. Verify.</span> </div> <div class="paragraph"> <span if="{TagG.jp}">モナパレットの処理はすべてブラウザ上で行われ、実際のコードは比較的読みやすい形で誰でも確認可能です。いちおう<a href="https://bitbucket.org/anipopina/monapalette/" target="_blank">リポジトリ</a>も公開しているので Don\'t Trust. Verify ということでよろしくお願いします。既知の問題は<a href="https://anipopina.hateblo.jp/entry/2021/02/19/221226" target="_blank">こちら</a>にまとめてあります。</span> <span if="{!TagG.jp}">The code repository is available <a href="https://bitbucket.org/anipopina/monapalette/" target="_blank">here</a>. Known issues are summarized <a href="https://anipopina.hateblo.jp/entry/2021/02/19/221226" target="_blank">here</a>.</span> </div> <div class="paragraph"> <span if="{TagG.jp}">推奨ブラウザはChromeです。それ以外のブラウザからの利用はあんまりサポートされません。</span> <span if="{!TagG.jp}">The recommended browser is Chrome. Other browsers may not be well supported.</span> </div> </div> <div class="section-frame"> <div class="section-title">{TagG.jp ? \'バックエンド\' : \'Backends\'}</div> <div class="paragraph"> <span if="{TagG.jp}">モナパレットはこれらのサービスを使って動いています。いつもありがとうございます。</span> <span if="{!TagG.jp}">Monapalette is powered by the following services. Thanks!</span> </div> <div class="service">Monaparty: <span each="{monapartyServers}" class="api-server">{endpoint}</span></div> <div if="{TagG.BLOCKBOOK_API_ENDPOINTS}" class="service">Monacoin Blockbook: <span each="{endpoint in TagG.BLOCKBOOK_API_ENDPOINTS}" class="api-server">{endpoint}</span></div> <div if="{TagG.INSIGHT_API_ENDPOINT}" class="service">Monacoin Insight: {TagG.INSIGHT_API_ENDPOINT}</div> <div if="{TagG.CARDSERVER_API}" class="service">Monacard: <a class="link" target="_blank" href="https://card.mona.jp/">card.mona.jp</a></div> <div class="service">Imgur (as D-Board image host): <a class="link" target="_blank" href="https://imgur.com/">imgur.com</a></div> </div> <div class="section-frame"> <div class="section-title">{TagG.jp ? \'ほかには\' : \'Other Apps\'}</div> <div class="service">{TagG.jp ? \'公式Twitter\' : \'Official Twitter\'}: <a class="link" target="_blank" href="https://twitter.com/monapalette">@monapalette<img class="inline-iconimg" width="40" height="40" riot-src="{TagG.RELATIVE_URL_BASE + \'img/logoicon_80.png\'}" loading="lazy"></a></div> <div class="service">{TagG.jp ? \'モナパちゃん\' : \'Monapachan\'}: <a class="link" target="_blank" href="https://twitter.com/monapachan">@monapachan<img class="inline-iconimg" width="40" height="40" riot-src="{TagG.RELATIVE_URL_BASE + \'img/monapachan-face-icon.png\'}" loading="lazy"></a></div> <div class="service">{TagG.jp ? \'オダイロイド１号\' : \'Odairoid 001\'}: <a class="link" target="_blank" href="https://twitter.com/odairoid_001">@odairoid_001<img class="inline-iconimg" width="40" height="40" riot-src="{TagG.RELATIVE_URL_BASE + \'img/odairoid-face-icon.png\'}" loading="lazy"></a></div> <div class="service">{TagG.jp ? \'そのた\' : \'And more\'}: <a class="link" target="_blank" href="https://komikikaku.com/">{TagG.jp ? \'こみ企画\' : \'Komi Kikaku\'}</a></div> </div> <div class="section-frame"> <div class="section-title">{TagG.jp ? \'開発の支援\' : \'Fuels for Development\'}</div> <div class="paragraph"> <span if="{TagG.jp}">モナパレットは個人の趣味により無償で開発運営されています。モナパレットを気に入ったらモナコインを投げて開発や運営を支えてくれると喜びます。</span> <span if="{!TagG.jp}">Monapalette is developed free of charge as a personal hobby. If you like Monapalette, it would be a pleasure to send MONA to support the development and operation.</span> </div> <div class="paragraph"><a class="link" target="_blank" href="https://monalist.komikikaku.com/list/ERgn9oScLi939FCyTSGYNzfykCU7Q3pfVM">{TagG.jp ? \'ほしいMONAリスト運営のほしいMONAリスト\' : \'Hoshi-MONA List\'}</a></div> <div class="paragraph"><a class="link" target="_blank" href="https://monalist.komikikaku.com/list/C7rFTwN9LzSgFrRB3XJioK4xw3NVdB6wTM">{TagG.jp ? \'ほしいMONAリスト運営のほしいMONAリスト ２枚目\' : \'Hoshi-MONA List 2\'}</a></div> <div class="paragraph">{TagG.jp ? \'こちらのアドレスにはMonapartyトークンも寄付できます\' : \'You can also donate Monaparty tokens to this address\'}: M8pCS86ufakMYWetAJWDv8FjF9mD9eZ3Hn</div> <div class="paragraph">{TagG.jp ? \'開発運営その他ぜんぶ\' : \'Developer\'}: <a class="link" target="_blank" href="https://twitter.com/anipopina">@anipopina<img class="inline-iconimg" width="30" height="30" riot-src="{TagG.RELATIVE_URL_BASE + \'img/sunagimo-icon.png\'}" loading="lazy"></a></div> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    this.monapartyServers = [];
    this.on('mount', async() => {
        if (TagG.MONAPARTY_ENDPOINTS) this.monapartyServers = TagG.MONAPARTY_ENDPOINTS.map(endpoint => ({ endpoint }));
        else {
            const { servers } = await Util.aGetJSON(Monaparty.defaults.serversFile, true);
            this.monapartyServers = servers;
        }
        this.update();
    });
});
riot.tag2('addr-from-mpurse-page', '<div class="app-page"> <div class="page-header"> <i class="material-icons page-icon">account_balance_wallet</i> <h2 class="page-title">Wallet</h2> <div class="page-helpbutton-frame"> <material-iconbutton icon="help_outline" on_click="{onClickPageHelpButton}"></material-iconbutton> </div> </div> <div class="page-content"> <div if="{!addrsLoaded}"> <div class="no-mpurse">{waitingMessage}</div> <div class="monapalette-link"><a href="https://monapalette.komikikaku.com/">{TagG.jp ? \'モナパレットに移動する\' : \'Go to Monapalette\'}</a></div> </div> <div if="{addrsLoaded}"> <div class="addrs-frame"> <address-card data="{TagG.addrsData[0]}"></address-card> </div> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    this.onClickPageHelpButton = () => {
        myswal({
            text: (TagG.jp ? 'Mpurseのアドレスをモナパレットのインターフェイスで操作することができます。モナカードとの連携をはじめとするモナパレットの機能をMpurseのアドレスで使いたい場合にご利用ください' : 'MpurseExte enables you to use Monapalette\'s functions with your Mpurse address.'),
            buttons: {},
        });
    };
    this.on('mount', async() => {
        if (!TagG.addrsData) {
            this.waitingMessage = TagG.jp ? 'Mpurseを読み込み中...' : 'Loading Mpurse ...';
            this.update();
        }
        if (window.mpurse) aOpenMpurseAddress();
        else this.tryMpurseIntervalID = setInterval(tryMpurse, 1000);
        await Util.aSleep(3500);
        if (!window.mpurse) {
            this.waitingMessage = TagG.jp ? 'Mpurseが有効になっていません' : 'Mpurse is not enabled.';
            this.update();
        }
    });
    this.on('before-unmount', () => {
        clearInterval(this.tryMpurseIntervalID);
    });
    const tryMpurse = () => {
        if (window.mpurse) {
            clearInterval(this.tryMpurseIntervalID);
            aOpenMpurseAddress();
        }
    };
    const aOpenMpurseAddress = async() => {
        await TagG.aUpdateAddrsDataWithUI(TagG.bodySpinner);
        if (TagG.addrsData) {
            TagG.addrsData[0].label = TagG.addrsData[0].label || TagG.getDefaultAddrLabel();
            this.addrsLoaded = true;
            this.update();
        }
        TagG.aLoadAssetsExtendedInfo().then(isUpdated => { if (isUpdated) this.update() });
    };
});
riot.tag2('addrbook-page', '<div class="app-page"> <div class="page-header"> <i class="material-icons page-icon">book</i> <h2 class="page-title">{TagG.jp ? \'アドレス帳\' : \'Address Book\'}</h2> <div class="page-helpbutton-frame"><material-iconbutton icon="help_outline" on_click="{onClickPageHelpButton}"></material-iconbutton></div> </div> <div class="page-content"> <div class="inputs"> <div class="centering centering-inputs"> <div class="label-input"><material-textfield ref="label_input" hint="Label"></material-textfield></div> <div class="addr-input"><material-textfield ref="addr_input" hint="Address" pattern="{TagG.ADDR_PATTERN}" invalid_message="Invalid Format"></material-textfield></div> <div class="memo-input"><material-textfield ref="memo_input" hint="{TagG.jp ? \'Text Memo (34バイトまで)\' : \'Text Memo (up to 34 bytes)\'}" helper="{TagG.jp ? \'設定しておくとアドレス選択時にメモも併せて入力されます\' : \'Memo will be entered together with the address selection.\'}" is_helper_persistent="true"></material-textfield></div> </div> <div class="centering"> <material-button label="ADD" icon="add" is_unelevated="true" on_click="{onClickAddButton}"></material-button> </div> </div> <div class="centering"> <div each="{addrbook}" class="addrbook-item"> <div class="item-label">{label}</div> <div class="item-addr"> <div>{address}</div> <div if="{memo}" class="item-memo"><i class="material-icons">text_snippet</i>{memo}</div> </div> <div class="delete-button"><material-iconbutton icon="delete" on_click="{onClickDeleteButton}"></material-iconbutton></div> </div> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    this.addrbook = [];
    this.onClickPageHelpButton = () => {
        myswal({
            text: (TagG.jp ? 'よく使うアドレスを登録しておくと送信時にワンタッチで入力できるようになります。トークンを送るときのメモも併せて保存できるので、モナパちゃんのアドレスなどを保存しておくと便利です' : 'Saving frequently used addresses to the address book, you will be able to enter them with a single touch when sending. You can save memos as well, so it\'s useful to save Monapachan\'s address.'),
            buttons: {},
        });
    };
    this.onClickAddButton = () => {
        const label = this.refs.label_input.getValue();
        const address = this.refs.addr_input.getValue();
        const memo = this.refs.memo_input.getValue();
        if (!label || !address) return;
        const existIndex = TagG.addrbook.findIndex(item => (item.label === label));
        if (existIndex !== -1) TagG.addrbook.splice(existIndex, 1);
        TagG.addrbook.push({ label, address, memo: memo || undefined });
        Util.saveStorage(TagG.STORAGEKEY_ADDRBOOK, TagG.addrbook);
        updateItems();
    };
    this.on('mount', () => {
        updateItems();
    });
    const updateItems = () => {
        const addrbook = [];
        for (const item of TagG.addrbook) {
            const onClickDeleteButton = () => {
                const label = item.label;
                const index = TagG.addrbook.findIndex(item => (item.label === label));
                if (index !== -1) TagG.addrbook.splice(index, 1);
                Util.saveStorage(TagG.STORAGEKEY_ADDRBOOK, TagG.addrbook);
                updateItems();
            };
            addrbook.push({ ...item, onClickDeleteButton });
        }
        this.addrbook = addrbook;
        this.update();
    };
});
riot.tag2('address-card', '<div class="address-card-frame {themeClass || \'\'}"> <div class="stylebg-addresscard"> <div class="stylebg-addresscard-inner"> <div class="styleimage styleimage-topleft"></div> <div class="styleimage styleimage-topright"></div> <div class="styleimage styleimage-bottomleft"></div> <div class="styleimage styleimage-bottomright"></div> </div> </div> <div class="header-frame"> <div class="label">{label}</div> <div class="address">{address}</div> <div class="hashcolors"><util-hashcolors seed_value="{address}"></util-hashcolors></div> <div class="toprightbutton-frame trb-leftleftleft"><material-iconbutton ref="update_button" icon="refresh" on_click="{onClickUpdateButton}"></material-iconbutton></div> <div class="toprightbutton-frame trb-leftleft"><material-iconbutton icon="{searchedQuery ? \'saved_search\' : \'search\'}" on_click="{onClickSearchButton}" reload_opts="true"></material-iconbutton></div> <div class="toprightbutton-frame trb-left"><material-iconbutton icon="collections" on_click="{onClickShowCardsButton}"></material-iconbutton></div> <div class="toprightbutton-frame"><material-iconbutton icon="more_horiz" on_click="{onClickAddrMoreButton}"></material-iconbutton></div> </div> <div if="{dispensers.length}" class="dispensers-frame"> <div class="dispenser-alert"> <span if="{TagG.jp}">ご注意！Dispenserを設定したアドレスはたとえ相手が取引所やTiproidでも{baseAsset}の入金に対して問答無用でトークンを返送します</span> <span if="{!TagG.jp}">CAUTION! An address with dispensers will send back tokens for {baseAsset} deposits without question, even if the recipient is an exchange or Tiproid.</span> </div> <div if="{dispensers.length > 1}" class="dispenser-alert"> <span if="{TagG.jp}">さらにご注意！複数のDispenserを設定したアドレスは{baseAsset}の入金1回に対して閾値に達したすべてのトークンを返送する併売りの状態になります</span> <span if="{!TagG.jp}">MORE CAUTION! An address with multiple dispensers will send back multiple tokens for single MONA deposit.</span> </div> <div each="{dispensers}" class="dispenser"><material-button label="{dispenserLabel}" icon="point_of_sale" is_fullwidth="true" on_click="{onClick}"></material-button></div> </div> <div if="{multisendData.list.length}" class="multisend-frame"> <div class="stylebg-multisend"></div> <div each="{multisendData.list}" class="multisend-item"> <div class="multisend-iteminfo"> {commafy(amount)} {assetData.easyLabel} <span class="multisend-addr">&rarr; {addrHead}<span class="multisend-colorbar">{addrMid}<util-hashcolors seed_value="{toAddr}"></util-hashcolors></span>{addrTail}</span> </div> <div class="multisend-deletebutton"><material-iconbutton icon="delete" on_click="{onClickRemoveButton}"></material-iconbutton></div> </div> <div if="{multisendData.memoMode !== MEMO_MODES.NONE}" class="multisend-memo">memo: {multisendData.memo}</div> <div class="multisend-sendbutton"><material-button label="{TagG.jp ? \'まとめて送る\' : \'SEND ALL\'}" icon="send" is_fullwidth="true" on_click="{onClickMultisendButton}"></material-button></div> </div> <div class="builtinassets-frame"> <div class="asset-frame asset-frame-baseasset"> <div class="asset-info-frame"> <div class="stylebg-asset stylebg-asset-baseasset"></div> <div class="asset-name">{baseAsset}</div> <div class="asset-qty">{commafy(baseAmount)} <span if="{baseUnconfSat}">(+{commafy(baseUnconfAmount)})</span></div> <div class="toprightbutton-frame"><material-iconbutton icon="send" is_white="true" on_click="{onClickSendBaseAssetButton}"></material-iconbutton></div> </div> </div><div class="asset-frame asset-frame-builtinasset"> <div class="asset-info-frame"> <div class="stylebg-asset stylebg-asset-builtinasset"></div> <div class="asset-name">{builtinAsset}</div> <div class="asset-qty"> {commafy(builtinAmount)} <span if="{builtinUnconfSat}">(+{commafy(builtinUnconfAmount)})</span> <span if="{builtinEscrAmount}">/<i class="material-icons">swap_horiz</i>{commafy(builtinEscrAmount)}</span> </div> <div class="toprightbutton-frame"><material-iconbutton icon="send" is_white="true" on_click="{onClickSendBuiltinAssetButton}"></material-iconbutton></div> <div class="issueassetbutton-frame"> <div class="stylebg-issuebutton"></div> <material-iconbutton icon="add" is_white="true" on_click="{onClickIssueAssetButton}"></material-iconbutton> </div> </div> </div> </div> <div if="{assetsToShow.length}" class="assets-frame"> <div each="{assetsToShow}" class="asset-frame {asset-frame-own: isOwn, asset-frame-notown: !isOwn, cardmode: isCardMode}"> <div if="{isCardMode}" class="asset-card-frame {asset-card-frame-square: appData.monacharat || appData.rakugakinft}"> <img if="{!imageAuto}" riot-src="{TagG.RELATIVE_URL_BASE + \'img/monapalette_nocard.png\'}" loading="lazy"> <img if="{imageAuto && !appData.rakugakinft}" riot-src="{imageAuto}" loading="lazy"> <img if="{imageAuto && appData.rakugakinft}" riot-src="{imageAuto + \'?\' + TagG.getRakugakiCacheID(asset)}" class="bg-checker" loading="lazy"> </div> <div class="{asset-info-frame: true, cardmode: isCardMode}"> <div class="stylebg-asset {stylebg-asset-own: isOwn, stylebg-asset-notown: !isOwn, stylebg-asset-cardmode: isCardMode, stylebg-asset-cardmode-card: isCardMode && imageAuto}"></div> <div if="{!group}"><div class="asset-name"><img if="{infoRef && infoRef.extendedInfo && infoRef.extendedInfo.imageURL}" riot-src="{infoRef.extendedInfo.imageURL}" class="asset-icon" loading="lazy">{shortenName}</div></div> <div if="{group}"> <div class="asset-name">{group} <span class="nft-tag"></span></div> <div class="asset-id">{name}</div> </div> <div class="asset-qty"> {commafy(amount)} <span if="{unconfSat}">(+{commafy(unconfAmount)})</span> <span if="{escrAmount}">/<i class="material-icons">swap_horiz</i>{commafy(escrAmount)}</span> </div> <div class="asset-issued" if="{isOwn}">{TagG.jp ? \'発行数\' : \'Issued\'} {commafy(issuedAmount)}<i if="{isLocked}" class="material-icons">lock</i></div> <div class="asset-contentsicon" if="{TagG.asset2treasures[asset]}"><i class="material-icons">attachment</i></div> <div class="toprightbutton-frame"><material-iconbutton icon="send" is_white="true" on_click="{onClickSendButton}"></material-iconbutton></div> <div class="toprightbutton-frame trb-lower"><material-iconbutton icon="more_vert" is_white="true" on_click="{onClickMoreButton}"></material-iconbutton></div> <div if="{assetName2onClickPaletteButton[name]}" class="toprightbutton-frame trb-left"><material-iconbutton icon="palette" is_white="true" on_click="{assetName2onClickPaletteButton[name]}"></material-iconbutton></div> </div> </div> </div> </div> <div ref="swalwindows_container" show="{false}"> <div ref="search_window" class="address-card-swalwindow search-swalwindow"> <div class="title">{label} - {TagG.jp ? \'検索\' : \'Search\'}</div> <div class="basic"> <material-textfield ref="search_input" hint="Search" on_input="{searchWindow.onInputSearch}"></material-textfield> <div class="input-clearbutton-frame"><material-iconbutton icon="clear" on_click="{searchWindow.onClickClearSearchButton}"></material-iconbutton></div> </div> <div class="basic"><div each="{searchChips}" class="chip-item"><material-chip label="{label}" on_click="{onClick}"></material-chip></div></div> </div> <div ref="addrdetail_window" class="address-card-swalwindow addrdetail-swalwindow"> <div class="title title-with-toprightbutton">{label}</div> <div class="qrcode-frame"><util-qrcode ref="addr_qrcode" qr_value="{address}"></util-qrcode></div> <div class="basic addr {addr-bech32: addrType === ADDR_TYPE.P2WPKH}">{address}</div> <div class="basic hashcolors"><util-hashcolors seed_value="{address}"></util-hashcolors></div> <div if="{windowSuccessText}" class="basic success-color">{windowSuccessText}</div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="buttons-frame"> <material-button label="{TagG.jp ? \'コピー\' : \'Copy\'}" icon="file_copy" on_click="{addrDetailWindow.onClickCopyButton}"></material-button> <material-button label="{TagG.jp ? \'キーペア\' : \'KeyPair\'}" icon="vpn_key" on_click="{addrDetailWindow.onClickKeyButton}"></material-button> <material-button label="{TagG.jp ? \'さけぶ\' : \'Broadcast\'}" icon="campaign" on_click="{addrDetailWindow.onClickBroadcastButton}"></material-button> <material-button label="{TagG.jp ? \'くばる\' : \'Dividend\'}" icon="zoom_out_map" on_click="{addrDetailWindow.onClickDividendButton}"></material-button> <material-button label="{TagG.jp ? \'自動販売\' : \'Dispenser\'}" icon="point_of_sale" on_click="{addrDetailWindow.onClickDispenserButton}"></material-button> <material-button label="{TagG.jp ? \'スイープ\' : \'Sweep\'}" icon="clear_all" on_click="{addrDetailWindow.onClickSweepButton}"></material-button> </div> <div class="section"> <div class="bottombutton-frame"> <material-button label="{baseAsset + \' NW\'}" icon="search" is_link="true" link_href="{addrDetailWindow.baseExplorerURL}" link_target="_blank"></material-button> <material-button label="MPCHAIN" icon="search" is_link="true" link_href="{addrDetailWindow.monapartyExplorerURL}" link_target="_blank"></material-button> </div> </div> <div class="toprightbutton-frame"><material-iconbutton icon="edit" on_click="{addrDetailWindow.onClickEditButton}"></material-iconbutton></div> </div> <div ref="addrkeypair_window" class="address-card-swalwindow"> <div class="title">{label} - {TagG.jp ? \'キーペア\' : \'KeyPair\'}</div> <div class="section"> <div class="section-title">{TagG.jp ? \'公開鍵\' : \'Public Key\'}</div> <div class="basic data">{addrKeypairWindow.publicKey}</div> </div> <div class="section"> <div class="section-title">{TagG.jp ? \'秘密鍵 (WIF)\' : \'Private Key (WIF)\'}</div> <div if="{!addrKeypairWindow.isOpenPrivateKey}" class="basic warning-color"> <span if="{TagG.jp}">秘密鍵を表示するときは誰にも画面を見られていない／録られていないことをよく確認しましょう</span> <span if="{!TagG.jp}">Make sure that no one is watching/recording your screen before displaying your private key.</span> </div> <div if="{!addrKeypairWindow.isOpenPrivateKey}" class="basic"><material-button label="{TagG.jp ? \'表示する\' : \'SHOW\'}" icon="visibility" on_click="{addrKeypairWindow.onClickOpenPrivateKeyButton}"></material-button></div> <div if="{addrKeypairWindow.isOpenPrivateKey}" class="basic data">{addrKeypairWindow.privateKey}</div> </div> <div class="section"> <div class="section-title">{TagG.jp ? \'メッセージに署名する\' : \'Sign Message\'}</div> <div class="basic"><material-textfield ref="keypair_sign_input" hint="Message to Sign" is_textarea="true" textarea_rows="2"></material-textfield></div> <div class="basic"><material-button label="{TagG.jp ? \'署名する\' : \'SIGN\'}" icon="arrow_downward" on_click="{addrKeypairWindow.onClickSignButton}"></material-button></div> <div if="{addrKeypairWindow.signedMessage}" class="basic data">{addrKeypairWindow.signedMessage}</div> </div> <div class="section"> <div class="section-title">{TagG.jp ? \'TXに署名 & 送信する\' : \'Sign & Broadcast TX\'}</div> <div class="basic"><material-textfield ref="keypair_tx_input" hint="Unsigned TX Hex" is_textarea="true" textarea_rows="2"></material-textfield></div> <div class="basic"><material-button label="{TagG.jp ? \'署名 & 送信する\' : \'SIGN & BROADCAST\'}" icon="send" on_click="{addrKeypairWindow.onClickSendTXButton}"></material-button></div> </div> </div> <div ref="addrbroadcast_window" class="address-card-swalwindow"> <div class="title">{label} - {TagG.jp ? \'さけぶ\' : \'Broadcast\'}</div> <div class="basic"> <span if="{TagG.jp}">Monapartyのブロードキャスト機能を使って{baseAsset}のチェーンにメッセージを記録できます。記録されたメッセージは<a href="https://mpchain.info/broadcasts" target="_blank">Mpchain</a>などから確認できます。全世界に公開される上に後から修正はできないので気を付けましょう</span> <span if="{!TagG.jp}">You can use Monaparty\'s broadcast function to record a message to the {baseAsset} chain. The recorded messages can be viewed from <a href="https://mpchain.info/broadcasts" target="_blank">Mpchain</a> and other sites. Be aware that the message will be shown to the whole world and cannot be modified afterwards.</span> </div> <div class="basic"><material-textfield ref="broadcast_message" hint="Message" is_textarea="true" textarea_rows="2"></material-textfield></div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="bottombutton-frame"><material-button label="{TagG.jp ? \'ブロードキャストする\' : \'BROADCAST\'}" icon="campaign" on_click="{addrBroadcastWindow.onClickBroadcastButton}" is_unelevated="true" is_fullwidth="true"></material-button></div> </div> <div ref="addrdividend_window" class="address-card-swalwindow"> <div class="title">{label} - {TagG.jp ? \'くばる\' : \'Dividend\'}</div> <div class="basic"> <span if="{TagG.jp}">このアドレスがオーナー権限をもっているトークンAの保有者に対して、トークンAの保有量に比例する量のトークンBまたは{baseAsset}を配ることができます</span> <span if="{!TagG.jp}">You can distribute token-B or {baseAsset} to the holders of token-A in proportion to their holdings of token-A. (You need to be the owner of token-A.)</span> </div> <div class="basic"><material-select if="{addrDividendWindow.recreateFlag}" ref="dividend_target" hint="Token-A" items="{addrDividendWindow.targetSelectItems}"></material-select></div> <div class="basic"><material-switch label="{TagG.jp ? baseAsset + \'を配る\' : \'dividend \' + baseAsset}" on_change="{addrDividendWindow.onChangeBaseAssetSwitch}"></material-switch></div> <div if="{!addrDividendWindow.isBaseAsset}" class="basic"><material-select if="{addrDividendWindow.recreateFlag}" ref="dividend_distasset" hint="{TagG.jp ? \'Token-B（配るほう）\' : \'Token-B\'}" items="{addrDividendWindow.distAssetSelectItems}"></material-select></div> <div class="basic"> <div class="upper-helper"> <span if="{TagG.jp}">トークンAの保有量1枚あたりに配る{addrDividendWindow.isBaseAsset ? baseAsset : \'トークンB\'}の量です</span> <span if="{!TagG.jp}">The amount of {addrDividendWindow.isBaseAsset ? baseAsset : \'token-B\'} to be distributed per 1 token-A</span> </div> <div class="inline-input assetamount-input"><material-textfield ref="dividend_amountperunit" hint="Amount per Unit" pattern="{\'[0-9]*(\\\\.[0-9]+)?\'}" invalid_message="Invalid Format"></material-textfield></div> <div class="text-with-inlineinput">{addrDividendWindow.isBaseAsset ? baseAsset : (TagG.jp ? \'枚\' : \'\')}</div> </div> <div if="{!addrDividendWindow.isBaseAsset}" class="basic"> <span if="{TagG.jp}">配布先アドレス1つにつき {TagG.DIVIDENDFEE_PER_RECIPIENT} {builtinAsset} ずつ配布手数料がかかります。次の画面でちゃんと確認してから実行しましょう</span> <span if="{!TagG.jp}">A dividend fee of {TagG.DIVIDENDFEE_PER_RECIPIENT} {builtinAsset} will be charged for each destination address.</span> </div> <div if="{addrDividendWindow.isBaseAsset}" class="basic"> <span if="{TagG.jp}">残高が足りていても残高不足の扱いになる場合は、自アドレスにALLで{baseAsset}を入れ直してUTXOをまとめてみてください</span> <span if="{!TagG.jp}">If you have enough balance but it is treated as insufficient balance, please reinsert {baseAsset} with "ALL" option and put UTXOs together.</span> </div> <div if="{addrDividendWindow.isBaseAsset}" class="basic"> <span if="{TagG.jp}">{baseAsset}の配布はMonapartyトランザクションではなく複数宛先に対する普通の{baseAsset}送金です。配布先アドレスが増えるほどトランザクション手数料も増えるので、次の画面でちゃんと確認してから実行しましょう</span> <span if="{!TagG.jp}">{baseAsset} dividend is not a Monaparty tx, but a normal {baseAsset} tx to multiple destinations. The more addresses you distribute to, the more tx fees you will have to pay, so make sure you check the fees on the next window.</span> </div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="bottombutton-frame"><material-button label="{TagG.jp ? \'くばる\' : \'DIVIDEND\'}" icon="zoom_out_map" on_click="{addrDividendWindow.onClickDividendButton}" is_unelevated="true" is_fullwidth="true"></material-button></div> </div> <div ref="addrdispenser_window" class="address-card-swalwindow"> <div class="title">{label} - {TagG.jp ? \'自動販売\' : \'Dispenser\'}</div> <div class="basic"> <span if="{TagG.jp}">このアドレスに{baseAsset}を送ってきたアドレスに自動でトークンを送り返すことができます</span> <span if="{!TagG.jp}">You can automatically send tokens back to the address that sent {baseAsset} to this address with dispensers</span> </div> <div class="basic warning-color"> <span if="{TagG.jp}">たとえ相手が取引所やTiproidでも{baseAsset}の入金に対して問答無用でトークンを返送してしまうので、自動販売には専用のアドレスを使うことを推奨します。右上の？ボタンもご覧ください</span> <span if="{!TagG.jp}">Even if the other party is an exchange or Tiproid, dispensers will send back tokens for {baseAsset} deposits without question, so it is recommended to use a dedicated address for dispensers. See also the "?" button in the upper right corner.</span> </div> <div class="basic"><material-select if="{addrDispenserWindow.recreateFlag}" ref="dispenser_asset" hint="Token" items="{addrDispenserWindow.assetSelectItems}"></material-select></div> <div class="basic"> <div if="{TagG.jp}" class="upper-helper">1クチとして扱う{baseAsset}の量です</div> <div class="inline-input assetamount-input"><material-textfield ref="dispenser_unitbaseamount" hint="Receive Unit Amount" pattern="{\'[0-9]*(\\\\.[0-9]+)?\'}" invalid_message="Invalid Format"></material-textfield></div> <div class="text-with-inlineinput">{baseAsset}</div> </div> <div class="basic"> <div if="{TagG.jp}" class="upper-helper">1クチの{baseAsset}に対して送り返すトークンの量です</div> <div class="inline-input assetamount-input"><material-textfield ref="dispenser_unitassetamount" hint="Dispense Unit Amount" pattern="{\'[0-9]*(\\\\.[0-9]+)?\'}" invalid_message="Invalid Format"></material-textfield></div> <div class="text-with-inlineinput">{TagG.jp ? \'枚\' : \'\'}</div> </div> <div class="basic"> <div if="{TagG.jp}" class="upper-helper">返送用として確保しておくトークンの量です</div> <div class="inline-input assetamount-input"><material-textfield ref="dispenser_escrowamount" hint="Escrow Amount" pattern="{\'[0-9]*(\\\\.[0-9]+)?\'}" invalid_message="Invalid Format"></material-textfield></div> <div class="text-with-inlineinput">{TagG.jp ? \'枚\' : \'\'}</div> </div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="bottombutton-frame"><material-button label="{TagG.jp ? \'設定する\' : \'SET DISPENSER\'}" icon="point_of_sale" on_click="{addrDispenserWindow.onClickDispenserButton}" is_unelevated="true" is_fullwidth="true"></material-button></div> <div class="toprightbutton-frame"><material-iconbutton icon="help_outline" on_click="{addrDispenserWindow.onClickHelpButton}"></material-iconbutton></div> </div> <div ref="addrsweep_window" class="address-card-swalwindow addrsweep-swalwindow"> <div class="title">{label} - {TagG.jp ? \'スイープ\' : \'Sweep\'}</div> <div class="basic"> <span if="{TagG.jp}">このアドレスに入っているトークン／トークンのオーナー権限を全て他のアドレスに移動できます。スイープ手数料が {TagG.SWEEPFEE_AMOUNT} {builtinAsset} かかります</span> <span if="{!TagG.jp}">All tokens / ownerships in this address can be moved to another address. There will be a {TagG.SWEEPFEE_AMOUNT} {builtinAsset} sweep fee.</span> </div> <div class="basic"><div class="mode-radiobuttons"><material-radiobuttons ref="sweep_mode" items="{addrSweepWindow.modeRadioItems}" is_block="true"></material-radiobuttons></div></div> <div class="basic"><material-textfield ref="sweep_addr" hint="Destination Address" pattern="{TagG.ADDR_PATTERN}" invalid_message="Invalid Format"></material-textfield></div> <div class="basic"><material-radiobuttons items="{addrSweepWindow.memoRadioItems}" on_change="{addrSweepWindow.onChangeMemoRadio}"></material-radiobuttons></div> <div if="{addrSweepWindow.memoMode === MEMO_MODES.TEXT}" class="basic"><material-textfield ref="sweep_memo" hint="{TagG.jp ? \'Text Memo (34バイトまで)\' : \'Text Memo (up to 34 bytes)\'}" pattern="{\'.{0,34}\'}" invalid_message="Too Long"></material-textfield></div> <div if="{addrSweepWindow.memoMode === MEMO_MODES.HEX}" class="basic"><material-textfield ref="sweep_memo" hint="{TagG.jp ? \'Hex Memo (68ケタまで)\' : \'Hex Memo (up to 68 digits)\'}" pattern="{\'[0-9A-Fa-f]{0,68}\'}" invalid_message="Invalid Format"></material-textfield></div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="bottombutton-frame"><material-button label="{TagG.jp ? \'移動する\' : \'SWEEP\'}" icon="point_of_sale" on_click="{addrSweepWindow.onClickSweepButton}" is_unelevated="true" is_fullwidth="true"></material-button></div> </div> <div ref="addrlabel_window" class="address-card-swalwindow"> <div class="title">{TagG.jp ? \'アドレスのラベルを変更する\' : \'Edit Address Label\'}</div> <div class="basic"><material-textfield ref="addr_label" hint="New Label" on_enter="{addrLabelWindow.onClickEditButton}"></material-textfield></div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="bottombutton-frame"><material-button label="{TagG.jp ? \'変更する\' : \'EDIT\'}" icon="edit" on_click="{addrLabelWindow.onClickEditButton}" is_unelevated="true" is_fullwidth="true"></material-button></div> </div> <div ref="assetdetail_window" class="address-card-swalwindow assetdetail-swalwindow"> <div if="{!assetDetail.group}" class="{title: true, title-with-toprightbutton: assetDetail.isOwn}"><img if="{assetDetail.extImageURL}" riot-src="{assetDetail.extImageURL}" class="asset-icon" loading="lazy">{assetDetail.name}</div> <div if="{assetDetail.group}" class="{title: true, title-with-toprightbutton: assetDetail.isOwn}">{assetDetail.group} <span class="nft-tag"></span></div> <div> <div if="{assetDetail.group}" class="basic"><i class="material-icons inline-icon">tag</i> {assetDetail.name}</div> <div if="{!assetDetail.parsedDesc && (assetDetail.hasExt ? assetDetail.extDescription : assetDetail.description)}" class="basic oblique">{assetDetail.hasExt ? assetDetail.extDescription : assetDetail.description}</div> <div if="{assetDetail.extSiteURL}" class="basic"> <span if="{!assetDetail.isExtSiteSecure}"><span class="material-icons inline-icon">link</span> {assetDetail.extSiteURL}</span> <a if="{assetDetail.isExtSiteSecure}" href="{assetDetail.extSiteURL}" target="_blank" rel="noopener noreferrer"><span class="material-icons inline-icon">link</span> {assetDetail.extSiteURL}</a> </div> <div class="basic"><i class="material-icons inline-icon">home</i> {assetDetail.owner}</div> <div class="basic">{TagG.jp ? \'発行数\' : \'Issued\'} {commafy(assetDetail.supplyAmount)} <i if="{assetDetail.isLocked}" class="material-icons inline-icon">lock</i></div> <div class="basic"> <span>{TagG.jp ? \'小数\' : \'Divisible\'} <i class="material-icons inline-icon">{assetDetail.isDivisible ? \'check_box\' : \'check_box_outline_blank\'}</i></span>&emsp; <span>DEX <i class="material-icons inline-icon">{assetDetail.isListed ? \'check_box\' : \'check_box_outline_blank\'}</i></span>&emsp; <span>{TagG.jp ? \'再移転\' : \'Reassignable\'} <i class="material-icons inline-icon">{assetDetail.isReassignable ? \'check_box\' : \'check_box_outline_blank\'}</i></span> </div> <div class="basic"><material-button if="{assetDetailWindow.recreateFlag}" label="MPCHAIN" icon="search" is_link="true" link_href="{assetDetail.explorerURL}" link_target="_blank"></material-button></div> </div> <div if="{assetDetail.appData?.attributes}" class="section"> <div class="basic attrs"> <div each="{assetDetail.appData.attributes}" class="attr"><div class="attr-name">{name}</div><div class="attr-value">{value}</div></div> </div> </div> <div if="{assetDetail.cardData}" class="section"> <div class="section-title">{assetDetail.cardData.cardName}</div> <div class="basic img-frame"><img riot-src="{assetDetail.cardData.imgurURL}" loading="lazy"></div> <div class="basic">by {assetDetail.cardData.ownerName} <span if="{assetDetail.cardData.ownerSN}">(<a href="{twitterURL(assetDetail.cardData.ownerSN)}" target="_blank">@{assetDetail.cardData.ownerSN}</a>)</span></div> <div class="basic">{assetDetail.cardData.description}</div> <div class="basic"><material-button if="{assetDetailWindow.recreateFlag}" label="Monacard" icon="open_in_new" is_link="true" link_href="{assetDetail.cardData.siteURL}" link_target="_blank"></material-button></div> </div> <div if="{assetDetail.appData?.monacharat}" class="section"> <div class="section-title">{TagG.jp ? \'モナキャラットのすがた\' : \'MonaCharaT\'}</div> <div class="basic img-frame"><img riot-src="{assetDetail.appData.monacharat.image}" class="square" loading="lazy"></div> <div class="basic"><material-button if="{assetDetailWindow.recreateFlag}" label="{TagG.jp ? \'モナキャラット\' : \'MonaCharaT\'}" icon="open_in_new" is_link="true" link_href="{assetDetail.appData.monacharat.url}" link_target="_blank"></material-button></div> </div> <div if="{assetDetail.appData?.rakugakinft}" class="section"> <div class="section-title">{TagG.jp ? \'らくがきNFT\' : \'Rakugaki NFT\'}</div> <div class="basic img-frame"><img riot-src="{assetDetail.appData.rakugakinft.image + \'?\' + TagG.getRakugakiCacheID(assetDetail.asset)}" class="square bg-checker" loading="lazy"></div> <div class="basic">{TagG.asset2rakugakiTitle[assetDetail.asset] ? \'「 \' + TagG.asset2rakugakiTitle[assetDetail.asset] + \' 」\' : \'No Title\'}</div> <div class="basic"><material-button if="{assetDetailWindow.recreateFlag}" label="{TagG.jp ? \'らくがきNFTをひらく\' : \'Open Rakugaki NFT\'}" icon="brush" is_link="true" link_href="{TagG.RELATIVE_LINK_BASE + \'rakugaki\'}"></material-button></div> </div> <div if="{assetDetail.appData?.monacute}" class="section"> <div class="section-title">Monacute Info</div> <div class="basic img-frame"><img riot-src="{assetDetail.appData.monacute.image}" class="square" loading="lazy"></div> <div class="basic">#{assetDetail.appData.monacute.id} 「{assetDetail.appData.monacute.name}」</div> <div class="basic">Model: {assetDetail.appData.monacute.model}</div> <div class="basic">DNA: {assetDetail.appData.monacute.dna}</div> <div class="basic"><material-button if="{assetDetailWindow.recreateFlag}" label="Monacute" icon="open_in_new" is_link="true" link_href="{assetDetail.appData.monacute.url}" link_target="_blank"></material-button></div> </div> <div if="{assetDetail.treasures}" class="section"> <div class="section-title">Token Vault</div> <div each="{assetDetail.treasures}" class="treasure-item"> <div class="treasure-imgframe"> <img riot-src="{tmbImage}" onclick="{onClickTreasure}" crossorigin="anonymous" loading="lazy"> <div if="{!assetDetail.isOwn && assetDetail.satoshi < qtyN}" class="treasure-lockoverlay"><i class="material-icons treasure-lockicon">lock</i></div> </div> <div class="treasure-texts"> <div class="treasure-amount">x {amountS}</div> <div class="treasure-memo">{memo}</div> </div> </div> </div> <div if="{assetDetail.isOwn}" class="toprightbutton-frame"><material-iconbutton icon="edit" on_click="{onClickFixAssetButton}"></material-iconbutton></div> </div> <div ref="assetissue_window" class="address-card-swalwindow assetissuefix-swalwindow"> <div class="title">{TagG.jp ? \'新しいトークンを発行する\' : \'Issue New Token\'}</div> <div class="basic"> <div if="{!issueWindow.isBuiltinSatEnough}" class="upper-helper"> <span if="{TagG.jp}">名前付きトークンを発行するには{TagG.ISSUEFEE_NAMED}{builtinAsset}が必要です</span> <span if="{!TagG.jp}">{TagG.ISSUEFEE_NAMED} {builtinAsset} is required to issue a named token.</span> </div> <div class="mode-radiobuttons"><material-radiobuttons if="{issueWindow.recreateFlag}" items="{issueWindow.nameRadioItems}" is_block="true" on_change="{issueWindow.onChangeNameRadio}"></material-radiobuttons></div> </div> <div if="{issueWindow.nameMode === ASSETNAME_MODES.CUSTOM}" class="basic"> <div class="upper-helper"> <span if="{TagG.jp}">先頭がA以外の大文字アルファベット4~12文字が設定できます</span> <span if="{!TagG.jp}">Must contain between 4 and 12 uppercase letters only (A-Z), and cannot start with \'A\'.</span> </div> <div class="inline-input assetname-input"><material-textfield ref="issue_customname" hint="Token Name" pattern="{assetPattern}" invalid_message="Invalid Format"></material-textfield></div> </div> <div if="{issueWindow.nameMode === ASSETNAME_MODES.NUMERIC}" class="basic"> <div class="inline-input numericassetname-input"><material-textfield ref="issue_numericname" hint="Token Name"></material-textfield></div> </div> <div if="{issueWindow.nameMode === ASSETNAME_MODES.NFT}" class="basic"> <div class="upper-helper"> <span if="{TagG.jp}">{\'半角英数字と . - _ @ ! が使えます\'}</span> <span if="{!TagG.jp}">{\'May contain A-Z, a-z, 0-9, or characters .-_@! and must not end with a period\'}</span> </div> <div class="upper-helper"> <span if="{TagG.jp}">既存のグループ名を指定することで1枚ずつ追加発行できます</span> <span if="{!TagG.jp}">You can also specify an existing group that you own and issue additional NFTs to the group one by one.</span> </div> <div class="inline-input groupname-input"><material-textfield ref="issue_groupname" hint="Group Name"></material-textfield></div> </div> <div if="{issueWindow.nameMode === ASSETNAME_MODES.SUBASSET}" class="basic"> <div class="upper-helper"> <span if="{TagG.jp}">{\'半角英数字と . - _ @ ! が使えます\'}</span> <span if="{!TagG.jp}">{\'May contain A-Z, a-z, 0-9, or characters .-_@! and must not end with a period\'}</span> </div> <div class="inline-input subassetname-select"><material-select if="{issueWindow.recreateFlag}" ref="issue_parent" hint="Parent" is_fullwidth="true" items="{issueWindow.parentSelectItems}"></material-select></div> <div class="text-with-inlineinput">.</div> <div class="inline-input subassetname-input"><material-textfield ref="issue_subname" hint="Sub Name" pattern="{subassetPattern}" invalid_message="Invalid Format"></material-textfield></div> </div> <div show="{issueWindow.nameMode !== ASSETNAME_MODES.NFT}" class="basic"> <div class="upper-helper"> <span if="{TagG.jp}">後から追加発行もできます。後から枚数をロックすることもできます</span> <span if="{!TagG.jp}">You can add or lock the amount later.</span> </div> <div class="inline-input assetamount-input"><material-textfield ref="issue_amount" hint="Amount" pattern="{\'[0-9]*(\\\\.[0-9]+)?\'}" invalid_message="Invalid Format"></material-textfield></div> <div class="text-with-inlineinput">{TagG.jp ? \'枚\' : \'\'}</div> </div> <div class="basic"><material-switch label="{\'Monacard2.0\'}" on_change="{issueWindow.onChangeCardSwitch}"></material-switch></div> <div if="{!issueWindow.isSettingCard}"> <div class="basic"> <div class="upper-helper">{TagG.jp ? \'後から変更できます\' : \'Can be changed later.\'}</div> <material-textfield ref="issue_description" hint="Description" is_textarea="true" textarea_rows="2"></material-textfield> </div> <div class="basic"> <div class="upper-helper">{TagG.jp ? \'以下の設定は後から変更できません\' : \'The following settings cannot be changed later.\'}</div> <div show="{issueWindow.nameMode !== ASSETNAME_MODES.NFT}" class="upper-helper"> <span if="{TagG.jp}">モナカード登録するトークンは｢小数で送れる｣のチェックを外しましょう</span> <span if="{!TagG.jp}">"Divisible" must be unchecked for tokens to be registered with Monacard.</span> </div> <div class="checkboxes"> <material-checkbox show="{issueWindow.nameMode !== ASSETNAME_MODES.NFT}" ref="issue_divisiblecheck" label="{TagG.jp ? \'小数で送れる\' : \'Divisible\'}"></material-checkbox> <material-checkbox ref="issue_listedcheck" label="{TagG.jp ? \'DEXで売買できる\' : \'Available on DEX\'}" is_checked="true"></material-checkbox> <material-checkbox ref="issue_reassignablecheck" label="{TagG.jp ? \'再移転できる\' : \'Reassignable\'}" is_checked="true"></material-checkbox> </div> </div> </div> <div if="{issueWindow.isSettingCard}"> <div class="basic danger"> <span if="{TagG.jp}">事前に<a href="https://card.mona.jp/regist/regist_caution" target="_blank">Monacardの規約</a>をご確認ください</span> <span if="{!TagG.jp}">Please review <a href="https://card.mona.jp/regist/regist_caution" target="_blank">terms and conditions of Monacard</a> in advance.</span> </div> <div class="basic"> <span if="{TagG.jp}">Monacard2.0ではブロックチェーンに設定を書込むので、カード設定が反映されるまでに少し時間がかかります</span> <span if="{!TagG.jp}">As Monacard2.0 writes the settings to the blockchain, it will take some time for the card settings to be reflected.</span> </div> <div class="basic"><material-textfield ref="issue_card_name" hint="{TagG.jp ? \'カード名\' : \'Card Name\'}"></material-textfield></div> <div class="basic"><material-textfield ref="issue_card_ownername" hint="{TagG.jp ? \'登録者名\' : \'Owner Name\'}"></material-textfield></div> <div class="basic"><material-textfield ref="issue_card_description" hint="{TagG.jp ? \'カードの説明\' : \'Card Description\'}"></material-textfield></div> <div class="basic"> <div class="upper-helper">{TagG.jp ? \'カンマ(,)区切りで複数設定できます\' : \'Multiple tags can be set separated by commas.\'}</div> <material-textfield ref="issue_card_tags" hint="{TagG.jp ? \'タグ\' : \'Tags\'}"></material-textfield> </div> <div class="basic"><monacard-droparea ref="issue_card_droparea"></monacard-droparea></div> </div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="bottombutton-frame"><material-button label="{TagG.jp ? \'発行する\' : \'ISSUE\'}" icon="add_circle" on_click="{issueWindow.onClickIssueButton}" is_unelevated="true" is_fullwidth="true"></material-button></div> </div> <div ref="assetfix_window" class="address-card-swalwindow assetissuefix-swalwindow"> <div class="title"> <span if="{TagG.jp}">{assetDetail.longLabel}の設定を変える</span> <span if="{!TagG.jp}">Edit {assetDetail.longLabel}</span> </div> <div class="basic"><div class="mode-radiobuttons"><material-radiobuttons if="{fixWindow.recreateFlag}" items="{fixWindow.modeRadioItems}" is_block="true" on_change="{fixWindow.onChangeModeRadio}"></material-radiobuttons></div></div> <div if="{fixWindow.mode === ASSETFIX_MODES.CHANGEDESC}"> <div if="{assetDetail.parsedDesc}" class="basic warning-color"> <span if="{TagG.jp}">Monacard2.0などのアプリの設定が書込まれた説明文をフォーマットに合わない形に変更すると、アプリの設定が無効になる場合があります</span> <span if="{!TagG.jp}">Changing the description in which the settings of applications such as Monacard2.0 are written in may invalidate the settings of the applications.</span> </div> <div class="basic"><material-textfield ref="fix_description" hint="New Description" is_textarea="true" textarea_rows="4"></material-textfield></div> </div> <div if="{fixWindow.mode === ASSETFIX_MODES.ADD}"> <div class="basic">{TagG.jp ? \'現在の発行数\' : \'Issued\'} {commafy(assetDetail.supplyAmount)}</div> <div class="basic"> <div class="inline-input assetamount-input"><material-textfield ref="fix_amount" hint="Additional Amount" pattern="{\'[0-9]*(\\\\.[0-9]+)?\'}" invalid_message="Invalid Format"></material-textfield></div> <div class="text-with-inlineinput">{TagG.jp ? \'枚\' : \'\'}</div> </div> </div> <div if="{fixWindow.mode === ASSETFIX_MODES.TRANSFER}" class="basic"><material-textfield ref="fix_transferaddr" hint="New Owner Address" pattern="{TagG.ADDR_PATTERN}" invalid_message="Invalid Format"></material-textfield></div> <div if="{fixWindow.mode === ASSETFIX_MODES.LOCK}" class="basic"> <span if="{TagG.jp}">現在の発行数は {commafy(assetDetail.supplyAmount)} 枚です。いちどロックしたら解除はできません</span> <span if="{!TagG.jp}">The current issued amount is {commafy(assetDetail.supplyAmount)}. Once locked, it cannot be unlocked.</span> </div> <div if="{fixWindow.mode === ASSETFIX_MODES.SETATTRIBUTES}" class="basic"> <div if="{assetDetail.description && !assetDetail.parsedDesc}" class="basic warning-color"> <span if="{TagG.jp}">属性の設定はトークンの説明文に書込まれるので、設定すると現在の説明文は上書きされます</span> <span if="{!TagG.jp}">As attribute settings are written into the token description, setting attributes will overwrite the current description.</span> </div> <div each="{index in fixWindow.attrIndices}" class="basic thin"> <div class="inline-input attrname-input"><material-textfield ref="fix_attr_name_{index}" hint="{TagG.jp ? \'属性名\' : \'Name\'}"></material-textfield></div> <div class="inline-input attrvalue-input"><material-textfield ref="fix_attr_value_{index}" hint="{TagG.jp ? \'値\' : \'Value\'}"></material-textfield></div> </div> <div class="basic thin"> <material-iconbutton icon="add_circle_outline" on_click="{fixWindow.onClickAttrAddButton}"></material-iconbutton> <material-iconbutton icon="remove_circle_outline" on_click="{fixWindow.onClickAttrRemoveButton}"></material-iconbutton> </div> </div> <div if="{fixWindow.mode === ASSETFIX_MODES.SETCARD_V2}"> <div class="basic danger"> <span if="{TagG.jp}">事前に<a href="https://card.mona.jp/regist/regist_caution" target="_blank">Monacardの規約</a>をご確認ください</span> <span if="{!TagG.jp}">Please review <a href="https://card.mona.jp/regist/regist_caution" target="_blank">terms and conditions of Monacard</a> in advance.</span> </div> <div if="{assetDetail.description && !assetDetail.parsedDesc}" class="basic warning-color"> <span if="{TagG.jp}">Monacard2.0ではトークンの説明文に設定を書込むので、Monacard2.0を設定すると現在の説明文は上書きされます</span> <span if="{!TagG.jp}">As Monacard2.0 writes the settings to the token description, setting Monacard2.0 will overwrite the current description.</span> </div> <div class="basic"> <span if="{TagG.jp}">Monacard2.0ではブロックチェーンに設定を書込むので、変更が反映されるまでに少し時間がかかります</span> <span if="{!TagG.jp}">As Monacard2.0 writes the settings to the blockchain, it will take some time for the changes to be reflected.</span> </div> <div class="basic"><material-textfield ref="fix_card_name" hint="{TagG.jp ? \'カード名\' : \'Card Name\'}"></material-textfield></div> <div class="basic"><material-textfield ref="fix_card_ownername" hint="{TagG.jp ? \'登録者名\' : \'Owner Name\'}"></material-textfield></div> <div class="basic"><material-textfield ref="fix_card_description" hint="{TagG.jp ? \'カードの説明\' : \'Card Description\'}"></material-textfield></div> <div class="basic"> <div class="upper-helper">{TagG.jp ? \'カンマ(,)区切りで複数設定できます\' : \'Multiple tags can be set separated by commas.\'}</div> <material-textfield ref="fix_card_tags" hint="{TagG.jp ? \'タグ\' : \'Tags\'}"></material-textfield> </div> <div class="basic"><monacard-droparea ref="fix_card_droparea" existing_src="{assetDetail.cardData?.imgurURL}"></monacard-droparea></div> </div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="bottombutton-frame"> <material-button if="{fixWindow.mode !== ASSETFIX_MODES.SETCARD_V2}" label="{TagG.jp ? \'変更する\' : \'SAVE\'}" icon="edit" on_click="{fixWindow.onClickFixButton}" is_unelevated="true" is_fullwidth="true"></material-button> <material-button if="{fixWindow.mode === ASSETFIX_MODES.SETCARD_V2}" label="{TagG.jp ? \'設定する\' : \'SAVE\'}" icon="edit" on_click="{fixWindow.onClickSetCardButton}" is_unelevated="true" is_fullwidth="true"></material-button> </div> </div> <div ref="assetsend_window" class="address-card-swalwindow"> <div class="title"> <span if="{TagG.jp}">{sendWindow.assetData.longLabel}を送る</span> <span if="{!TagG.jp}">Send {sendWindow.assetData.longLabel}</span> </div> <div class="basic"> <span if="{TagG.jp}">{commafy(sendWindow.assetData.amount)} 枚あります</span> <span if="{!TagG.jp}">Balance: {commafy(sendWindow.assetData.amount)}</span> </div> <div class="basic"><material-switch label="{\'ALL\'}" on_change="{sendWindow.onChangeAllSwitch}"></material-switch></div> <div show="{!sendWindow.isAll}" class="basic"> <div class="inline-input assetamount-input"><material-textfield ref="asset_send_amount" hint="Amount" pattern="{\'[0-9]*(\\\\.[0-9]+)?\'}" invalid_message="Invalid Format"></material-textfield></div> <div class="text-with-inlineinput">{TagG.jp ? \'枚\' : \'\'}</div> </div> <div class="basic"><div each="{addrbook}" class="chip-item"><material-chip label="{label}" on_click="{onClickAddrbookItem}"></material-chip></div></div> <div class="basic"> <material-textfield ref="asset_send_addr" hint="Address"></material-textfield> <div class="inputenterbutton-frame"><material-iconbutton icon="emoji_emotions" on_click="{sendWindow.onClickAsset2AddrButton}"></material-iconbutton></div> </div> <div class="basic"><material-radiobuttons ref="asset_send_memoradio" items="{sendWindow.memoRadioItems}" on_change="{sendWindow.onChangeMemoRadio}"></material-radiobuttons></div> <div if="{sendWindow.memoMode === MEMO_MODES.TEXT}" class="basic"><material-textfield ref="asset_send_memo" hint="{TagG.jp ? \'Text Memo (34バイトまで)\' : \'Text Memo (up to 34 bytes)\'}" pattern="{\'.{0,34}\'}" invalid_message="Too Long"></material-textfield></div> <div if="{sendWindow.memoMode === MEMO_MODES.HEX}" class="basic"><material-textfield ref="asset_send_memo" hint="{TagG.jp ? \'Hex Memo (68ケタまで)\' : \'Hex Memo (up to 68 digits)\'}" pattern="{\'[0-9A-Fa-f]{0,68}\'}" invalid_message="Invalid Format"></material-textfield></div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="bottombutton-frame"><material-button label="{TagG.jp ? \'送る\' : \'SEND\'}" icon="send" on_click="{sendWindow.onClickSendButton}" is_unelevated="true" is_fullwidth="true"></material-button></div> <div class="bottombutton-frame"><material-button label="{TagG.jp ? \'まとめて送るリストに追加\' : \'ADD TO MULTISEND LIST\'}" icon="add_circle" on_click="{sendWindow.onClickMultisendAddButton}" is_fullwidth="true"></material-button></div> </div> <div ref="baseassetsend_window" class="address-card-swalwindow"> <div class="title"> <span if="{TagG.jp}">{baseAsset}を送る</span> <span if="{!TagG.jp}">Send {baseAsset}</span> </div> <div class="basic"> <span if="{TagG.jp}">{commafy(baseAmount)} {baseAsset} あります</span> <span if="{!TagG.jp}">Balance: {commafy(baseAmount)} {baseAsset}</span> </div> <div class="basic"><material-switch label="{\'ALL\'}" on_change="{baseSendWindow.onChangeAllSwitch}"></material-switch></div> <div show="{!baseSendWindow.isAll}" class="basic"> <div class="inline-input baseamount-input"><material-textfield ref="baseasset_send_amount" hint="Amount" pattern="{\'[0-9]*(\\\\.[0-9]+)?\'}" invalid_message="Invalid Format"></material-textfield></div> <div class="text-with-inlineinput">{baseAsset}</div> </div> <div class="basic"><div each="{addrbook}" class="chip-item"><material-chip label="{label}" on_click="{onClickAddrbookItem}"></material-chip></div></div> <div class="basic"> <material-textfield ref="baseasset_send_addr" hint="Address"></material-textfield> <div class="inputenterbutton-frame"><material-iconbutton icon="emoji_emotions" on_click="{baseSendWindow.onClickAsset2AddrButton}"></material-iconbutton></div> </div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="bottombutton-frame"><material-button label="{TagG.jp ? \'送る\' : \'SEND\'}" icon="send" on_click="{baseSendWindow.onClickSendButton}" is_unelevated="true" is_fullwidth="true"></material-button></div> </div> </div>', '', '', function(opts) {
    const ASSETFIX_MODES = {
        CHANGEDESC: 'CHANGEDESC',
        ADD: 'ADD',
        TRANSFER: 'TRANSFER',
        LOCK: 'LOCK',
        SETATTRIBUTES: 'SETATTRIBUTES',
        SETCARD_V2: 'SETCARD_V2',
    };
    const MEMO_MODES = {
        NONE: 'NONE',
        TEXT: 'TEXT',
        HEX: 'HEX',
    };
    const ASSETNAME_MODES = {
        CUSTOM: 'CUSTOM',
        SUBASSET: 'SUBASSET',
        NFT: 'NFT',
        NUMERIC: 'NUMERIC',
    };
    const SWEEP_MODES = {
        TOKEN: 'TOKEN',
        OWNER: 'OWNER',
        FULL: 'FULL',
    };
    const commafy = this.commafy = Util.commafy;
    const baseAsset = this.baseAsset = TagG.BASECHAIN_ASSET;
    const builtinAsset = this.builtinAsset = TagG.BUILTIN_ASSET;
    this.TagG = TagG;
    this.searchWindow = {};
    this.addrDetailWindow = {};
    this.addrKeypairWindow = {};
    this.addrBroadcastWindow = {};
    this.addrDividendWindow = {};
    this.addrDispenserWindow = {};
    this.addrSweepWindow = {};
    this.addrLabelWindow = {};
    this.assetDetailWindow = {};
    this.issueWindow = {};
    this.fixWindow = {};
    this.baseSendWindow = {};
    this.sendWindow = {};
    this.assetDetail = {};
    this.multisendData = {
        list: [],
        memoMode: MEMO_MODES.NONE,
        memo: null,
    };

    const init = () => {
        this.ADDR_TYPE = ADDR_TYPE;
        this.ASSETFIX_MODES = ASSETFIX_MODES;
        this.MEMO_MODES = MEMO_MODES;
        this.ASSETNAME_MODES = ASSETNAME_MODES;
        this.SWEEP_MODES = SWEEP_MODES;
        this.assetPattern = '[B-Z][A-Z]{3,11}';
        this.subassetPattern = '[a-zA-Z0-9\\.\\-_@!]{1,}';
        this.fixWindow.modeRadioItems = [];
        this.sendWindow.memoRadioItems = [
            { value: MEMO_MODES.NONE, label: 'No Memo' },
            { value: MEMO_MODES.TEXT, label: 'Text' },
            { value: MEMO_MODES.HEX, label: 'Hex' },
        ];
        this.addrSweepWindow.modeRadioItems = [
            { value: SWEEP_MODES.TOKEN, label: (TagG.jp ? 'トークンを全て移動する' : 'Transfer All Tokens') },
            { value: SWEEP_MODES.OWNER, label: (TagG.jp ? 'オーナー権限を全て移動する' : 'Transfer All Ownerships') },
            { value: SWEEP_MODES.FULL, label: (TagG.jp ? '両方とも全て移動する' : 'Transfer Both') },
        ];
        this.addrSweepWindow.memoRadioItems = [
            { value: MEMO_MODES.NONE, label: 'No Memo' },
            { value: MEMO_MODES.TEXT, label: 'Text' },
            { value: MEMO_MODES.HEX, label: 'Hex' },
        ];
        this.issueWindow.nameRadioItems = [];
        this.searchChips = [
            { label: 'NFT', onClick: () => search('[NFT]') },
            { label: 'Locked', onClick: () => search('[LOCKED]') },
            { label: 'Styles', onClick: () => search('モナパレット スタイルトークン') },
            { label: 'Rakugaki', onClick: () => search('Rakugaki [NFT]') },
            { label: 'OR ex.', onClick: () => search('オダイロイド１号 OR モナパちゃん OR Tiproid') },
            { label: (TagG.jp ? '非表示' : 'Hide'), onClick: () => search(`${Math.floor(Math.random() * 1e12)}`) },
        ];
        this.twitterURL = (userSN) => TagG.TWITTERUSERURL_FORMAT.replace('{SN}', userSN);
        this.searchedQuery = '';
        this.isCardMode = false;
        this.fixWindow.mode = ASSETFIX_MODES.CHANGEDESC;
        this.sendWindow.assetData = {};
        this.sendWindow.memoMode = MEMO_MODES.NONE;
        this.baseSendWindow.isAll = false;
        this.addrDividendWindow.isBaseAsset = false;
        this.addrSweepWindow.memoMode = MEMO_MODES.NONE;
        setData(opts.data);
        this.addrDetailWindow.baseExplorerURL = TagG.ADDR_EXPLORERURL_FORMAT.replace('{ADDR}', this.address);
        this.addrDetailWindow.monapartyExplorerURL = TagG.ADDR_PARTYEXPURL_FORMAT.replace('{ADDR}', this.address);
        this.addrKeypairWindow.publicKey = this.addrDataRef.publicKey ?
            this.addrDataRef.publicKey.toString('hex') : 'Unavailable for this address';
        if (!this.addrDataRef.ecPair && !TagG.isReadOnlyMode) {
            this.addrKeypairWindow.privateKey = 'Unavailable for this address';
            this.addrKeypairWindow.isOpenPrivateKey = true;
        }
        this.createParamsBase = {
            'source': this.address,
            'fee_per_kb': TagG.FEESAT_PERBYTE * 1000,
            'extended_tx_info': true,
        };
        if (this.addrDataRef.publicKey) this.createParamsBase['pubkey'] = this.addrDataRef.publicKey.toString('hex');
        if (TagG.confNum === 0) this.createParamsBase['allow_unconfirmed_inputs'] = true;
        this.assetName2onClickPaletteButton = {
            'MPLT.AJISAI': () => { switchTheme('MPLT.AJISAI', false) },
            'MPLT.MIKAN': () => { switchTheme('MPLT.MIKAN', false) },
            'MPLT.PTRN01': () => { switchTheme('MPLT.PTRN01', false) },
            'MPLT.GRAD01': () => { switchTheme('MPLT.GRAD01', false) },
            'MOTATOKEN': () => { switchTheme('MOTATOKEN', false) },
            'MPLTARH.A': () => { switchTheme('MPLTARH.A', false) },
            'NNSNPLT': () => { switchTheme('NNSNPLT', false) },
            'MPLTVOXEL': () => { switchTheme('MPLTVOXEL', false) },
            'MPLCHOCA': () => { switchTheme('MPLCHOCA', false) },
            'MPLTYAGI': () => { switchTheme('MPLTYAGI', false) },
            'MPLTYAGINI': () => { switchTheme('MPLTYAGINI', false) },
            'RKGK.MPST01': () => { switchTheme('RKGK.MPST01', false) },
            'ROOMBA': () => { switchTheme('ROOMBA', false) },
            'NFTCHAN.ST1': () => { switchTheme('NFTCHAN.ST1', false) },
            'GEMST.A': () => { switchTheme('GEMST.A', false) },
            'GEMST.B': () => { switchTheme('GEMST.B', false) },
            'HETABUSTYL': () => { switchTheme('HETABUSTYL', false) },
            'CUSTOM.STYLE': () => { switchTheme('CUSTOM.STYLE', false) },
        };
        setupAddrbook();
    };

    this.onClickUpdateButton = () => {
        if (this.refs.update_button) this.refs.update_button.setDisabled(true);
        aUpdateData(false, true);
        setTimeout(() => {
            if (this.refs.update_button) this.refs.update_button.setDisabled(false);
        }, 10000);
    };
    this.onClickSearchButton = () => {
        Util.aOpenContentSwal(this.refs.search_window);
    };
    this.onClickShowCardsButton = () => {
        this.isCardMode = !this.isCardMode;
        this.update();
    };
    this.onClickAddrMoreButton = () => {
        clearWindowMessages();
        Util.aOpenContentSwal(this.refs.addrdetail_window);
    };
    this.onClickMultisendButton = () => {
        aTemplateProcessTX(() => this.multisendData, aPrepareMultisend, afterMultisend);
    };
    this.onClickSendBaseAssetButton = () => {
        clearWindowMessages();
        Util.aOpenContentSwal(this.refs.baseassetsend_window);
    };
    this.onClickSendBuiltinAssetButton = () => {
        openSendAssetWindow(getBuiltinAssetData());
    };
    this.onClickIssueAssetButton = () => {
        this.issueWindow.recreateFlag = true;
        clearWindowMessages();
        setNumericName();
        Util.aOpenContentSwal(this.refs.assetissue_window);
    };
    this.onClickFixAssetButton = () => {
        this.fixWindow.modeRadioItems = [];
        this.fixWindow.modeRadioItems.push({ value: ASSETFIX_MODES.CHANGEDESC, label: (TagG.jp ? '説明文を変更する' : 'Change Description') });
        this.fixWindow.modeRadioItems.push({ value: ASSETFIX_MODES.TRANSFER, label: (TagG.jp ? 'オーナーを変更する' : 'Transfer Ownership') });
        if (!this.assetDetail.isLocked) {
            this.fixWindow.modeRadioItems.push({ value: ASSETFIX_MODES.ADD, label: (TagG.jp ? '追加で発行する' : 'Issue Additional Amount') });
            this.fixWindow.modeRadioItems.push({ value: ASSETFIX_MODES.LOCK, label: (TagG.jp ? '発行枚数をロックする' : 'Lock Amount') });
        }
        this.fixWindow.modeRadioItems.push({ value: ASSETFIX_MODES.SETATTRIBUTES, label: (TagG.jp ? '属性を設定する' : 'Set Attributes') });
        if (!this.assetDetail.isDivisible) {
            this.fixWindow.modeRadioItems.push({ value: ASSETFIX_MODES.SETCARD_V2, label: (TagG.jp ? 'Monacard2.0の設定' : 'Set Monacard2.0') });
        }
        this.fixWindow.mode = this.fixWindow.modeRadioItems[0].value;
        this.fixWindow.recreateFlag = false;
        this.update();
        this.fixWindow.recreateFlag = true;
        clearWindowMessages();
        setFixWindowValues();
        Util.aOpenContentSwal(this.refs.assetfix_window);
    };
    this.issueWindow.onChangeNameRadio = (nameMode) => {
        this.issueWindow.nameMode = nameMode;
        this.update();
        setNumericName();
    };
    this.fixWindow.onChangeModeRadio = (fixMode) => {
        this.fixWindow.mode = fixMode;
        this.update();
        setFixWindowValues();
    };
    this.issueWindow.onChangeCardSwitch = (isChecked) => {
        this.issueWindow.isSettingCard = isChecked;
        this.update();
    };
    this.baseSendWindow.onChangeAllSwitch = (isChecked) => {
        this.baseSendWindow.isAll = isChecked;
        this.update();
    };
    this.sendWindow.onChangeAllSwitch = (isChecked) => {
        this.sendWindow.isAll = isChecked;
        this.update();
    };
    this.sendWindow.onChangeMemoRadio = (value) => {
        this.sendWindow.memoMode = value;
        this.update();
    };
    this.addrDividendWindow.onChangeBaseAssetSwitch = (isChecked) => {
        this.addrDividendWindow.isBaseAsset = isChecked;
        this.update();
    };
    this.addrSweepWindow.onChangeMemoRadio = (value) => {
        this.addrSweepWindow.memoMode = value;
        this.update();
    };
    this.issueWindow.onClickIssueButton = () => {
        aTemplateProcessTX(aCheckAssetIssueInputs , aPrepareAssetIssue, afterAssetIssue, true);
    };
    this.fixWindow.onClickAttrAddButton = () => {
        this.fixWindow.attrIndices.push(this.fixWindow.attrIndices.length);
        this.update();
    };
    this.fixWindow.onClickAttrRemoveButton = () => {
        if (this.fixWindow.attrIndices.length > 1) {
            this.fixWindow.attrIndices.pop();
            this.update();
        }
        else {
            this.refs.fix_attr_name_0.setValue('');
            this.refs.fix_attr_value_0.setValue('');
        }
    };
    this.fixWindow.onClickFixButton = () => {
        aTemplateProcessTX(checkAssetFixInputs, aPrepareAssetIssue, afterAssetIssue, true);
    };
    this.fixWindow.onClickSetCardButton = async() => {
        const cardName = this.refs.fix_card_name.getValue().trim();
        const ownerName = this.refs.fix_card_ownername.getValue().trim();
        const description = this.refs.fix_card_description.getValue().trim();
        const tags = this.refs.fix_card_tags.getValue().split(',').map(tag => tag.trim()).join(',');
        if (!cardName) return showWindowError(TagG.jp ? 'だめです。カード名を入力してね' : 'Oops! No card name.');
        if (!ownerName) return showWindowError(TagG.jp ? 'だめです。発行者名を入力してね' : 'Oops! No owner name.');
        if (!description) return showWindowError(TagG.jp ? 'だめです。カードの説明を入力してね' : 'Oops! No description.');
        if (this.fixWindow.mode === ASSETFIX_MODES.SETCARD_V2) {
            const imgBlob = this.refs.fix_card_droparea.blob;
            if (!imgBlob) return showWindowError(TagG.jp ? 'だめです。画像を選択してね' : 'Oops! No image.');
            const cid = await aPostCardImage(imgBlob).catch(showWindowError);
            if (!cid) return;
            this.fixWindow.cardParams = formatMonacardV2Params(cardName, ownerName, description, tags, cid);
            aTemplateProcessTX(checkAssetFixInputs, aPrepareAssetIssue, afterAssetIssue, true);
        }
    };
    this.baseSendWindow.onClickAsset2AddrButton = async() => { onClickAsset2AddrButton(this.refs.baseasset_send_addr) };
    this.baseSendWindow.onClickSendButton = () => {
        let satoshi = null;
        const address = this.refs.baseasset_send_addr.getValue().trim();
        if (!this.baseSendWindow.isAll) {
            const amount = Number(this.refs.baseasset_send_amount.getValue().trim());
            if (isNaN(amount)) return showWindowError(TagG.jp ? 'だめです。数量に変な値が入っています' : 'Oops! Invalid amount.');
            satoshi = Math.floor(amount * TagG.SATOSHI_RATIO);
            if (satoshi <= 0) return showWindowError(TagG.jp ? 'だめです。数量は正の値にしてね' : 'Oops! Invalid amount.');
            if (satoshi < TagG.SENDMIN_SAT) return showWindowError(TagG.jp ? 'だめです。数量が小さすぎます' : 'Oops! Invalid amount.');
        }
        if (!TagG.validateAddress(address)) return showWindowError(TagG.jp ? 'だめです。アドレスが間違っているかサポートしていないフォーマットです' : 'Oops! Invalid address.');
        aPrepareBaseassetSend(satoshi, address, this.baseSendWindow.isAll);
    };
    this.sendWindow.onClickAsset2AddrButton = async() => { onClickAsset2AddrButton(this.refs.asset_send_addr) };
    this.sendWindow.onClickSendButton = () => {
        aTemplateProcessTX(checkAssetSendInputs, aPrepareAssetSend, afterAssetSend, true);
    };
    this.sendWindow.onClickMultisendAddButton = () => {
        let readData;
        try { readData = checkAssetSendInputs() }
        catch (error) {
            console.error(error);
            return showWindowError(error);
        }
        const { assetData, satoshi, toAddr, memoMode, memo } = readData;
        const lastMemo = this.multisendData.memo;
        const isValid = addMultisend(assetData, satoshi, toAddr, memoMode, memo);
        if (!isValid) return;
        this.update();
        swal.close();
        if (lastMemo && memo !== lastMemo) myswal(TagG.jp ? 'メモが変更されました' : 'Memo Changed', TagG.jp ? 'まとめて送るときに設定できるメモは1つだけです。前に設定したメモは新しいメモで上書きされました' : 'Only one memo can be set in multisend. The previous memo has been overwritten by the new one.', 'warning');
    };
    this.addrDetailWindow.onClickCopyButton = () => {
        const success = Util.execCopy(this.address);
        if (success) showWindowSuccess(TagG.jp ? 'アドレスをコピーしました' : 'Address copied.');
        else showWindowError(TagG.jp ? 'コピーできませんでした。このブラウザだとできないかも' : 'Copy failed. May not be supported by this browser.');
    };
    this.addrDetailWindow.onClickKeyButton = () => {
        if (this.addrDataRef.ecPair || TagG.isReadOnlyMode) this.addrKeypairWindow.isOpenPrivateKey = false;
        this.addrKeypairWindow.signedMessage = '';
        clearWindowMessages();
        Util.aOpenContentSwal(this.refs.addrkeypair_window);
    };
    this.addrDetailWindow.onClickBroadcastButton = () => {
        clearWindowMessages();
        Util.aOpenContentSwal(this.refs.addrbroadcast_window);
    };
    this.addrDetailWindow.onClickDividendButton = () => {
        this.addrDividendWindow.recreateFlag = true;
        clearWindowMessages();
        Util.aOpenContentSwal(this.refs.addrdividend_window);
    };
    this.addrDetailWindow.onClickDispenserButton = () => {
        this.addrDispenserWindow.recreateFlag = true;
        clearWindowMessages();
        Util.aOpenContentSwal(this.refs.addrdispenser_window);
    };
    this.addrDetailWindow.onClickSweepButton = () => {
        clearWindowMessages();
        Util.aOpenContentSwal(this.refs.addrsweep_window);
    };
    this.addrDetailWindow.onClickEditButton = () => {
        if (this.refs.addr_label) this.refs.addr_label.setValue(this.addrDataRef.label || '');
        clearWindowMessages();
        Util.aOpenContentSwal(this.refs.addrlabel_window);
    };
    this.addrKeypairWindow.onClickOpenPrivateKeyButton = async() => {
        if (TagG.isReadOnlyMode) await TagG.aUpgradeFromReadOnlyMode();
        if (TagG.isReadOnlyMode) return;
        this.addrKeypairWindow.privateKey = this.addrDataRef.ecPair.toWIF();
        this.addrKeypairWindow.isOpenPrivateKey = true;
        this.update();
    };
    this.addrKeypairWindow.onClickSignButton = async() => {
        const message = this.refs.keypair_sign_input.getValue();
        if (message) this.addrKeypairWindow.signedMessage = await TagG.aSignMessage(message, this.addrDataRef);
        else this.addrKeypairWindow.signedMessage = '';
        this.update();
    };
    this.addrKeypairWindow.onClickSendTXButton = async() => {
        try {
            TagG.bodySpinner.start();
            const txHex = this.refs.keypair_tx_input.getValue();
            const signedTXHex = await TagG.aSignTXHex(txHex, this.addrDataRef, null);
            const txID = await TagG.aBroadcast(signedTXHex);
            console.log({ txID });
            myswal({ title: 'Success', text: (TagG.jp ? '署名 & ブロードキャストできました。次の操作を続けて行うには少し待ってから残高を更新してください' : 'Successfully signed & broadcasted. To continue with the next operation, please wait a moment and then update your balance.'), icon: 'success' });
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `失敗しました。ヒント：${error}` : `Failed. hint: ${error}`), 'error');
        }
        finally { TagG.bodySpinner.stop() }
    };
    this.addrBroadcastWindow.onClickBroadcastButton = () => {
        const message = this.refs.broadcast_message.getValue();
        aTemplateProcessTX(() => ({ message }), aPrepareAddrBroadcast, afterAddrBroadcast, true);
    };
    this.addrDividendWindow.onClickDividendButton = () => {
        aTemplateProcessTX(checkAddrDividendInputs, aPrepareAddrDividend, afterAddrDividend, true);
    };
    this.addrDispenserWindow.onClickDispenserButton = () => {
        aTemplateProcessTX(checkAddrDispenserInputs, aPrepareAddrDispenser, afterAddrDispenser, true);
    };
    this.addrSweepWindow.onClickSweepButton = () => {
        aTemplateProcessTX(checkAddrSweepInputs, aPrepareAddrSweep, afterAddrSweep, true);
    };
    this.addrDispenserWindow.onClickHelpButton = async() => {
        if (TagG.jp) await myswal({
            title: '自動販売の心得',
            text: `自動販売は別々のトークンであれば1つのアドレスに複数設定し、併せ売りのようにすることができます\n\nまた同じトークンの自動販売設定を再送することで、返送用の在庫を補充することができます。1クチあたりの${baseAsset}やトークンの量は変更できないので、修正したい場合はいちど停止してから設定し直してください\n\n自動販売を設定すると、送金の意図とは無関係に1クチ以上の${baseAsset}に対して問答無用でクチ数に応じたトークンを返送するようになります。汎用のアドレスで設定すると思わぬ返送をしてしまうかもしれないので気をつけましょう\n\n管理上の問題に加えて、売上によってUTXOが多く溜まると処理が遅くなる場合があるので、自動販売には専用のアドレスを使うのがオススメです`,
            button: { text: '戻る', closeModal: false },
            isCutin: true,
        });
        else await myswal({
            title: 'Dispenser Tips',
            text: `Multiple dispensers of different tokens can be set up at the same address, and then the tokens will be sold together.\n\nYou can restock escrow amounts by resending same dispenser settings. If you want to change unit amounts, you will need to stop and reconfigure them.\n\nAs an address with dispensers will send back tokens for ${baseAsset} deposits without question, be careful not to send tokens unexpectedly.\n\nDue to management and performance issues, it is recommended to use dedicated addresses for dispensers.`,
            button: { text: 'OK', closeModal: false },
            isCutin: true,
        });
    };
    this.addrLabelWindow.onClickEditButton = () => {
        const label = this.refs.addr_label.getValue() || null;
        if (label && label.length > 40) return showWindowError(TagG.jp ? 'だめです。長すぎます' : 'Oops! Too long.');
        Util.saveStorage(this.labelStorageKey, label);
        this.addrDataRef.label = label;
        this.label = label || TagG.getDefaultAddrLabel(this.index);
        this.update();
        TagG.updatefirstAddrLabel();
        myswal({ title: 'Success', text: (TagG.jp ? '変更しました' : 'Changed.'), icon: 'success', timer: TagG.SWAL_TIMER, buttons: {} });
    };
    this.searchWindow.onInputSearch = (query) => {
        query = query.trim();
        if (query === this.searchedQuery) return;
        search(query);
    };
    this.searchWindow.onClickClearSearchButton = () => {
        if (this.searchedQuery === '') return;
        search('');
    };

    this.on('mount', async() => {
        const container = this.refs.swalwindows_container;
        while (container && container.firstChild) container.removeChild(container.firstChild);
        if (this.assets.length > 500) myswal({
            title: (TagG.jp ? 'ご注意！' : 'CAUTION!'),
            text: (TagG.jp ? 'トークンの種類が多いと性能が悪くなるので複数のウォレットに適度に分散するのがオススメです' : 'As too many tokens make the performance worse, it is recommended to spread them moderately across multiple wallets.'),
            icon: 'warning',
            isCutin: true,
        });
    });

    const onClickAsset2AddrButton = async(addrInputRef) => {
        let asset = addrInputRef.getValue();
        if (!asset || asset.length < 4) return openAsset2AddrHelp();
        if (!asset.includes('.')) {
            if (asset.length > 12) return openAsset2AddrHelp();
            asset = asset.toUpperCase();
        }
        TagG.bodySpinner.start();
        try { await TagG.aLoadAssetsInfo([asset], false) }
        catch (error) { return showWindowError(TagG.jp ? `エラー。ヒント: ${error}` : `Failed. hint: ${error}`) }
        finally { TagG.bodySpinner.stop() }
        const assetInfo = TagG.asset2info[asset];
        if (!assetInfo || !assetInfo.owner) return showWindowError(TagG.jp ? `${asset}というトークンは存在しません` : `Token named "${asset}" does not exist.`);
        addrInputRef.setValue(assetInfo.owner);
    };
    const openSendAssetWindow = (assetData) => {
        this.sendWindow.assetData = assetData;
        clearWindowMessages();
        if (this.refs.asset_send_amount) {
            if (!assetData.isDivisible) this.refs.asset_send_amount.setValue('1');
            else this.refs.asset_send_amount.setValue('');
        }
        Util.aOpenContentSwal(this.refs.assetsend_window);
    };
    const aOpenAssetDetailWindow = async(assetData) => {
        try {
            TagG.bodySpinner.start();
            await TagG.aLoadAssets([assetData.asset], true, true);
            const assetInfo = TagG.asset2info[assetData.asset];
            const extendedInfo = assetInfo.extendedInfo || {};
            const cardData = TagG.asset2card[assetData.name];
            let treasures = TagG.asset2treasures[assetData.asset] || null;
            if (treasures) treasures = treasures.map(tre => ({
                ...tre,
                onClickTreasure: () => { aOpenAssetDetailTreasure(tre.asset, tre.qtyS) },
            }));
            this.assetDetail = {
                ...assetData,
                description: assetInfo.description,
                supplySat: assetInfo.supply,
                supplyAmount: TagG.qty2amount(assetInfo.supply, assetInfo.divisible),
                owner: assetInfo.owner,
                isLocked: assetInfo.locked,
                isDivisible: assetInfo.divisible,
                isListed: assetInfo.listed,
                isReassignable: assetInfo.reassignable,
                hasExt: Boolean(assetInfo.extendedInfo),
                extImageURL: extendedInfo.imageURL,
                extDescription: extendedInfo.description || '',
                extSiteURL: extendedInfo.website,
                isExtSiteSecure: extendedInfo.isWebsiteSecure,
                cardData: cardData || null,
                treasures,
            };
            this.assetDetailWindow.recreateFlag = false;
            this.update();
            this.assetDetailWindow.recreateFlag = true;
            clearWindowMessages();
            Util.aOpenContentSwal(this.refs.assetdetail_window);
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `データの取得に失敗しました。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}` : `Failed to fetch data. hint: ${error}`), 'error');
        }
        finally { TagG.bodySpinner.stop() }
    };
    const aOpenAssetDetailTreasure = async(asset, qtyS) => {
        try {
            TagG.bodySpinner.start();
            await TagG.aOpenTreasure(asset, qtyS, this.addrDataRef);
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `エラーが発生しました。ヒント：${error}` : `Failed. hint: ${error}`), 'error');
        }
        finally { TagG.bodySpinner.stop() }
    };
    const aPostCardImage = async(imgBlob) => {
        try {
            TagG.bodySpinner.start();
            return await TagG.aPostCardImage(imgBlob);
        }
        catch (error) {
            console.error(error);
            throw (TagG.jp ? `画像をアップロードできませんでした。画像のサイズや形式が間違っていないか確認してみてください。間違っていない場合はサーバの調子が悪いかもしれないので、しばらくたっても直らなければご連絡ください。ヒント：${error}` : `Failed to upload image. Make sure that the image size/format are correct. hint: ${error}`);
        }
        finally { TagG.bodySpinner.stop() }
    };
    const aUpdateCard = async(assetData) => {
        await TagG.aLoadCardsData([assetData.name], false);
        assetData.imageL = TagG.getAssetImageURL('L', assetData.asset);
        assetData.imageM = TagG.getAssetImageURL('M', assetData.asset);
        assetData.imageS = TagG.getAssetImageURL('S', assetData.asset);
        assetData.imageAuto = TagG.getAssetImageURL('auto', assetData.asset);
        assetData.cardData = TagG.asset2card[assetData.name] || null;
        this.update();
    };
    const aPrepareAddrBroadcast = async(addrBroadcastData, utxo) => {
        const { message } = addrBroadcastData;
        const broadcastParams = { ...this.createParamsBase, ...{
            'fee_fraction': 0,
            'text': message,
            'timestamp': Math.floor(new Date().getTime() / 1000),
            'value': -1,
            'custom_inputs': [utxo],
        }};
        if (Buffer.byteLength(message) >= 55) broadcastParams['fee_per_kb'] = TagG.MULTISIG_FEESAT_PERBYTE * 1000;
        await TagG.aMonapartyTXCreate('create_broadcast', broadcastParams, addrBroadcastData);
        addrBroadcastData.confTitle = TagG.jp ? 'ブロードキャストする' : 'Broadcast';
        addrBroadcastData.confText = TagG.jp?
        `以下のメッセージをブロードキャストします。トランザクション手数料${addrBroadcastData.dustSat ? 'など' : ''}が ${commafy(addrBroadcastData.feedustAmount)} ${baseAsset} かかりますがOKですか？\n\n${message}\n`:
        `The following message will be broadcast. You will be charged a fee of ${commafy(addrBroadcastData.feedustAmount)} ${baseAsset}, OK?\n\n${message}\n`;
    };
    const afterAddrBroadcast = () => {
        if (this.refs.broadcast_message) this.refs.broadcast_message.setValue('');
    };
    const checkAddrDividendInputs = () => {
        const isBaseAssetDist = this.addrDividendWindow.isBaseAsset;
        const targetAsset = this.refs.dividend_target.getValue();
        const distAsset = isBaseAssetDist ? baseAsset : this.refs.dividend_distasset.getValue();
        const amountPerUnit = Number(this.refs.dividend_amountperunit.getValue());
        if (!targetAsset) throw TagG.jp ? 'だめです。トークンAを選択してね' : 'Oops! Token-A not selected.';
        if (!distAsset) throw TagG.jp ? 'だめです。配るトークンを入力してね' : 'Oops! No Token-B.';
        if (isNaN(amountPerUnit)) throw TagG.jp ? 'だめです。数量に変な値が入っています' : 'Oops! Invalid amount.';
        if (amountPerUnit <= 0) throw TagG.jp ? 'だめです。数量は正の値にしてね' : 'Oops! Invalid amount.';
        const targetAssetData = this.assets.find(assetData => (assetData.asset === targetAsset));
        let distAssetData = null;
        if (!isBaseAssetDist) {
            distAssetData = this.assets.find(assetData => (assetData.asset === distAsset || assetData.name === distAsset));
            if (distAsset === builtinAsset) distAssetData = getBuiltinAssetData();
            if (!distAssetData) throw TagG.jp ? 'だめです。配るトークンを持ってないか、トークン名が間違っています' : 'Oops! No Token-B in this address.';
            if (!distAssetData.isDivisible && !Number.isInteger(amountPerUnit)) throw TagG.jp ? 'だめです。このトークンは整数でしか配れません' : 'Oops! Token-B is not divisible but the amount is not an integer.';
        }
        return { isBaseAssetDist, targetAssetData, distAssetData, amountPerUnit };
    };
    const aPrepareAddrDividend = async(addrDividendData, utxo) => {
        const { isBaseAssetDist, targetAssetData, distAssetData, amountPerUnit } = addrDividendData;
        const qtyPerUnit = TagG.amount2qty(amountPerUnit, isBaseAssetDist || distAssetData.isDivisible);
        const dividendParams = { ...this.createParamsBase, ...{
            'asset': targetAssetData.asset,
            'dividend_asset': isBaseAssetDist ? baseAsset : distAssetData.asset,
            'quantity_per_unit': qtyPerUnit,
            'custom_inputs': [utxo],
        }};
        await TagG.aMonapartyTXCreate('create_dividend', dividendParams, addrDividendData);
        addrDividendData.targetAssetName = targetAssetData.name;
        addrDividendData.distAssetData = isBaseAssetDist ? null : distAssetData;
        addrDividendData.distAssetName = isBaseAssetDist ? baseAsset : distAssetData.name;
        if (isBaseAssetDist) {
            addrDividendData.totalSat = addrDividendData.dustSat;
            addrDividendData.totalAmount = addrDividendData.dustAmount;
            addrDividendData.builtinFeeSat = 0;
            addrDividendData.builtinFeeAmount = 0;
        }
        else {
            const holders = await Monaparty.aParty('get_holders', { asset: targetAssetData.asset });
            const otherHolders = holders.filter(holder => (holder.address !== this.address));
            const otherHolderAddrs = otherHolders.map(holder => holder.address);
            const targetCount = Util.uniq(otherHolderAddrs).length;
            const distAmount = otherHolders.reduce((distAmountSum, holder) => {
                const targetAmount = TagG.qty2amount(holder.address_quantity, targetAssetData.isDivisible);
                const qty = Math.floor(targetAmount * qtyPerUnit);
                const amount = TagG.qty2amount(qty, distAssetData.isDivisible);
                return distAmountSum + amount;
            }, 0);
            addrDividendData.totalAmount = Util.round(distAmount, TagG.SATOSHI_DECIMAL);
            addrDividendData.totalSat = TagG.amount2qty(distAmount, distAssetData.isDivisible);
            addrDividendData.builtinFeeSat = Math.round(TagG.DIVIDENDFEE_PER_RECIPIENT * TagG.SATOSHI_RATIO * targetCount);
            addrDividendData.builtinFeeAmount = addrDividendData.builtinFeeSat / TagG.SATOSHI_RATIO;
        }
        addrDividendData.confTitle = TagG.jp ? `${addrDividendData.distAssetName}を配る` : `Dividend ${addrDividendData.distAssetName}`;
        if (addrDividendData.isBaseAssetDist) {
            addrDividendData.confText = TagG.jp?
            `1枚の ${addrDividendData.targetAssetName} につき ${commafy(addrDividendData.amountPerUnit)} ${baseAsset}（合計で ${commafy(addrDividendData.totalAmount)} ${baseAsset}）を配ります。トランザクション手数料が ${commafy(addrDividendData.feeAmount)} ${baseAsset} かかりますがOKですか？`:
            `${commafy(addrDividendData.amountPerUnit)} ${baseAsset} will be sent per 1 ${addrDividendData.targetAssetName} holdings (total: ${commafy(addrDividendData.totalAmount)} ${baseAsset}). You will be charged a tx fee of ${commafy(addrDividendData.feeAmount)} ${baseAsset}, OK?`;
        }
        else {
            addrDividendData.confText = TagG.jp?
            `1枚の ${addrDividendData.targetAssetName} につき ${commafy(addrDividendData.amountPerUnit)} 枚の ${addrDividendData.distAssetName}（たぶん合計で ${commafy(addrDividendData.totalAmount)} 枚）を配ります。トランザクション手数料が ${commafy(addrDividendData.feeAmount)} ${baseAsset}、配布手数料がたぶん ${commafy(addrDividendData.builtinFeeAmount)} ${builtinAsset} かかりますがOKですか？`:
            `${commafy(addrDividendData.amountPerUnit)} ${addrDividendData.distAssetName} will be sent per 1 ${addrDividendData.targetAssetName} holdings (total: about ${commafy(addrDividendData.totalAmount)}). You will be charged a tx fee of ${commafy(addrDividendData.feeAmount)} ${baseAsset} and a dividend fee of about ${commafy(addrDividendData.builtinFeeAmount)} ${builtinAsset}, OK?`;
        }
    };
    const afterAddrDividend = (addrDividendData) => {
        this.builtinSat -= addrDividendData.builtinFeeSat;
        if (addrDividendData.distAssetData) {
            if (addrDividendData.distAssetName === builtinAsset) this.builtinSat -= addrDividendData.totalSat;
            else addrDividendData.distAssetData.satoshi -= addrDividendData.totalSat;
        }
        if (this.refs.dividend_amountperunit) this.refs.dividend_amountperunit.setValue('');
    };
    const checkAddrDispenserInputs = () => {
        const asset = this.refs.dispenser_asset.getValue();
        const unitBaseAmount = Number(this.refs.dispenser_unitbaseamount.getValue());
        const unitAssetAmount = Number(this.refs.dispenser_unitassetamount.getValue());
        const escrowAmount = Number(this.refs.dispenser_escrowamount.getValue());
        if (!asset) throw TagG.jp ? 'だめです。トークンを選択してね' : 'Oops! No token selected.';
        if (isNaN(unitBaseAmount)) throw TagG.jp ? 'だめです。数量に変な値が入っています' : 'Oops! Invalid amount.';
        if (isNaN(unitAssetAmount)) throw TagG.jp ? 'だめです。数量に変な値が入っています' : 'Oops! Invalid amount.';
        if (isNaN(escrowAmount)) throw TagG.jp ? 'だめです。数量に変な値が入っています' : 'Oops! Invalid amount.';
        if (unitBaseAmount <= 0) throw TagG.jp ? 'だめです。数量は正の値にしてね' : 'Oops! Invalid amount.';
        if (unitAssetAmount <= 0) throw TagG.jp ? 'だめです。数量は正の値にしてね' : 'Oops! Invalid amount.';
        if (escrowAmount <= 0) throw TagG.jp ? 'だめです。数量は正の値にしてね' : 'Oops! Invalid amount.';
        let assetData = this.assets.find(_assetData => (_assetData.asset === asset));
        if (asset === builtinAsset) assetData = getBuiltinAssetData();
        const unitBaseSat = Math.round(unitBaseAmount * TagG.SATOSHI_RATIO);
        const unitAssetSat = TagG.amount2qty(unitAssetAmount, assetData.isDivisible);
        const escrowSat = TagG.amount2qty(escrowAmount, assetData.isDivisible);
        return { assetData, unitBaseSat, unitAssetSat, escrowSat };
    };
    const aPrepareAddrDispenser = async(addrDispenserData, utxo) => {
        const { assetData, unitBaseSat, unitAssetSat, escrowSat } = addrDispenserData;
        const dispenserParams = { ...this.createParamsBase, ...{
            'asset': assetData.asset,
            'give_quantity': unitAssetSat,
            'escrow_quantity': escrowSat,
            'mainchainrate': unitBaseSat,
            'status': DISPENSER_STATUS.OPEN,
            'custom_inputs': [utxo],
        }};
        await TagG.aMonapartyTXCreate('create_dispenser', dispenserParams, addrDispenserData);
        addrDispenserData.assetName = assetData.name;
        addrDispenserData.unitBaseAmount = unitBaseSat / TagG.SATOSHI_RATIO;
        addrDispenserData.unitAssetAmount = TagG.qty2amount(unitAssetSat, assetData.isDivisible);
        addrDispenserData.escrowAmount = TagG.qty2amount(escrowSat, assetData.isDivisible);
        addrDispenserData.escrowUnit = Math.floor(escrowSat / unitAssetSat);
        addrDispenserData.confTitle = TagG.jp ? `${addrDispenserData.assetName}を自動で売る` : `Setup ${addrDispenserData.assetName} dispenser`;
        addrDispenserData.confText = TagG.jp?
        `1クチあたり ${commafy(addrDispenserData.unitBaseAmount)} ${baseAsset} → ${commafy(addrDispenserData.unitAssetAmount)} ${addrDispenserData.assetName} で合計 ${commafy(addrDispenserData.escrowAmount)} ${addrDispenserData.assetName}（${commafy(addrDispenserData.escrowUnit)} クチ）の自動販売を設定します。トランザクション手数料が ${commafy(addrDispenserData.feeAmount)} ${baseAsset} かかりますがOKですか？`:
        `Dispenser of ${commafy(addrDispenserData.unitBaseAmount)} ${baseAsset} → ${commafy(addrDispenserData.unitAssetAmount)} ${addrDispenserData.assetName} (total ${commafy(addrDispenserData.escrowAmount)} ${addrDispenserData.assetName} = ${commafy(addrDispenserData.escrowUnit)} units) will be set. You will be charged a tx fee of ${commafy(addrDispenserData.feeAmount)} ${baseAsset}, OK?`;
    };
    const afterAddrDispenser = (addrDispenserData) => {
        if (addrDispenserData.assetName === builtinAsset) this.builtinSat -= addrDispenserData.escrowSat;
        else addrDispenserData.assetData.satoshi -= addrDispenserData.escrowSat;

        if (this.refs.dispenser_unitbaseamount) this.refs.dispenser_unitbaseamount.setValue('');
        if (this.refs.dispenser_unitassetamount) this.refs.dispenser_unitassetamount.setValue('');
        if (this.refs.dispenser_escrowamount) this.refs.dispenser_escrowamount.setValue('');
    };
    const checkAddrSweepInputs = () => {
        const toAddr = this.refs.sweep_addr.getValue().trim();
        const sweepMode = this.refs.sweep_mode.getValue();
        const memoMode = this.addrSweepWindow.memoMode;
        let memo = this.refs.sweep_memo ? this.refs.sweep_memo.getValue() : null;
        if (!TagG.validateAddress(toAddr)) throw TagG.jp ? 'だめです。アドレスが間違っているかサポートしていないフォーマットです' : 'Oops! Invalid address.';
        if (memoMode === MEMO_MODES.HEX) {
            memo = memo.trim().toUpperCase();
            if (!memo.match(/^[0-9A-F]{0,68}$/)) throw TagG.jp ? 'だめです。メモはHexで入力してね' : 'Oops! Memo is not HEX.';
            if (memo.length % 2 === 1) throw TagG.jp ? 'だめです。Hexメモは偶数ケタでしか入力できません' : 'Oops! HEX memo must have even digits.';
        }
        return { toAddr, sweepMode, memoMode, memo };
    };
    const aPrepareAddrSweep = async(addrSweepData, utxo) => {
        const { toAddr, sweepMode, memoMode, memo } = addrSweepData;
        const sweepParams = { ...this.createParamsBase, ...{
            'destination': toAddr,
            'flags': 0,
            'custom_inputs': [utxo],
        }};
        switch (sweepMode) {
            case SWEEP_MODES.TOKEN: sweepParams.flags += 1; break;
            case SWEEP_MODES.OWNER: sweepParams.flags += 2; break;
            case SWEEP_MODES.FULL: sweepParams.flags += 3; break;
        }
        switch (memoMode) {
            case MEMO_MODES.TEXT:
                sweepParams.memo = memo;
                break;
            case MEMO_MODES.HEX:
                sweepParams.flags += 4;
                sweepParams.memo = memo;
                break;
        }
        await TagG.aMonapartyTXCreate('create_sweep', sweepParams, addrSweepData);
        switch (sweepMode) {
            case SWEEP_MODES.TOKEN:
                addrSweepData.confTitle = TagG.jp ? 'トークンを全て移動する' : 'Transfer all tokens';
                addrSweepData.confText = TagG.jp ? '以下のアドレスにトークンを全て移動します。' : 'All tokens will be transferred to the following address.';
                break;
            case SWEEP_MODES.OWNER:
                addrSweepData.confTitle = TagG.jp ? 'オーナー権限を全て移動する' : 'Transfer all ownerships';
                addrSweepData.confText = TagG.jp ? '以下のアドレスにオーナー権限を全て移動します。' : 'All ownerships will be transferred to the following address.';
                break;
            case SWEEP_MODES.FULL:
                addrSweepData.confTitle = TagG.jp ? 'トークンとオーナー権限を全て移動する' : 'Transfer all tokens and ownerships';
                addrSweepData.confText = TagG.jp ? 'トークンとオーナー権限を全て移動します。' : 'All tokens and ownerships will be transferred to the following address.';
                break;
        }
        addrSweepData.confText += TagG.jp?
        `トランザクション手数料が ${commafy(addrSweepData.feeAmount)} ${baseAsset}、スイープ手数料が ${commafy(TagG.SWEEPFEE_AMOUNT)} ${builtinAsset} かかりますがOKですか？`:
        `You will be charged a tx fee of ${commafy(addrSweepData.feeAmount)} ${baseAsset} and a sweep fee of ${commafy(TagG.SWEEPFEE_AMOUNT)} ${builtinAsset}, OK?`;
    };
    const afterAddrSweep = () => {
        this.builtinSat -= Math.round(TagG.SWEEPFEE_AMOUNT * TagG.SATOSHI_RATIO);
        if (this.refs.sweep_addr) this.refs.sweep_addr.setValue('');
        if (this.refs.sweep_memo) this.refs.sweep_memo.setValue('');
    };
    const aCheckAssetIssueInputs  = async() => {
        let assetName, builtinFeeSat, builtinFeeAmount;
        let isNFT = false;
        switch (this.issueWindow.nameMode) {
            case ASSETNAME_MODES.CUSTOM:
                assetName = this.refs.issue_customname.getValue();
                builtinFeeSat = TagG.ISSUEFEE_NAMED * TagG.SATOSHI_RATIO;
                break;
            case ASSETNAME_MODES.NUMERIC:
                assetName = this.refs.issue_numericname.getValue();
                builtinFeeSat = TagG.ISSUEFEE_NUMERIC * TagG.SATOSHI_RATIO;
                break;
            case ASSETNAME_MODES.NFT:
                assetName = this.refs.issue_groupname.getValue();
                builtinFeeSat = TagG.ISSUEFEE_NFT * TagG.SATOSHI_RATIO;
                isNFT = true;
                break;
            case ASSETNAME_MODES.SUBASSET:
                assetName = `${this.refs.issue_parent.getValue()}.${this.refs.issue_subname.getValue()}`;
                builtinFeeSat = TagG.ISSUEFEE_SUB * TagG.SATOSHI_RATIO;
                break;
        }
        builtinFeeAmount = builtinFeeSat / TagG.SATOSHI_RATIO;
        if (!assetName) throw TagG.jp ? 'だめです。トークン名を入力してね' : 'Oops! No token name.';
        if (!ASSETNAME_MODES.NFT && this.assets.some(ast => ast.name === assetName)) throw TagG.jp ? 'だめです。すでに発行されているトークンです' : 'Oops! Already issued.';
        let isDivisible, isListed, isReassignable;
        if (this.issueWindow.isSettingCard) [isDivisible, isListed, isReassignable] = [false, true, true];
        else {
            isDivisible = isNFT ? false : this.refs.issue_divisiblecheck.getChecked();
            isListed = this.refs.issue_listedcheck.getChecked();
            isReassignable = this.refs.issue_reassignablecheck.getChecked();
        }
        const amount = isNFT ? 1 : Number(this.refs.issue_amount.getValue().trim());
        if (isNaN(amount)) throw TagG.jp ? 'だめです。数量に変な値が入っています' : 'Oops! Invalid amount.';
        if (!isDivisible && !Number.isInteger(amount)) throw TagG.jp ? 'だめです。数量は整数にしてね' : 'Oops! Invalid amount.';
        const satoshi = TagG.amount2qty(amount, isDivisible);
        if (satoshi <= 0) throw TagG.jp ? 'だめです。数量は正の値にしてね' : 'Oops! Invalid amount.';
        const actualAmount = TagG.qty2amount(satoshi, isDivisible);
        let description;
        if (this.issueWindow.isSettingCard) {
            const cardName = this.refs.issue_card_name.getValue().trim();
            const cardOwnerName = this.refs.issue_card_ownername.getValue().trim();
            const cardDescription = this.refs.issue_card_description.getValue().trim();
            const cardTags = this.refs.issue_card_tags.getValue().split(',').map(tag => tag.trim()).join(',');
            if (!cardName) throw TagG.jp ? 'だめです。カード名を入力してね' : 'Oops! No card name.';
            if (!cardOwnerName) throw TagG.jp ? 'だめです。発行者名を入力してね' : 'Oops! No card owner name.';
            if (!cardDescription) throw TagG.jp ? 'だめです。カードの説明を入力してね' : 'Oops! No card description.';
            const imgBlob = this.refs.issue_card_droparea.blob;
            if (!imgBlob) throw TagG.jp ? 'だめです。画像を選択してね' : 'Oops! No card image.';
            const cid = await aPostCardImage(imgBlob);
            const cardParams = formatMonacardV2Params(cardName, cardOwnerName, cardDescription, cardTags, cid);
            description = JSON.stringify({ monacard: cardParams });
        }
        else description = this.refs.issue_description.getValue();
        const confTitle = TagG.jp ? '新しいトークンを発行する' : 'Issue new token';
        const confText = isNFT?
            (TagG.jp ? `グループ ${assetName} のNFTを ${commafy (actualAmount)} 枚発行します` : `A new NFT of group ${assetName} will be issued.`):
            (TagG.jp ? `${assetName} を ${commafy (actualAmount)} 枚発行します` : `${commafy (actualAmount)} ${assetName} will be issued.`);
        return { confTitle, confText, builtinFeeSat, builtinFeeAmount,
            assetName, satoshi, description, isDivisible, isListed, isReassignable, isNFT };
    };
    const checkAssetFixInputs = () => {
        const assetDetail = this.assetDetail;
        const { asset, name: assetName, isDivisible, isListed, isReassignable } = assetDetail;
        const builtinFeeSat = 0, builtinFeeAmount = 0;
        let confTitle, confText;
        let satoshi = 0;
        let description = '';
        let transferDest = null;
        switch (this.fixWindow.mode) {
            case ASSETFIX_MODES.CHANGEDESC: {
                description = this.refs.fix_description.getValue();
                confTitle = TagG.jp ? 'トークンの説明文を変更する' : 'Change token description';
                confText = TagG.jp ? `${assetName} の説明文をこれに変更します\n\n${description}` : `Description of ${assetName} will be changed to this.\n\n${description}`;
                break;
            }
            case ASSETFIX_MODES.SETATTRIBUTES: {
                description = assetDetail.parsedDesc || {};
                description.attr = {};
                for (const index of this.fixWindow.attrIndices) {
                    const attrName = this.refs[`fix_attr_name_${index}`].getValue();
                    const attrValue = this.refs[`fix_attr_value_${index}`].getValue();
                    if (attrName && attrValue) description.attr[attrName] = attrValue;
                }
                description = JSON.stringify(description);
                confTitle = TagG.jp ? '属性を設定する' : 'Set Attributes';
                confText = TagG.jp ? `${assetName} の説明文にこんな感じで属性の設定を書込みます\n\n${description}` : `Attribute settings will be written in the description of ${assetName} like this.\n\n${description}`;
                break;
            }
            case ASSETFIX_MODES.SETCARD_V2: {
                description = assetDetail.parsedDesc || {};
                description.monacard = this.fixWindow.cardParams;
                description = JSON.stringify(description);
                confTitle = TagG.jp ? 'Monacard2.0を設定する' : 'Set Monacard2.0';
                confText = TagG.jp ? `${assetName} の説明文にこんな感じでMonacard2.0の設定を書込みます\n\n${description}` : `Monacard2.0 settings will be written in the description of ${assetName} like this.\n\n${description}`;
                break;
            }
            case ASSETFIX_MODES.ADD: {
                const amount = Number(this.refs.fix_amount.getValue().trim());
                if (isNaN(amount)) throw TagG.jp ? 'だめです。数量に変な値が入っています' : 'Oops! Invalid amount.';
                if (!isDivisible && !Number.isInteger(amount)) throw TagG.jp ? 'だめです。数量は整数にしてね' : 'Oops! Invalid amount.';
                satoshi = TagG.amount2qty(amount, isDivisible);
                if (satoshi <= 0) throw TagG.jp ? 'だめです。数量は正の値にしてね' : 'Oops! Invalid amount.';
                const actualAmount = TagG.qty2amount(satoshi, isDivisible);
                confTitle = TagG.jp ? 'トークンを追加で発行する' : 'Issue additional amount';
                confText = TagG.jp ? `${assetName} を ${commafy (actualAmount)} 枚追加発行します` : `Additional ${commafy (actualAmount)} ${assetName} will be issued.`;
                break;
            }
            case ASSETFIX_MODES.TRANSFER: {
                transferDest = this.refs.fix_transferaddr.getValue().trim();
                if (!TagG.validateAddress(transferDest)) throw TagG.jp ? 'だめです。アドレスが間違っているかサポートしていないフォーマットです' : 'Oops! Invalid address.';
                confTitle = TagG.jp ? 'トークンのオーナーを変更する' : 'Transfer ownership';
                confText = TagG.jp ? `${assetName} のオーナーを ${transferDest} に変更します` : `Ownership of ${assetName} will be transferred to ${transferDest}.`;
                break;
            }
            case ASSETFIX_MODES.LOCK: {
                description = 'LOCK';
                confTitle = TagG.jp ? 'トークンの発行枚数をロックする' : 'Lock issued amount';
                confText = TagG.jp ? `${assetName} の発行枚数をロックします` : `Issued amount of ${assetName} will be locked.`;
                break;
            }
        }
        return { confTitle, confText, builtinFeeSat, builtinFeeAmount,
            assetName, satoshi, description, isDivisible, isListed, isReassignable, transferDest, asset };
    }
    const aPrepareAssetIssue = async(issueData, utxo) => {
        const { assetName, satoshi, description, isDivisible, isListed, isReassignable,
            transferDest = null, isNFT = false, asset = null } = issueData;
        const issueParams = { ...this.createParamsBase, ...{
            'asset': asset || assetName,
            'quantity': satoshi,
            'divisible': isDivisible,
            'listed': isListed,
            'reassignable': isReassignable,
            'description': description,
            'custom_inputs': [utxo],
        }};
        if (transferDest) issueParams['transfer_destination'] = transferDest;
        if (isNFT) issueParams['fungible'] = false;
        if (Buffer.byteLength(description) >= 46) issueParams['fee_per_kb'] = TagG.MULTISIG_FEESAT_PERBYTE * 1000;
        await TagG.aMonapartyTXCreate('create_issuance', issueParams, issueData);
        issueData.amount = TagG.qty2amount(satoshi, isDivisible);
        issueData.isTransfer = Boolean(transferDest);
        issueData.confText += TagG.jp?
        `\n\nトランザクション手数料${issueData.dustSat ? 'など' : '' }が ${commafy(issueData.feedustAmount)} ${baseAsset}${issueData.builtinFeeAmount ? `、トークン発行手数料が ${issueData.builtinFeeAmount} ${builtinAsset} ` : ''}かかりますがOKですか？`:
        `\n\nYou will be charged a fee of ${commafy(issueData.feedustAmount)} ${baseAsset}${issueData.builtinFeeAmount ? ` and an issuance fee of ${issueData.builtinFeeAmount} ${builtinAsset}` : ''}, OK?`;
    };
    const afterAssetIssue = (issueData) => {
        this.builtinSat -= issueData.builtinFeeSat;
        const existingAsset = this.assets.find(asset => (asset.name === issueData.assetName || asset.asset === issueData.assetName));
        if (existingAsset) {
            existingAsset.unconfSat += issueData.satoshi;
            existingAsset.issuedSat += issueData.satoshi;
            if (issueData.isTransfer) existingAsset.isOwn = false;
            TagG.changedAssets.push(existingAsset.asset);
            if (this.fixWindow.mode === ASSETFIX_MODES.SETCARD_V2) {
                const updateCard = () => {
                    const assetData = this.addrDataRef.assets.find(data => (data.name === issueData.assetName || data.asset === issueData.assetName));
                    aUpdateCard(assetData);
                };
                setTimeout(() => { updateCard() }, TagG.AVG_BLOCKINTERVAL_MS * 4);
                setTimeout(() => { updateCard() }, TagG.AVG_BLOCKINTERVAL_MS * 8);
            }
        }
        else {
            if (this.issueWindow.isSettingCard) {
                const existingAssets = this.assets.map(data => data.asset);
                const updateNewCards = () => {
                    this.addrDataRef.assets.filter(data => (!existingAssets.includes(data.asset))).forEach(newAssetData => {
                        aUpdateCard(newAssetData);
                    });
                };
                setTimeout(() => { updateNewCards() }, TagG.AVG_BLOCKINTERVAL_MS * 4);
                setTimeout(() => { updateNewCards() }, TagG.AVG_BLOCKINTERVAL_MS * 8);
            }
        }
        setAssets(this.assets);
        if (this.refs.issue_customname) this.refs.issue_customname.setValue('');
        if (this.refs.issue_numericname) this.refs.issue_numericname.setValue('');
        if (this.refs.issue_subname) this.refs.issue_subname.setValue('');
        if (this.refs.issue_description) this.refs.issue_description.setValue('');
        if (this.refs.issue_amount) this.refs.issue_amount.setValue('');
    };
    const checkAssetSendInputs = () => {
        const assetData = this.sendWindow.assetData;
        let satoshi = null;
        const toAddr = this.refs.asset_send_addr.getValue().trim();
        const memoMode = this.sendWindow.memoMode;
        let memo = this.refs.asset_send_memo ? this.refs.asset_send_memo.getValue() : null;
        if (!this.sendWindow.isAll) {
            const amount = Number(this.refs.asset_send_amount.getValue().trim());
            if (isNaN(amount)) throw TagG.jp ? 'だめです。数量に変な値が入っています' : 'Oops! Invalid amount.';
            if (!assetData.isDivisible && !Number.isInteger(amount)) throw TagG.jp ? 'だめです。このトークンは整数でしか送れません' : 'Oops! Invalid amount.';
            satoshi = TagG.amount2qty(amount, assetData.isDivisible);
            if (satoshi > assetData.satoshi) throw TagG.jp ? 'だめです。残高が足りません' : 'Oops! Insufficient token balance.';
            if (satoshi <= 0) throw TagG.jp ? 'だめです。数量は正の値にしてね' : 'Oops! Invalid amount.';
        }
        else satoshi = Util.floorLargeInt(assetData.satoshi);
        if (!TagG.validateAddress(toAddr)) throw TagG.jp ? 'だめです。アドレスが間違っているかサポートしていないフォーマットです' : 'Oops! Invalid address.';
        if (memoMode === MEMO_MODES.HEX) {
            memo = memo.trim().toUpperCase();
            if (!memo.match(/^[0-9A-F]{0,68}$/)) throw TagG.jp ? 'だめです。メモはHexで入力してね' : 'Oops! Memo is not HEX.';
            if (memo.length % 2 === 1) throw TagG.jp ? 'だめです。Hexメモは偶数ケタでしか入力できません' : 'Oops! HEX memo must have even digits.';
        }
        return { assetData, satoshi, toAddr, memoMode, memo };
    };
    const aPrepareAssetSend = async(sendData, utxo) => {
        const { assetData, satoshi, toAddr, memoMode, memo } = sendData;
        const sendParams = { ...this.createParamsBase, ...{
            'destination': toAddr,
            'asset': assetData.asset,
            'quantity': satoshi,
            'use_enhanced_send': true,
            'custom_inputs': [utxo],
        }};
        if (memoMode === MEMO_MODES.TEXT) {
            sendParams['memo'] = memo;
            sendParams['memo_is_hex'] = false;
        }
        else if (memoMode === MEMO_MODES.HEX) {
            sendParams['memo'] = memo;
            sendParams['memo_is_hex'] = true;
        }
        await TagG.aMonapartyTXCreate('create_send', sendParams, sendData);
        sendData.assetName = assetData.name;
        sendData.amount = TagG.qty2amount(satoshi, assetData.isDivisible);
        sendData.confTitle = TagG.jp ? `${sendData.assetName}を送る` : `Send ${sendData.assetName}`;
        sendData.confText = TagG.jp?
        `${commafy(sendData.amount)} ${sendData.assetName} を以下のアドレスに送ります。トランザクション手数料が ${commafy(sendData.feeAmount)} ${baseAsset} かかりますがOKですか？\n\n${sendData.toAddr}`:
        `${commafy(sendData.amount)} ${sendData.assetName} will be sent to the following address. You will be charged a tx fee of ${commafy(sendData.feeAmount)} ${baseAsset}, OK?`;
    };
    const afterAssetSend = (sendData) => {
        if (sendData.assetName === builtinAsset) this.builtinSat -= sendData.satoshi;
        else sendData.assetData.satoshi -= sendData.satoshi;
        if (this.refs.asset_send_amount) this.refs.asset_send_amount.setValue('');
    };
    const aPrepareMultisend = async(multisendData, utxo) => {
        const sendList = multisendData.list;
        const sendParams = { ...this.createParamsBase, ...{
            'destination': sendList.map(item => item.toAddr),
            'asset': sendList.map(item => item.assetData.asset),
            'quantity': sendList.map(item => item.satoshi),
            'use_enhanced_send': true,
            'custom_inputs': [utxo],
        }};
        if (sendList.length === 1) {
            sendParams.destination = sendParams.destination[0];
            sendParams.asset = sendParams.asset[0];
            sendParams.quantity = sendParams.quantity[0];
        }
        else sendParams['fee_per_kb'] = TagG.MULTISIG_FEESAT_PERBYTE * 1000;
        if (multisendData.memoMode === MEMO_MODES.TEXT) {
            sendParams['memo'] = multisendData.memo;
            sendParams['memo_is_hex'] = false;
        }
        else if (multisendData.memoMode === MEMO_MODES.HEX) {
            sendParams['memo'] = multisendData.memo;
            sendParams['memo_is_hex'] = true;
        }
        await TagG.aMonapartyTXCreate('create_send', sendParams, multisendData);
        multisendData.confTitle = TagG.jp ? 'まとめて送る' : 'Send multiple tokens';
        multisendData.confText = TagG.jp?
        `リストしたトークンを一括で送ります。トランザクション手数料${multisendData.dustSat ? 'など' : ''}が ${commafy(multisendData.feedustAmount)} ${baseAsset} かかりますがOKですか？`:
        `Tokens listed on the list will be sent at once. You will be charged a fee of ${commafy(multisendData.feedustAmount)} ${baseAsset}, OK?`;
    };
    const afterMultisend = (multisendData) => {
        for (const sendItem of multisendData.list) {
            if (sendItem.assetData.asset === builtinAsset) this.builtinSat -= sendItem.satoshi;
            else sendItem.assetData.satoshi -= sendItem.satoshi;
        }
        multisendData.list = [];
        multisendData.memo = null;
        if (this.refs.asset_send_amount) this.refs.asset_send_amount.setValue('');
    };
    const aPrepareDeleteDispenser = async(deleteDispenserData, utxo) => {
        const assetData = deleteDispenserData.assetData;
        const dispenserParams = { ...this.createParamsBase, ...{
            'asset': assetData.asset,
            'give_quantity': 0,
            'escrow_quantity': 0,
            'mainchainrate': 0,
            'status': DISPENSER_STATUS.CLOSE,
            'custom_inputs': [utxo],
        }};
        await TagG.aMonapartyTXCreate('create_dispenser', dispenserParams, deleteDispenserData);
        deleteDispenserData.assetName = assetData.name;
        deleteDispenserData.confTitle = TagG.jp ? `${deleteDispenserData.assetName}の自動販売を止める` : `Stop ${deleteDispenserData.assetName} dispenser`;
        deleteDispenserData.confText = TagG.jp?
        `${deleteDispenserData.assetName} の自動販売を停止します。トランザクション手数料が ${commafy(deleteDispenserData.feeAmount)} ${baseAsset} かかりますがOKですか？`:
        `${deleteDispenserData.assetName} dispenser will be stopped. You will be charged a tx fee of ${commafy(deleteDispenserData.feeAmount)} ${baseAsset}, OK?`;
    };
    const afterDeleteDispenser = (deleteDispenserData) => {
        this.dispensers = this.dispensers.filter(dsps => (dsps.assetName !== deleteDispenserData.assetName));
    };
    const aTemplateProcessTX = async(aCheckInputs, aPrepareTX, aAfterSendTX, fromInputWindow = false) => {
        let txData;
        let isUnconfSatUsed = false;
        try { txData = await aCheckInputs() }
        catch (error) {
            console.error(error);
            if (fromInputWindow) showWindowError(error);
            else myswal('Error', error, 'error');
            return;
        }
        try {
            TagG.bodySpinner.start();
            if (!this.isUTXOsFresh) await aUpdateData(true, false);
            const utxo = getBiggestUTXO();
            if (!utxo) throw TagG.jp ? `承認数${TagG.confNum}以上の${baseAsset}がありません` : `No ${baseAsset} with confirmation >= ${TagG.confNum}`;
            if (utxo.confirmations === 0) isUnconfSatUsed = true;
            await aPrepareTX(txData, utxo);
            console.log(txData);
            if (fromInputWindow) clearWindowMessages();
        }
        catch (error) {
            console.error(error);
            const message = TagG.jp?
            `トランザクションの準備に失敗しました。入力した値に問題があるか、必要な${TagG.BASECHAIN_ASSET}やトークンが足りないかもしれません。データの更新などいろいろ試してもうまくいかない場合は情報を揃えてご連絡ください。ヒント：${error}`:
            `Failed to prepare tx. hint: ${error}`;
            if (fromInputWindow) showWindowError(message);
            else myswal('Error', message, 'error');
            return;
        }
        finally { TagG.bodySpinner.stop() }
        const ok = await myswal({
            title: txData.confTitle,
            text: txData.confText,
            button: { text: 'OK', closeModal: false },
            className: 'swalcustom-singlebutton-center',
        });
        if (!ok) return;
        try {
            TagG.bodySpinner.start();
            this.isUTXOsFresh = false;
            const signedTXHex = await TagG.aSignTXHex(txData.txHex, this.addrDataRef, [txData.inputsSat]);
            txData.txID = await TagG.aBroadcast(signedTXHex);
            console.log({ txID: txData.txID });
            this.baseSat -= isUnconfSatUsed ? 0 : txData.inputsSat;
            this.baseUnconfSat += isUnconfSatUsed ? txData.oturiSat - txData.inputsSat : txData.oturiSat;
            await aAfterSendTX(txData);
            setAmounts();
            this.update();
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `トランザクションのブロードキャストに失敗しました。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}` : `Failed to broadcast tx. hint: ${error}`), 'error');
            return;
        }
        finally { TagG.bodySpinner.stop() }
        myswal({
            title: 'Success',
            text: (TagG.jp ? `トランザクションを送信しました。しばらくするとブロックチェーンに反映されます\n\nTxID: ${txData.txID}` : `Tx successfuly sent. It will be reflected to the blockchain after a while.\n\nTxID: ${txData.txID}`),
            icon: 'success',
            buttons: {},
        });
    };
    const aPrepareBaseassetSend = async(satoshi, toAddr, isAll) => {
        try {
            TagG.bodySpinner.start();
            if (!this.isUTXOsFresh) await aUpdateData(true, false);
            else if (satoshi > (TagG.confNum ? this.baseSat : this.baseSat + this.baseUnconfSat)) await aUpdateData(true, false);
            const confirmedUTXOs = this.utxos.filter(utxo => (utxo.confirmations >= TagG.confNum));
            let selectResult;
            if (isAll) {
                const targets = [{ address: toAddr }];
                selectResult = TagG.coinselectSplit(confirmedUTXOs, targets, TagG.FEESAT_PERBYTE);
            }
            else {
                const targets = [{ address: toAddr, value: satoshi }];
                selectResult = TagG.coinselect(confirmedUTXOs, targets, TagG.FEESAT_PERBYTE);
            }
            const { inputs, outputs, fee } = selectResult;
            if (!inputs) return showWindowError(TagG.jp ? `承認数${TagG.confNum}以上の${baseAsset}が足りません` : `No ${baseAsset} with confirmation >= ${TagG.confNum}`);
            if (!outputs) throw 'coinselect returns no outputs';
            for (const output of outputs) {
                if (!output.address) output.address = isAll ? toAddr : this.address;
            }
            const inputsSat = inputs.reduce((sum, input) => (sum + input.value), 0);
            const sendSat = isAll ? inputsSat - fee : satoshi;
            const baseSendData = {};
            baseSendData.inputs = inputs;
            baseSendData.outputs = outputs;
            baseSendData.amount = sendSat / TagG.SATOSHI_RATIO;
            baseSendData.feeAmount = fee / TagG.SATOSHI_RATIO;
            baseSendData.toAddr = toAddr;
            baseSendData.inputsSat = inputsSat;
            baseSendData.inputsUnconfSat = inputs.filter(input => input.confirmations === 0).reduce((sum, input) => (sum + input.value), 0);
            baseSendData.oturiSat = outputs.filter(output => output.address === this.address).reduce((sum, output) => (sum + output.value), 0);
            console.log(baseSendData);
            clearWindowMessages();
            aConfBaseassetSend(baseSendData);
        }
        catch (error) {
            console.error(error);
            showWindowError(TagG.jp?
            `送金の準備に失敗しました。トランザクション手数料も必要なので全部を引き出すなら[ALL]を選んでください。値の調整やデータの更新などいろいろ試してもうまくいかない場合はサーバの調子が悪いかもしれないのでご連絡ください。ヒント：${error}`:
            `Failed to prepare tx. hint: ${error}`);
            return;
        }
        finally { TagG.bodySpinner.stop() }
    }
    const aConfBaseassetSend = async(baseSendData) => {
        const confTitle = TagG.jp ? `${baseAsset}を送る` : `Send ${baseAsset}`;
        const confText = TagG.jp?
        `${commafy(baseSendData.amount)} ${baseAsset} を以下のアドレスに送ります。トランザクション手数料が ${commafy(baseSendData.feeAmount)} ${baseAsset} かかりますがOKですか？\n\n${baseSendData.toAddr}`:
        `${commafy(baseSendData.amount)} ${baseAsset} will be sent to the following address. You will be charged a tx fee of ${commafy(baseSendData.feeAmount)} ${baseAsset}, OK?`;
        const ok = await myswal({
            title: confTitle,
            text: confText,
            button: { text: 'OK', closeModal: false },
            className: 'swalcustom-singlebutton-center',
        });
        if (!ok) return;
        aSendBaseasset(baseSendData);
    };
    const aSendBaseasset = async(baseSendData) => {
        try {
            TagG.bodySpinner.start();
            this.isUTXOsFresh = false;
            baseSendData.txID = await TagG.aBroadcastInOut(baseSendData.inputs, baseSendData.outputs, this.addrDataRef);
            console.log({ txID: baseSendData.txID });
            this.baseSat -= baseSendData.inputsSat - baseSendData.inputsUnconfSat;
            this.baseUnconfSat += baseSendData.oturiSat - baseSendData.inputsUnconfSat;
            if (this.refs.baseasset_send_amount) this.refs.baseasset_send_amount.setValue('');
            setAmounts();
            this.update();
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `トランザクションのブロードキャストに失敗しました。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}` : `Failed to broadcast tx. hint: ${error}`), 'error');
            return;
        }
        finally { TagG.bodySpinner.stop() }
        myswal({
            title: 'Success',
            text: (TagG.jp ? `トランザクションを送信しました。しばらくするとブロックチェーンに反映されます\n\nTxID: ${baseSendData.txID}` : `Tx successfuly sent. It will be reflected to the blockchain after a while.\n\nTxID: ${baseSendData.txID}`),
            icon: 'success',
            buttons: {},
        });
    };
    const aUpdateData = async(isBaseAssetOnly, withSpin) => {
        try {
            if (withSpin) TagG.bodySpinner.start();
            const data = this.addrDataRef;
            await Promise.all([
                TagG.aUpdateBaseAssetBalances([data]),
                isBaseAssetOnly || TagG.aUpdateAssetBalances([data]),
            ]);
            setData(data);
            this.update();
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `データの取得に失敗しました。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}` : `Failed to fetch data. hint: ${error}`), 'error');
        }
        finally { TagG.bodySpinner.stop() }
    };
    const setData = (data) => {
        this.addrDataRef = data;
        const {
            index, address, addrType, utxos, baseSat, baseUnconfSat,
            builtinSat, builtinUnconfSat, builtinEscrSat, assets, dispensers, label,
            labelStorageKey, styleStorageKey,
        } = data;
        this.index = index;
        this.address = address;
        this.addrType = addrType;
        this.baseSat = baseSat;
        this.baseUnconfSat = baseUnconfSat;
        this.builtinSat = builtinSat;
        this.builtinUnconfSat = builtinUnconfSat;
        this.builtinEscrSat = builtinEscrSat;
        this.utxos = utxos;
        this.isUTXOsFresh = true;
        this.labelStorageKey = labelStorageKey;
        this.styleStorageKey = styleStorageKey;
        this.label = label || TagG.getDefaultAddrLabel(this.index);
        setAssets(assets);
        setDispensers(dispensers);
        setAmounts();
        setIssueWindowControls();
        setAddrDividendWindowControls();
        setAddrDispenserWindowControls();
    };
    const setAssets = (assets) => {
        assets.sort((assetA, assetB) => {
            if (assetA.isOwn && !assetB.isOwn) return -1;
            if (!assetA.isOwn && assetB.isOwn) return 1;
            let [nameA, nameB] = [assetA.name.toLowerCase(), assetB.name.toLocaleLowerCase()];
            if (nameA[0] === 'a') nameA = `~${nameA}`;
            if (nameB[0] === 'a') nameB = `~${nameB}`;
            if (assetA.group) nameA = `~~G${assetA.group}${nameA}`;
            if (assetB.group) nameB = `~~G${assetB.group}${nameB}`;
            if (nameA < nameB) return -1;
            if (nameA > nameB) return 1;
            return 0;
        });
        for (const asset of assets) {
            asset.onClickSendButton = () => { openSendAssetWindow(asset) };
            asset.onClickMoreButton = () => { aOpenAssetDetailWindow(asset) };
        }
        const styleAssetName = Util.loadStorage(this.styleStorageKey);
        const isStyleAssetIncluded = assets.some(asset => (asset.name === styleAssetName));
        if (isStyleAssetIncluded) switchTheme(styleAssetName, true);
        this.assets = assets;
        search(this.searchedQuery);
    };
    const setDispensers = (dispensers) => {
        dispensers.sort((dpsrA, dpsrB) => (dpsrA.assetName > dpsrB.assetName ? 1 : -1));
        for (const dispenser of dispensers) {
            let assetData = this.assets.find(_assetData => (_assetData.asset === dispenser.asset));
            if (dispenser.asset === builtinAsset) assetData = getBuiltinAssetData();
            const message = TagG.jp?
            `1クチあたり ${dispenser.unitBaseAmount} ${baseAsset} → ${dispenser.unitAssetAmount} ${dispenser.assetName} で販売中。残りの在庫は ${dispenser.remainingAmount} 枚（${dispenser.zaikoNum} クチ）です`:
            `Unit: ${dispenser.unitBaseAmount} ${baseAsset} → ${dispenser.unitAssetAmount} ${dispenser.assetName}\n\nRemaining: ${dispenser.remainingAmount} ${dispenser.assetName} (${dispenser.zaikoNum} units)`;
            dispenser.dispenserLabel = `${dispenser.unitAssetAmount} ${dispenser.assetInfo.easyLabel} / ${dispenser.unitBaseAmount} ${baseAsset}`;
            dispenser.onClick = async() => {
                const doDelete = await myswal({
                    title: TagG.jp ? `${dispenser.assetInfo.longLabel} の自動販売` : `${dispenser.assetInfo.longLabel} dispenser`,
                    text: message,
                    button: { text: (TagG.jp ? '自動販売を停止する' : `STOP`), closeModal: false },
                });
                if (!doDelete) return;
                aTemplateProcessTX(() => ({ assetData }), aPrepareDeleteDispenser, afterDeleteDispenser);
            };
        }
        this.dispensers = dispensers;
    };
    const setAmounts = () => {
        const ratio = TagG.SATOSHI_RATIO;
        this.baseAmount = this.baseSat / ratio;
        this.baseUnconfAmount = this.baseUnconfSat / ratio;
        this.builtinAmount = this.builtinSat / ratio;
        this.builtinUnconfAmount = this.builtinUnconfSat / ratio;
        this.builtinEscrAmount = this.builtinEscrSat / ratio;
        for (const asset of this.assets) {
            const assetRatio = asset.isDivisible ? ratio : 1;
            asset.amount = asset.satoshi / assetRatio;
            asset.unconfAmount = asset.unconfSat / assetRatio;
            asset.escrAmount = asset.escrSat / assetRatio;
            if (asset.issuedSat) asset.issuedAmount = asset.issuedSat / assetRatio;
        }
    };
    const setIssueWindowControls = () => {
        const parentAssetRegExp = new RegExp(this.assetPattern);
        const parentSelectItems = this.assets
            .filter(asset => (asset.isOwn && asset.asset.match(parentAssetRegExp)))
            .map(asset => ({ value: asset.asset, label: asset.asset }));
        const nameRadioItems = [];
        if (this.builtinSat >= TagG.ISSUEFEE_NAMED * TagG.SATOSHI_RATIO)
            nameRadioItems.push({ value: ASSETNAME_MODES.CUSTOM, label: `${TagG.jp ? 'カスタム' : 'Named'} (${TagG.ISSUEFEE_NAMED}${builtinAsset})` });
        if (this.builtinSat >= TagG.ISSUEFEE_SUB * TagG.SATOSHI_RATIO && parentSelectItems.length > 0)
            nameRadioItems.push({ value: ASSETNAME_MODES.SUBASSET, label: `${TagG.jp ? 'サブアセット' : 'Subasset'} (${TagG.ISSUEFEE_SUB}${builtinAsset})` });
        if (this.builtinSat >= TagG.ISSUEFEE_NFT * TagG.SATOSHI_RATIO)
            nameRadioItems.push({ value: ASSETNAME_MODES.NFT, label: `NFT (${TagG.ISSUEFEE_NFT}${builtinAsset})` });
        if (this.builtinSat >= TagG.ISSUEFEE_NUMERIC * TagG.SATOSHI_RATIO)
            nameRadioItems.push({ value: ASSETNAME_MODES.NUMERIC, label: `${TagG.jp ? 'ナンバー' : 'Numeric'} (${TagG.ISSUEFEE_NUMERIC}${builtinAsset})` });
        this.issueWindow.parentSelectItems = parentSelectItems;
        this.issueWindow.nameRadioItems = nameRadioItems;
        this.issueWindow.isBuiltinSatEnough = (this.builtinSat >= TagG.ISSUEFEE_NAMED * TagG.SATOSHI_RATIO);
        this.issueWindow.nameMode = nameRadioItems[0].value;
        this.issueWindow.recreateFlag = false;
    };
    const setAddrDividendWindowControls = () => {
        this.addrDividendWindow.targetSelectItems = this.assets
            .filter(asset => asset.isOwn).map(asset => ({ value: asset.asset, label: asset.longLabel }));
        this.addrDividendWindow.distAssetSelectItems = this.assets
            .filter(asset => asset.satoshi).map(asset => ({ value: asset.asset, label: asset.longLabel }));
        if (this.builtinSat) this.addrDividendWindow.distAssetSelectItems.unshift({ value: builtinAsset, label: builtinAsset });
        this.addrDividendWindow.recreateFlag = false;
    };
    const setAddrDispenserWindowControls = () => {
        this.addrDispenserWindow.assetSelectItems = this.assets
            .filter(asset => asset.satoshi).map(asset => ({ value: asset.asset, label: asset.longLabel }));
        if (this.builtinSat) this.addrDispenserWindow.assetSelectItems.unshift({ value: builtinAsset, label: builtinAsset });
        this.addrDispenserWindow.recreateFlag = false;
    };
    const setNumericName = () => {
        if (this.refs.issue_numericname) {
            const upperDigits = Util.randomInt(954289567, 184467440737).toString();
            const lowerDigits = Util.zeroPadding(Util.randomInt(0, 99999999), 8);
            const numericName = `A${upperDigits}${lowerDigits}`;
            this.refs.issue_numericname.setValue(numericName);
        }
    };
    const setFixWindowValues = () => {
        if (this.refs.fix_description) this.refs.fix_description.setValue(this.assetDetail.description);
        const cardData = this.assetDetail.cardData;
        if (cardData) {
            if (this.refs.fix_card_name) this.refs.fix_card_name.setValue(cardData.cardName);
            if (this.refs.fix_card_ownername) this.refs.fix_card_ownername.setValue(cardData.ownerName);
            if (this.refs.fix_card_description) this.refs.fix_card_description.setValue(cardData.description);
            if (this.refs.fix_card_tags) this.refs.fix_card_tags.setValue(cardData.tags);
        }
        if (this.fixWindow.mode === ASSETFIX_MODES.SETATTRIBUTES) {
            if (this.assetDetail.appData?.attributes) {
                this.fixWindow.attrIndices = this.assetDetail.appData.attributes.map((_, i) => i);
                this.update();
                let index = 0;
                for (const { name, value } of this.assetDetail.appData.attributes) {
                    this.refs[`fix_attr_name_${index}`].setValue(name);
                    this.refs[`fix_attr_value_${index}`].setValue(value);
                    index++;
                }
            }
            else {
                this.fixWindow.attrIndices = [0];
                this.update();
            }
        }
    };
    const addMultisend = (assetData, satoshi, address, memoMode, memo) => {
        const multisendData = this.multisendData;
        const isDuplicated = multisendData.list.some(item => (item.assetData.asset === assetData.asset && item.toAddr === address));
        if (isDuplicated) return showWindowError(TagG.jp ? 'だめです。同じトークン&宛先アドレスの組み合わせがすでにリストされています' : 'Oops! This token & address is already listed.');
        multisendData.memoMode = memoMode;
        multisendData.memo = memo;
        const amount = TagG.qty2amount(satoshi, assetData.isDivisible);
        let addrHead, addrMid, addrTail;
        if (address.indexOf(TagG.NETWORK.bech32) === 0) {
            const [prefix, body] = address.split('1');
            [addrHead, addrMid, addrTail] = [`${prefix}1${body.slice(0, 2)}`, body.slice(2, body.length - 4), body.slice(-4)];
        }
        else [addrHead, addrMid, addrTail] = [address.slice(0, 3), address.slice(3, address.length - 4), address.slice(-4)];
        const onClickRemoveButton = () => {
            multisendData.list = multisendData.list
                .filter(item => !(item.assetData.asset === assetData.asset && item.toAddr === address));
            this.update();
        };
        const multisendItem = { assetData, satoshi, amount, toAddr: address, addrHead, addrMid, addrTail, onClickRemoveButton };
        multisendData.list.push(multisendItem);
        return true;
    };
    const setupAddrbook = () => {
        const addrbook = [];
        for (const item of TagG.addrbook) {
            const onClickAddrbookItem = () => fillAddrInput(item.address, item.memo);
            addrbook.push({ ...item, onClickAddrbookItem });
        }
        this.addrbook = addrbook;
    };
    const fillAddrInput = (address, memo) => {
        if (this.refs.assetsend_window.parentElement) {
            this.refs.asset_send_addr.setValue(address);
            if (memo) {
                this.refs.asset_send_memoradio.setValue(MEMO_MODES.TEXT);
                this.sendWindow.onChangeMemoRadio(MEMO_MODES.TEXT);
                this.refs.asset_send_memo.setValue(memo);
            }
            else {
                this.refs.asset_send_memoradio.setValue(MEMO_MODES.NONE);
                this.sendWindow.onChangeMemoRadio(MEMO_MODES.NONE);
            }
        }
        if (this.refs.baseassetsend_window.parentElement) this.refs.baseasset_send_addr.setValue(address);
    };
    const search = (query) => {
        this.searchedQuery = query;
        let isOR = false;
        let words = query.split(/\s+OR\s+/).map(word => word.trim()).filter(word => word);
        if (words.length > 1) isOR = true;
        else words = query.split(/\s+/).filter(word => word);
        let filteredList;
        if (words.length === 0) filteredList = this.assets.slice();
        else {
            if (isOR) {
                filteredList = this.assets.filter(asset => {
                    for (const word of words) {
                        if (asset.searchText.includes(word)) return true;
                    }
                    return false;
                });
            }
            else {
                filteredList = this.assets.filter(asset => {
                    for (const word of words) {
                        if (!asset.searchText.includes(word)) return false;
                    }
                    return true;
                });
            }
        }
        if (this.refs.search_input && this.refs.search_input.getValue() !== query) this.refs.search_input.setValue(query);
        const FIRST = 30;
        const ONCE = 300;
        this.assetsToShow = filteredList.slice(0, FIRST);
        this.update();
        let length = FIRST;
        const showMore = () => {
            if (length >= filteredList.length || query !== this.searchedQuery) return;
            length += ONCE;
            this.assetsToShow = filteredList.slice(0, length);
            this.update();
            setTimeout(showMore, 0);
        };
        setTimeout(showMore, 0);
    };
    const switchTheme = (assetName, isSet) => {
        const asset2themeClass = {
            'MPLT.AJISAI': 'theme-ajisai',
            'MPLT.MIKAN': 'theme-mikan',
            'MPLT.PTRN01': 'theme-ptrn01',
            'MPLT.GRAD01': 'theme-grad01',
            'MOTATOKEN': 'theme-mmt01',
            'MPLTARH.A': 'theme-arh01',
            'NNSNPLT': 'theme-nns01',
            'MPLTVOXEL': 'theme-nzt01',
            'MPLCHOCA': 'theme-ckt01',
            'MPLTYAGI': 'theme-yag01',
            'MPLTYAGINI': 'theme-yag02',
            'RKGK.MPST01': 'theme-rkg01',
            'ROOMBA': 'theme-nmm01',
            'NFTCHAN.ST1': 'theme-kdm01',
            'GEMST.A': 'theme-gem01',
            'GEMST.B': 'theme-gem02',
            'HETABUSTYL': 'theme-hms01',
            'CUSTOM.STYLE': 'theme-sample',
        };
        const themeClass = asset2themeClass[assetName] || null;
        if (isSet || this.themeClass !== themeClass) {
            this.themeClass = themeClass;
            Util.saveStorage(this.styleStorageKey, assetName);
        }
        else {
            this.themeClass = null;
            Util.saveStorage(this.styleStorageKey, null);
        }
        this.update();
    };
    const getBiggestUTXO = () => {
        const confirmedUTXOs = this.utxos.filter(utxo => (utxo.confirmations >= TagG.confNum));
        if (confirmedUTXOs.length === 0) return null;
        const biggestUTXO = confirmedUTXOs.reduce((biggest, utxo) => (utxo.value > biggest.value ? utxo : biggest));
        return biggestUTXO;
    };
    const openAsset2AddrHelp = () => {
        if (TagG.jp) myswal({
            title: 'アドレス変換ボタン',
            text: 'トークン名を入力してこのボタンを押すと、トークンのオーナーのアドレスに変換されます。べんり！',
            button: { text: '戻る', closeModal: false },
            isCutin: true,
        });
        else myswal({
            title: 'Token → Owner Button',
            text: 'Enter the token name and press this button to convert it to the address of the token owner. So convenient!',
            button: { text: 'OK', closeModal: false },
            isCutin: true,
        });
    };
    const clearWindowMessages = () => {
        this.windowSuccessText = '';
        this.windowErrorText = '';
        this.update();
    };
    const showWindowSuccess = (message) => {
        this.windowSuccessText = message;
        this.windowErrorText = '';
        this.update();
    };
    const showWindowError = (message) => {
        this.windowSuccessText = '';
        this.windowErrorText = message;
        this.update();
    };
    const formatMonacardV2Params = (cardName, ownerName, description, tagStr, cid) => ({
        name: cardName,
        owner: ownerName,
        desc: description,
        tag: tagStr,
        cid,
        ver: '2',
    });
    const getBuiltinAssetData = () => {
        return {
            asset: builtinAsset,
            name: builtinAsset,
            shortenName: builtinAsset,
            easyLabel: builtinAsset,
            longLabel: builtinAsset,
            satoshi: this.builtinSat,
            amount: this.builtinAmount,
            isOwn: false,
            owner: null,
            isDivisible: true,
            isReassignable: true,
            isListed: true,
            isLocked: true,
        };
    };
    init();
});
riot.tag2('addrs-window', '<div if="{TagG.firstAddrLabel}" class="addrswindow-bottomfloat" onclick="{onClickFloat}"><i class="material-icons">{TagG.isReadOnlyMode ? \'lock\' : \'account_balance_wallet\'}</i>&ensp;{TagG.firstAddrLabel}</div> <div ref="swalwindows_container" show="{false}"> <div ref="addrs_window" class="addrswindow-swalwindow"> <div class="title">{label}</div> <div class="qrcode-frame"><util-qrcode ref="addr_qrcode"></util-qrcode></div> <div class="basic addr {addr-bech32: addrType === ADDR_TYPE.P2WPKH}">{address}</div> <div class="basic hashcolors"><util-hashcolors seed_value="{address}"></util-hashcolors></div> <div class="balances"> <div class="balance">{baseAmountStr}<span class="currency-unit">{TagG.BASECHAIN_ASSET}</span></div> <div class="balance">{builtinAmountStr}<span class="currency-unit">{TagG.BUILTIN_ASSET}</span></div> </div> <div class="section"><div class="bottombutton-frame"> <material-button label="{TagG.BASECHAIN_ASSET + \' NW\'}" icon="search" is_link="true" link_href="{baseExplorerURL}" link_target="_blank" reload_opts="true"></material-button> <material-button label="MPCHAIN" icon="search" is_link="true" link_href="{monapartyExplorerURL}" link_target="_blank" reload_opts="true"></material-button> </div></div> <div class="prevnext-button prev-button"><material-iconbutton icon="navigate_before" on_click="{onClickPrevButton}"></material-iconbutton></div> <div class="prevnext-button next-button"><material-iconbutton icon="navigate_next" on_click="{onClickNextButton}"></material-iconbutton></div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    this.ADDR_TYPE = ADDR_TYPE;
    this.onClickFloat = () => {
        if (!TagG.addrsData) return;
        setIndex(0);
        Util.aOpenContentSwal(this.refs.addrs_window);
    };
    this.onClickPrevButton = () => {
        const addrNum = TagG.addrsDataMaxIndex + 1;
        const newIndex = (this.index + addrNum - 1) % addrNum;
        setIndex(newIndex);
    };
    this.onClickNextButton = () => {
        const addrNum = TagG.addrsDataMaxIndex + 1;
        const newIndex = (this.index + 1) % addrNum;
        setIndex(newIndex);
    };
    this.on('mount', async() => {
        const container = this.refs.swalwindows_container;
        while (container && container.firstChild) container.removeChild(container.firstChild);
    });
    const setIndex = (index) => {
        const addrData = TagG.addrsData[index];
        this.index = index;
        this.label = addrData.label || TagG.getDefaultAddrLabel(index);
        this.address = addrData.address;
        this.addrType = addrData.addrType;
        this.baseAmountStr = Util.commafy(TagG.qty2amount(addrData.baseSat, true));
        if (addrData.baseUnconfSat) this.baseAmountStr += `(+${Util.commafy(TagG.qty2amount(addrData.baseUnconfSat, true))})`;
        this.builtinAmountStr = Util.commafy(TagG.qty2amount(addrData.builtinSat, true));
        this.baseExplorerURL = TagG.ADDR_EXPLORERURL_FORMAT.replace('{ADDR}', this.address);
        this.monapartyExplorerURL = TagG.ADDR_PARTYEXPURL_FORMAT.replace('{ADDR}', this.address);
        this.refs.addr_qrcode.setValue(this.address);
        this.update();
    };
});
riot.tag2('appconnect-page', '<div class="app-page {connecting: appURL}"> <div class="page-header"> <i class="material-icons page-icon">cable</i> <h2 class="page-title">AppConnect</h2> <div class="page-helpbutton-frame"> <material-iconbutton if="{appURL}" icon="block" on_click="{onClickDisconnectButton}"></material-iconbutton> <material-iconbutton if="{!appURL}" icon="help_outline" on_click="{onClickPageHelpButton}"></material-iconbutton> </div> </div> <div class="page-content"> <div class="connect-frame"> <div if="{!appURL}"> <div if="{!addrsLoaded}" class="nologin-message">{TagG.jp ? \'AppConnectを使用するにはWalletを開いてください\' : \'To use AppConnect, open your wallet.\'}</div> <div class="connectselect-frame"> <div class="input-frame" if="{addrsLoaded}"><material-select ref="addr_select" hint="Address" is_fullwidth="true" is_menuwidth_fixed="true" items="{TagG.addrSelectItems}" default_value="{TagG.addrSelectDefaultValue}"></material-select></div> <div class="app-list"> <div each="{listedApps}" class="app-list-item"> <img riot-src="{thumbURL}" class="thumbnail" width="300" height="300" loading="lazy"> <div class="label-frame"> <div class="title">{name}</div> <div class="description">{description}</div> </div> <div class="clickable" onclick="{onClick}"></div> </div> </div> <div class="input-frame"> <material-textfield ref="appurl_input" hint="Custom App URL" on_enter="{onClickAppURLEnterButton}"></material-textfield> <div class="enterbutton-frame"><material-iconbutton icon="arrow_forward" on_click="{onClickAppURLEnterButton}"></material-iconbutton></div> </div> </div> </div> <iframe if="{appURL}" ref="app_frame" title="External Application" riot-src="{appURL}" loading="lazy" referrerpolicy="origin" sandbox="allow-downloads allow-forms allow-modals allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts"> </iframe> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    const LISTED_APPS = [
        {
            name: 'GoriFi',
            url: 'https://gorifi.komikikaku.com',
            origin: 'https://gorifi.komikikaku.com',
            description: 'MonapartyのDEX体験をハッピーにするゴリファイです',
            thumbURL: TagG.RELATIVE_URL_BASE + 'img/gorifi.png',
        },
        {
            name: 'monanosu space',
            url: 'https://monanosu.kotaroo.work/space',
            origin: 'https://monanosu.kotaroo.work',
            description: 'モナカードな空間',
            thumbURL: 'https://cdn.komikikaku.com/extappimg/monanosu_space.png',
        },
        {
            name: 'Knights of Monadom',
            url: 'https://knightsofmonadom.monatoka.com',
            origin: 'https://knightsofmonadom.monatoka.com',
            description: 'ChatGPTとモナカードの融合。新感覚ゲーム ／ Fusion of ChatGPT and Monacard. A new type of game.',
            thumbURL: 'https://cdn.komikikaku.com/extappimg/monadom.jpg',
        },
        {
            name: 'モナレッジ',
            url: 'https://monaledge.com',
            origin: 'https://monaledge.com',
            description: '記事を書いてMONAがもらえる！気に入った記事にMONAが送れる！',
            thumbURL: 'https://cdn.komikikaku.com/extappimg/monaledge.jpg',
        },
        {
            name: 'Ask Mona 3.0',
            url: 'https://web3.askmona.org',
            origin: 'https://web3.askmona.org',
            description: 'Ask Mona 3.0では、掲示板形式で簡単にMONAをやり取りすることができます。質問したり、答えたりしてMONAを手に入れてください。',
            thumbURL: 'https://cdn.komikikaku.com/extappimg/askmona.png',
        },
        {
            name: 'Monacute',
            url: 'https://monacute.art',
            origin: 'https://monacute.art',
            description: 'AIでつくられた、とってもキュートなNFT。',
            thumbURL: 'https://cdn.komikikaku.com/extappimg/monacute.jpg',
        },
        {
            name: 'もなちぇん',
            url: 'https://monachen.monatoka.com',
            origin: 'https://monachen.monatoka.com',
            description: 'モナコインでJPYCを買える・売れる！ ／ You can buy and sell JPYC for MONACOIN!',
            thumbURL: 'https://cdn.komikikaku.com/extappimg/monachen.png',
        },
        {
            name: 'もなこっと',
            url: 'https://monacotto.monatoka.com',
            origin: 'https://monacotto.monatoka.com',
            description: 'モナカードの売買が簡単！ ／ You can buy and sell monacards easily!',
            thumbURL: 'https://cdn.komikikaku.com/extappimg/monacotto.png',
        },
        {
            name: 'monabless',
            url: 'https://monabless.monatrust.com',
            origin: 'https://monabless.monatrust.com',
            description: 'May you bless tokens! ／ トークンに祝福を！',
            thumbURL: 'https://cdn.komikikaku.com/extappimg/monabless.png',
        },
        {
            name: 'monatrust',
            url: 'https://www.monatrust.com',
            origin: 'https://www.monatrust.com',
            description: 'Trust who you trust in.',
            thumbURL: 'https://cdn.komikikaku.com/extappimg/monatrust.png',
        },
        {
            name: 'Mona Tools',
            url: 'https://mona-tools.com',
            origin: 'https://mona-tools.com',
            description: 'MonapartyのDispenserを検索したり便利なツールです。',
            thumbURL: 'https://cdn.komikikaku.com/extappimg/monatools.png',
        },
        {
            name: 'AppConnect Attacks',
            url: 'https://monapalette.komikikaku.com/appconnect_sample',
            origin: 'https://monapalette.komikikaku.com',
            description: (TagG.jp ? '悪意あるアプリによるなりすまし攻撃の例を掲載しています。確認しておいてください。' : 'This page contains examples of spoofing attacks by malicious apps. Please check it out.'),
            thumbURL: TagG.RELATIVE_URL_BASE + 'img/bikkuri.png',
        },
    ];

    const init = () => {
        this.appURL = null;
        this.selectedOrigin = 'BLOCK';
        this.listedApps = LISTED_APPS.map(app => {
            const onClick = () => { openApp(app.url, app.origin) };
            return { ...app, onClick };
        });
    };
    this.onClickPageHelpButton = () => {
        myswal({
            text: (TagG.jp ? '外部のAppConnect対応アプリをモナパレットに接続して利用できます。AppConnectではMpurse互換のAPIを提供しているので、Mpurse連携アプリはわずかな修正でAppConnectにも対応することができます\n\nアプリからメッセージやトランザクションへの署名を伴う操作を要求されたときは確認ダイアログが出ます。各アプリの信頼性についてはモナパレットでは検証していないので、ダイアログの内容はよく確認しましょう。また、アプリによるなりすまし攻撃のサンプルをアプリリスト内の「AppConnect Attacks」に掲載しているので確認しておいてください' : 'External web apps can be connected to MonaPalette through AppConnect. As AppConnect provides Mpurse-compatible APIs, apps for Mpurse apps can also support AppConnect with only minor modifications.\n\nWhen an app requests an operation that involves signing a message or transaction, a confirmation dialog will appear. The reliability of each app has not been verified by MonaPalette, so check the dialog carefully. Also, please check the "AppConnect Attacks" in the app list for samples of spoofing attacks by apps.'),
            buttons: {},
        });
    };
    this.onClickDisconnectButton = () => {
        this.appURL = null;
        this.selectedOrigin = 'BLOCK';
        this.update();
    };
    this.onClickAppURLEnterButton = () => {
        const appURL = this.refs.appurl_input.getValue().trim();
        try { new URL(appURL) }
        catch (_) { return myswal('Error', (TagG.jp ? '無効なURLです' : 'Invalid URL'), 'error') }
        openApp(appURL, null);
    };
    this.on('mount', async() => {
        if (TagG.addrsData) {
            await TagG.aUpdateAddrsDataWithUI(TagG.bodySpinner);
            this.addrsLoaded = true;
            TagG.windowMessageListeners.length = 0;
            TagG.windowMessageListeners.push(onMessage);
            this.update();
        }
    });
    const openApp = (url, origin) => {
        if (TagG.addrsData) {
            const address = this.refs.addr_select.getValue();
            this.addrData = TagG.addrsData.find(data => (data.address === address));
        }
        else myswal(TagG.jp ? 'モナパレット未接続の状態でアプリを表示します。アプリからモナパレットを利用するには、先にWalletを開いてください' : 'You are accessing the app with Monapalette unconnected. To use Monapalette from the app, open your wallet first.');
        this.appURL = url;
        this.selectedOrigin = origin;
        this.update();
    };
    const onMessage = async(event) => {
        const origin = event.origin;
        if (this.selectedOrigin && origin !== this.selectedOrigin) return console.warn(`message from unknown origin: ${origin}`);
        const originHost = origin.split('//')[1];
        const appWindow = this.refs.app_frame.contentWindow;
        const addrData = this.addrData;
        const { id, method, params } = event.data;
        if (!id || !method) return console.warn('message not for AppConnect');
        try {
            let value = null;
            switch (method) {
                case 'getAddress': {
                    value = addrData.address;
                    break;
                }
                case 'sendAsset': {
                    const [toAddr, asset, amount, memoType, memoValue] = params;
                    let unsignedTXHex, inputs, outputs, feeAmount;
                    TagG.bodySpinner.start();
                    if (asset === TagG.BASECHAIN_ASSET) {
                        await TagG.aUpdateBaseAssetBalances([addrData]);
                        const targets = [{ address: toAddr, value: amount * TagG.SATOSHI_RATIO }];
                        const confirmedUTXOs = addrData.utxos.filter(utxo => (utxo.confirmations >= TagG.confNum));
                        const selectResult = TagG.coinselect(confirmedUTXOs, targets, TagG.FEESAT_PERBYTE);
                        [inputs, outputs] = [selectResult.inputs, selectResult.outputs];
                        if (!inputs) throw (TagG.jp ? `承認数${TagG.confNum}以上の${TagG.BASECHAIN_ASSET}が足りません` : `Insufficient ${TagG.BASECHAIN_ASSET} with confirmation >= ${TagG.confNum}`);
                        if (!outputs) throw 'coinselect returns no outputs';
                        for (const output of outputs) {
                            if (!output.address) output.address = addrData.address;
                        }
                        feeAmount = selectResult.fee / TagG.SATOSHI_RATIO;
                    }
                    else {
                        await TagG.aLoadAssetsInfo([asset], true);
                        const assetInfo = TagG.asset2info[asset];
                        if (!assetInfo) throw (TagG.jp ? `${asset}というトークンは存在しません` : `Token named "${asset}" does not exist.`);
                        const sendParams = {
                            'destination': toAddr,
                            'asset': assetInfo.asset,
                            'quantity': TagG.amount2qty(amount, assetInfo.isDivisible),
                            'use_enhanced_send': true,
                            'source': addrData.address,
                            'fee_per_kb': TagG.FEESAT_PERBYTE * 1000,
                            'extended_tx_info': true,
                        };
                        if (memoType === 'plain') {
                            sendParams['memo'] = memoValue;
                            sendParams['memo_is_hex'] = false;
                        }
                        if (memoType === 'hex') {
                            sendParams['memo'] = memoValue;
                            sendParams['memo_is_hex'] = true;
                        }
                        if (addrData.publicKey) sendParams['pubkey'] = addrData.publicKey.toString('hex');
                        if (TagG.confNum === 0) sendParams['allow_unconfirmed_inputs'] = true;
                        const sendData = {};
                        await TagG.aMonapartyTXCreate('create_send', sendParams, sendData);
                        unsignedTXHex = sendData.txHex;
                        feeAmount = sendData.feedustAmount;
                    }
                    TagG.bodySpinner.stop();
                    const isOK = await myswal({
                        title: (TagG.jp ? `${asset}を送る` : `Send ${asset}`),
                        text: (TagG.jp?
                        `${originHost} が以下のアドレスへの ${amount} ${asset} の送付を要求しています (トランザクション手数料: ${feeAmount} ${TagG.BASECHAIN_ASSET})\n\n${toAddr}`:
                        `${originHost} is requesting to send ${amount} ${asset} to the following address. (tx fee: ${feeAmount} ${TagG.BASECHAIN_ASSET})\n\n${toAddr}`),
                        buttons: ['Cancel', 'OK'],
                        className: 'swalcustom-okorcancel-center',
                    });
                    if (!isOK) return appWindow.postMessage({ id, error: 'User Cancelled' }, origin);
                    TagG.bodySpinner.start();
                    const signedTXHex = unsignedTXHex ? await TagG.aSignTXHex(unsignedTXHex, addrData, null) : await TagG.aSignInOut(inputs, outputs, addrData);
                    value = await TagG.aBroadcast(signedTXHex);
                    break;
                }
                case 'signMessage': {
                    const [message] = params;
                    const isOK = await myswal({
                        title: (TagG.jp ? 'メッセージへの署名' : 'Sign message'),
                        text: (TagG.jp ? `${originHost} が以下のメッセージへの署名を要求しています\n\n${message}` : `${originHost} is requesting to sign the following message.\n\n${message}`),
                        buttons: ['Cancel', 'OK'],
                        className: 'swalcustom-okorcancel-center',
                    });
                    if (!isOK) return appWindow.postMessage({ id, error: 'User Cancelled' }, origin);
                    TagG.bodySpinner.start();
                    value = await TagG.aSignMessage(message, addrData);
                    break;
                }
                case 'signRawTransaction':
                case 'sendRawTransaction': {
                    const [unsignedTXHex] = params;
                    const isOK = await myswal({
                        title: (TagG.jp ? 'トランザクションへの署名' : 'Sign tx'),
                        text: (TagG.jp ? `${originHost} が以下のトランザクションへの署名を要求しています\n\n${unsignedTXHex}` : `${originHost} is requesting to sign the following tx.\n\n${unsignedTXHex}`),
                        buttons: ['Cancel', 'OK'],
                        className: 'swalcustom-okorcancel-center',
                    });
                    if (!isOK) return appWindow.postMessage({ id, error: 'User Cancelled' }, origin);
                    TagG.bodySpinner.start();
                    const signedTXHex = await TagG.aSignTXHex(unsignedTXHex, addrData, null);
                    if (method === 'signRawTransaction') {
                        value = signedTXHex;
                        break;
                    }
                    value = await TagG.aBroadcast(signedTXHex);
                    break;
                }
                case 'counterBlock': {
                    const [cbMethod, cbParams] = params;
                    value = await Monaparty.aBlock(cbMethod, cbParams);
                    break;
                }
                case 'counterParty': {
                    const [cpMethod, cpParams] = params;
                    value = await Monaparty.aParty(cpMethod, cpParams);
                    break;
                }
                default:
                    console.error(`unsupported method: ${method}`);
                    return appWindow.postMessage({ id, error: 'AppConnect does not support this method' }, origin);
            }
            appWindow.postMessage({ id, value }, origin);
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `失敗しました。ヒント：${error}` : `Failed. hint: ${error}`), 'error');
            appWindow.postMessage({ id, error: 'Something went wrong' }, origin);
        }
        finally { TagG.bodySpinner.stop() }
    };
    init();
});
riot.tag2('appconnect-sample', '<div class="app-page"> <div class="page-header"> <i class="material-icons page-icon">science</i> <h2 class="page-title">AppConnect Attacks</h2> </div> <div class="page-content"> <div class="section-frame"> <div class="paragraph"> <span if="{TagG.jp}">AppConnectではモナパレット内に別のサイトを表示しています。悪意のあるサイトに接続してしまうと以下のような攻撃を受ける可能性があるので注意しましょう。</span> <span if="{!TagG.jp}">AppConnect shows another site in the Monapalette. Be aware that if you connect to a malicious site, you may be subject to the following attacks.</span> </div> </div> <div class="section-frame"> <div class="section-title">{TagG.jp ? \'攻撃例 1\' : \'Attack Example 1\'}</div> <div class="paragraph"> <span if="{TagG.jp}">このボタンを押すとアプリがモナパレットを装って秘密の情報を入力させようとします。ウィンドウがアプリ内のものか、外側のモナパレットのものかをよく確認しましょう。悪意のないアプリからはニーモニックや秘密鍵、モナパレット用のパスワードの入力を求められることは絶対にありません。</span> <span if="{!TagG.jp}">When you press this button, the app will pose as Monapalette and try to get you to enter secret information. Check carefully whether the window is from within the app or from the outer Monapalette. A non-malicious app will never ask you to enter a mnemonic, private key, or password for Monapalette.</span> </div> <div class="paragraph"><material-button label="ATTACK1" icon="new_releases" is_unelevated="true" on_click="{onClickAttack1Button}"></material-button></div> </div> <div class="section-frame"> <div class="section-title">{TagG.jp ? \'攻撃例 2\' : \'Attack Example 2\'}</div> <div class="paragraph"> <span if="{TagG.jp}">このボタンを押すとアプリがブラウザ組み込みのダイアログを使って秘密の情報を入力させようとします。モナパレットではブラウザ組み込みのダイアログを使ってニーモニックや秘密鍵、パスワードの入力を求めることは絶対にありません。</span> <span if="{!TagG.jp}">When you press this button, the app will try to ask you to enter your secret information using the browser built-in dialog. Monapalette will never ask you to enter mnemonics, private keys, or passwords using the browser built-in dialog.</span> </div> <div class="paragraph"><material-button label="ATTACK2" icon="new_releases" is_unelevated="true" on_click="{onClickAttack2Button}"></material-button></div> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    this.onClickAttack1Button = () => {
        const swalContent = TagG.swalContents.getPasswordWin({
            title: (TagG.jp ? 'モナパレットみたいなダイアログ' : 'Dialog disguised as the Monapalette\'s dialog'),
            text: (TagG.jp ? 'ここにヒミツの情報を入力することー！' : 'Enter your secret information.'),
        });
        Util.aOpenContentSwal(swalContent);
    };
    this.onClickAttack2Button = () => {
        window.prompt(TagG.jp ? 'ここにヒミツの情報を入力することー！' : 'Enter your secret information.')
    };
});
riot.tag2('dboard-page', '<div class="app-page"> <div class="page-header"> <i class="material-icons page-icon">chat</i> <h2 class="page-title">D-Board</h2> </div> <div class="page-content"> <div class="controls-frame"> <div if="{!addrsLoaded}" class="nologin-message">{TagG.jp ? \'投稿するにはWalletを開いてください\' : \'To post, open your wallet.\'}</div> <div if="{addrsLoaded}"> <material-iconbutton icon="send" on_click="{onClickSendMessageButton}"></material-iconbutton> <material-iconbutton icon="settings_applications" on_click="{onClickSetUserButton}"></material-iconbutton> <material-iconbutton ref="update_button" icon="refresh" on_click="{onClickUpdateButton}"></material-iconbutton> </div> <util-spinner ref="addrs_spinner"></util-spinner> </div> <div class="timeline-frame"> <div each="{messages}" class="message"> <div class="root {mentioned: isMentioned}"> <div if="{isNewMentioned}" class="newmention-sign"></div> <div if="{imageURL}" class="image-frame"><a href="{imageURL}" target="_blank"><img riot-src="{previewImageURL}" loading="lazy"></a></div> <div if="{assetThumbs}" class="assets-frame" onclick="{onClickAssets}"><div each="{assetThumbs}" class="img-frame {square: !isCard}"><img riot-src="{url}" loading="lazy"></div></div> <div if="{text}" class="text">{text + \'\\n\'}</div> <div class="footer"> <div class="information {me: TagG.addr2isMine[address]}" onclick="{() => onClickUser(address)}"> <div class="colorbar"><util-hashcolors seed_value="{address}"></util-hashcolors></div> <div class="name-and-block"> <div class="block">block {block}</div> <div class="name">{(userDict[address] && userDict[address].name) || address}</div> </div><div if="{userDict[address] && userDict[address].iconURL}" class="icon"><img riot-src="{userDict[address].iconURL}" loading="lazy"></div> </div> <div if="{addrsLoaded}" class="footer-button"><material-iconbutton icon="chat_bubble_outline" on_click="{() => onClickReplyButton(index, address)}"></material-iconbutton></div> <div if="{TagG.addr2isMine[address]}" class="footer-button second"><material-iconbutton icon="delete" on_click="{() => onClickDeleteButton(index, address)}"></material-iconbutton></div> </div> </div> <div each="{reply in replies}" class="reply {mentioned: reply.isMentioned}"> <div if="{reply.isNewMentioned}" class="newmention-sign"></div> <div if="{reply.imageURL}" class="image-frame"><a href="{reply.imageURL}" target="_blank"><img riot-src="{reply.previewImageURL}" loading="lazy"></a></div> <div if="{reply.assetThumbs}" class="assets-frame" onclick="{reply.onClickAssets}"><div each="{reply.assetThumbs}" class="img-frame {square: !isCard}"><img riot-src="{url}" loading="lazy"></div></div> <div class="text">{reply.text + \'\\n\'}</div> <div class="footer"> <div class="information" onclick="{() => onClickUser(reply.address)}"> <div class="colorbar"><util-hashcolors seed_value="{reply.address}"></util-hashcolors></div> <div class="name-and-block"> <div class="block">block {reply.block}</div> <div class="name">{(userDict[reply.address] && userDict[reply.address].name) || reply.address}</div> </div><div if="{userDict[reply.address] && userDict[reply.address].iconURL}" class="icon"><img riot-src="{userDict[reply.address].iconURL}" loading="lazy"></div> </div> <div if="{TagG.addr2isMine[reply.address]}" class="footer-button"><material-iconbutton icon="delete" on_click="{() => onClickDeleteButton(reply.index, reply.address)}"></material-iconbutton></div> </div> </div> </div> <util-spinner ref="timeline_spinner"></util-spinner> </div> </div> </div> <div ref="swalwindows_container" show="{false}"> <div ref="sendmessage_window" class="dboard-page-swalwindow"> <div if="{!sendMessageWindow.isReply}" class="title">{TagG.jp ? \'メッセージを投稿する\' : \'Post message\'}</div> <div if="{sendMessageWindow.isReply}" class="title">{TagG.jp ? \'リプライを投稿する\' : \'Post reply\'}</div> <div if="{!sendMessageWindow.isReply}" class="basic"> <span if="{TagG.jp}">D-Boardにメッセージや画像を投稿できます。投稿内容はブロックチェーン上にずっと残るので気を付けましょう</span> <span if="{!TagG.jp}">You can post messages and images to D-Board. Be aware that your postings will remain on the blockchain forever.</span> </div> <div if="{sendMessageWindow.isReply}" class="basic"> <span if="{TagG.jp}">{sendMessageWindow.replyTo} の書き込みにリプライを付けます。投稿内容はブロックチェーン上にずっと残るので気を付けましょう</span> <span if="{!TagG.jp}">You can reply to the {sendMessageWindow.replyTo} post. Be aware that your postings will remain on the blockchain forever.</span> </div> <div class="basic"><material-select if="{addrsLoaded}" ref="sendmessage_addr" hint="Address" is_fullwidth="true" is_menuwidth_fixed="true" items="{TagG.addrSelectItems}" default_value="{TagG.addrSelectDefaultValue}"></material-select></div> <div class="basic"><material-textfield ref="message_input" hint="Message" is_textarea="true" textarea_rows="2"></material-textfield></div> <div class="basic"><material-radiobuttons ref="imagetype_radio" items="{sendMessageWindow.imageRadioItems}" on_change="{sendMessageWindow.onChangeImageRadio}"></material-radiobuttons></div> <div class="basic" if="{sendMessageWindow.imageMode === IMAGE_MODES.URL}"><material-textfield ref="image_input" hint="Imgur Direct Link"></material-textfield></div> <div class="basic" if="{sendMessageWindow.imageMode === IMAGE_MODES.ASSET}"> <div class="upper-helper">{TagG.jp ? \'カンマ(,)区切りで複数指定\' : \'Multiple tokens can be set separated by commas.\'}</div> <material-textfield ref="image_input" hint="Tokens"></material-textfield> </div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="bottombutton-frame"><material-button label="{TagG.jp ? \'投稿する\' : \'POST\'}" icon="send" on_click="{sendMessageWindow.onClickSendButton}" is_unelevated="true" is_fullwidth="true"></material-button></div> </div> <div ref="setuser_window" class="dboard-page-swalwindow"> <div class="title">{TagG.jp ? \'ユーザーを設定する\' : \'Set address profile\'}</div> <div class="basic"> <span if="{TagG.jp}">D-Board上でアドレス毎に表示される名前とアイコンを設定できます。両方とも空欄のまま設定すれば、現在の設定をクリアできます</span> <span if="{!TagG.jp}">You can set profile of each address shown on D-Board. Set them both blank to clear current profile.</span> </div> <div class="basic"><material-select if="{addrsLoaded}" ref="setuser_addr" hint="Address" is_fullwidth="true" is_menuwidth_fixed="true" items="{TagG.addrSelectItems}" default_value="{TagG.addrSelectDefaultValue}"></material-select></div> <div class="basic"><material-textfield ref="username_input" hint="Name"></material-textfield></div> <div class="basic"><material-textfield ref="usericon_input" hint="Icon (Imgur Direct Link)"></material-textfield></div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="bottombutton-frame"><material-button label="{TagG.jp ? \'設定する\' : \'SET\'}" icon="settings_applications" on_click="{setUserWindow.onClickSetButton}" is_unelevated="true" is_fullwidth="true"></material-button></div> </div> <div ref="broadcast_conf_window" class="dboard-page-swalwindow"> <div class="title">{TagG.jp ? \'ブロードキャストする\' : \'Broadcast\'}</div> <div class="basic"> <span if="{TagG.jp}">以下のデータをブロードキャストします。トランザクション手数料<span if="{broadcastData.dustSat}">など</span>が {commafy(broadcastData.feedustAmount)} {TagG.BASECHAIN_ASSET} かかりますがOKですか？</span> <span if="{!TagG.jp}">The following data will be broadcast. You will be charged a fee of {commafy(broadcastData.feedustAmount)} {TagG.BASECHAIN_ASSET}, OK?</span> </div> <div class="basic data">{broadcastData.message}</div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="bottombutton-frame"> <material-button label="OK" on_click="{broadcastConfWindow.onClickOKButton}" is_unelevated="true" is_fullwidth="true"></material-button> </div> </div> <div ref="userdetail_window" class="dboard-page-swalwindow userdetail-swalwindow"> <div class="title">{userDetailWindow.name || \'Anonymous User\'}<img if="{userDetailWindow.iconURL}" riot-src="{userDetailWindow.iconURL}" loading="lazy"></div> <div if="{userDetailWindow.address}" class="basic qrcode-frame"><util-qrcode ref="addr_qrcode" qr_value="{userDetailWindow.address}"></util-qrcode></div> <div class="basic addr">{userDetailWindow.address}</div> <div class="basic hashcolors"><util-hashcolors seed_value="{userDetailWindow.address}"></util-hashcolors></div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    const DBOARD_PREFIX = '@b.';
    const DMESSAGE_PREFIX = '@b.m ';
    const DUSER_PREFIX = '@b.u ';
    const IMAGE_MODES = this.IMAGE_MODES = { NONE: 'NONE', URL: 'URL', ASSET: 'ASSET' };
    this.sendMessageWindow = {};
    this.setUserWindow = {};
    this.broadcastConfWindow = {};
    this.broadcastData = {};
    this.userDetailWindow = {};

    const init = () => {
        this.commafy = Util.commafy;
        this.messages = [];
        this.assetsToLoad = [];
        this.asset2isInvalid = {};
        this.userDict = {};
        this.sendMessageWindow.imageRadioItems = [
            { value: IMAGE_MODES.NONE, label: 'No Image' },
            { value: IMAGE_MODES.URL, label: 'Imgur URL' },
            { value: IMAGE_MODES.ASSET, label: 'Token Showcase' },
        ];
        this.sendMessageWindow.imageMode = IMAGE_MODES.NONE;
        this.readIndex = 0;
    };

    this.onClickSendMessageButton = () => {
        this.sendMessageWindow.isReply = false;
        this.sendMessageWindow.replyToIndex = null;
        clearWindowMessages();
        Util.aOpenContentSwal(this.refs.sendmessage_window);
    };
    this.onClickSetUserButton = () => {
        clearWindowMessages();
        Util.aOpenContentSwal(this.refs.setuser_window);
    };
    this.onClickUpdateButton = async() => {
        if (this.refs.update_button) this.refs.update_button.setDisabled(true);
        await aShowTimeline();
        setMentionFlags();
        this.update();
        setTimeout(() => {
            if (this.refs.update_button) this.refs.update_button.setDisabled(false);
        }, 10000);
    };
    this.onClickReplyButton = (replyToIndex, replyToAddr) => {
        const replyToUser = this.userDict[replyToAddr] || {};
        this.sendMessageWindow.isReply = true;
        this.sendMessageWindow.replyToIndex = replyToIndex;
        this.sendMessageWindow.replyTo = replyToUser.name || replyToAddr;
        clearWindowMessages();
        Util.aOpenContentSwal(this.refs.sendmessage_window);
    };
    this.onClickDeleteButton = async(messageIndex, walletAddr) => {
        const ok = await myswal({
            title: (TagG.jp ? 'メッセージの削除' : 'Delete message'),
            text: (TagG.jp ? 'ブロックチェーン上のデータは消せませんが、D-Board上に表示されないようにすることができます' : 'Data on the blockchain cannot be deleted, but it can be prevented from appearing on the D-Board.'),
            button: { text: (TagG.jp ? '削除する' : 'DELETE'), closeModal: false },
            className: 'swalcustom-singlebutton-center',
        });
        if (!ok) return;
        const messageData = { d: messageIndex };
        aPrepareBroadcast(walletAddr, `${DMESSAGE_PREFIX}${JSON.stringify(messageData)}`, true);
    };
    this.onClickUser = (address) => {
        const user = this.userDict[address] || {};
        this.userDetailWindow.address = address;
        this.userDetailWindow.name = user.name;
        this.userDetailWindow.iconURL = user.iconURL;
        if (this.refs.addr_qrcode) this.refs.addr_qrcode.setValue(address);
        this.update();
        Util.aOpenContentSwal(this.refs.userdetail_window);
    };
    this.sendMessageWindow.onChangeImageRadio = (value) => {
        this.sendMessageWindow.imageMode = value;
        this.update();
    };
    this.sendMessageWindow.onClickSendButton = async() => {
        let messageData = {};
        const address = this.refs.sendmessage_addr.getValue();
        const text = this.refs.message_input.getValue();
        const imageInput = this.refs.image_input ? this.refs.image_input.getValue().trim() : null;
        const replyToIndex = this.sendMessageWindow.replyToIndex;
        let imageSelector = null;
        if (!address) return showWindowError(TagG.jp ? 'だめです。アドレスを選択してね' : 'Oops! No address selected.');
        if (imageInput) {
            switch (this.sendMessageWindow.imageMode) {
                case IMAGE_MODES.URL: {
                    const imgurMatch = imageInput.match(/^https?:\/\/i\.imgur\.com\/([a-zA-Z0-9]+\.[a-zA-Z0-9]+)$/);
                    if (imgurMatch) imageSelector = '1' + imgurMatch[1];
                    else return showWindowError(TagG.jp ? 'だめです。サポートしていない形式の画像URLです' : 'Oops! This image URL is not supported.');
                    break;
                }
                case IMAGE_MODES.ASSET: {
                    const assets = imageInput.split(/\s*,\s*/).filter(ast => ast);
                    const imageCheckErrorMessage = await aCheckAssetsImage(assets);
                    if (imageCheckErrorMessage) return showWindowError(imageCheckErrorMessage);
                    imageSelector = '2' + assets.join(',');
                    break;
                }
            }
        }
        if (!imageSelector && !text) return showWindowError(TagG.jp ? 'だめです。メッセージか画像のどちらかは入力してね' : 'Oops! Neither message nor image is entered.');
        if (!imageSelector && !replyToIndex) messageData = text;
        else {
            if (text) messageData.m = text;
            if (imageSelector) messageData.p = imageSelector;
            if (replyToIndex) messageData.r = replyToIndex;
        }
        aPrepareBroadcast(address, `${DMESSAGE_PREFIX}${JSON.stringify(messageData)}`);
    };
    const aCheckAssetsImage = async(assets) => {
        try {
            TagG.bodySpinner.start();
            await TagG.aLoadAssets(assets, true, true);
        }
        catch (error) { return (TagG.jp ? `データの取得に失敗しました。ヒント：${error}` : `Failed to fetch data. hint: ${error}`) }
        finally { TagG.bodySpinner.stop() }
        for (const asset of assets) {
            const assetInfo = TagG.asset2info[asset];
            if (!assetInfo) return (TagG.jp ? `"${asset}"という名前のトークンは存在しません` : `Token named "${asset}" does not exist.`);
            if (!TagG.getAssetImageURL('M', assetInfo.asset)) return (TagG.jp ? `${asset} には画像が設定されていません` : `${asset} does not have image to show.`);
        }
        return null;
    };
    this.setUserWindow.onClickSetButton = () => {
        const messageData = {};
        const address = this.refs.setuser_addr.getValue();
        const name = this.refs.username_input.getValue();
        const iconURL = this.refs.usericon_input.getValue().trim();
        if (!address) return showWindowError(TagG.jp ? 'だめです。アドレスを選択してね' : 'Oops! No address selected.');
        if (name) messageData.n = name;
        if (iconURL) {
            const imgurMatch = iconURL.match(/^https?:\/\/i\.imgur\.com\/([a-zA-Z0-9]+\.[a-zA-Z0-9]+)$/);
            if (imgurMatch) messageData.i = '1' + imgurMatch[1];
            else return showWindowError(TagG.jp ? 'だめです。サポートしていない形式の画像URLです' : 'Oops! This image URL is not supported.');
        }
        if (!name && !iconURL) {
            messageData.n = '';
            messageData.i = '';
        }
        aPrepareBroadcast(address, `${DUSER_PREFIX}${JSON.stringify(messageData)}`);
    };
    this.broadcastConfWindow.onClickOKButton = () => { aBroadCast() };

    this.on('mount', async() => {
        const container = this.refs.swalwindows_container;
        while (container && container.firstChild) container.removeChild(container.firstChild);
        initDynamicGallery();
        await Promise.all([
            aShowTimeline(),
            TagG.addrData ? TagG.aUpdateAddrsDataWithUI(this.refs.addrs_spinner) : null,
        ]);
        if (TagG.addrsData) {
            setMentionFlags();
            TagG.bodyMenu.setNotifications({});
            TagG.dboardReadIndex = this.readIndex;
            Util.saveStorage(TagG.STORAGEKEY_DBOARDREADINDEX_FORMAT.replace('{FIRST_ADDR}', TagG.addrsData[0].address), TagG.dboardReadIndex);
            this.addrsLoaded = true;
        }
        this.update();
    });
    const aShowTimeline = async() => {
        try {
            if (aShowTimeline.isLoading) return;
            aShowTimeline.isLoading = true;
            if (this.refs.timeline_spinner) this.refs.timeline_spinner.start();
            try {
                const { messages, users, invalidAssets, lastBlock } = await Util.aGetJSON(`${TagG.DBOARD_CACHEJSON}?${TagG.getCacheQuery(10)}`);
                for (const asset of invalidAssets) this.asset2isInvalid[asset] = true;
                const params = {
                    'filters': [{ field: 'text', op: 'LIKE', value: `${DBOARD_PREFIX}%` }],
                    'start_block': lastBlock + 1,
                };
                const broadcasts = await Monaparty.aCounterPartyRequest('get_broadcasts', params);
                const newMessages = broadcasts.filter(bro => bro.text.indexOf(DMESSAGE_PREFIX) === 0).sort(sortByTimeDesc);
                const newUsers = broadcasts.filter(bro => bro.text.indexOf(DUSER_PREFIX) === 0).sort(sortByTimeAsc);
                const rawMessages = [...newMessages, ...messages];
                this.readIndex = rawMessages[0]?.tx_index || 0;
                this.messages = parseDBoardMessages(rawMessages);
                updateUserDict([...users, ...newUsers]);
            }
            catch (e) {
                const rawMessages = await aGetRawMessages();
                this.readIndex = rawMessages[0]?.tx_index || 0;
                this.messages = parseDBoardMessages(rawMessages);
                updateUserDict(await aGetRawUsers());
            }
            if (!TagG.IS_LAZYLOADAVAILABLE) this.messages = this.messages.slice(0, 300);
            this.update();
            await aLoadAssets();
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `データの取得に失敗しました。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}` : `Failed to fetch data. hint: ${error}`), 'error');
        }
        finally {
            if (this.refs.timeline_spinner) this.refs.timeline_spinner.stop();
            aShowTimeline.isLoading = false;
        }
    };
    const aGetRawMessages = async() => {
        const getDMessagesParams = {
            'limit': 1000,
            'order_by': 'tx_index',
            'order_dir': 'DESC',
            'filters': [{ field: 'text', op: 'LIKE', value: `${DMESSAGE_PREFIX}%` }],
        };
        return await Monaparty.aParty('get_broadcasts', getDMessagesParams);
    };
    const aGetRawUsers = async() => {
        const getDUsersParams = {
            'limit': 1000,
            'order_by': 'tx_index',
            'order_dir': 'DESC',
            'filters': [{ field: 'text', op: 'LIKE', value: `${DUSER_PREFIX}%` }],
        };
        return await Monaparty.aParty('get_broadcasts', getDUsersParams);
    };
    const aLoadAssets = async() => {
        await TagG.aLoadAssets(this.assetsToLoad, true, true);
        for (const message of this.messages) {
            setAssetThumbs(message);
            for (const reply of message.replies) setAssetThumbs(reply);
        }
        this.update();
    };
    const setAssetThumbs = (message) => {
        if (!message.assets) return;
        message.assetThumbs = message.assets.slice(0, 4).map(asset => {
            let url = null, isCard = false;
            const assetInfo = TagG.asset2info[asset];
            if (assetInfo) {
                url = TagG.getAssetImageURL('auto', assetInfo.asset);
                if (TagG.asset2card[assetInfo.name]) isCard = true;
            }
            return { url, isCard }
        }).filter(thumb => thumb.url);
    };
    const aBroadCast = async() => {
        try {
            TagG.bodySpinner.start();
            const addrData = TagG.addrsData.find(data => (data.address === this.broadcastData.address));
            const signedTXHex = await TagG.aSignTXHex(this.broadcastData.txHex, addrData, null);
            const txID = await TagG.aBroadcast(signedTXHex);
            console.log({ txID });
            if (this.refs.message_input) this.refs.message_input.setValue('');
            if (this.refs.image_input) this.refs.image_input.setValue('');
            if (this.refs.username_input) this.refs.username_input.setValue('');
            if (this.refs.usericon_input) this.refs.usericon_input.setValue('');
            myswal({ title: 'Success', text: (TagG.jp ? 'ブロードキャストしました' : 'Broadcasted.'), icon: 'success', timer: TagG.SWAL_TIMER, buttons: {} });
        }
        catch (error) {
            console.error(error);
            showWindowError(TagG.jp ? `ブロードキャストできませんでした。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}` : `Failed to broadcast. hint: ${error}`);
        }
        finally { TagG.bodySpinner.stop() }
    };
    const aPrepareBroadcast = async(address, message, useSwalOnErr = false) => {
        try {
            TagG.bodySpinner.start();
            const addrData = TagG.addrsData.find(data => (data.address === address));
            const broadcastParams = {
                'source': address,
                'fee_fraction': 0,
                'text': message,
                'timestamp': Math.floor(new Date().getTime() / 1000),
                'value': -1,
                'fee_per_kb': TagG.FEESAT_PERBYTE * 1000,
                'extended_tx_info': true,
            };
            if (addrData.publicKey) broadcastParams['pubkey'] = addrData.publicKey.toString('hex');
            if (TagG.confNum === 0) broadcastParams['allow_unconfirmed_inputs'] = true;
            if (Buffer.byteLength(message) >= 55) broadcastParams['fee_per_kb'] = TagG.MULTISIG_FEESAT_PERBYTE * 1000;
            const broadcastData = this.broadcastData;
            await TagG.aMonapartyTXCreate('create_broadcast', broadcastParams, broadcastData);
            broadcastData.address = address;
            broadcastData.message = message;
            console.log(broadcastData);
            clearWindowMessages();
            Util.aOpenContentSwal(this.refs.broadcast_conf_window);
        }
        catch (error) {
            console.error(error);
            const errMessage = TagG.jp?
                `トランザクションの準備に失敗しました。入力した値に問題があるか、必要な${TagG.BASECHAIN_ASSET}やトークンが足りないかもしれません。データの更新などいろいろ試してもうまくいかない場合は情報を揃えてご連絡ください。ヒント：${error}`:
                `Failed to prepare tx. hint: ${error}`;
            useSwalOnErr ? myswal('Error', errMessage, 'error') : showWindowError(errMessage);
        }
        finally { TagG.bodySpinner.stop() }
    };
    const updateUserDict = (rawUsers) => {

        rawUsers.sort(sortByTimeAsc);
        for (const rawUser of rawUsers) {
            try {
                const address = rawUser.source;
                const user = this.userDict[address] || {};
                const rawText = rawUser.text.slice(DUSER_PREFIX.length);
                const parsedText = JSON.parse(rawText);
                if (typeof parsedText.n === 'string') user.name = parsedText.n || null;
                if (typeof parsedText.i === 'string') {
                    const formatCode = parsedText.i.slice(0, 1);
                    const imageID = parsedText.i.slice(1);
                    switch (formatCode) {
                        case '1': {
                            if (!imageID.match(/^[a-zA-Z0-9]+\.[a-zA-Z0-9]+$/)) throw 'Invalid imageID of Imgur';
                            const imageURL = `https://i.imgur.com/${imageID}`;
                            user.iconURL = TagG.getSizedImgurImageURL(imageURL, 's');
                            break;
                        }
                        case '':
                            user.iconURL = null;
                            break;
                        default:
                            throw 'Unknown image formatCode';
                    }
                }
                this.userDict[address] = user;
            }
            catch (error) {

            }
        }
    };
    const parseDBoardMessages = (rawMessages) => {
        rawMessages.sort(sortByTimeDesc);
        const messages = [];
        const replies = [];
        const isDeletedIndexAddr = {};
        this.assetsToLoad = [];
        for (const rawMessage of rawMessages) {
            try {
                const address = rawMessage.source;
                const block = rawMessage.block_index;
                const index = rawMessage.tx_index;
                const rawText = rawMessage.text.slice(DMESSAGE_PREFIX.length);
                const parsedText = JSON.parse(rawText);
                if (isDeletedIndexAddr[`${index}.${address}`]) continue;
                if (typeof parsedText === 'string') messages.push({ address, block, index, text: parsedText, replies: [] });
                else if (typeof parsedText.d === 'number') isDeletedIndexAddr[`${parsedText.d}.${address}`] = true;
                else {
                    if (typeof parsedText.m !== 'string' && parsedText.m !== undefined) throw 'Non string message';
                    const message = { address, block, index, text: parsedText.m || '', replies: [] };
                    if (typeof parsedText.p === 'string') {
                        const formatCode = parsedText.p.slice(0, 1);
                        const imageIDs = parsedText.p.slice(1).split(',');
                        if (imageIDs.length === 0) throw 'imageIDs.length === 0';
                        switch (formatCode) {
                            case '1': {
                                const imageID = imageIDs[0];
                                if (!imageID.match(/^[a-zA-Z0-9]+\.[a-zA-Z0-9]+$/)) throw 'Invalid imageID of Imgur';
                                message.imageURL = `https://i.imgur.com/${imageID}`;
                                message.previewImageURL = TagG.getSizedImgurImageURL(message.imageURL, 'l');
                                break;
                            }
                            case '2': {
                                message.assets = imageIDs;
                                message.onClickAssets = () => { openAssetCollection(message.assets) };
                                for (const asset of imageIDs) {
                                    if (!this.asset2isInvalid[asset]) this.assetsToLoad.push(asset);
                                }
                                break;
                            }
                            default:
                                throw 'Unknown image formatCode';
                        }
                    }
                    if (!(message.text || message.imageURL || message.assets)) throw 'Empty message';
                    if (typeof parsedText.r === 'number') {
                        message.replyToIndex = parsedText.r;
                        replies.push(message);
                    }
                    else messages.push(message);
                }
            }
            catch (error) {}
        }
        this.assetsToLoad = Util.uniq(this.assetsToLoad);
        replies.reverse();
        const index2message = Util.dictizeListWithKey(messages, 'index');
        for (const reply of replies) {
            if (index2message[reply.replyToIndex]) index2message[reply.replyToIndex].replies.push(reply);
        }
        return messages;
    };
    const setMentionFlags = () => {
        if (!TagG.addrsData) return;
        for (const message of this.messages) {
            setMentionFlag(message);
            if (TagG.addr2isMine[message.address]) {
                for (const reply of message.replies) {
                    reply.isMentioned = true;
                    if (reply.index > TagG.dboardReadIndex) reply.isNewMentioned = true;
                }
            }
            else {
                for (const reply of message.replies) setMentionFlag(reply);
            }
        }
    };
    const setMentionFlag = (message) => {
        if (!message.text || !message.text.includes('@')) return;
        let isMentioned = false;
        let matches;
        matches = message.text.match(setMentionFlag.addrReg);
        if (matches && matches.map(txt => txt.slice(1)).some(addr => TagG.addr2isMine[addr])) isMentioned = true;
        matches = message.text.match(setMentionFlag.holdersReg);
        if (matches && matches.map(txt => txt.split(/[()]/)[1]).some(asset => TagG.asset2isInMyWallet[asset])) isMentioned = true;
        matches = message.text.match(setMentionFlag.assetReg);
        if (matches && matches.map(txt => txt.slice(1)).some(asset => TagG.asset2isMyOwn[asset])) isMentioned = true;
        if (isMentioned) {
            message.isMentioned = true;
            if (message.index > TagG.dboardReadIndex) message.isNewMentioned = true;
        }
    };
    setMentionFlag.addrReg = new RegExp(`@${TagG.ADDR_PATTERN}`, 'g');
    setMentionFlag.holdersReg = /@holders\([A-Z0-9]{4,}(\.[a-zA-Z0-9.\-_@!]+)?\)/g;
    setMentionFlag.assetReg = /@[A-Z0-9]{4,}(\.[a-zA-Z0-9.\-_@!]+)?/g;
    const openAssetCollection = (assets) => {
        const galleryItems = [];
        for (const asset of assets) {
            const assetInfo = TagG.asset2info[asset];
            if (!assetInfo) continue;
            const imageURL = TagG.getAssetImageURL('L', assetInfo.asset);
            if (!imageURL) continue;
            const title = assetInfo.group ? `${assetInfo.group} #${assetInfo.name}` : assetInfo.name;
            let description = TagG.getMainDescription(assetInfo);
            let url = assetInfo.appData.monacharat?.url || null;
            const cardData = TagG.asset2card[assetInfo.name];
            if (cardData) {
                description = cardData.cardName;
                url = cardData.siteURL;
            }
            const subHtml = url?
                `<h4><a href="${url}" target="_blank">${Util.escapeHTML(title)}</a></h4><p><a href="${url}" target="_blank">${Util.escapeHTML(description)}</a></p>`:
                `<h4>${Util.escapeHTML(title)}</h4><p>${Util.escapeHTML(description)}</p>`;
            galleryItems.push({ src: imageURL, subHtml });
        }
        this.dynamicGallery.refresh(galleryItems);
        this.dynamicGallery.openGallery(0);
    };
    const initDynamicGallery = () => {
        const dummyButton = document.createElement('button');
        this.dynamicGallery = lightGallery(dummyButton, {
            dynamic: true,
            dynamicEl: [{
                src: 'https://monapalette.komikikaku.com/ogimage.png',
                subHtml: '<h4>dummy</h4><p>dummy</p>',
            }],
        });
    };
    const clearWindowMessages = () => {
        this.windowErrorText = '';
        this.update();
    };
    const showWindowError = (message) => {
        this.windowErrorText = message;
        this.update();
    };
    const sortByTimeAsc = (a, b) => {
        if (a.tx_index > b.tx_index) return 1;
        if (a.tx_index < b.tx_index) return -1;
        return (a.timestamp > b.timestamp ? 1 : -1);
    };
    const sortByTimeDesc = (a, b) => {
        if (a.tx_index < b.tx_index) return 1;
        if (a.tx_index > b.tx_index) return -1;
        return (a.timestamp < b.timestamp ? 1 : -1);
    };
    init();
});
riot.tag2('dex-tab', '<div class="tab-frame"> <div class="asset-inputs-frame"> <div class="asset-input-frame"><material-textfield ref="asset1_input" hint="Token1" on_enter="{onEnterAssets}"></material-textfield></div> <div class="asset-input-frame"> <material-textfield ref="asset2_input" hint="Token2" on_enter="{onEnterAssets}"></material-textfield> <div class="enterbutton-frame"><material-iconbutton icon="refresh" on_click="{onClickAssetsEnterButton}"></material-iconbutton></div> </div> </div> <div if="{recentPairs.length}" class="recentpairs-frame"> <div each="{recentPairs}" class="pair"><material-chip label="{baseName + \' / \' + quoteName}" on_click="{() => onClickPair(baseName, quoteName)}"></material-chip></div> </div> <div class="chart-frame"> <div class="card-frame card-frame-quoteasset"> <img if="{quoteAssetImage && !quoteAssetInfo.appData.rakugakinft}" class="card" riot-src="{quoteAssetImage}" loading="lazy"> <img if="{quoteAssetImage && quoteAssetInfo.appData.rakugakinft}" class="card bg-checker" riot-src="{quoteAssetImage + \'?\' + TagG.getRakugakiCacheID(quoteAssetInfo.asset)}" loading="lazy"> <div> <div if="{!quoteAssetInfo.group}" class="asset-name">{quoteAssetInfo.name}</div> <div if="{quoteAssetInfo.group}" class="asset-name">{quoteAssetInfo.group} <span class="nft-tag"></span></div> <div if="{quoteAssetInfo.group}" class="asset-id">{quoteAssetInfo.name}</div> </div> </div> <div class="card-frame card-frame-baseasset"> <div> <div if="{!baseAssetInfo.group}" class="asset-name">{baseAssetInfo.name}</div> <div if="{baseAssetInfo.group}" class="asset-name">{baseAssetInfo.group} <span class="nft-tag"></span></div> <div if="{baseAssetInfo.group}" class="asset-id">{baseAssetInfo.name}</div> </div> <img if="{baseAssetImage && !baseAssetInfo.appData.rakugakinft}" class="card" riot-src="{baseAssetImage}" loading="lazy"> <img if="{baseAssetImage && baseAssetInfo.appData.rakugakinft}" class="card bg-checker" riot-src="{baseAssetImage + \'?\' + TagG.getRakugakiCacheID(baseAssetInfo.asset)}" loading="lazy"> </div> <canvas ref="chart_canvas" width="400" height="400"></canvas> </div> <div class="centering-frame"> <div if="{isMarketLoaded}" class="depth-frame"><div class="inner"> <div class="labels-row"> <div class="price-cell">Price<span if="{quoteAssetName}"> [{quoteAssetName}]</span> </div><div class="amount-cell">Amount<span if="{baseAssetName}"> [{baseAssetName}]</span> </div><div class="total-cell">Total<span if="{quoteAssetName}"> [{quoteAssetName}]</span> </div> </div> <div each="{asks}" class="ask-row"> <div class="price-cell" onclick="{onClickPrice}">{priceS}</div><div class="amount-cell" onclick="{onClickAmount}">{amountS}</div><div class="total-cell">{totalS}</div> </div> <div class="last"> <span if="{lastPrice && isLastBuy}" class="buy"><i class="material-icons icon">arrow_upward</i>{lastPrice}</span> <span if="{lastPrice && !isLastBuy}" class="sell"><i class="material-icons icon">arrow_downward</i>{lastPrice}</span> </div> <div each="{bids}" class="bid-row"> <div class="price-cell" onclick="{onClickPrice}">{priceS}</div><div class="amount-cell" onclick="{onClickAmount}">{amountS}</div><div class="total-cell">{totalS}</div> </div> </div></div><div if="{isMarketLoaded}" class="orderform-frame"> <div if="{!addrsLoaded}" class="orderform-nologin">{TagG.jp ? \'売買するにはWalletを開いてください\' : \'To trade, open your wallet.\'}</div> <div if="{addrsLoaded}"> <div class="tabs-frame"> <div class="tab tab-buy"><material-button label="{TagG.jp ? \'買い\' : \'Buy\'}" on_click="{onClickBuyTabButton}" is_white="true" is_fullwidth="true"></material-button> </div><div class="tab tab-sell"><material-button label="{TagG.jp ? \'売り\' : \'Sell\'}" on_click="{onClickSellTabButton}" is_white="true" is_fullwidth="true"></material-button></div> </div> <div class="{form-frame: true, form-frame-buy: isFormBuy, form-frame-sell: !isFormBuy}"> <div class="address-frame"><material-select ref="addr_select" hint="Address" is_fullwidth="true" is_menuwidth_fixed="true" items="{TagG.addrSelectItems}" default_value="{TagG.addrSelectDefaultValue}" on_change="{onChangeAddrSelect}"></material-select></div> <div class="balance-frame">{TagG.jp ? \'残高\' : \'Balance\'}: {baseAssetBalance}&nbsp;{baseAssetName} / {quoteAssetBalance}&nbsp;{quoteAssetName}</div> <div class="amount-frame"> <div class="inline-input"><material-textfield ref="price_input" hint="Price" on_change="{onChangePriceInput}"></material-textfield></div> <div class="text-with-inlineinput">{quoteAssetName}</div> </div> <div class="amount-frame"> <div class="inline-input"><material-textfield ref="amount_input" hint="Amount" helper="Max: 0" is_helper_persistent="true" on_change="{onChangeAmountInput}"></material-textfield></div> <div class="text-with-inlineinput">{baseAssetName}</div> </div> <div class="total-frame">Total: {totalAmount} {quoteAssetName}</div> <div if="{quoteAssetName === TagG.BASECHAIN_ASSET}" class="basechainalert-frame"> <span if="{TagG.jp}">{TagG.BASECHAIN_ASSET}建の注文はいちおう出すことはできますが、通常の方法では約定しません。トークンを{TagG.BASECHAIN_ASSET}建で販売したい場合は自動販売(Dispenser)の利用をオススメします</span> <span if="{!TagG.jp}">You can place {TagG.BASECHAIN_ASSET} orders, but it will not be matched in the usual way. If you want to sell your tokens in {TagG.BASECHAIN_ASSET}, using dispenser is recommended.</span> </div> <div class="{buysell-button-frame: true, buy-button-frame: isOrderable && isFormBuy, sell-button-frame: isOrderable && !isFormBuy}"><material-button ref="order_button" label="{TagG.jp ? \'注文する\' : \'ORDER\'}" icon="send" on_click="{onClickOrderButton}" is_white="true" is_fullwidth="true" is_disabled="true"></material-button></div> </div> </div> <util-spinner ref="orderform_spinner"></util-spinner> </div> </div> <div if="{myOrders.length > 0}" class="myorders-frame"> <div class="title">{TagG.jp ? \'あなたの注文\' : \'Your orders\'}</div> <div each="{myOrders}" class="myorder"> <div class="description">{giveAmount}&nbsp;{giveAssetName} → {getAmount}&nbsp;{getAssetName}</div> <div class="percent">{filledPercent}% filled</div> <div class="address">{address}</div> <div class="cancel"> <material-iconbutton icon="delete" on_click="{onClickCancelButton}"></material-iconbutton> </div> </div> </div> <div class="recentorderedpairs-frame"> <div if="{recentOrderedPairs.length > 0}"> <div class="title">{TagG.jp ? \'注文のあるペア\' : \'Pairs with open orders\'}</div> <div each="{recentOrderedPairs}" class="pair" onclick="{() => onClickPair(baseName, quoteName)}"> <img if="{baseImage}" class="{card-image: true, has-ask: hasAsk}" riot-src="{baseImage}" alt="{baseName}" loading="lazy"> <div if="{!baseImage}" class="{image-alt: true, has-ask: hasAsk}"><div class="image-alt-inner">{baseName}</div></div> <i class="material-icons icon">swap_horiz</i> <img if="{quoteImage}" class="{card-image: true, has-bid: hasBid}" riot-src="{quoteImage}" alt="{quoteName}" loading="lazy"> <div if="{!quoteImage}" class="{image-alt: true, has-bid: hasBid}"><div class="image-alt-inner">{quoteName}</div></div> </div> </div> <util-spinner ref="recentorderedpairs_spinner"></util-spinner> </div> </div> <div ref="swalwindows_container" show="{false}"> <div ref="order_conf_window" class="dex-tab-swalwindow"> <div class="title"> <span if="{TagG.jp}">{baseAssetName}を{isFormBuy ? \'買う\' : \'売る\'}</span> <span if="{!TagG.jp}">{isFormBuy ? \'Buy\' : \'Sell\'} {baseAssetName}</span> </div> <div class="basic"> <span if="{TagG.jp}">1 枚あたり {commafy(orderData.price)} {quoteAssetName} で {commafy(orderData.amount)} {baseAssetName} の{isFormBuy ? \'買い\' : \'売り\'}注文を出します。トランザクション手数料が {commafy(orderData.feeAmount)} {TagG.BASECHAIN_ASSET} かかりますがOKですか？</span> <span if="{!TagG.jp}">{isFormBuy ? \'Buy\' : \'Sell\'} order for {commafy(orderData.amount)} {baseAssetName} at a price of {commafy(orderData.price)} {quoteAssetName} will be placed. You will be charged a tx fee of {commafy(orderData.feeAmount)} {TagG.BASECHAIN_ASSET}, OK?</span> </div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="bottombutton-frame"> <material-button label="OK" on_click="{orderConfWindow.onClickOKButton}" is_unelevated="true" is_fullwidth="true"></material-button> </div> </div> <div ref="cancel_conf_window" class="dex-tab-swalwindow"> <div class="title">{TagG.jp ? \'注文を取り消す\' : \'Delete order\'}</div> <div class="basic"> <span if="{TagG.jp}">注文を取り消します。トランザクション手数料が {commafy(cancelData.feeAmount)} {TagG.BASECHAIN_ASSET} かかりますがOKですか？</span> <span if="{!TagG.jp}">The order will be deleted. You will be charged a tx fee of {commafy(cancelData.feeAmount)} {TagG.BASECHAIN_ASSET}, OK?</span> </div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="bottombutton-frame"> <material-button label="OK" on_click="{orderCancelWindow.onClickOKButton}" is_unelevated="true" is_fullwidth="true"></material-button> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    const RECENT_PAIRS = 6;
    const DEPTH_ROWS = 8;
    const defaultCommafy = Util.commafy;
    const decimalCommafy = (num) => Util.commafy(num, { decimal: TagG.SATOSHI_DECIMAL, removeDecimalZero: false });
    this.orderConfWindow = {};
    this.orderCancelWindow = {};
    this.orderData = {};
    this.cancelData = {};

    const init = () => {
        this.commafy = Util.commafy;
        this.recentPairs = Util.loadStorage(TagG.STORAGEKEY_RECENTPAIRS) || [];
        [this.initAsset1, this.initAsset2] = [opts.asset1, opts.asset2];
        this.baseAssetInfo = {};
        this.quoteAssetInfo = {};
        this.asks = [];
        this.bids = [];
        this.myOrders = [];
        this.recentOrderedPairs = [];
        while (this.asks.length < DEPTH_ROWS) this.asks.unshift({});
        while (this.bids.length < DEPTH_ROWS) this.bids.push({});
        this.isFormBuy = true;
        this.baseAssetBalance = 0;
        this.quoteAssetBalance = 0;
        this.totalAmount = 0;
    };

    this.onEnterAssets = () => {
        const asset1Name = this.refs.asset1_input.getValue();
        const asset2Name = this.refs.asset2_input.getValue();
        if (!asset1Name || !asset2Name) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'トークン名を入れてね' : 'No token name.', 'error');
        aShowMarket(asset1Name, asset2Name);
    };
    this.onClickAssetsEnterButton = () => {
        this.onEnterAssets();
    };
    this.onClickPair = (baseName, quoteName) => {
        aShowMarket(baseName, quoteName);
        try { scrollTo({ left: 0, top: 0, behavior: 'smooth' }) }
        catch (error) { scrollTo(0, 0) }
    };
    this.onChangeAddrSelect = () => { onChangeOrderInputs() };
    this.onChangePriceInput = () => { onChangeOrderInputs() };
    this.onChangeAmountInput = () => { onChangeOrderInputs() };
    this.onClickBuyTabButton = () => {
        this.isFormBuy = true;
        setMaxAmount();
        this.update();
    };
    this.onClickSellTabButton = () => {
        this.isFormBuy = false;
        setMaxAmount();
        this.update();
    };
    this.onClickOrderButton = () => {
        const address = this.refs.addr_select.getValue();
        const price = Number(this.refs.price_input.getValue());
        const amount = Number(this.refs.amount_input.getValue());
        if (!address) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'アドレスを選択してください' : 'No address selected.', 'error');
        if (isNaN(price)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '価格に変な値が入っています' : 'Invalid price.', 'error');
        if (isNaN(amount)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '数量に変な値が入っています' : 'Invalid amount.', 'error');
        if (price <= 0) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '価格は正の値で入力してください' : 'Invalid price.', 'error');
        if (amount <= 0) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '数量は正の値で入力してください' : 'Invalid amount.', 'error');
        const totalAmount = price * amount;
        if (!this.isBaseDivisible && !Number.isInteger(amount)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '数量は整数で入力してください' : 'Invalid amount.', 'error');
        if (!this.isQuoteDivisible && !Number.isInteger(totalAmount)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'Totalが整数になるように入力してください' : '"Total" must be an integer.', 'error');
        this.orderData.price = price;
        this.orderData.amount = amount;
        if (this.isFormBuy) {
            const giveSat = TagG.amount2qty(totalAmount, this.isQuoteDivisible);
            const getSat = TagG.amount2qty(amount, this.isBaseDivisible);
            aPrepareOrder(address, this.quoteAsset, giveSat, this.baseAsset, getSat);
        }
        else {
            const giveSat = TagG.amount2qty(amount, this.isBaseDivisible);
            const getSat = TagG.amount2qty(totalAmount, this.isQuoteDivisible);
            aPrepareOrder(address, this.baseAsset, giveSat, this.quoteAsset, getSat);
        }
    };
    this.orderConfWindow.onClickOKButton = () => { aOrder() };
    this.orderCancelWindow.onClickOKButton = () => { aCancel() };

    this.on('mount', async() => {
        const container = this.refs.swalwindows_container;
        while (container && container.firstChild) container.removeChild(container.firstChild);
        createChart();
        aShowRecentOrderedPairs();
        if (this.initAsset1 && this.initAsset2) aShowMarket(this.initAsset1, this.initAsset2);
        if (TagG.addrsData) {
            await TagG.aUpdateAddrsDataWithUI(this.refs.orderform_spinner);
            this.addrData = TagG.addrsData.find(data => (data.address === TagG.addrSelectDefaultValue));
            setBalances();
            this.addrsLoaded = true;
            this.update();
            aShowMyOrders();
        }
    });
    const onChangeOrderInputs = () => {
        const address = this.refs.addr_select.getValue();
        const price = Number(this.refs.price_input.getValue());
        const amount = Number(this.refs.amount_input.getValue());
        if (isNaN(price) || isNaN(amount)) this.totalAmount = 0;
        else this.totalAmount = Util.round(price * amount, TagG.SATOSHI_DECIMAL);
        if (address && this.totalAmount > 0) {
            this.isOrderable = true;
            this.refs.order_button.setDisabled(false);
        }
        else {
            this.isOrderable = false;
            this.refs.order_button.setDisabled(true);
        }
        this.addrData = TagG.addrsData.find(data => (data.address === address));
        setBalances();
        setMaxAmount();
        this.update();
    };
    const aOrder = async() => {
        try {
            TagG.bodySpinner.start();
            const signedTXHex = await TagG.aSignTXHex(this.orderData.txHex, this.addrData, null);
            const txID = await TagG.aBroadcast(signedTXHex);
            console.log({ txID });
            reduceBalance(this.orderData.giveAsset, this.orderData.giveSat);
            if (this.refs.amount_input) this.refs.amount_input.setValue('');
            onChangeOrderInputs();
            myswal({ title: 'Success', text: (TagG.jp ? '注文しました' : 'Ordered.'), icon: 'success', timer: TagG.SWAL_TIMER, buttons: {} });
        }
        catch (error) {
            console.error(error);
            showWindowError(TagG.jp ? `注文できませんでした。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}` : `Failed to order. hint: ${error}`);
        }
        finally { TagG.bodySpinner.stop() }
    };
    const aCancel = async() => {
        try {
            TagG.bodySpinner.start();
            const addrData = TagG.addrsData.find(data => (data.address === this.cancelData.address));
            const signedTXHex = await TagG.aSignTXHex(this.cancelData.txHex, addrData, null);
            const txID = await TagG.aBroadcast(signedTXHex);
            console.log({ txID });
            this.myOrders = this.myOrders.filter(order => (order.txID !== this.cancelData.orderTxID));
            this.update();
            myswal({ title: 'Success', text: (TagG.jp ? '取り消しました' : 'Deleted.'), icon: 'success', timer: TagG.SWAL_TIMER, buttons: {} });
        }
        catch (error) {
            console.error(error);
            showWindowError(TagG.jp ? `取り消せませんでした。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}` : `Failed to delete. hint: ${error}`);
        }
        finally { TagG.bodySpinner.stop() }
    };
    const aPrepareOrder = async(address, giveAsset, giveSat, getAsset, getSat) => {
        try {
            TagG.bodySpinner.start();
            const addrData = TagG.addrsData.find(data => (data.address === address));
            const orderParams = {
                'source': address,
                'give_asset': giveAsset,
                'give_quantity': giveSat,
                'get_asset': getAsset,
                'get_quantity': getSat,
                'expiration': 65535,
                'fee_provided': 0,
                'fee_required': 0,
                'fee_per_kb': TagG.FEESAT_PERBYTE * 1000,
                'extended_tx_info': true,
            };
            if (addrData.publicKey) orderParams['pubkey'] = addrData.publicKey.toString('hex');
            if (TagG.confNum === 0) orderParams['allow_unconfirmed_inputs'] = true;
            const orderData = this.orderData;
            await TagG.aMonapartyTXCreate('create_order', orderParams, orderData);
            orderData.address = address;
            orderData.giveAsset = giveAsset;
            orderData.giveSat = giveSat;
            console.log(orderData);
            clearWindowMessages();
            Util.aOpenContentSwal(this.refs.order_conf_window);
        }
        catch (error) { onCatchPrepareError(error) }
        finally { TagG.bodySpinner.stop() }
    };
    const aPrepareCancel = async(address, txID) => {
        try {
            TagG.bodySpinner.start();
            const addrData = TagG.addrsData.find(data => (data.address === address));
            const cancelParams = {
                'source': address,
                'offer_hash': txID,
                'fee_per_kb': TagG.FEESAT_PERBYTE * 1000,
                'extended_tx_info': true,
            };
            if (addrData.publicKey) cancelParams['pubkey'] = addrData.publicKey.toString('hex');
            if (TagG.confNum === 0) cancelParams['allow_unconfirmed_inputs'] = true;
            const cancelData = this.cancelData;
            await TagG.aMonapartyTXCreate('create_cancel', cancelParams, cancelData);
            cancelData.address = address;
            cancelData.orderTxID = txID;
            console.log(cancelData);
            clearWindowMessages();
            Util.aOpenContentSwal(this.refs.cancel_conf_window);
        }
        catch (error) { onCatchPrepareError(error) }
        finally { TagG.bodySpinner.stop() }
    };
    const aShowMarket = async(asset1Name, asset2Name) => {
        try {
            TagG.bodySpinner.start();
            await TagG.aLoadAssets([asset1Name, asset2Name], true, true);
            const asset1Info = TagG.asset2info[asset1Name];
            const asset2Info = TagG.asset2info[asset2Name];
            if (!asset1Info) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? `${asset1Name}というトークンはありません` : `There is no token named ${asset1Name}`, 'error');
            if (!asset2Info) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? `${asset2Name}というトークンはありません` : `There is no token named ${asset2Name}`, 'error');
            const [asset1, asset2] = [asset1Info.asset, asset2Info.asset];
            [this.asset1Info, this.asset2Info] = [asset1Info, asset2Info];
            await Promise.all([
                aShowDepth(asset1, asset2, asset1Name, asset2Name),
                aShowChart(asset1, asset2),
            ]);
            setBalances();
            setMaxAmount();
            addRecentPair();
            this.baseAssetImage = TagG.getAssetImageURL('auto', this.baseAsset);
            this.quoteAssetImage = TagG.getAssetImageURL('auto', this.quoteAsset);
            this.isMarketLoaded = true;
            this.update();
            this.refs.asset1_input.setValue(this.baseAssetName);
            this.refs.asset2_input.setValue(this.quoteAssetName);
            route(`/dex/${encodeURIComponent(this.baseAssetName)}/${encodeURIComponent(this.quoteAssetName)}`);
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `データの取得に失敗しました。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}` : `Failed to fetch data. hint: ${error}`), 'error');
        }
        finally { TagG.bodySpinner.stop() }
    };
    const aShowDepth = async(asset1, asset2, asset1Name, asset2Name) => {
        const marketParams = { 'asset1': asset1, 'asset2': asset2 };
        const marketDetail = await Monaparty.aBlock('get_market_details', marketParams);
        this.asks = marketDetail.sell_orders
            .map(order => parseDepthOrder(order, marketDetail.base_asset_divisible, marketDetail.quote_asset_divisible))
            .sort(sortByPriceDesc)
            .slice(-DEPTH_ROWS);
        this.bids = marketDetail.buy_orders
            .map(order => parseDepthOrder(order, marketDetail.base_asset_divisible, marketDetail.quote_asset_divisible))
            .sort(sortByPriceDesc)
            .slice(0, DEPTH_ROWS);
        while (this.asks.length < DEPTH_ROWS) this.asks.unshift({});
        while (this.bids.length < DEPTH_ROWS) this.bids.push({});
        const isAsset1Base = (marketDetail.base_asset === asset1);
        this.baseAssetName = isAsset1Base ? asset1Name : asset2Name;
        this.quoteAssetName = isAsset1Base ? asset2Name : asset1Name;
        this.baseAssetInfo = isAsset1Base ? this.asset1Info : this.asset2Info;
        this.quoteAssetInfo = isAsset1Base ? this.asset2Info : this.asset1Info;
        this.baseAsset = marketDetail.base_asset;
        this.quoteAsset = marketDetail.quote_asset;
        this.isBaseDivisible = Boolean(marketDetail.base_asset_divisible);
        this.isQuoteDivisible = Boolean(marketDetail.quote_asset_divisible);
        if (marketDetail.last_trades.length === 0) this.lastPrice = null;
        else {
            const lastTrade = marketDetail.last_trades.reduce((lastTrade, trade) => (trade.block_time > lastTrade.block_time ? trade : lastTrade));
            this.lastPrice = marketDetail.quote_asset_divisible ? decimalCommafy(lastTrade.price) : defaultCommafy(lastTrade.price);
            this.isLastBuy = (lastTrade.type === 'BUY');
        }
    };
    const aShowChart = async(asset1, asset2) => {
        const priceHistoryParams = { 'asset1': asset1, 'asset2': asset2, 'as_dict': true };
        const priceHistory = await Monaparty.aBlock('get_market_price_history', priceHistoryParams) || [];
        const priceData = priceHistory.map(tick => ({ t: new Date(tick.interval_time), y: tick.close }));
        const volumeData = priceHistory.map(tick => ({ t: new Date(tick.interval_time), y: tick.vol }));
        const volumeMin = priceHistory.reduce((min, tick) => (tick.vol < min ? tick.vol : min), Number.POSITIVE_INFINITY);
        const volumeMax = priceHistory.reduce((max, tick) => (tick.vol > max ? tick.vol : max), 0);
        const volumeBase = Math.sqrt((volumeMin + volumeMax) / 2);
        const volumeSizes = priceHistory.map(tick => (1 + Math.sqrt(tick.vol) / volumeBase * 10));
        this.chart.data.datasets[0].data = priceData;
        this.chart.data.datasets[1].data = volumeData;
        this.chart.data.datasets[1].pointRadius = volumeSizes;
        this.chart.update();
    };
    const aShowMyOrders = async() => {
        try {
            const addrs = TagG.addrsData.map(data => data.address);
            const getOrdersParams = {
                'status': 'open',
                'filters': [{ field: 'source', op: 'IN', value: addrs }],
            };
            const myOrders = await Monaparty.aParty('get_orders', getOrdersParams);
            const assets = [];
            for (const order of myOrders) {
                assets.push(order.give_asset);
                assets.push(order.get_asset);
            }
            await TagG.aLoadAssets(assets, true, true);
            this.myOrders = myOrders.map(order => parseMyOrder(order));
            this.update();
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `データの取得に失敗しました。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}` : `Failed to fetch data. hint: ${error}`), 'error');
        }
    };
    const aShowRecentOrderedPairs = async() => {
        try {
            if (this.refs.recentorderedpairs_spinner) this.refs.recentorderedpairs_spinner.start();
            const PRIOR_ASSETS = ['MONANA', 'MPYEN', 'CHAOSMONA', 'SPACEMONA', TagG.BUILTIN_ASSET, TagG.BASECHAIN_ASSET];
            const getOrdersParams = {
                'limit': 300,
                'order_by': 'tx_index',
                'order_dir': 'DESC',
                'status': 'open',
                'filters': [
                    { field: 'give_asset', op: '!=', value: TagG.BASECHAIN_ASSET },
                    { field: 'get_asset', op: '!=', value: TagG.BASECHAIN_ASSET },
                ],
            };
            const recentOrders = await Monaparty.aParty('get_orders', getOrdersParams);
            const recentOrderedPairs = [];
            let assets = [];
            for (const order of recentOrders) {
                assets.push(order.give_asset);
                assets.push(order.get_asset);
            }
            assets = Util.uniq(assets);
            await TagG.aLoadAssets(assets, true, true);
            const sortedAssets = assets.slice();
            sortedAssets.sort((asset1, asset2) => asset1 > asset2 ? -1 : 1);
            for (const asset of PRIOR_ASSETS) sortedAssets.unshift(asset);
            for (const order of recentOrders) {
                let baseAsset, quoteAsset, isBid;
                if (sortedAssets.indexOf(order.give_asset) < sortedAssets.indexOf(order.get_asset)) {
                    baseAsset = order.get_asset;
                    quoteAsset = order.give_asset;
                    isBid = true;
                }
                else {
                    baseAsset = order.give_asset;
                    quoteAsset = order.get_asset;
                    isBid = false;
                }
                const baseInfo = TagG.asset2info[baseAsset];
                const quoteInfo = TagG.asset2info[quoteAsset];
                const baseName = baseInfo.name;
                const quoteName = quoteInfo.name;
                const existPair = recentOrderedPairs.find(pair => (pair.baseName === baseName && pair.quoteName === quoteName));
                if (existPair) {
                    if (isBid) existPair.hasBid = true;
                    else existPair.hasAsk = true;
                }
                else {
                    let baseImage = TagG.getAssetImageURL('S', baseInfo.asset);
                    let quoteImage = TagG.getAssetImageURL('S', quoteInfo.asset);
                    if (baseInfo.appData.rakugakinft) baseImage += `?${TagG.getRakugakiCacheID(baseInfo.asset)}`;
                    if (quoteInfo.appData.rakugakinft) quoteImage += `?${TagG.getRakugakiCacheID(quoteInfo.asset)}`;
                    const pair = { baseName, quoteName, baseImage, quoteImage };
                    if (isBid) pair.hasBid = true;
                    else pair.hasAsk = true;
                    recentOrderedPairs.push(pair);
                }
            }
            this.recentOrderedPairs = recentOrderedPairs;
            this.update();
        }
        catch (error) { console.error(error) }
        finally {
            if (this.refs.recentorderedpairs_spinner) this.refs.recentorderedpairs_spinner.stop();
        }
    };
    const createChart = () => {
        const context = this.refs.chart_canvas.getContext('2d');
        this.chart = new Chart(context, {
            data: {
                datasets: [{
                    label: 'Price',
                    type: 'line',
                    backgroundColor: 'rgba(255, 140, 0, 0.4)',
                    borderColor: 'rgba(255, 140, 0, 1.0)',
                    borderJoinStyle: 'round',
                    lineTension: 0,
                    fill: false,
                    yAxisID: 'price_axis',
                    data: [],
                }, {
                    label: 'Volume',
                    type: 'line',
                    backgroundColor: 'rgba(140, 255, 0, 0.4)',
                    borderColor: 'rgba(140, 255, 0, 1.0)',
                    pointRadius: [],
                    lineTension: 0,
                    fill: false,
                    showLine: false,
                    yAxisID: 'volume_axis',
                    data: [],
                }],
            },
            options: {
                maintainAspectRatio: false,
                legend: { onClick: null },
                scales: {
                    xAxes: [{
                        type: 'time',
                        distribution: 'linear',
                        time: {
                            minUnit: 'hour',
                            displayFormats: {
                                hour: 'MM/DD HH:mm',
                                day: 'MM/DD',
                                week: 'MM/DD',
                                month: 'YYYY/MM/DD',
                            },
                        },
                    }],
                    yAxes: [{
                        id: 'price_axis',
                        position: 'right',
                        ticks: { beginAtZero: true },
                    }, {
                        id: 'volume_axis',
                        display: false,
                        ticks: { beginAtZero: true },
                        gridLines: { drawOnChartArea: false },
                    }],
                }
            }
        });
    };
    const reduceBalance = (asset, satoshi) => {
        const balanceData = this.addrData.assets.find(assetData => (assetData.asset === asset));
        if (balanceData) balanceData.satoshi -= satoshi;
        else if (asset === TagG.BUILTIN_ASSET) this.addrData.builtinSat -= satoshi;
    };
    const setBalances = () => {
        if (!this.addrData) return;
        const baseAssetBalanceData = this.addrData.assets.find(asset => (asset.asset === this.baseAsset));
        const quoteAssetBalanceData = this.addrData.assets.find(asset => (asset.asset === this.quoteAsset));
        if (baseAssetBalanceData) this.baseAssetBalance = TagG.qty2amount(baseAssetBalanceData.satoshi, this.isBaseDivisible);
        else if (this.baseAsset === TagG.BUILTIN_ASSET) this.baseAssetBalance = this.addrData.builtinSat / TagG.SATOSHI_RATIO;
        else this.baseAssetBalance = 0;
        if (quoteAssetBalanceData) this.quoteAssetBalance = TagG.qty2amount(quoteAssetBalanceData.satoshi, this.isQuoteDivisible);
        else if (this.quoteAsset === TagG.BUILTIN_ASSET) this.quoteAssetBalance = this.addrData.builtinSat / TagG.SATOSHI_RATIO;
        else this.quoteAssetBalance = 0;
    };
    const setMaxAmount = () => {
        if (!this.refs.price_input) return;
        let maxAmount = 0
        if (this.isFormBuy) {
            const price = Number(this.refs.price_input.getValue());
            if (price > 0) {
                if (this.isBaseDivisible) maxAmount = Util.round(this.quoteAssetBalance / price, TagG.SATOSHI_DECIMAL);
                else maxAmount = Math.floor(this.quoteAssetBalance / price);
            }
            else maxAmount = 0;
        }
        else maxAmount = this.baseAssetBalance;
        if (this.refs.amount_input) this.refs.amount_input.setHelperMessage(`Max: ${maxAmount}`);
    };
    const addRecentPair = () => {
        const [baseName, quoteName] = [this.baseAssetName, this.quoteAssetName];
        const index = this.recentPairs.findIndex(pair => (pair.baseName === baseName && pair.quoteName === quoteName));
        if (index >= 0) this.recentPairs.splice(index, 1);
        this.recentPairs.unshift({ baseName, quoteName });
        this.recentPairs = this.recentPairs.slice(0, RECENT_PAIRS);
        Util.saveStorage(TagG.STORAGEKEY_RECENTPAIRS, this.recentPairs);
    };
    const parseDepthOrder = (order, baseDivisible, quoteDivisible) => {
        const price = Number(order.price);
        const amount = TagG.qty2amount(order.amount, baseDivisible);
        const total = TagG.qty2amount(order.total, quoteDivisible);
        return {
            price, amount, total,
            priceS: quoteDivisible ? decimalCommafy(price) : defaultCommafy(price),
            amountS: baseDivisible ? decimalCommafy(amount) : defaultCommafy(amount),
            totalS: quoteDivisible ? decimalCommafy(total) : defaultCommafy(total),
            onClickPrice: () => setPrice(price),
            onClickAmount: () => setAmount(amount),
        };
    };
    const parseMyOrder = (order) => {
        const giveAssetInfo = TagG.asset2info[order.give_asset];
        const getAssetInfo = TagG.asset2info[order.get_asset];
        return {
            txID: order.tx_hash,
            address: order.source,
            giveAsset: giveAssetInfo.asset,
            getAsset: getAssetInfo.asset,
            giveAssetName: giveAssetInfo.name,
            getAssetName: getAssetInfo.name,
            giveAmount: TagG.qty2amount(order.give_quantity, giveAssetInfo.divisible),
            getAmount: TagG.qty2amount(order.get_quantity, getAssetInfo.divisible),
            filledPercent: ((order.give_quantity - order.give_remaining) / order.give_quantity * 100).toFixed(2),
            onClickCancelButton: () => aPrepareCancel(order.source, order.tx_hash),
        };
    };
    const setPrice = (price) => {
        if (!this.refs.price_input) return;
        const noExpPrice = defaultCommafy(price).replace(/,/g, '');
        this.refs.price_input.setValue(noExpPrice);
        onChangeOrderInputs();
    };
    const setAmount = (amount) => {
        if (!this.refs.amount_input) return;
        const noExpAmount = defaultCommafy(amount).replace(/,/g, '');
        this.refs.amount_input.setValue(noExpAmount);
        onChangeOrderInputs();
    };
    const clearWindowMessages = () => {
        this.windowErrorText = '';
        this.update();
    };
    const showWindowError = (message) => {
        this.windowErrorText = message;
        this.update();
    };
    const onCatchPrepareError = (error) => {
        console.error(error);
        myswal('Error', (TagG.jp ? `トランザクションの準備に失敗しました。入力した値に問題があるか、必要な${TagG.BASECHAIN_ASSET}やトークンが足りないかもしれません。データの更新などいろいろ試してもうまくいかない場合は情報を揃えてご連絡ください。ヒント：${error}` : `Failed to prepare tx. hint: ${error}`), 'error');
    };
    const sortByPriceDesc = (a, b) => (a.price < b.price ? 1 : -1);
    init();
});
riot.tag2('dispensers-tab', '<div class="tab-frame"> <div class="search-frame"> <material-textfield ref="search_input" hint="Search" on_input="{onInputSearch}"></material-textfield> <div class="search-clearbutton-frame"><material-iconbutton icon="clear" on_click="{onClickClearSearchButton}"></material-iconbutton></div> </div> <div class="search-chips"> <div each="{searchChips}" class="search-chip"><material-chip label="{label}" on_click="{onClick}"></material-chip></div> </div> <div class="tokensale-items"> <div each="{dispensersToShow}" class="tokensale-item {tokensale-item-square: assetInfo.appData.monacharat || assetInfo.appData.rakugakinft}" onclick="{onClick}"> <img if="{!imageURL}" class="image" riot-src="{TagG.RELATIVE_URL_BASE + \'img/noimage.png\'}" loading="lazy"> <img if="{imageURL && !assetInfo.appData.rakugakinft}" class="image" riot-src="{imageURL}" loading="lazy"> <img if="{imageURL && assetInfo.appData.rakugakinft}" class="image bg-checker" riot-src="{imageURL + \'?\' + TagG.getRakugakiCacheID(asset)}" loading="lazy"> <div class="info-frame"> <div if="{!assetGroup}">{assetInfo.shortenName}<span if="{unitAssetAmount !== 1}" class="amount"> x&nbsp;{unitAssetAmountS}</span></div> <div if="{assetGroup}">{assetGroup} <span class="nft-tag"></span></div> <div if="{assetGroup}" class="asset-id">{assetName}</div> <div>{unitBaseAmountS}<span class="unit">{baseAsset}</span></div> <div class="addr-button" onclick="{onClickAddr}"> <div if="{awaseuriCount > 1}" class="setcount">{awaseuriCount} set</div> <div class="hashcolors"><util-hashcolors seed_value="{address}"></util-hashcolors></div> </div> </div> <div if="{TagG.asset2isInMyWallet[asset]}" class="status-icon status-icon-wallet"><i class="material-icons">account_balance_wallet</i></div> <div if="{TagG.asset2treasures[asset]}" class="status-icon status-icon-treasure"><i class="material-icons">attachment</i></div> </div> <util-spinner ref="spinner"></util-spinner> </div> <div class="toprightbutton-frame lefter"><material-iconbutton icon="swap_vert" on_click="{onClickReverseButton}"></material-iconbutton></div> <div class="toprightbutton-frame"><material-iconbutton ref="update_button" icon="refresh" on_click="{onClickUpdateButton}"></material-iconbutton></div> </div>', '', '', function(opts) {
    const init = () => {
        this.TagG = TagG;
        this.commafy = Util.commafy;
        this.baseAsset = TagG.BASECHAIN_ASSET;
        this.dispensers = [];
        this.dispensersToShow = [];
        this.searchedQuery = '';
        this.isReverse = false;
        this.searchChips = [
            { label: 'XMP', onClick: () => search('XMP CC-BY-SA') },
            { label: 'NFT', onClick: () => search('[NFT]') },
            { label: 'Locked', onClick: () => search('[LOCKED]') },
            { label: 'Rakugaki', onClick: () => search('Rakugaki [NFT]') },
            { label: 'MonaCharaT', onClick: () => search('MonaCharaT [NFT]') },
            { label: 'AND ex.', onClick: () => search('モナパレット スタイルトークン') },
            { label: 'OR ex.', onClick: () => search('オダイロイド１号 OR モナパちゃん OR Tiproid') },
        ];
        if (TagG.query[TagG.QUERYKEY_SEARCH]) {
            this.searchedQuery = decodeURIComponent(TagG.query[TagG.QUERYKEY_SEARCH].replace(/\+/g, ' '));
            delete TagG.query[TagG.QUERYKEY_SEARCH];
        }
    };
    this.onInputSearch = (query) => {
        query = query.trim();
        if (query === this.searchedQuery) return;
        search(query);
    };
    this.onClickClearSearchButton = () => {
        if (this.searchedQuery === '') return;
        search('');
    }
    this.onClickReverseButton = () => {
        this.isReverse = !this.isReverse;
        search(this.searchedQuery);
    };
    this.onClickUpdateButton = async() => {
        if (this.refs.update_button) this.refs.update_button.setDisabled(true);
        await TagG.aLoadRakugakiTitles().catch(console.warn);
        await aShowDispensers();
        await aUpdateAddrs();
        setTimeout(() => {
            if (this.refs.update_button) this.refs.update_button.setDisabled(false);
        }, 10000);
    };
    this.on('mount', async() => {
        await TagG.aLoadRakugakiTitles().catch(console.warn);
        await aShowDispensers();
        await aUpdateAddrs();
        TagG.aLoadTreasures().then(() => { this.update() });
        TagG.aLoadAssetsExtendedInfo().then(isUpdated => { if (isUpdated) this.update() });
    });
    const aUpdateAddrs = async() => {
        if (!TagG.addrsData) return;
        await TagG.aUpdateAddrsDataWithUI(null);
        this.update();
    };
    const aShowDispensers = async() => {
        try {
            if (this.refs.spinner) this.refs.spinner.start();
            this.dispensers = await TagG.aGetDispensers();
            const addr2count = {};
            for (const dispenser of this.dispensers) {
                dispenser.searchText = getSearchText(dispenser);
                dispenser.onClick = () => { TagG.bodyDispenserPurchase.open(dispenser, 'dispensers') };
                dispenser.onClickAddr = (event) => {
                    event.stopPropagation();
                    search(dispenser.address);
                };
                if (!addr2count[dispenser.address]) addr2count[dispenser.address] = 0;
                addr2count[dispenser.address]++;
            }
            for (const dispenser of this.dispensers) dispenser.awaseuriCount = addr2count[dispenser.address];
            search(this.searchedQuery);
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `データの取得に失敗しました。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}` : `Failed to fetch data. hint: ${error}`), 'error');
        }
        finally { if (this.refs.spinner) this.refs.spinner.stop() }
    };
    const getSearchText = (dispenser) => {
        const { asset, assetName, address, assetInfo, cardData } = dispenser;
        let text = `${assetName.toLowerCase()} ${assetName} ${asset} ${address} ${assetInfo.description} ${assetInfo.isLocked ? '[LOCKED]' : ''}`;
        if (assetInfo.group) text += ` ${assetInfo.group} [NFT]`;
        if (!cardData) return text;
        text += ` ${cardData.cardName} ${cardData.ownerName} ${cardData.description} ${cardData.tags}`;
        return text;
    };
    const search = (query) => {
        this.searchedQuery = query;
        const searchID = Math.random();
        this.searchID = searchID;
        let isOR = false;
        let words = query.split(/\s+OR\s+/).map(word => word.trim()).filter(word => word);
        if (words.length > 1) isOR = true;
        else words = query.split(/\s+/).filter(word => word);
        let filteredList;
        if (words.length === 0) filteredList = this.dispensers.slice();
        else {
            if (isOR) {
                filteredList = this.dispensers.filter(dispenser => {
                    for (const word of words) {
                        if (dispenser.searchText.includes(word)) return true;
                    }
                    return false;
                });
            }
            else {
                filteredList = this.dispensers.filter(dispenser => {
                    for (const word of words) {
                        if (!dispenser.searchText.includes(word)) return false;
                    }
                    return true;
                });
            }
        }
        if (this.isReverse) filteredList.reverse();
        if (this.refs.search_input && this.refs.search_input.getValue() !== query) this.refs.search_input.setValue(query);
        setURLQuery(query);
        const FIRST = 30;
        const ONCE = 300;
        this.dispensersToShow = filteredList.slice(0, FIRST);
        this.update();
        let length = FIRST;
        const showMore = () => {
            if (length >= filteredList.length || searchID !== this.searchID) return;
            if (!TagG.IS_LAZYLOADAVAILABLE && length > 300) return;
            length += ONCE;
            this.dispensersToShow = filteredList.slice(0, length);
            this.update();
            setTimeout(showMore, 0);
        };
        setTimeout(showMore, 0);
    };
    const setURLQuery = (query) => {
        const urlObj = new URL(location);
        if (query) urlObj.searchParams.set(TagG.QUERYKEY_SEARCH, query);
        else urlObj.searchParams.delete(TagG.QUERYKEY_SEARCH);
        history.replaceState(null, '', urlObj.toString());
    };
    init();
});
riot.tag2('generate-kirakiraaddr-page', '<div class="app-page"> <div class="page-header"> <i class="material-icons page-icon">star_outline</i> <h2 class="page-title">{TagG.jp ? \'キラキラアドレス生成\' : \'KIRA KIRA Address\'}</h2> <div class="page-helpbutton-frame"> <material-iconbutton icon="help_outline" on_click="{onClickPageHelpButton}"></material-iconbutton> </div> </div> <div class="page-content"> <div if="{isWIFMode}" class="centering alert"> <span if="{TagG.jp}">ニーモニックと比べると人間が直接取り扱うには難があるため、ウォレットのバックアップをWIFで管理することは現在推奨されていません。アドレスのスキャンはWIFモードのほうが速いですが、あくまでホビー用途のアドレスに留めておくことをオススメします</span> <span if="{!TagG.jp}">Managing wallet backups in WIF is not recommended. Scanning of addresses is faster in WIF mode, but it is recommended to keep the addresses from WIF for hobby use only.</span> </div> <div class="centering"><material-switch ref="segwit_switch" label="{\'Native Segwit\'}" on_change="{onChangeSegwitSwitch}"></material-switch></div> <div class="centering">{TagG.jp ? \'使える文字\' : \'Allowed characters\'}&ensp;<code class="allowed-chars">{allowedChars}</code></div> <div class="centering prewrap">{probabilityMessage}</div> <div class="centering"> <material-textfield if="{isSegwit}" ref="postfix_input" hint="Postfix" pattern="{\'[02-9ac-hj-np-z,\\\\s]*\'}" invalid_message="Non-Bech32 Character"></material-textfield> <material-textfield if="{!isSegwit}" ref="postfix_input" hint="Postfix" pattern="{\'[1-9A-HJ-NP-Za-km-z,\\\\s]*\'}" invalid_message="Non-Base58 Character"></material-textfield> </div> <div if="{!isWIFMode}" class="centering maxindex-frame"> <div class="text-with-inlineinput">{TagG.jp ? \'\' : \'Check up to\'}</div> <div class="inline-input"><material-select ref="maxindex_select" hint="Index" is_fullwidth="true" items="{INDEX_SELECT_ITEMS}" default_value="{INDEX_SELECT_ITEMS[0].value + \'\'}"></material-select></div> <div class="text-with-inlineinput">{TagG.jp ? \'番目のアドレスまでチェック\' : \'th address\'}</div> </div> <div class="centering"> <material-button if="{!mining}" label="START ADDRESS MINING" icon="play_circle_filled" is_unelevated="true" on_click="{onClickStartButton}"></material-button> <material-button if="{mining}" label="STOP" icon="stop_circle" is_unelevated="true" on_click="{onClickStopButton}"></material-button> </div> <div if="{Number.isInteger(addrCount)}" class="centering">{commafy(addrCount)}{TagG.jp ? \'個のアドレスをスキャンしました\' : \' addresses scanned.\'}</div> <div class="spinner-box"><util-spinner ref="spinner"></util-spinner></div> <div class="centering"> <div each="{generatedList}" class="generated-item"> <div if="{mnemonic}"> <div class="item-description"> <span if="{TagG.jp}">{addrIndex + 1} 番目のアドレスが {address} になるニーモニック</span> <span if="{!TagG.jp}">Mnemonic with {addrIndex + 1}th address {address}</span> </div> <div class="item-mnemonic">{mnemonic}</div> </div> <div if="{wif}"> <div class="item-description"> <span if="{TagG.jp}">アドレス {address} のWIF</span> <span if="{!TagG.jp}">WIF of {address}</span> </div> <div class="item-mnemonic">{wif}</div> </div> </div> <div if="{generatedList.length && !isWIFMode}" class="new-mnemonic-note"> <span if="{TagG.jp}">生成されたニーモニックは間違いのないように記録して、あなたしか見られない安全な場所に保管しましょう。ニーモニックを普段使いするのは不安なので、ニーモニックを記録したら<a href="{LINK_BASE + \'regenerate_quickurl\'}">Quick Access URL 再生成ツール</a>でQuick Access URLを作るとよいです。</span> <span if="{!TagG.jp}">Keep the generated mnemonics in a safe place where only you can access. As directly using mnemonics on a regular basis is not secure, <a href="{LINK_BASE + \'regenerate_quickurl\'}">generating quick-access URL</a> is recommended.</span> </div> </div> <div if="{!mining}" class="page-footer"> <material-button if="{!isWIFMode}" label="{TagG.jp ? \'WIFモード\' : \'Go to WIF mode\'}" icon="warning" on_click="{onClickWIFModeSwitchButton}"></material-button> <material-button if="{isWIFMode}" label="{TagG.jp ? \'ニーモニックモードに戻る\' : \'Go to mnemonic mode\'}" on_click="{onClickWIFModeSwitchButton}"></material-button> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    this.INDEX_SELECT_ITEMS = [...Array(20).keys()].map(i => ({ value: i, label: i + 1 }));
    this.LINK_BASE = TagG.RELATIVE_LINK_BASE;
    this.commafy = Util.commafy;
    this.generatedList = [];
    this.isWIFMode = false;

    this.onClickPageHelpButton = () => {
        myswal({
            text: (TagG.jp?
            '末尾が指定した文字列になっているアドレスが出るまでひたすらニーモニックを生成します。おしゃれなアドレスをマイニングしてお友達に自慢しましょう\n\n文字数が増えるほど当たりづらくなります。レガシーなアドレス(Base58)とNative Segwitのアドレス(Bech32)では使える文字も当たりやすさも違うので注意してください。カンマ区切りで複数指定すると、どれか1つにマッチした時点で終了するので速くなります\n\nニーモニックからseedを計算するくだりにいちばん時間がかかるので、1つのニーモニックに対してチェックするアドレスの数を増やしたほうが速くなります':
            'You can keep generating mnemonics until you get an address that ends with the specified string.\n\nThe longer the length of the string you specify, the harder it is to hit. Please note that the legacy address (Base58) and the native segwit address (Bech32) differ in both the allowed characters and the ease of matching. If you specify multiple strings separated by commas, it will be faster because it will terminate when it matches one of them.\n\nAs the most time consuming part is calculating the seed from the mnemonic, increasing the number of addresses to check for each mnemonic will make it faster.'),
            buttons: {},
        });
    };
    this.onChangeSegwitSwitch = (isSegwit) => { setSegwit(isSegwit) };
    this.onClickStartButton = () => {
        const postfixs = this.refs.postfix_input.getValue().trim().split(/\s*,\s*/);
        const maxAddrIndex = this.refs.maxindex_select ? Number(this.refs.maxindex_select.getValue()) : null;
        const addrType = this.isSegwit ? ADDR_TYPE.P2WPKH : ADDR_TYPE.P2PKH;
        for (const postfix of postfixs) {
            if (postfix.length === 0) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '空文字列は指定できません' : 'No postfix specified.', 'error');
        }
        aMine(postfixs, addrType, maxAddrIndex);
    };
    this.onClickStopButton = async() => {
        this.cancelled = true;
    };
    this.onClickWIFModeSwitchButton = () => {
        this.isWIFMode = !this.isWIFMode;
        this.update();
    };

    this.on('mount', () => {
        setSegwit(false);
    });
    this.on('before-unmount', () => {
        this.cancelled = true;
        clearInterval(this.counterIntervalID);
    });
    const setSegwit = (isSegwit) => {
        this.isSegwit = isSegwit;
        this.allowedChars = isSegwit ? '023456789acdefghjklmnpqrstuvwxyz' : '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz';
        const charNum = this.allowedChars.length;
        this.probabilityMessage = TagG.jp?
        `2文字で${Util.commafy(charNum**2)}個に1つ、3文字で${Util.commafy(charNum**3)}個に1つ、4文字で${Util.commafy(charNum**4)}個に1つの割合`:
        `2chars → 1/${Util.commafy(charNum**2)}    3chars → 1/${Util.commafy(charNum**3)}    4chars → 1/${Util.commafy(charNum**4)}`;
        this.update();
    };
    const aMine = async(postfixs, addrType, maxAddrIndex = null) => {
        if (!this.isWIFMode) return await aMineMnemonic(postfixs, addrType, maxAddrIndex);
        else return await aMineWIF(postfixs, addrType);
    };
    const aMineMnemonic = async(postfixs, addrType, maxAddrIndex) => {
        this.refs.spinner.start();
        this.mining = true;
        this.cancelled = false;
        this.addrCount = 0;
        const addrFunc = (addrType === ADDR_TYPE.P2WPKH) ? TagG.bitcoinjs.payments.p2wpkh : TagG.bitcoinjs.payments.p2pkh;
        const paths = [...Array(maxAddrIndex + 1).keys()].map(index => `${TagG.addrType2defaultAccountPath(addrType)}/0/${index}`);
        const firstMnemonic = await TagG.aGenerateStrongMnemonic(128);
        const entropyBuffer = Buffer.from(TagG.bip39.mnemonicToEntropy(firstMnemonic), 'hex');
        let mnemonic, addrIndex, address;
        let isFound = false;
        this.update();
        this.counterIntervalID = setInterval(() => { this.update() }, 5000);
        let loopCount = 0;
        while (!this.cancelled && !isFound) {
            incrementLittleEdianBuffer(entropyBuffer);
            const entropy = entropyBuffer.toString('hex');
            mnemonic = TagG.bip39.entropyToMnemonic(entropy);
            const seed = TagG.bip39.mnemonicToSeed(mnemonic);
            addrIndex = 0;
            for (const path of paths) {
                const hdKey = TagG.HDKey.fromMasterSeed(seed).derive(path);
                address = addrFunc({ pubkey: hdKey.publicKey, network: TagG.NETWORK }).address;
                this.addrCount++;
                if (postfixs.some(postfix => (address.slice(-postfix.length) === postfix))) {
                    isFound = true;
                    break;
                }
                addrIndex++;
            }
            if (++loopCount % 10 === 0) await Util.aSleep(1);
        }
        if (isFound) {
            if (addrType === ADDR_TYPE.P2WPKH) mnemonic += ' #segwit';
            this.generatedList.unshift({ mnemonic, addrIndex, address });
        }
        clearInterval(this.counterIntervalID);
        this.mining = false;
        this.update();
        this.refs.spinner.stop();
    };
    const aMineWIF = async(postfixs, addrType) => {
        this.refs.spinner.start();
        this.mining = true;
        this.cancelled = false;
        this.addrCount = 0;
        const addrFunc = (addrType === ADDR_TYPE.P2WPKH) ? TagG.bitcoinjs.payments.p2wpkh : TagG.bitcoinjs.payments.p2pkh;
        const mnemonic = await TagG.aGenerateStrongMnemonic(128);
        const seed = TagG.bip39.mnemonicToSeed(mnemonic);
        const hdKey = TagG.HDKey.fromMasterSeed(seed);
        let privateKey = hdKey.privateKey;
        let ecPair, address;
        let isFound = false;
        this.update();
        this.counterIntervalID = setInterval(() => { this.update() }, 5000);
        let loopCount = 0;
        while (!this.cancelled && !isFound) {
            incrementLittleEdianBuffer(privateKey);
            ecPair = TagG.bitcoinjs.ECPair.fromPrivateKey(hdKey.privateKey, { network: TagG.NETWORK });
            address = addrFunc({ pubkey: ecPair.publicKey, network: TagG.NETWORK }).address;
            this.addrCount++;
            if (postfixs.some(postfix => (address.slice(-postfix.length) === postfix))) {
                isFound = true;
                break;
            }
            if (++loopCount % 500 === 0) await Util.aSleep(1);
        }
        if (isFound) {
            const wif = ecPair.toWIF();
            this.generatedList.unshift({ wif, address });
        }
        clearInterval(this.counterIntervalID);
        this.mining = false;
        this.update();
        this.refs.spinner.stop();
    };
    const incrementLittleEdianBuffer = (buffer) => {
        for (let i = 0; i < buffer.length; i++) {
            if (buffer[i]++ !== 255) break;
        }
    }
});
riot.tag2('markets-page', '<div class="app-page"> <div class="page-header"> <i class="material-icons page-icon">local_grocery_store</i> <h2 class="page-title">Markets</h2> </div> <div class="page-content"> <material-tabbar ref="tabbar" items="{TAB_ITEMS}" default_value="{tab}" on_change="{onChangeTab}"></material-tabbar> <div> <dispensers-tab if="{tab === TABS.DISPENSERS}"></dispensers-tab> <dex-tab if="{tab === TABS.DEX}" asset1="{initAsset1}" asset2="{initAsset2}"></dex-tab> <shops-tab if="{tab === TABS.SHOPS}" shop_id="{shopID}"></shops-tab> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    const TABS = this.TABS = { DEX: 'DEX', DISPENSERS: 'DISPENSERS', SHOPS: 'SHOPS' };
    this.TAB_ITEMS = [
        { value: TABS.DISPENSERS, label: 'Dispensers', icon: 'point_of_sale' },
        { value: TABS.DEX, label: 'DEX', icon: 'swap_horiz' },
    ];
    if (TagG.SHOPS_JSON) this.TAB_ITEMS.unshift({ value: TABS.SHOPS, label: 'Shops', icon: 'storefront' });
    this.tab = opts.tab || this.TABS.SHOPS;
    [this.initAsset1, this.initAsset2] = [opts.param1, opts.param2];
    this.shopID = opts.param1;

    this.onChangeTab = (value) => {
        this.tab = value;
        switch (value) {
            case TABS.DISPENSERS:
                route('/dispensers');
                break;
            case TABS.DEX:
                [this.initAsset1, this.initAsset2] = [null, null];
                route('/dex');
                break;
            case TABS.SHOPS:
                this.shopID = null;
                route('/shops');
                break;
        }
        this.update();
    };
});
riot.tag2('monacard-droparea', '<div class="imagedrop-frame"> <div ref="preview_frame" class="imagedrop-previewframe"><img riot-src="{TagG.RELATIVE_URL_BASE + \'img/transparent_card.png\'}" loading="lazy"></div> <div class="imagedrop-hint">{TagG.jp ? \'【カード画像】\' : \'[ Card Image ]\'}<br><br>{TagG.jp ? \'ヨコxタテ=560x800/840x1200/1260x1800 のPNG/JPG/GIF\' : \'560x800/840x1200/1260x1800 PNG/JPG/GIF\'}</div> <div ref="droparea" class="imagedrop-droparea {loaded: img}"><input type="file" accept="image/png,image/jpeg,image/gif" ref="file_input" ondragover="{onFileDragOver}" ondragleave="{onFileDragLeave}" ondrop="{onFileDrop}" onchange="{onFileChange}"></div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    const ARROWED_TYPES = ['image/png', 'image/jpeg', 'image/gif'];
    this.existingImageURL = opts.existing_src;
    this.img = null;
    this.blob = null;
    this.onFileDragOver = (event) => {
        event.stopPropagation();
        event.preventDefault();
        this.refs.droparea.style.borderColor = 'black';
    };
    this.onFileDragLeave = (event) => {
        event.stopPropagation();
        event.preventDefault();
        this.refs.droparea.style.removeProperty('border-color');
    };
    this.onFileDrop = (event) => {
        event.stopPropagation();
        event.preventDefault();
        this.refs.droparea.style.removeProperty('border-color');
        const files = event.dataTransfer.files;
        if (files.length > 1) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '選択できる画像は1つだけです' : 'Multiple files selected.', 'error');
        this.refs.file_input.files = files;
        loadFile(files[0]);
    };
    this.onFileChange = (event) => {
        if (event.target.files && event.target.files[0]) loadFile(event.target.files[0]);
    };
    this.on('mount', async() => {
        if (this.existingImageURL) {
            try {
                const response = await fetch(this.existingImageURL);
                const blob = await response.blob();
                loadFile(blob);
            }
            catch (err) { console.error(err) }
        }
    });
    const loadFile = (file) => {
        if (!ARROWED_TYPES.includes(file.type)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'サポートしていない画像形式です' : 'Unsupported image format.', 'error');
        const img = new Image();
        const reader = new FileReader();
        reader.onload = (fileLoadEvent) => {
            const dataURL = fileLoadEvent.target.result;
            img.src = dataURL;
        };
        img.onload = () => {
            const container = this.refs.preview_frame;
            while (container && container.firstChild) container.removeChild(container.firstChild);
            container.appendChild(img);
        };
        reader.readAsDataURL(file);
        this.img = img;
        this.blob = file;
    };
});
riot.tag2('monaparty-api-page', '<div class="app-page"> <div class="page-header"> <i class="material-icons page-icon">api</i> <h2 class="page-title">Monaparty API</h2> <div class="page-helpbutton-frame"><material-iconbutton icon="help_outline" on_click="{onClickPageHelpButton}"></material-iconbutton></div> </div> <div class="page-content"> <div class="inputs"> <div class="margin chips"><div each="{monapartyServers}" class="server-item"><material-chip label="{endpoint}" on_click="{onClick}"></material-chip></div></div> <div class="margin"><material-textfield ref="endpoint_input" hint="Monaparty Server"></material-textfield></div> <div class="margin"><material-switch ref="proxy_switch" label="Proxy to counterpartyd" on_change="{onChangeProxy}"></material-switch></div> <div class="margin chips"><div each="{sampleCommands}" class="server-item"><material-chip label="{command}" on_click="{onClick}"></material-chip></div></div> <div class="margin"><material-textfield ref="command_input" hint="Command"></material-textfield></div> <div class="margin json-input"><material-textfield ref="params_input" hint="Parameters JSON" is_outlined="true" is_textarea="true" textarea_rows="8" invalid_message="Invalid JSON" check_func="{checkParams}"></material-textfield></div> <div class="margin"><div class="button"> <util-spinner ref="spinner"></util-spinner> <material-button label="POST" icon="send" is_unelevated="true" is_fullwidth="true" on_click="{onClickPostButton}"></material-button> </div></div> </div> <div class="margin json-input"><material-textfield ref="response_input" hint="Response" is_outlined="true" is_textarea="true" textarea_rows="12"></material-textfield></div> </div> </div>', '', '', function(opts) {
    const main = () => {
        this.TagG = TagG;
        this.monapartyServers = [];
        this.sampleCommands = COUNTERBLOCK_COMMANDS;
        this.onClickPageHelpButton = () => {
            myswal({
                text: TagG.jp?
                '好きなパラメータでMonapartyのAPIにリクエストが送れます。テスト用途や、モナパレットが対応していない機能を使いたい場合にご利用ください\n\nWalletには未署名トランザクションのHEXに署名&ブロードキャストする機能が付いているので、create_xxxの結果を実行したいときは併せて活用してください':
                'This tool helps you to send custom requests to Monaparty API. Use it for testing purposes or when you want to use features that Monapalette does not support.',
                buttons: {},
            });
        };
        this.onChangeProxy = (isProxy) => {
            this.sampleCommands = isProxy ? COUNTERPARTY_COMMANDS : COUNTERBLOCK_COMMANDS;
            this.update();
        }
        this.onClickPostButton = async() => {
            const endpoint = this.refs.endpoint_input.getValue().trim();
            const isProxy = this.refs.proxy_switch.getChecked();
            const command = this.refs.command_input.getValue().trim();
            let params = this.refs.params_input.getValue().trim();
            if (!endpoint) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'サーバを入力してください' : 'No server specified.', 'error');
            if (!command) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'コマンドを入力してください' : 'No command specified.', 'error');
            if (!params) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'パラメータを入力してください' : 'No parameters specified.', 'error');
            try { params = JSON.parse(params) }
            catch (error) { return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'JSONのフォーマットがへんです' : 'Invalid JSON.', 'error') }
            try {
                if (this.working) return;
                this.working = true;
                this.refs.spinner.start();
                const responseText = isProxy?
                await aCounterPartyRequest(endpoint, command, params) : await aCounterBlockRequest(endpoint, command, params);
                this.refs.response_input.setValue(responseText);
            }
            finally {
                this.refs.spinner.stop();
                this.working = false;
            }
        };
        this.checkParams = (value) => {
            if (!value) return true;
            try { JSON.parse(value) }
            catch (error) { return false }
            return true;
        };
        this.on('mount', async() => {
            if (TagG.MONAPARTY_ENDPOINTS) this.monapartyServers = TagG.MONAPARTY_ENDPOINTS.map(endpoint => ({ endpoint }));
            else {
                const { servers } = await Util.aGetJSON(Monaparty.defaults.serversFile, true);
                this.monapartyServers = servers;
            }
            for (const server of this.monapartyServers) {
                server.onClick = () => { this.refs.endpoint_input.setValue(server.endpoint) };
            }
            this.update();
            this.refs.params_input.setValue('{}');
        });
        const aCounterPartyRequest = async(endpoint, command, params) => {
            const counterBlockParams = { method: command, params };
            return await aCounterBlockRequest(endpoint, 'proxy_to_counterpartyd', counterBlockParams);
        }
        const aCounterBlockRequest = async(endpoint, command, params) => {
            const body = { jsonrpc: '2.0', id: 0, method: command, params };
            const bodyStr = JSON.stringify(body);
            const fetchParams = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' },
                body: bodyStr,
            };
            try {
                return await fetch(endpoint, fetchParams).then(res => res.text());
            }
            catch (error) {
                console.error(error);
                myswal('Error', `${error}`, 'error');
            }
        }
        for (const item of [...COUNTERBLOCK_COMMANDS, ...COUNTERPARTY_COMMANDS]) {
            item.onClick = () => {
                this.refs.command_input.setValue(item.command);
                this.refs.params_input.setValue(JSON.stringify(item.params, null, 2));
            };
        }
    };
    const COUNTERBLOCK_COMMANDS = [
        { command: 'get_chain_address_info', params: { addresses: ['__ADDRESS__']}},
        { command: 'get_normalized_balances', params: { addresses: ['__ADDRESS__']}},
        { command: 'get_assets_info', params: { assetsList: ['XMP', 'SPACEMONA', 'DEVILROID']}},
        { command: 'get_chain_address_info', params: { addresses: ['__ADDRESS__']}},
    ];
    const COUNTERPARTY_COMMANDS = [
        { command: 'get_broadcasts', params: { limit: 10, order_by: 'tx_index', order_dir: 'DESC', filters: [{ field: 'text', op: 'LIKE', value: '@b.m %' }]}},
        { command: 'get_holders', params: { asset: 'SPACEMONA' }},
        { command: 'create_send', params: { source: '__FROM_ADDRESS__', destination: '__TO_ADDRESS__', asset: 'XMP', quantity: 100000000 }},
    ];
    main();
});
riot.tag2('rakugaki-page', '<div class="app-page"> <div class="page-header"> <i class="material-icons page-icon">brush</i> <h2 class="page-title">{TagG.jp ? \'らくがきNFT\' : \'Rakugaki NFT\'}</h2> <div class="page-helpbutton-frame"><material-iconbutton icon="help_outline" on_click="{onClickPageHelpButton}"></material-iconbutton></div> </div> <div class="page-content"> <div if="{!addrsLoaded}" class="unavailable-message">{TagG.jp ? \'らくがきNFTで遊ぶにはWalletを開いてください\' : \'To play with Rakugaki NFT, open your wallet.\'}</div> <div if="{addrsLoaded}"> <div class="rakugaki-canvas-frame"> <canvas ref="main_canvas" width="600" height="600" class="main-canvas bg-checker"></canvas> <div ref="image_droparea" class="image-droparea"><input type="file" accept="image/png,image/jpeg,image/gif" ref="file_input" ondragover="{onFileDragOver}" ondragleave="{onFileDragLeave}" ondrop="{onFileDrop}" onchange="{onFileChange}"></div> <canvas if="{isPlacingMode}" ref="placing_canvas" width="600" height="600" class="placing-canvas" onmousedown="{onPlacingMouseDown}" onmousemove="{onPlacingMouseMove}" ontouchstart="{onPlacingMouseDown}" ontouchmove="{onPlacingMouseMove}" onmouseup="{onPlacingMouseEnd}" onmouseout="{onPlacingMouseEnd}" ontouchend="{onPlacingMouseEnd}"></canvas> </div> <div show="{!isPlacingMode}"> <div class="canvas-buttons"> <material-iconbutton icon="delete" on_click="{onClickClearButton}"></material-iconbutton> </div> <div class="rakugaki-titleinput"><material-textfield ref="rakugaki_title" is_outlined="true"></material-textfield></div> </div> <div if="{isPlacingMode}"> <div if="{isPlacingMode}" class="canvas-buttons canvas-buttons-placing"> <material-iconbutton icon="flip" on_click="{onClickPlacingFlipButton}"></material-iconbutton> <material-iconbutton icon="cached" on_click="{onClickPlacingRotateButton}"></material-iconbutton> <material-iconbutton icon="zoom_in" on_click="{onClickPlacingZoomInButton}"></material-iconbutton> <material-iconbutton icon="zoom_out" on_click="{onClickPlacingZoomOutButton}"></material-iconbutton> </div> <div if="{isPlacingMode}" class="canvas-buttons2"> <material-iconbutton icon="close" on_click="{onClickPlacingCancelButton}"></material-iconbutton> <material-iconbutton icon="done" on_click="{onClickPlacingDoneButton}"></material-iconbutton> </div> </div> <div if="{!rakugakis.length}" class="unavailable-message"> <span if="{TagG.jp}">いま開いているウォレットには Rakugaki NFT がありません。からっぽの Rakugaki NFT はDEXで1枚20XMPで買えます</span> <span if="{!TagG.jp}">This wallet has no Rakugaki NFT. Empty Rakugaki NFT can be. Empty Rakugaki NFT can be purchased at DEX for 20XMP.</span> </div> <div if="{rakugakis.length}" class="rakugaki-items"> <div each="{rakugakis}" class="rakugaki-item"> <div if="{!isPlacingMode}" class="rakugaki-item-buttons"> <material-iconbutton icon="arrow_circle_up" on_click="{onClickDrawButton}"></material-iconbutton> <material-iconbutton icon="arrow_circle_down" on_click="{onClickSaveButton}"></material-iconbutton> </div> <div if="{isPlacingMode}" class="rakugaki-item-buttons"> <material-iconbutton icon="arrow_circle_up" is_disabled="true"></material-iconbutton> <material-iconbutton icon="arrow_circle_down" is_disabled="true"></material-iconbutton> </div> <div class="rakugaki-item-asset">{asset}</div> <img ref="{imgRef}" riot-src="{image + \'?\' + TagG.getRakugakiCacheID(asset, true)}" class="bg-checker" crossorigin="anonymous" loading="lazy"> <div class="rakugaki-item-title">{TagG.asset2rakugakiTitle[asset] || \'No Title\'}</div> </div> </div> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    const [IMAGE_WIDTH, IMAGE_HEIGHT] = [600, 600];
    const [CANVAS_WIDTH, CANVAS_HEIGHT] = [300, 300];
    const RAKUGAKI_MAXMBLENGTH = 2;
    const RAKUGAKI_MAXBYTELENGTH = 1024 * 1024 * RAKUGAKI_MAXMBLENGTH;
    const TITLE_MAXLENGTH = 30;
    const ARROWED_TYPES = ['image/png', 'image/jpeg', 'image/gif'];
    const ZOOMS = [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.75, 2.0, 2.25, 2.5, 3.0];
    this.rakugakis = [];
    this.isPlacingMode = false;
    let placingImage = null;
    let placingMouseDownPos = { x: 0, y: 0 };
    let placingTransform, placingTransformCache;
    let isPlacingDragging = false;

    this.onClickPageHelpButton = () => {
        myswal({
            text: TagG.jp?
            '専用の Rakugaki NFT に画像をペタペタして保存できます。ウォレットに入っている別の Rakugaki NFT の画像をペタペタすることもできます。からっぽの Rakugaki NFT はDEXで1枚20XMPで買えます\n\nペタペタする画像の推奨サイズは 600px x 600px です。上の大きなキャンバスにドラッグ & ドロップなりしましょう。透過もOKなので、透過する画像をペタペタすることでシナジーが生まれるかも\n\nRakugaki NFT に保存した画像はだれでも見ることができます。さらに Rakugaki NFT を保有している人は画像のイメージを損なう／公序良俗に反する／社会的な許容限度を超える場合を除いて、保有している Rakugaki NFT に保存された画像を利用する権利をもつものとします。これに同意できる場合だけ、権利的に問題のない画像を Rakugaki NFT に保存してください':
            'You can peta-peta images to Rakugaki NFTs. You can also peta-peta images from other Rakugaki NFTs in your wallet. Empty Rakugaki NFT can be purchased at DEX for 20XMP.\n\nTry drag and drop an image onto the large canvas above. The recommended size of the image to be peta-peta is 600px x 600px. You can create a synergy by peta-petaing images with transparency.\n\nImages saved in Rakugaki NFT can be viewed by anyone. Rakugaki NFT owners have the right to use the images stored in their Rakugaki NFTs, except in the case of damaging the impression, offending public order and morals, or exceeding socially acceptable limits. Only if you agree to this, you can save images in Rakugaki NFT.',
            buttons: {},
        });
    };
    this.onClickClearButton = () => {
        getMainCanvasContext().clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
    };
    this.onClickPlacingCancelButton = () => {
        getPlacingCanvasContext().clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        this.isPlacingMode = false;
        this.update();
    };
    this.onClickPlacingZoomInButton = () => { changeZoom(true) };
    this.onClickPlacingZoomOutButton = () => { changeZoom(false) };
    this.onClickPlacingRotateButton = () => { changeAngle() };
    this.onClickPlacingFlipButton = () => { changeFlip() };
    this.onClickPlacingDoneButton = () => { placeImage() };
    this.onFileDragOver = (event) => {
        event.stopPropagation();
        event.preventDefault();
        this.refs.image_droparea.style.borderColor = 'black';
    };
    this.onFileDragLeave = (event) => {
        event.stopPropagation();
        event.preventDefault();
        this.refs.image_droparea.style.removeProperty('border-color');
    };
    this.onFileDrop = (event) => {
        event.stopPropagation();
        event.preventDefault();
        this.refs.image_droparea.style.removeProperty('border-color');
        const files = event.dataTransfer.files;
        if (files.length > 1) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '選択できる画像は1つだけです' : 'Multiple files selected.', 'error');
        this.refs.file_input.files = files;
        loadFile(files[0]);
    };
    this.onFileChange = (event) => {
        if (event.target.files && event.target.files[0]) loadFile(event.target.files[0]);
    };
    this.onPlacingMouseDown = (event) => {
        event.preventDefault();
        if (isPlacingDragging) return;
        [placingMouseDownPos.x, placingMouseDownPos.y] = getXYOnElement(event);
        placingTransformCache = {...placingTransform};
        isPlacingDragging = true;
    };
    this.onPlacingMouseMove = (event) => {
        event.preventDefault();
        if (!isPlacingDragging) return;
        const [eventX, eventY] = getXYOnElement(event);
        placingTransform.dx = placingTransformCache.dx + (eventX - placingMouseDownPos.x);
        placingTransform.dy = placingTransformCache.dy + (eventY - placingMouseDownPos.y);
        updatePlacingCanvas();
    };
    this.onPlacingMouseEnd = () => {
        event.preventDefault();
        isPlacingDragging = false;
    };
    this.on('mount', async() => {
        if (!TagG.addrsData) return;
        await TagG.aUpdateAddrsDataWithUI(TagG.bodySpinner);
        await aSetRakugakis();
        this.addrsLoaded = true;
        this.update();
    });
    const aSetRakugakis = async() => {
        const rakugakis = [];
        for (const addrData of TagG.addrsData) {
            for (const assetData of addrData.assets) {
                if (assetData.appData.rakugakinft && assetData.satoshi > 0) {
                    const asset = assetData.asset;
                    const imgRef = `img_${asset}`;
                    rakugakis.push({
                        asset,
                        image: assetData.imageM,
                        imgRef,
                        addrData,
                        onClickDrawButton: () => drawRakugaki(imgRef),
                        onClickSaveButton: async() => {
                            const newTitle = this.refs.rakugaki_title.getValue().trim();
                            if (newTitle.length > TITLE_MAXLENGTH) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? `タイトルは${TITLE_MAXLENGTH}文字以内で入力してください` : `Title must be no more than ${TITLE_MAXLENGTH} chars.`, 'error');
                            const ok = await myswal({
                                title: (TagG.jp ? 'らくがきの保存' : 'Save Rakugaki'),
                                text: (TagG.jp ? `${asset} にらくがきを保存します。いま保存されているらくがきは消えますがOKですか？` : `Rakugaki will be saved on ${asset}. Currently saved Rakugaki will be overwritten, OK?`),
                                button: { text: 'OK', closeModal: false },
                                className: 'swalcustom-singlebutton-center',
                            });
                            if (!ok) return;
                            aSaveRakugaki(asset, newTitle, addrData);
                        },
                    });
                }
            }
        }
        rakugakis.sort((rakuA, rakuB) => (rakuA.asset < rakuB.asset ? -1 : 1));
        this.rakugakis = rakugakis;
    };
    const drawRakugaki = (imgRef) => { startPlacing(this.refs[imgRef]) };
    const startPlacing = (image) => {
        placingImage = image;
        initPlacingTransform();
        this.isPlacingMode = true;
        this.update();
        updatePlacingCanvas();
    };
    const updatePlacingCanvas = () => {
        const context = getPlacingCanvasContext();
        context.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        context.translate(placingTransform.dx, placingTransform.dy);
        if (placingTransform.isFlipped) context.scale(-1, 1);
        context.rotate(placingTransform.angle / 180 * Math.PI);
        const drawWidth = placingTransform.initWidth * placingTransform.zoom;
        const drawHeight = placingTransform.initHeight * placingTransform.zoom;
        context.drawImage(placingImage, -drawWidth / 2, -drawHeight / 2, drawWidth, drawHeight);
    };
    const placeImage = () => {
        getMainCanvasContext().drawImage(this.refs.placing_canvas, 0, 0, IMAGE_WIDTH, IMAGE_HEIGHT, 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        getPlacingCanvasContext().clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        this.isPlacingMode = false;
        this.update();
    };
    const initPlacingTransform = () => {
        placingTransform = { initWidth: 0, initHeight: 0, dx: 0, dy: 0, zoom: 1, angle: 0, isFlipped: false };
        if (placingImage.naturalHeight / placingImage.naturalWidth > CANVAS_HEIGHT / CANVAS_WIDTH) {
            placingTransform.initWidth = CANVAS_WIDTH;
            placingTransform.initHeight = CANVAS_WIDTH * placingImage.naturalHeight / placingImage.naturalWidth;
            placingTransform.dx = placingTransform.dy = placingTransform.initWidth / 2;
        }
        else {
            placingTransform.initHeight = CANVAS_HEIGHT;
            placingTransform.initWidth = CANVAS_HEIGHT * placingImage.naturalWidth / placingImage.naturalHeight;
            placingTransform.dx = placingTransform.dy = placingTransform.initHeight / 2;
        }
    };
    const changeZoom = (isZoomIn) => {
        const nowZoom = placingTransform.zoom;
        const nowIndex = ZOOMS.indexOf(nowZoom);
        placingTransform.zoom = isZoomIn ? (ZOOMS[nowIndex + 1] || ZOOMS[ZOOMS.length - 1]) : (ZOOMS[nowIndex - 1] || ZOOMS[0]);
        updatePlacingCanvas();
    };
    const changeAngle = () => {
        if (placingTransform.isFlipped) placingTransform.angle += 11;
        else placingTransform.angle -= 11;
        if (placingTransform.angle < 0) placingTransform.angle += 360;
        if (placingTransform.angle > 360) placingTransform.angle -= 360;
        updatePlacingCanvas();
    };
    const changeFlip = () => {
        placingTransform.isFlipped = !placingTransform.isFlipped;
        updatePlacingCanvas();
    };
    const aSaveRakugaki = async(asset, title, addrData) => {
        try {
            TagG.bodySpinner.start();
            const blob = await aGetBlob(this.refs.main_canvas, 'image/png');
            console.log(`png size: ${blob.size / 1024} KB`);
            if (blob.size > RAKUGAKI_MAXBYTELENGTH) return myswal(TagG.jp ? 'だめ' : 'Oops!', (TagG.jp ? `PNGに変換して${RAKUGAKI_MAXMBLENGTH}MB以内になるものしか保存できません。いまの画像は${blob.size / 1024 / 1024}MBになります` : `Only images that will be less than ${RAKUGAKI_MAXMBLENGTH}MB when converted to PNG can be saved. Current size is ${blob.size / 1024 / 1024}MB.`), 'error');
            const { rakugakiUploadInfo } = await TagG.aSignedJsonRPCRequest(TagG.TOKENVAULT_API, 'setRakugaki', { asset, title }, addrData);
            const formData = new FormData();
            formData.append('Content-Type', blob.type);
            Object.entries(rakugakiUploadInfo.fields).forEach(([key, value]) => formData.append(key, value));
            formData.append('file', blob);
            const response = await fetch(rakugakiUploadInfo.url, { method: 'POST', body: formData });
            const result = await response.text();
            if (!response.ok) throw new Error(`rakugaki upload failed\n${result}`);
            console.log('rakugaki uploaded');
            console.log(result);
            myswal({ title: 'Success', text: (TagG.jp ? '保存しました' : 'Saved.'), icon: 'success', timer: TagG.SWAL_TIMER, buttons: {} });
            TagG.rakugakiAsset2cacheID[asset] = new Date().getTime();
            TagG.asset2rakugakiTitle[asset] = title;
            this.update();
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `エラーが発生しました。ヒント：${error}` : `Failed. hint: ${error}`), 'error');
        }
        finally { TagG.bodySpinner.stop() }
    };
    const loadFile = (file) => {
        if (!ARROWED_TYPES.includes(file.type)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'サポートしていない画像形式です' : 'Unsupported image format.', 'error');
        const image = new Image();
        const reader = new FileReader();
        reader.onload = (fileLoadEvent) => {
            const dataURL = fileLoadEvent.target.result;
            image.src = dataURL;
        };
        image.onload = () => { startPlacing(image) };
        reader.readAsDataURL(file);
    };
    const getMainCanvasContext = () => {
        const context = this.refs.main_canvas.getContext('2d');
        context.setTransform(1, 0, 0, 1, 0, 0);
        context.scale(2, 2);
        return context;
    };
    const getPlacingCanvasContext = () => {
        const context = this.refs.placing_canvas.getContext('2d');
        context.setTransform(1, 0, 0, 1, 0, 0);
        context.scale(2, 2);
        return context;
    };
    const getXYOnElement = (event) => {
        const _event = event.changedTouches ? event.changedTouches[0] : event;
        let x = _event.offsetX || _event.layerX;
        let y = _event.offsetY || _event.layerY;
        if (x === undefined) {
            const clientRect = _event.target.getBoundingClientRect();
            x = _event.pageX - (clientRect.left + window.pageXOffset);
            y = _event.pageY - (clientRect.top + window.pageYOffset);
        }
        return [x, y];
    };
    const aGetBlob = (canvas, mimeType, qualityArgument) => {
        return new Promise(resolve => canvas.toBlob(resolve, mimeType, qualityArgument));
    };
});
riot.tag2('regenerate-quickurl-page', '<div class="app-page"> <div class="page-header"> <i class="material-icons page-icon">build</i> <h2 class="page-title">{TagG.jp ? \'URL再生成ツール\' : \'Quick-Access URL\'}</h2> </div> <div class="page-content"> <div class="centering mnemonic-frame"> <material-textfield ref="existing_mnemonic" hint="Enter existing mnemonic" pattern="{\'([a-z]+\\\\s+){11}[a-z]+(\\\\s+#[0-9a-zA-Z]+)?\'}" invalid_message="Invalid Mnemonic" is_password="true"></material-textfield> </div> <div class="centering or">AND</div> <div class="centering password-frame"> <material-textfield ref="new_password" hint="Enter NEW quick-access password" pattern="{\'.{12,}\'}" invalid_message="Too Short" is_password="true"></material-textfield> </div> <div class="centering"> <material-button label="GENERATE NEW QUICK-ACCESS URL" on_click="{onClickGenerateNewURLButton}" is_unelevated="true"></material-button> </div> <div if="{newQuickAccessURL}" class="centering"> <div class="new-mnemonic-label">New Quick Access URL</div> <div class="new-url">{newQuickAccessURL}</div> <div class="new-url-copy"><material-iconbutton icon="file_copy" on_click="{onClickURLCopyButton}"></material-iconbutton></div> <div class="new-mnemonic-note"> <span if="{TagG.jp}">十分に複雑なパスワードを使っていればQuick Access URLはブックマークなどに保存しても良いですが、他人に見せびらかさないようにしましょう。</span> <span if="{!TagG.jp}">With sufficiently complex password, you can save the quick-access URL in your bookmarks, but do not share it with others.</span> </div> <div class="new-mnemonic-note"> <span if="{TagG.jp}">同じニーモニックとパスワードの組み合わせであっても、Quick Access URLは生成するたびに異なる値になります。</span> <span if="{!TagG.jp}">Even with the same mnemonic and password combination, the quick-access URL will have a different value each time it is generated.</span> </div> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    this.onClickGenerateNewURLButton = async() => {
        const input = this.refs.existing_mnemonic.getValue();
        const mnemonic = input.trim().split(/\s+/).slice(0, 12).join(' ');
        const password = this.refs.new_password.getValue();
        if (!TagG.bip39.validateMnemonic(mnemonic)) {
            if (TagG.validateWIF(mnemonic)) myswal('WIF detected', 'いちおうWIFにも対応していますが開発者が自分で使うために仕込んだ秘密機能なので動作は保証しません', 'warning');
            else return myswal('Invalid Mnemonic', '', 'error');
        }
        if (password.match(/\s/)) return myswal('Invalid Password', 'Password cannot contain white spaces.', 'error');
        if (password.length < 12) return myswal('Password Too Short', 'Password must contain at least 12 characters.', 'error');
        const encMnemonic = TagG.CryptoJS.AES.encrypt(mnemonic, password).toString();
        const uriEncMnemonic = encodeURIComponent(encMnemonic);
        this.newQuickAccessURL = `${location.protocol}//${location.host}${location.pathname}?${TagG.QUERYKEY_ENCMNEMONIC}=${uriEncMnemonic}`;
        if (input.includes('#P2WPKH') || input.includes('#segwit')) this.newQuickAccessURL += `&${TagG.QUERYKEY_ADDRTYPE}=${ADDR_TYPE.P2WPKH}`;
        this.update();
    };
    this.onClickURLCopyButton = () => {
        const success = Util.execCopy(this.newQuickAccessURL);
        if (success) myswal({ title: 'Copied', text: 'Successfully copied.', icon: 'success', timer: TagG.SWAL_TIMER, buttons: {} });
        else myswal('Copy Failed', 'This browser does not support copy execution.', 'error');
    };
});
riot.tag2('shops-tab', '<div class="tab-frame"> <div class="search-frame"> <material-textfield ref="search_input" hint="Search" on_input="{onInputSearch}"></material-textfield> <div class="search-clearbutton-frame"><material-iconbutton icon="clear" on_click="{onClickClearSearchButton}"></material-iconbutton></div> </div> <div class="shops"> <div each="{shopsToShow}" class="shop-item" onclick="{onClick}"> <div class="image-frame"><img riot-src="{image || TagG.RELATIVE_URL_BASE + \'img/transparent_1200x630.png\'}" width="1200" height="630" loading="lazy"></div> <div class="info-frame"> <div class="shop-icon"><img riot-src="{icon || TagG.RELATIVE_URL_BASE + \'img/monapalette_notmb.png\'}" loading="lazy"></div> <div class="shop-name">{name}</div> <div class="shop-twitter">@{twitterSN}</div> <div class="shop-description">{description}</div> </div> </div> <util-spinner ref="spinner"></util-spinner> </div> </div> <div if="{shop}" class="overlay"><div ref="overlay_slider" class="overlay-slider"> <div class="overlay-header" riot-style="background-image: url(\'{shop.image}\');"> <div class="icon"><img riot-src="{shop.icon || TagG.RELATIVE_URL_BASE + \'img/monapalette_notmb.png\'}" loading="lazy"></div> <div class="title">{shop.name}</div> <div class="description"><pre>{shop.description}</pre></div> <div class="twitter"><a href="{shop.twitterURL}" target="_blank">@{shop.twitterSN}</a></div> </div> <div class="overlay-content"> <div if="{!shop.dispensers.length && !shop.dexCurrencies.length}" class="soldout">SOLD OUT</div> <div if="{shop.dispensers.length}" class="section"> <div class="section-name"><i class="material-icons inline-icon">point_of_sale</i> Dispensers</div> <div class="tokensale-items"> <div each="{shop.dispensers}" class="tokensale-item {tokensale-item-square: assetInfo.appData.monacharat || assetInfo.appData.rakugakinft}" onclick="{onClickDispenser}"> <img if="{!imageURL}" class="image" riot-src="{TagG.RELATIVE_URL_BASE + \'img/noimage.png\'}" loading="lazy"> <img if="{imageURL && !assetInfo.appData.rakugakinft}" class="image" riot-src="{imageURL}" loading="lazy"> <img if="{imageURL && assetInfo.appData.rakugakinft}" class="image bg-checker" riot-src="{imageURL + \'?\' + TagG.getRakugakiCacheID(asset)}" loading="lazy"> <div class="info-frame"> <div if="{!assetGroup}">{assetInfo.shortenName}<span if="{unitAssetAmount !== 1}" class="amount"> x&nbsp;{unitAssetAmountS}</span></div> <div if="{assetGroup}">{assetGroup} <span class="nft-tag"></span></div> <div if="{assetGroup}" class="asset-id">{assetName}</div> <div>{unitBaseAmountS}<span class="unit">{baseAsset}</span></div> <div class="addr-button"> <div if="{awaseuriCount > 1}" class="setcount">{awaseuriCount} set</div> <div class="hashcolors"><util-hashcolors seed_value="{address}"></util-hashcolors></div> </div> </div> <div if="{TagG.asset2isInMyWallet[asset]}" class="status-icon status-icon-wallet"><i class="material-icons">account_balance_wallet</i></div> <div if="{TagG.asset2treasures[asset]}" class="status-icon status-icon-treasure"><i class="material-icons">attachment</i></div> </div> </div> </div> <div if="{shop.dexCurrencies.length}" class="section"> <div class="section-name"><i class="material-icons inline-icon">swap_horiz</i> DEX Sales</div> <div each="{currency in shop.dexCurrencies}" class="subsection"> <div class="subsection-name" if="{shop.dexCurrencies.length > 1}"><i class="material-icons inline-icon">payments</i> {currency}</div> <div class="tokensale-items"> <div each="{shop.dexCurrency2sales[currency]}" class="tokensale-item {tokensale-item-square: assetInfo.appData.monacharat || assetInfo.appData.rakugakinft}" onclick="{onClickDexSale}"> <img if="{!imageURL}" class="image" riot-src="{TagG.RELATIVE_URL_BASE + \'img/noimage.png\'}" loading="lazy"> <img if="{imageURL && !assetInfo.appData.rakugakinft}" class="image" riot-src="{imageURL}" loading="lazy"> <img if="{imageURL && assetInfo.appData.rakugakinft}" class="image bg-checker" riot-src="{imageURL + \'?\' + TagG.getRakugakiCacheID(assetInfo.asset)}" loading="lazy"> <div class="info-frame"> <div if="{!assetInfo.group}">{assetInfo.shortenName}</div> <div if="{assetInfo.group}">{assetInfo.group} <span class="nft-tag"></span></div> <div if="{assetInfo.group}" class="asset-id">{assetInfo.name}</div> <div>{priceS}<span class="unit">{currencyAssetInfo.name}</span></div> </div> <div if="{TagG.asset2isInMyWallet[assetInfo.asset]}" class="status-icon status-icon-wallet"><i class="material-icons">account_balance_wallet</i></div> <div if="{TagG.asset2treasures[assetInfo.asset]}" class="status-icon status-icon-treasure"><i class="material-icons">attachment</i></div> </div> </div> </div> </div> </div> <div class="overlay-closebutton"><material-iconbutton icon="close" on_click="{onClickOverlayCloseButton}"></material-iconbutton></div> </div></div>', '', '', function(opts) {
    const ROUTE_NAME = 'shops';
    const init = () => {
        this.TagG = TagG;
        this.commafy = Util.commafy;
        this.baseAsset = TagG.BASECHAIN_ASSET;
        this.initShopID = opts.shop_id;
        this.shops = [];
        this.shopsToShow = [];
        this.searchedQuery = '';
        this.shop = null;
        this.docTitleCache = document.title;
        if (TagG.query[TagG.QUERYKEY_SEARCH]) {
            this.searchedQuery = decodeURIComponent(TagG.query[TagG.QUERYKEY_SEARCH].replace(/\+/g, ' '));
            delete TagG.query[TagG.QUERYKEY_SEARCH];
        }
    };
    this.onInputSearch = (query) => {
        query = query.trim();
        if (query === this.searchedQuery) return;
        search(query);
    };
    this.onClickClearSearchButton = () => {
        if (this.searchedQuery === '') return;
        search('');
    }
    this.onClickOverlayCloseButton = () => {
        route(`/${ROUTE_NAME}`);
        setURLQuery(this.searchedQuery);
        document.title = this.docTitleCache;
        this.refs.overlay_slider.style.pointerEvents = 'none';
        TweenLite.to(this.refs.overlay_slider, 0.5, {
            top: '100vh',
            onComplete: () => {
                this.shop = null;
                this.update();
            },
        });
    };
    this.on('mount', async() => {
        await aShowShops();
        if (this.initShopID) {
            const shop = this.shops.find(_shop => _shop.shopID === this.initShopID);
            if (shop) shop.onClick();
        }
        if (TagG.addrsData) await TagG.aUpdateAddrsDataWithUI(null).then(() => { this.update() });
        TagG.aLoadAssetsExtendedInfo().then(isUpdated => { if (isUpdated) this.update() });
    });
    const aShowShops = async() => {
        try {
            if (this.refs.spinner) this.refs.spinner.start();
            const { twID2shopsData } = await Util.aGetJSON(`${TagG.SHOPS_JSON}?${TagG.getCacheQuery(10)}`);
            this.shops = [];
            for (const twID in twID2shopsData) {
                const { twSN, shops } = twID2shopsData[twID];
                for (const { hex, name, desc, img, ico, addrs } of shops) {
                    const shopID = `${twID}_${hex}`;
                    const shop = {
                        shopID,
                        name,
                        description: desc,
                        twitterSN: twSN,
                        twitterURL: TagG.TWITTERUSERURL_FORMAT.replace('{SN}', twSN),
                        addrs,
                        dispensers: [],
                        dexSales: [],
                        dexCurrencies: [],
                        dexCurrency2sales: {},
                        score: 0,
                    };
                    if (img) shop.image = `${TagG.SHOPS_IMGROOT}${shopID}`;
                    if (ico) shop.icon = `${TagG.SHOPS_IMGROOT}${shopID}_icon`;
                    shop.onClick = () => {
                        this.shop = shop;
                        this.update();
                        route(`/${ROUTE_NAME}/${encodeURIComponent(shopID)}`);
                        document.title = `${this.docTitleCache} / ${shop.name}`;
                        TweenLite.to(this.refs.overlay_slider, 0.5, { top: '0vh' });
                    };
                    this.shops.push(shop);
                }
            }
            await Promise.all([aSetDispensers(), aSetDexSales(), aSetScore().catch(console.error)]);
            for (const shop of this.shops) shop.searchText = getSearchText(shop);
            this.shops.sort(shopSort);
            search(this.searchedQuery);
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `データの取得に失敗しました。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}` : `Failed to fetch data. hint: ${error}`), 'error');
        }
        finally { if (this.refs.spinner) this.refs.spinner.stop() }
    };
    const aSetScore = async() => {
        if (!TagG.HOLDERS_CACHEJSON) return;
        const SCORE_ASSET = 'MONAPALETTE';
        const { asset2addr2qty } = await Util.aGetJSON(TagG.HOLDERS_CACHEJSON);
        const addr2qty = asset2addr2qty[SCORE_ASSET] || {};
        for (const shop of this.shops) {
            shop.score = 0;
            for (const addr of shop.addrs) shop.score += addr2qty[addr] || 0;
        }
    };
    const aSetDispensers = async() => {
        const dispensers = await TagG.aGetDispensers();
        const addr2dispensers = {};
        for (const dispenser of dispensers) {
            if (!addr2dispensers[dispenser.address]) addr2dispensers[dispenser.address] = [];
            addr2dispensers[dispenser.address].push(dispenser);
            dispenser.onClickDispenser = () => { TagG.bodyDispenserPurchase.open(dispenser, 'shops') };
        }
        for (const dispenser of dispensers) dispenser.awaseuriCount = addr2dispensers[dispenser.address].length;
        for (const shop of this.shops) {
            shop.dispensers = [];
            for (const addr of shop.addrs) {
                if (addr2dispensers[addr]) shop.dispensers.push(...addr2dispensers[addr]);
            }
        }
    };
    const aSetDexSales = async() => {
        const orders = await Monaparty.aPartyTableAll('get_orders', { 'status': 'open' });
        const addr2orders = {};
        for (const order of orders) {
            if (order.give_asset === TagG.BASECHAIN_ASSET || order.get_asset === TagG.BASECHAIN_ASSET) continue;
            if (!addr2orders[order.source]) addr2orders[order.source] = [];
            addr2orders[order.source].push(order);
        }
        const assetsInShops = [];
        for (const shop of this.shops) {
            for (const addr of shop.addrs) {
                const addrOrders = addr2orders[addr] || [];
                assetsInShops.push(...addrOrders.map(order => order.give_asset), ...addrOrders.map(order => order.get_asset));
            }
        }
        await TagG.aLoadAssets(assetsInShops, true, true);
        for (const shop of this.shops) {
            const shopOrders = [];
            for (const addr of shop.addrs) {
                for (const order of (addr2orders[addr] || [])) {
                    if (order.give_asset === TagG.BUILTIN_ASSET) continue;
                    const existingOrder = shopOrders.find(_order => (_order.give_asset === order.give_asset && _order.get_asset === order.get_asset));
                    if (!existingOrder) shopOrders.push(order);
                    else {
                        const ratio = order.get_quantity / order.give_quantity;
                        const existingRatio = existingOrder.get_quantity / existingOrder.give_quantity;
                        if (ratio < existingRatio) shopOrders.push(order);
                        else if (ratio === existingRatio) {
                            existingOrder.get_quantity += order.get_quantity;
                            existingOrder.get_remaining += order.get_remaining;
                            existingOrder.give_quantity += order.give_quantity;
                            existingOrder.give_remaining += order.give_remaining;
                        }
                    }
                }
            }
            const shopSales = shopOrders.map(order2sale);
            shop.dexSales = shopSales;
            shop.dexCurrencies = Util.uniq(shopSales.map(sale => sale.currencyAssetInfo.asset));
            shop.dexCurrencies.sort(currencySort);
            shop.dexCurrency2sales = {};
            for (const currency of shop.dexCurrencies) {
                shop.dexCurrency2sales[currency] = shopSales.filter(sale => sale.currencyAssetInfo.asset === currency);
            }
        }
    };
    const shopSort = (s1, s2) => {
        if (s1.score > s2.score) return -1;
        if (s2.score > s1.score) return 1;
        return s1.addrs.length > s2.addrs.length ? -1 : 1;
    };
    const currencySort = (c1, c2) => {
        if (c1 === TagG.BUILTIN_ASSET) return -1;
        if (c2 === TagG.BUILTIN_ASSET) return 1;
        return c1 < c2 ? -1 : 1;
    };
    const order2sale = (order) => {
        const assetInfo = TagG.asset2info[order.give_asset];
        const currencyAssetInfo = TagG.asset2info[order.get_asset];
        const price = getSalePrice(order);
        const cardData = TagG.asset2card[assetInfo.asset] || null;
        const mainDescription = TagG.getMainDescription(assetInfo);
        const sale = {
            assetInfo,
            currencyAssetInfo,
            price,
            priceS: Util.commafy(price),
            remainingSat: order.give_remaining,
            remainingAmount: TagG.qty2amount(order.give_remaining, assetInfo.isDivisible),
            imageURL: TagG.getAssetImageURL('auto', assetInfo.asset),
            largeImageURL: TagG.getAssetImageURL('L', assetInfo.asset),
            cardData,
            mainDescription,
        };
        sale.onClickDexSale = () => { TagG.bodyDexPurchase.open(sale) };
        return sale;
    };
    const getSalePrice = (saleOrder) => {
        const assetInfo = TagG.asset2info[saleOrder.give_asset];
        const currencyAssetInfo = TagG.asset2info[saleOrder.get_asset];
        const getQtyN = BigInt(saleOrder.get_quantity);
        const giveQtyN = BigInt(saleOrder.give_quantity);
        let priceQtyN;
        if (assetInfo.isDivisible) {
            priceQtyN = getQtyN * BigInt(TagG.SATOSHI_RATIO) / giveQtyN;
            if (getQtyN * BigInt(TagG.SATOSHI_RATIO) % giveQtyN) priceQtyN++;
        }
        else {
            priceQtyN = getQtyN / giveQtyN;
            if (getQtyN % giveQtyN) priceQtyN++;
        }
        const priceQty = Number(priceQtyN);
        const price = TagG.qty2amount(priceQty, currencyAssetInfo.isDivisible);
        return price;
    };
    const getSearchText = (shop) => {
        const { name, description, twitterSN, dispensers, dexSales } = shop;
        let text = `${name} ${description} @${twitterSN} ${twitterSN.toLowerCase()} `;
        for (const dispenser of dispensers) text += `${dispenser.assetName} `;
        for (const sale of dexSales) text += `${sale.assetInfo.name} `;
        return text;
    };
    const search = (query) => {
        this.searchedQuery = query;
        const searchID = Math.random();
        this.searchID = searchID;
        let isOR = false;
        let words = query.split(/\s+OR\s+/).map(word => word.trim()).filter(word => word);
        if (words.length > 1) isOR = true;
        else words = query.split(/\s+/).filter(word => word);
        let filteredList;
        if (words.length === 0) filteredList = this.shops.slice();
        else {
            if (isOR) {
                filteredList = this.shops.filter(shop => {
                    for (const word of words) {
                        if (shop.searchText.includes(word)) return true;
                    }
                    return false;
                });
            }
            else {
                filteredList = this.shops.filter(shop => {
                    for (const word of words) {
                        if (!shop.searchText.includes(word)) return false;
                    }
                    return true;
                });
            }
        }
        if (this.isReverse) filteredList.reverse();
        if (this.refs.search_input && this.refs.search_input.getValue() !== query) this.refs.search_input.setValue(query);
        setURLQuery(query);
        const FIRST = 30;
        const ONCE = 300;
        this.shopsToShow = filteredList.slice(0, FIRST);
        this.update();
        let length = FIRST;
        const showMore = () => {
            if (length >= filteredList.length || searchID !== this.searchID) return;
            if (!TagG.IS_LAZYLOADAVAILABLE && length > 300) return;
            length += ONCE;
            this.shopsToShow = filteredList.slice(0, length);
            this.update();
            setTimeout(showMore, 0);
        };
        setTimeout(showMore, 0);
    };
    const setURLQuery = (query) => {
        const urlObj = new URL(location);
        if (query) urlObj.searchParams.set(TagG.QUERYKEY_SEARCH, query);
        else urlObj.searchParams.delete(TagG.QUERYKEY_SEARCH);
        history.replaceState(null, '', urlObj.toString());
    };
    init();
});
riot.tag2('step-page', '<div> <div class="button" onclick="{onClickUnlockButton}"> <i class="material-icons icon">lock_open</i> <div class="message">{TagG.jp ? \'タッチしてMpurseを開く\' : \'Touch to open Mpurse.\'}</div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    this.onClickUnlockButton = () => { route('/') };
    this.on('mount', () => {
        if (window.mpurse) route('/');
    });
});
riot.tag2('tokenvault-edit-tab', '<div class="edit-tab"> <div class="center"> <div if="{!addrsLoaded}" class="unavailable-message">{TagG.jp ? \'お宝を登録するにはWalletを開いてください\' : \'To set treasure, open your wallet.\'}</div> <div if="{addrsLoaded}"> <div if="{!tokenSelectItems.length}" class="unavailable-message">{TagG.jp ? \'いま開いているウォレットにはオーナー権限のあるトークンがありません\' : \'This wallet has no token ownership.\'}</div> <div if="{tokenSelectItems.length}"> <div class="edit-token"><material-select ref="edit_token" hint="Token" items="{tokenSelectItems}" on_change="{onChangeToken}"></material-select></div> <div class="edit-x">x</div> <div class="edit-amount"><material-textfield ref="edit_amount" hint="Amount Required" pattern="{\'[0-9]*(\\\\.[0-9]+)?\'}" invalid_message="Invalid Format" on_change="{onChangeAmount}"></material-textfield></div> <div class="edit-memo"><material-textfield ref="edit_memo" hint="Description" pattern="{\'.{0,60}\'}" invalid_message="Too Long" on_change="{onChangeMemo}"></material-textfield></div> <div class="treasureitem-preview"> <div class="treasure-tmbframe"> <canvas class="thumbnail-droparea-preview" ref="thumbnail_canvas" width="600" height="600"></canvas> <div ref="edit_thumbnail_droparea" class="thumbnail-droparea"> <div if="{!isThumbnailLoaded}" class="thumbnail-droparea-label">{TagG.jp ? \'サムネイル画像\' : \'Thumbnail Image\'}<br><br>{TagG.jp ? \'推奨サイズ\' : \'recommended size\'}<br>600px x 600px</div> <input type="file" accept="image/png,image/jpeg,image/gif" ref="edit_thumbnail" ondragover="{onThumbnailDragOver}" ondragleave="{onThumbnailDragLeave}" ondrop="{onThumbnailDrop}" onchange="{onThumbnailChange}"> </div> <div class="treasure-amount open"><i class="material-icons">lock_open</i>&ensp;M / {amount || \'N\'}</div> <div class="treasure-buttonsframe"> <div if="{isThumbnailLoaded}" class="treasureitem-button"><material-iconbutton icon="delete" is_white="true" on_click="{onClickThumbnailClearButton}"></material-iconbutton></div> </div> </div> <div class="treasure-labelframe"> <div if="{!assetGroup}" class="assetname">{assetName || \'PREVIEW\'}</div> <div if="{assetGroup}" class="assetname">{assetGroup} <span class="nft-tag"></span></div> <div if="{assetGroup}" class="asset-id">{assetName}</div> <div if="{memo}" class="description">{memo}</div> </div> </div> <div ref="edit_editor" class="editor"></div> <div class="edit-upload-button"><material-button label="{TagG.jp ? \'アップロード\' : \'UPLOAD\'}" icon="cloud_upload" is_unelevated="true" is_fullwidth="true" on_click="{onClickUploadButton}"></material-button></div> </div> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    const TREASURE_PER_ASSET = 5;
    const [CANVAS_WIDTH, CANVAS_HEIGHT] = [300, 300];
    const TREASURE_MAXMBLENGTH = 2;
    const TREASURE_MAXBYTELENGTH = 1024 * 1024 * TREASURE_MAXMBLENGTH;
    const MEMO_MAXLENGTH = 60;
    this.initTrsrData = opts.trsr_data;
    this.isThumbnailLoaded = false;
    this.isThumbnailChanged = false;
    this.isEditingInitData = false;
    this.tokenSelectItems = [];

    this.onChangeToken = (value) => {
        const item = this.tokenSelectItems[value];
        if (item) {
            this.assetName = item.assetData.name;
            this.assetGroup = item.assetData.group;
            this.isEditingInitData = false;
            this.isThumbnailChanged = true;
            this.update();
        }
    };
    this.onChangeAmount = (value) => {
        this.amount = value;
        this.isEditingInitData = false;
        this.isThumbnailChanged = true;
        this.update();
    };
    this.onChangeMemo = (value) => { this.memo = value; this.update() };
    this.onThumbnailDragOver = (event) => {
        event.stopPropagation();
        event.preventDefault();
        this.refs.edit_thumbnail_droparea.style.borderColor = 'black';
    };
    this.onThumbnailDragLeave = (event) => {
        event.stopPropagation();
        event.preventDefault();
        this.refs.edit_thumbnail_droparea.style.removeProperty('border-color');
    };
    this.onThumbnailDrop = (event) => {
        event.stopPropagation();
        event.preventDefault();
        this.refs.edit_thumbnail_droparea.style.removeProperty('border-color');
        const files = event.dataTransfer.files;
        if (files.length > 1) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '選択できる画像は1つだけです' : 'Multiple files selected.', 'error');
        this.refs.edit_thumbnail.files = files;
        previewThumbnail(files[0]);
    };
    this.onThumbnailChange = (event) => {
        if (event.target.files && event.target.files[0]) previewThumbnail(event.target.files[0]);
    };
    this.onClickThumbnailClearButton = () => {
        getThumbnailCanvasContext().clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        this.isThumbnailLoaded = false;
        this.isThumbnailChanged = true;
        this.update();
    };
    this.onClickUploadButton = async() => {
        try {
            TagG.bodySpinner.start();
            const tokenItem = this.tokenSelectItems[this.refs.edit_token.getValue()];
            if (!tokenItem) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'トークンを選択してください' : 'No token selected.', 'error');
            const { assetData, addrData } = tokenItem;
            const amountS = this.refs.edit_amount.getValue().trim();
            const quantityS = amountS2quantityS(amountS, assetData.isDivisible);
            if (!quantityS) return;
            const memo = this.refs.edit_memo.getValue();
            if (memo.length > MEMO_MAXLENGTH) return myswal(TagG.jp ? 'だめ' : 'Oops!', (TagG.jp ? `Descriptionは${MEMO_MAXLENGTH}文字以内で入力してください` : `Description must be no more than ${MEMO_MAXLENGTH} chars.`), 'error');
            const treasureHTML = this.editor.getData();
            const treasureSize = new Blob([treasureHTML], { type: 'text/plain' }).size;
            const treasureMBSizeS = (treasureSize / 1024 / 1024).toFixed(2);
            console.log(`treasure size: ${treasureMBSizeS}MB`);
            if (treasureSize > TREASURE_MAXBYTELENGTH)
                return myswal(TagG.jp ? 'だめ' : 'Oops!', (TagG.jp ? `記事は画像も含めて${TREASURE_MAXMBLENGTH}MB以内にしてください。現在のサイズは${treasureMBSizeS}MBあります` : `Articles should be no larger than ${TREASURE_MAXMBLENGTH}MB, including images. Current size is ${treasureMBSizeS}MB.`), 'error');
            if (this.treasures) {
                const isOverwrite = this.treasures.some(trsr => (trsr.asset === assetData.asset && trsr.qty === quantityS));
                if (!isOverwrite) {
                    const count = this.treasures.filter(trsr => (trsr.asset === assetData.asset)).length;
                    if (count >= TREASURE_PER_ASSET) return myswal(TagG.jp ? 'だめ' : 'Oops!', (TagG.jp ? `1つのトークンに登録できるお宝は${TREASURE_PER_ASSET}個までです` : `You can only register up to ${TREASURE_PER_ASSET} treasures per token.`), 'error');
                }
            }
            const treasureData = { html: treasureHTML };
            let thumbnailBlob, thumbnailType;
            if (this.isThumbnailLoaded) {
                if (this.isThumbnailChanged) {
                    const thumbnailBlobPNG = await aGetBlob(this.refs.thumbnail_canvas, 'image/png');
                    const thumbnailBlobJPG = await aGetBlob(this.refs.thumbnail_canvas, 'image/jpeg', 0.85);
                    console.log(`thumbnail png size: ${thumbnailBlobPNG.size / 1024}KB`);
                    console.log(`thumbnail jpg size: ${thumbnailBlobJPG.size / 1024}KB`);
                    if (thumbnailBlobPNG.size < thumbnailBlobJPG.size) {
                        thumbnailBlob = thumbnailBlobPNG;
                        thumbnailType = 'PNG';
                    }
                    else {
                        thumbnailBlob = thumbnailBlobJPG;
                        thumbnailType = 'JPG';
                    }
                }
                else thumbnailType = 'UNCHANGED';
            }
            else {
                if (this.isEditingInitData && !this.isThumbnailChanged) thumbnailType = 'UNCHANGED';
                else thumbnailType = 'NONE';
            }
            const treasureParams = { asset: assetData.asset, quantity: quantityS, type: 'HTML', thumbnailType, memo };
            const { treasureUploadInfo, thumbnailUploadInfo } =
                await TagG.aSignedJsonRPCRequest(TagG.TOKENVAULT_API, 'setTreasure', treasureParams, addrData);
            const trsrFormData = new FormData();
            trsrFormData.append('Content-Type', 'application/json');
            Object.entries(treasureUploadInfo.fields).forEach(([key, value]) => trsrFormData.append(key, value));
            trsrFormData.append('file', JSON.stringify(treasureData));
            const treasureResponse = await fetch(treasureUploadInfo.url, { method: 'POST', body: trsrFormData });
            const treasureResult = await treasureResponse.text();
            if (!treasureResponse.ok) throw new Error('treasure upload failed.\n', treasureResult);
            console.log('treasure uploaded');
            console.log(treasureResult);
            if (thumbnailUploadInfo && thumbnailBlob) {
                const tmbFormData = new FormData();
                tmbFormData.append('Content-Type', thumbnailBlob.type);
                Object.entries(thumbnailUploadInfo.fields).forEach(([key, value]) => tmbFormData.append(key, value));
                tmbFormData.append('file', thumbnailBlob);
                const thumbnailResponse = await fetch(thumbnailUploadInfo.url, { method: 'POST', body: tmbFormData });
                const thumbnailResult = await thumbnailResponse.text();
                if (!thumbnailResponse.ok) throw new Error('thumbnail upload failed.\n\n', thumbnailResult);
                console.log('thumbnail uploaded');
                console.log(thumbnailResult);
            }
            myswal({ title: 'Success', text: (TagG.jp ? 'アップロードしました' : 'Uploaded.'), icon: 'success', timer: TagG.SWAL_TIMER, buttons: {} });
            TagG.tokenVaultCacheID = new Date().getTime();
        }
        catch (error) {
            console.error(error);
            myswal('Error', TagG.jp ? `エラーが発生しました。ヒント：${error}` : `Failed. hint: ${error}`, 'error');
        }
        finally { TagG.bodySpinner.stop() }
    };

    this.on('mount', async() => {
        if (!TagG.addrsData) return;
        await TagG.aUpdateAddrsDataWithUI(TagG.bodySpinner);
        const tokenSelectItems = [];
        for (const addrData of TagG.addrsData) {
            for (const asset of addrData.assets) {
                if (asset.isOwn) tokenSelectItems.push({ label: asset.longLabel, assetData: asset, addrData: addrData });
            }
        }
        tokenSelectItems.sort((a, b) => {
            const [assetA, assetB] = [a.assetData, b.assetData];
            let [nameA, nameB] = [assetA.name.toLowerCase(), assetB.name.toLowerCase()];
            if (nameA[0] === 'a') nameA = `~${nameA}`;
            if (nameB[0] === 'a') nameB = `~${nameB}`;
            if (assetA.group) nameA = `~~G${assetA.group}${nameA}`;
            if (assetB.group) nameB = `~~G${assetB.group}${nameB}`;
            if (nameA < nameB) return -1;
            if (nameA > nameB) return 1;
            return 0;
        });
        tokenSelectItems.forEach((item, index) => { item.value = `${index}` });
        this.tokenSelectItems = tokenSelectItems;
        this.addrsLoaded = true;
        this.update();
        setTimeout(aSetupEditor, 0);
        aLoadTreasures();
    });
    this.on('before-unmount', () => {
        if (this.editor) this.editor.destroy();
    });
    const aLoadTreasures = async() => {
        const { treasures } = await Util.aGetJSON(`${TagG.TOKENVAULT_ROOT}public/tokenvault.json?${TagG.tokenVaultCacheID}`, true);
        this.treasures = treasures;
    };
    const previewThumbnail = (file) => {
        const ARROWED_TYPES = ['image/png', 'image/jpeg', 'image/gif'];
        const [CROP_WIDTH_RATE, CROP_HEIGHT_RATE] = [600, 600];
        if (!ARROWED_TYPES.includes(file.type)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'サポートしていない画像形式です' : 'Unsupported image format.', 'error');
        const image = new Image();
        const reader = new FileReader();
        reader.onload = (fileLoadEvent) => {
            const dataURL = fileLoadEvent.target.result;
            image.src = dataURL;
        };
        image.onload = () => {
            let cropX, cropY, cropWidth, cropHeight;
            if (image.height / image.width > CROP_HEIGHT_RATE / CROP_WIDTH_RATE) {
                cropWidth = image.width;
                cropHeight = cropWidth * CROP_HEIGHT_RATE / CROP_WIDTH_RATE;
                cropX = 0;
                cropY = (image.height - cropHeight) / 2;
            }
            else {
                cropHeight = image.height;
                cropWidth = cropHeight * CROP_WIDTH_RATE / CROP_HEIGHT_RATE;
                cropY = 0;
                cropX = (image.width - cropWidth) / 2;
            }
            const context = getThumbnailCanvasContext();
            context.fillStyle = 'rgb(255, 255, 255)';
            context.fillRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
            context.drawImage(image, cropX, cropY, cropWidth, cropHeight, 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
            this.isThumbnailLoaded = true;
            this.isThumbnailChanged = true;
            this.update();
        };
        reader.readAsDataURL(file);
    };
    const getThumbnailCanvasContext = () => {
        const context = this.refs.thumbnail_canvas.getContext('2d');
        context.setTransform(1, 0, 0, 1, 0, 0);
        context.scale(2, 2);
        return context;
    };
    const aSetupEditor = async() => {
        if (!this.refs.edit_editor) return;
        this.editor = await BalloonBlockEditor.create(this.refs.edit_editor, BALLOONBLOCKEDITOR_SETTINGS);
        await aFillWithInitData();
    };
    const aFillWithInitData = async() => {
        try {
            if (!this.initTrsrData) return;
            TagG.bodySpinner.start();
            const { thumbnailURL, assetName, amountS, memo, treasureDownloadURL } = this.initTrsrData;
            const tokenItem = this.tokenSelectItems.find(item => item.assetData.name === assetName);
            if (!tokenItem) return;
            this.refs.edit_token.setValue(tokenItem.value);
            this.refs.edit_amount.setValue(amountS);
            this.refs.edit_memo.setValue(memo);
            this.assetName = assetName;
            this.assetGroup = tokenItem.assetData.group;
            this.amount = amountS;
            this.memo = memo;
            this.update();
            const { html } = await Util.aGetJSON(treasureDownloadURL);
            this.editor.setData(html);
            if (thumbnailURL) {
                const image = new Image();
                image.crossOrigin = 'anonymous';
                image.onload = () => {
                    const context = getThumbnailCanvasContext();
                    context.fillStyle = 'rgb(255, 255, 255)';
                    context.fillRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
                    context.drawImage(image, 0, 0, image.width, image.height, 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
                    this.isThumbnailLoaded = true;
                    this.isEditingInitData = true;
                    this.update();
                };
                image.src = thumbnailURL;
            }
        }
        catch (error) { console.error(error) }
        finally { TagG.bodySpinner.stop() }
    };
    const amountS2quantityS = (amountS, isDivisible) => {
        if (isDivisible) {
            if (!amountS.match(/^[0-9]{1,11}(\.[0-9]{1,8})?$/)) { myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'トークン必要量は0以上の数値を半角で入力してください' : 'Invalid amount.', 'error'); return null }
            const [intPart, fracPart] = amountS.split('.');
            const quantityS = intPart + ((fracPart || '') + '00000000').slice(0, 8);
            return quantityS.replace(/^0*/, '') || '0';
        }
        else {
            if (!amountS.match(/^[0-9]{1,19}$/)) { myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'トークン必要量は0以上の整数を半角で入力してください' : 'Invalid amount.', 'error'); return null }
            return amountS.replace(/^0*/, '') || '0';
        }
    };
    const aGetBlob = (canvas, mimeType, qualityArgument) => {
        return new Promise(resolve => canvas.toBlob(resolve, mimeType, qualityArgument));
    };

    const BALLOONBLOCKEDITOR_SETTINGS = {
        toolbar: {
            items: ['bold', 'link', 'code', 'fontColor', 'fontSize'],
        },
        language: 'ja',
        blockToolbar: [
            'heading', 'blockQuote', 'imageUpload', 'codeBlock', 'insertTable', 'mediaEmbed', '|',
            'bulletedList', 'numberedList', 'indent', 'outdent',
        ],
        image: {
            toolbar: ['imageTextAlternative', 'imageStyle:full', 'imageStyle:side'],
            upload: {
                panel: {
                    items: ['insertImageViaUrl'],
                },
            },
        },
        table: {
            contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells'],
        },
        licenseKey: '',
        placeholder: (TagG.jp ? 'ここに書いてね' : 'Write here.'),
    };
});
riot.tag2('tokenvault-list-tab', '<div class="list-tab"> <div if="{!TagG.addrsData}" class="nologin-message"> <span if="{TagG.jp}">Walletを開くと持っているトークンに応じてロックが解除されます</span> <span if="{!TagG.jp}">To unlock treasures, open wallet with sufficient key tokens in it.</span> </div> <div class="search-frame"> <material-textfield ref="search_input" hint="Search" on_input="{onInputSearch}"></material-textfield> <div class="search-clearbutton-frame"><material-iconbutton icon="clear" on_click="{onClickClearSearchButton}"></material-iconbutton></div> </div> <div class="treasure-items"> <div each="{treasuresToShow}" class="treasure-item" onclick="{onClick}"> <div class="treasure-tmbframe"> <img riot-src="{tmbImage}" class="thumbnail" crossorigin="anonymous" loading="lazy" width="300" height="300"> <div if="{isLocked}" class="lock-overlay"></div> <div if="{isLocked}" class="treasure-amount"><i class="material-icons">lock</i>&ensp;{balanceAmountS} / {amountS}</div> <div if="{!isLocked}" class="treasure-amount open"><i class="material-icons">lock_open</i>&ensp;{isOwn ? \'OWNER\' : balanceAmountS} / {amountS}</div> <div class="treasure-buttonsframe"> <div if="{isOwn}" class="treasureitem-button"><material-iconbutton icon="delete" is_white="true" on_click="{onClickDelete}"></material-iconbutton> </div><div if="{isOwn}" class="treasureitem-button"><material-iconbutton icon="edit" is_white="true" on_click="{onClickEdit}"></material-iconbutton> </div><div if="{asset2dispenser[asset] && isLocked && !isOwn}" class="treasureitem-button"><material-iconbutton icon="point_of_sale" is_white="true" on_click="{onClickDispenser}"></material-iconbutton> </div> </div> </div> <div class="treasure-labelframe"> <div if="{!assetGroup}" class="assetname">{assetInfo.shortenName}</div> <div if="{assetGroup}" class="assetname">{assetGroup} <span class="nft-tag"></span></div> <div if="{assetGroup}" class="asset-id">{assetName}</div> <div if="{memo}" class="description">{memo}</div> </div> </div> </div> </div>', '', '', function(opts) {
    const init = () => {
        this.TagG = TagG;
        this.gotoEdit = opts.goto_edit;
        this.treasures = [];
        this.treasuresToShow = [];
        this.asset2dispenser = {};
        this.searchedQuery = '';
        if (TagG.query[TagG.QUERYKEY_SEARCH]) {
            this.searchedQuery = decodeURIComponent(TagG.query[TagG.QUERYKEY_SEARCH].replace(/\+/g, ' '));
            delete TagG.query[TagG.QUERYKEY_SEARCH];
        }
    };
    this.onInputSearch = (query) => {
        query = query.trim();
        if (query === this.searchedQuery) return;
        search(query);
    };
    this.onClickClearSearchButton = () => {
        if (this.searchedQuery === '') return;
        search('');
    }
    this.on('mount', async() => {
        if (TagG.addrsData) {
            await TagG.aUpdateAddrsDataWithUI(TagG.bodySpinner);
            this.update();
        }
        await aShowTreasures();
        aSetAsset2DispenserData().then(() => { this.update() });
        TagG.aLoadAssetsExtendedInfo().then(isUpdated => { if (isUpdated) this.update() });
    });
    const aShowTreasures = async() => {
        try {
            if (aShowTreasures.isLoading) return;
            aShowTreasures.isLoading = true;
            TagG.bodySpinner.start();
            await TagG.aLoadTreasures();
            const assets = Object.keys(TagG.asset2treasures);
            await TagG.aLoadAssets(assets, true, true);
            setDisplayList();
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `データの取得に失敗しました。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}` : `Failed to fetch data. hint: ${error}`), 'error');
        }
        finally {
            TagG.bodySpinner.stop();
            aShowTreasures.isLoading = false;
        }
    };
    const aSetAsset2DispenserData = async() => {
        const dispensers = await TagG.aGetDispensers();
        for (const dispenser of dispensers) {
            dispenser.rate = dispenser.unitBaseSat / dispenser.unitAssetSat;
            const existingData = this.asset2dispenser[dispenser.asset];
            if (existingData && existingData.rate < dispenser.rate) continue;
            this.asset2dispenser[dispenser.asset] = dispenser;
        }
    };
    const setDisplayList = () => {
        const hasAsset = {};
        if (TagG.addrsData) {
            for (const addrData of TagG.addrsData) {
                for (const asset of addrData.assets) hasAsset[asset.asset] = true;
            }
        }
        const treasures = [];
        for (const asset in TagG.asset2treasures) {
            const assetInfo = TagG.asset2info[asset];
            for (const treasure of TagG.asset2treasures[asset]) {
                const { asset, qtyN, isDivisible } = treasure;
                const isFree = !qtyN;
                let isLocked = isFree ? false : true;
                let isOwn = false;
                let addrData = null, maxBalanceSat = 0;
                if (hasAsset[asset]) {
                    for (const _addrData of TagG.addrsData) {
                        const assetData = _addrData.assets.find(_asset => (_asset.asset === asset));
                        if (assetData) {
                            if (assetData.isOwn) {
                                isOwn = true;
                                isLocked = false;
                                addrData = _addrData;
                                break;
                            }
                            if (assetData.satoshi >= qtyN && assetData.satoshi > maxBalanceSat) {
                                isLocked = false;
                                addrData = _addrData;
                                maxBalanceSat = assetData.satoshi;
                            }
                        }
                    }
                }
                const balanceAmountS = TagG.qtyS2amountS(maxBalanceSat, isDivisible);
                const searchText = getSearchText(treasure, assetInfo);
                const onClick = async() => {
                    if (isLocked) return;
                    await onClickTreasure(treasure, addrData);
                };
                const onClickEdit = async(event) => {
                    event.stopPropagation();
                    await onClickTreasureEdit(treasure, addrData);
                };
                const onClickDelete = async(event) => {
                    event.stopPropagation();
                    await onClickTreasureDelete(treasure, addrData);
                };
                const onClickDispenser = (event) => {
                    event.stopPropagation();
                    TagG.bodyDispenserPurchase.open(this.asset2dispenser[asset], 'tokenvault_list');
                };
                treasures.push({
                    ...treasure,
                    isFree, isLocked, isOwn, balanceAmountS, searchText, assetInfo, assetGroup: assetInfo.group,
                    onClick, onClickEdit, onClickDelete, onClickDispenser,
                });
            }
        }
        treasures.sort(treasureSortFunc);
        this.treasures = treasures;
        search(this.searchedQuery);
    };
    const treasureSortFunc = (treA, treB) => {
        if (treA.isOwn && !treB.isOwn) return -1;
        if (treB.isOwn && !treA.isOwn) return 1;
        if (!treA.isLocked && treB.isLocked) return -1;
        if (!treB.isLocked && treA.isLocked) return 1;
        if (treA.assetName === 'TOKENVAULT' && treB.assetName !== 'TOKENVAULT') return -1;
        if (treB.assetName === 'TOKENVAULT' && treA.assetName !== 'TOKENVAULT') return 1;
        let nameA = (treA.assetGroup || treA.assetName), nameB = (treB.assetGroup || treB.assetName);
        if (treA.assetGroup || nameA[0] === 'A') nameA = `~${nameA}`;
        if (treB.assetGroup || nameB[0] === 'A') nameB = `~${nameB}`;
        if (nameA < nameB) return -1;
        if (nameB < nameA) return 1;
        if (treA.qtyN < treB.qtyN) return -1;
        if (treB.qtyN < treA.qtyN) return 1;
        return 0;
    };
    const onClickTreasure = async(treasure, addrData) => {
        const { asset, qtyS } = treasure;
        try {
            TagG.bodySpinner.start();
            await TagG.aOpenTreasure(asset, qtyS, addrData);
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `エラーが発生しました。ヒント：${error}` : `Failed. hint: ${error}`), 'error');
        }
        finally { TagG.bodySpinner.stop() }
    };
    const onClickTreasureEdit = async(treasure, addrData) => {
        const { asset, assetName, qtyS, amountS, memo, hasTmb, tmbImage } = treasure;
        try {
            TagG.bodySpinner.start();
            const treasureDownloadURL = await TagG.aGetTreasureURL(asset, qtyS, addrData);
            this.gotoEdit({ thumbnailURL : hasTmb ? tmbImage : null, assetName, amountS, memo, treasureDownloadURL });
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `エラーが発生しました。ヒント：${error}` : `Failed. hint: ${error}`), 'error');
        }
        finally { TagG.bodySpinner.stop() }
    };
    const onClickTreasureDelete = async(treasure, addrData) => {
        const { asset, assetName, qtyS, amountS } = treasure;
        const isOK = await myswal({
            title: (TagG.jp ? 'お宝の削除' : 'Delete treasure'),
            text: (TagG.jp ? `${assetName} x ${amountS} のお宝を削除します` : `Treasure of ${assetName} x ${amountS} will be deleted.`),
            button: { closeModal: false },
        });
        if (!isOK) return;
        try {
            TagG.bodySpinner.start();
            const params = { asset, quantity: qtyS };
            await TagG.aSignedJsonRPCRequest(TagG.TOKENVAULT_API, 'deleteTreasure', params, addrData);
            TagG.tokenVaultCacheID = new Date().getTime();
            this.treasures = this.treasures.filter(tre => !(tre.asset === asset && tre.qtyS === qtyS));
            search(this.searchedQuery);
            myswal({ title: 'Success', text: (TagG.jp ? '削除しました' : 'Deleted.'), icon: 'success', timer: TagG.SWAL_TIMER, buttons: {} });
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `エラーが発生しました。ヒント：${error}` : `Failed. hint: ${error}`), 'error');
        }
        finally { TagG.bodySpinner.stop() }
    };
    const getSearchText = (treasure, assetInfo) => {
        const { asset, assetName, memo } = treasure;
        let text = `${assetName.toLowerCase()} ${assetName} ${asset} ${assetInfo.owner} ${assetInfo.description} ${assetInfo.isLocked ? '[LOCKED]' : ''} ${memo}`;
        if (assetInfo.group) text += ` ${assetInfo.group} [NFT]`;
        const cardData = TagG.asset2card[assetName];
        if (!cardData) return text;
        text += ` ${cardData.cardName} ${cardData.ownerName} ${cardData.description} ${cardData.tags}`;
        return text;
    };
    const search = (query) => {
        this.searchedQuery = query;
        let isOR = false;
        let words = query.split(/\s+OR\s+/).map(word => word.trim()).filter(word => word);
        if (words.length > 1) isOR = true;
        else words = query.split(/\s+/).filter(word => word);
        let filteredList;
        if (words.length === 0) filteredList = this.treasures.slice();
        else {
            if (isOR) {
                filteredList = this.treasures.filter(treasure => {
                    for (const word of words) {
                        if (treasure.searchText.includes(word)) return true;
                    }
                    return false;
                });
            }
            else {
                filteredList = this.treasures.filter(treasure => {
                    for (const word of words) {
                        if (!treasure.searchText.includes(word)) return false;
                    }
                    return true;
                });
            }
        }
        if (this.refs.search_input && this.refs.search_input.getValue() !== query) this.refs.search_input.setValue(query);
        setURLQuery(query);
        const FIRST = 30;
        const ONCE = 300;
        this.treasuresToShow = filteredList.slice(0, FIRST);
        this.update();
        let length = FIRST;
        const showMore = () => {
            if (length >= filteredList.length || query !== this.searchedQuery) return;
            if (!TagG.IS_LAZYLOADAVAILABLE && length > 300) return;
            length += ONCE;
            this.treasuresToShow = filteredList.slice(0, length);
            this.update();
            setTimeout(showMore, 0);
        };
        setTimeout(showMore, 0);
    };
    const setURLQuery = (query) => {
        const urlObj = new URL(location);
        if (query) urlObj.searchParams.set(TagG.QUERYKEY_SEARCH, query);
        else urlObj.searchParams.delete(TagG.QUERYKEY_SEARCH);
        history.replaceState(null, '', urlObj.toString());
    };
    init();
});
riot.tag2('tokenvault-page', '<div class="app-page"> <div class="page-header"> <i class="material-icons page-icon">attachment</i> <h2 class="page-title">Token Vault</h2> <div class="page-helpbutton-frame"><material-iconbutton icon="help_outline" on_click="{onClickPageHelpButton}"></material-iconbutton></div> </div> <div class="page-content"> <material-tabbar ref="tabbar" items="{TAB_ITEMS}" default_value="{tab}" on_change="{onChangeTab}"></material-tabbar> <div> <tokenvault-list-tab if="{tab === TABS.LIST}" goto_edit="{gotoEdit}"></tokenvault-list-tab> <tokenvault-edit-tab if="{tab === TABS.EDIT}" trsr_data="{treasureData}"></tokenvault-edit-tab> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    const TABS = this.TABS = { LIST: 'LIST', EDIT: 'EDIT' };
    this.TAB_ITEMS = [
        { value: TABS.LIST, label: (TagG.jp ? 'お宝リスト' : 'Treasures'), icon: 'view_module' },
        { value: TABS.EDIT, label: (TagG.jp ? 'お宝の登録' : 'Editor'), icon: 'cloud_upload' },
    ];
    this.tab = this.TABS.LIST;

    this.onChangeTab = (value) => { this.tab = value; this.update() };
    this.onClickPageHelpButton = () => {
        myswal({
            text: TagG.jp?
            '特定のトークンを特定の枚数以上持っている人にしか見られないお宝を登録したり閲覧したりできます。お宝リストの中に説明書が入っているので、まずはそれを読んでみてね':
            'You can set treasures that can only be accessed from wallets with the specified tokens.',
            buttons: {},
        });
    };
    this.gotoEdit = (treasureData) => {
        this.treasureData = treasureData;
        this.tab = this.TABS.EDIT;
        this.refs.tabbar.setValue(this.tab);
        this.update();
        this.treasureData = null;
    };
});
riot.tag2('tools-page', '<div class="app-page"> <div class="page-header"> <i class="material-icons page-icon">build</i> <h2 class="page-title">Tools</h2> </div> <div class="page-content"> <div class="tool-frame"> <div><a class="tool-link" href="{TagG.RELATIVE_LINK_BASE + \'wallet_settings\'}">{TagG.jp ? \'ウォレット設定\' : \'Wallet Settings\'}</a></div> <div class="description">{TagG.jp ? \'ウォレットのチューニングができます。\' : \'You can change the settings of your wallet.\'}</div> </div> <div class="tool-frame"> <div><a class="tool-link" href="{TagG.RELATIVE_LINK_BASE + \'addrbook\'}">{TagG.jp ? \'アドレス帳\' : \'Address Book\'}</a></div> <div class="description"> <span if="{TagG.jp}">よく使うアドレスを登録しておくと送信時にワンタッチで入力できるようになります。トークンを送るときのメモも併せて保存できるので、モナパちゃんのアドレスなどを保存しておくと便利です。</span> <span if="{!TagG.jp}">Saving frequently used addresses to the address book, you will be able to enter them with a single touch when sending. You can save memos as well, so it\'s useful to save Monapachan\'s address.</span> </div> </div> <div class="tool-frame"> <div><a class="tool-link" href="{TagG.RELATIVE_LINK_BASE + \'monaparty_api\'}">{TagG.jp ? \'Monaparty API リクエストツール\' : \'Monaparty API Client\'}</a></div> <div class="description"> <span if="{TagG.jp}">好きなパラメータでMonapartyのAPIにリクエストが送れます。テスト用途や、モナパレットが対応していない機能を使いたい場合にご利用ください。</span> <span if="{!TagG.jp}">This tool helps you to send custom requests to Monaparty API. Use it for testing purposes or when you want to use features that Monapalette does not support.</span> </div> </div> <div if="{!TagG.IS_MPURSEEX}" class="tool-frame"> <div><a class="tool-link" href="{TagG.RELATIVE_LINK_BASE + \'regenerate_quickurl\'}">{TagG.jp ? \'Quick Access URL 再生成ツール\' : \'Quick-Access URL Generator\'}</a></div> <div class="description"> <span if="{TagG.jp}">モナパレットの既存のニーモニックからQuick Access URLを再生成することができます。Quick Access URLやパスワードを誤って削除してしまった場合や、新しいパスワードを使いたい場合にご利用ください。</span> <span if="{!TagG.jp}">Quick-access URLs can be regenerated from existing BIP39 mnemonics.</span> </div> </div> <div if="{!TagG.IS_MPURSEEX}" class="tool-frame"> <div><a class="tool-link" href="{TagG.RELATIVE_LINK_BASE + \'generate_kirakiraaddr\'}">{TagG.jp ? \'キラキラアドレス生成ツール\' : \'KIRA KIRA Address Generator\'}</a></div> <div class="description"> <span if="{TagG.jp}">末尾が指定した文字列になっているアドレスが出るまでひたすらニーモニックを生成します。おしゃれなアドレスをマイニングしてお友達に自慢しよう。</span> <span if="{!TagG.jp}">You can keep generating mnemonics until you get an address that ends with the specified string.</span> </div> </div> <div if="{!TagG.IS_MPURSEEX}" class="tool-frame"> <div><a class="tool-link" href="https://mpurse-extension.komikikaku.com/step" target="_blank">{TagG.jp ? \'Mpurseエクステ\' : \'MpurseExte\'}</a></div> <div class="description"> <span if="{TagG.jp}">Mpurseはとても便利ですが機能はシンプルなので、モナパレットの機能をMpurseのアドレスで使えるようにしてみました。これでMpurseのアドレスからでもトークンを送る以外のMonapartyの操作ができるし、モナカードとの連携もお手軽です。やったね。</span> <span if="{!TagG.jp}">MpurseExte enables you to use Monapalette\'s functions with your Mpurse address.</span> </div> <div class="description"> <span if="{TagG.jp}">ブラウザプラグインに対してモナパレットへのアクセスを許可するのはあまり好ましくないので、サイトを分けてあります。</span> <span if="{!TagG.jp}">As it is not very nice to allow access to Monapalette for browser plugins, MpurseExe is a separate site.</span> </div> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
});
riot.tag2('treasure-body', '<div class="treasure-body"> <div class="center"> <div ref="readonly_editor" class="readonly-editor s1 s2 s3"></div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    this.on('mount', async() => {
        let treasureURL = TagG.query[TagG.QUERYKEY_TREASUREURL];
        if (!treasureURL) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'お宝のURLが指定されてません' : 'Treasure URL is not specified.', 'error');
        treasureURL = decodeURIComponent(treasureURL);
        await aSetupEditor();
        try {
            const { html } = await Util.aGetJSON(treasureURL);
            this.readonlyEditor.setData(html);
        }
        catch(error) {
            return myswal(TagG.jp ? 'だめ' : 'Oops!', (TagG.jp ? 'お宝にアクセスできませんでした。お宝のURLは発行してから一定時間しかアクセスできないようになっているので、もういちど見るときはお宝リストから開き直してください' : 'Access failed. As the URL of the treasure can only be accessed for a certain period of time after it is issued, please reopen it from the treasure list if you want to see it again.'), 'error');
        }
    });
    const aSetupEditor = async() => {
        this.readonlyEditor = await BalloonBlockEditor.create(this.refs.readonly_editor, BALLOONBLOCKEDITOR_SETTINGS);
        this.readonlyEditor.isReadOnly = true;
    };
    const BALLOONBLOCKEDITOR_SETTINGS = {
        toolbar: {
            items: ['bold', 'link', 'code', 'fontColor', 'fontSize'],
        },
        language: 'ja',
        blockToolbar: [
            'heading', 'blockQuote', 'imageUpload', 'codeBlock', 'insertTable', 'mediaEmbed', '|',
            'bulletedList', 'numberedList', 'indent', 'outdent',
        ],
        image: {
            toolbar: ['imageTextAlternative', 'imageStyle:full', 'imageStyle:side'],
            upload: {
                panel: {
                    items: ['insertImageViaUrl'],
                },
            },
        },
        table: {
            contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells'],
        },
        licenseKey: '',
        placeholder: '',
    };
});
riot.tag2('txmanager-dexpurchase', '<div ref="swalwindows_container" show="{false}"> <div ref="purchase_window" class="dexpurchase-swalwindow purchase-window"> <div if="{!assetInfo.group}" class="title"> <span if="{TagG.jp}">{assetInfo.name}を買う</span> <span if="{!TagG.jp}">Buy {assetInfo.name}</span> </div> <div if="{assetInfo.group}" class="title"> <span if="{TagG.jp}">{assetInfo.group} <span class="nft-tag"></span> を買う</span> <span if="{!TagG.jp}">Buy {assetInfo.group} <span class="nft-tag"></span></span> </div> <div if="{assetInfo.group}" class="basic"><i class="material-icons inline-icon">tag</i> {assetInfo.name}</div> <div if="{saleData.imageURL}" class="basic card-frame"> <img if="{!assetInfo.appData?.rakugakinft}" riot-src="{saleData.largeImageURL}" loading="lazy"> <img if="{assetInfo.appData?.rakugakinft}" riot-src="{saleData.largeImageURL + \'?\' + TagG.getRakugakiCacheID(assetInfo.asset)}" class="bg-checker" loading="lazy"> </div> <div if="{saleData.mainDescription}" class="basic oblique">{saleData.mainDescription}</div> <div class="basic">{TagG.jp ? \'発行数\' : \'Issued\'}: {commafy(assetInfo.issuedAmount)} <i if="{assetInfo.isLocked}" class="material-icons inline-icon">lock</i></div> <div if="{assetInfo.appData?.attributes}" class="basic attrs"> <div each="{assetInfo.appData.attributes}" class="attr"><div class="attr-name">{name}</div><div class="attr-value">{value}</div></div> </div> <div class="buttons-frame"> <material-button label="MPCHAIN" icon="open_in_new" is_link="true" link_href="{assetInfo.explorerURL}" link_target="_blank" reload_opts="true"></material-button> <material-button if="{saleData.cardData}" label="Monacard" icon="open_in_new" is_link="true" link_href="{saleData.cardData.siteURL}" link_target="_blank" reload_opts="true"></material-button> <material-button if="{assetInfo.appData?.monacharat}" label="{TagG.jp ? \'モナキャラット\' : \'MonaCharaT\'}" icon="open_in_new" is_link="true" link_href="{assetInfo.appData.monacharat.url}" link_target="_blank" reload_opts="true"></material-button> <material-button if="{assetInfo.appData?.monacute}" label="Monacute" icon="open_in_new" is_link="true" link_href="{assetInfo.appData.monacute.url}" link_target="_blank" reload_opts="true"></material-button> <material-button if="{purchaseWindow.treasuresURL}" label="Token Vault" icon="search" is_link="true" link_href="{purchaseWindow.treasuresURL}" reload_opts="true"></material-button> </div> <div class="section"> <div class="basic"> <span if="{TagG.jp}">1 枚あたり {saleData.priceS}&nbsp;{currencyAssetInfo.name} で {commafy(saleData.remainingAmount)}&nbsp;枚まで買えます</span> <span if="{!TagG.jp}">You can buy up to {commafy(saleData.remainingAmount)} tokens at {saleData.priceS}&nbsp;{currencyAssetInfo.name} per token.</span> </div> <div if="{!TagG.addrsData}" class="basic">{TagG.jp ? \'ここから購入するにはWalletを開いてください\' : \'To buy from here, open your wallet.\'}</div> <div if="{TagG.addrsData}"> <div class="basic"><material-select if="{recreateFlag}" ref="purchase_addr" hint="Address" is_fullwidth="true" is_menuwidth_fixed="true" items="{TagG.addrSelectItems}" default_value="{lastSelectedAddr || TagG.addrSelectDefaultValue}" on_change="{purchaseWindow.onChangeAddrSelect}"></material-select></div> <div class="basic"><i class="material-icons inline-icon">account_balance_wallet</i> {purchaseWindow.currencyBalanceStr} {currencyAssetInfo.name}</div> <div class="basic"> <div class="inline-input amount-input"><material-textfield ref="purchase_amount" hint="Amount" pattern="{\'[0-9]*(\\\\.[0-9]+)?\'}" invalid_message="Invalid Format" on_input="{purchaseWindow.onInputAmount}"></material-textfield></div> <div class="text-with-inlineinput">{TagG.jp ? \'枚\' : \'tokens\'}</div> </div> <div class="basic">{TagG.jp ? \'計\' : \'Total:\'} {commafy(purchaseWindow.currencyAmount)}&nbsp;{currencyAssetInfo.name} &rarr; {commafy(purchaseWindow.assetAmount)}&nbsp;{assetInfo.name}</div> </div> </div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div if="{TagG.addrsData}" class="bottombutton-frame"> <material-button label="{TagG.jp ? \'買う\' : \'BUY\'}" icon="shopping_cart" on_click="{purchaseWindow.onClickPurchaseButton}" is_unelevated="true" is_fullwidth="true"></material-button> </div> </div> <div ref="purchase_conf_window" class="dexpurchase-swalwindow"> <div class="title"> <span if="{TagG.jp}">{assetInfo.name}を買う</span> <span if="{!TagG.jp}">Buy {assetInfo.name}</span> </div> <div class="basic"> <span if="{TagG.jp}">1 枚あたり {saleData.priceS}&nbsp;{currencyAssetInfo.name} で {commafy(orderData.getAmount)}&nbsp;{assetInfo.name} の買い注文を出します。トランザクション手数料が {commafy(orderData.feeAmount)}&nbsp;{TagG.BASECHAIN_ASSET} かかりますがOKですか？</span> <span if="{!TagG.jp}">Buy order for {commafy(orderData.getAmount)}&nbsp;{assetInfo.name} at a price of {saleData.priceS}&nbsp;{currencyAssetInfo.name} will be placed. You will be charged a tx fee of {commafy(orderData.feeAmount)}&nbsp;{TagG.BASECHAIN_ASSET}, OK?</span> </div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="bottombutton-frame"> <material-button label="OK" on_click="{purchaseConfWindow.onClickOKButton}" is_unelevated="true" is_fullwidth="true"></material-button> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    this.commafy = Util.commafy;
    this.purchaseWindow = {};
    this.purchaseConfWindow = {};
    this.orderData = {};
    this.saleData = {};
    this.assetInfo = {};
    this.currencyAssetInfo = {};

    this.open = (saleData) => {
        this.recreateFlag = false;
        this.update();
        this.recreateFlag = true;
        this.saleData = saleData;
        this.assetInfo = saleData.assetInfo;
        this.currencyAssetInfo = saleData.currencyAssetInfo;
        this.purchaseWindow.currencyAmount = 0;
        this.purchaseWindow.assetAmount = 0;
        this.purchaseWindow.treasuresURL = TagG.asset2treasures[this.assetInfo.asset]?
           `${TagG.RELATIVE_LINK_BASE}tokenvault?${TagG.QUERYKEY_SEARCH}=${encodeURIComponent(this.assetInfo.asset)}` : null;
        if (TagG.addrsData) {
            const defaultAddrData = TagG.addrsData.find(data => (data.address === (this.lastSelectedAddr || TagG.addrSelectDefaultValue)));
            this.purchaseWindow.currencyBalanceStr = Util.commafy(defaultAddrData.getBalanceAmount(this.currencyAssetInfo.asset));
        }
        if (this.refs.purchase_amount) {
            const defaultAmount = this.assetInfo.isDivisible ? '' : '1';
            this.refs.purchase_amount.setValue(defaultAmount);
            this.purchaseWindow.onInputAmount(defaultAmount);
        }
        clearWindowMessages();
        Util.aOpenContentSwal(this.refs.purchase_window);
    };
    this.purchaseWindow.onChangeAddrSelect = (address) => {
        this.lastSelectedAddr = address;
        const addrData = TagG.addrsData.find(data => (data.address === address));
        this.purchaseWindow.currencyBalanceStr = Util.commafy(addrData.getBalanceAmount(this.currencyAssetInfo.asset));
        this.update();
    };
    this.purchaseWindow.onInputAmount = (value) => {
        const amount = Number(value);
        if (isNaN(amount) || amount < 0) return;
        this.purchaseWindow.currencyAmount = TagG.qty2amount(calcTotalSatToBuy(amount), this.currencyAssetInfo.isDivisible);
        this.purchaseWindow.assetAmount = amount;
        this.update();
    };
    this.purchaseWindow.onClickPurchaseButton = () => {
        const fromAddr = this.refs.purchase_addr.getValue();
        const amount = Number(this.refs.purchase_amount.getValue());
        if (!fromAddr) return showWindowError(TagG.jp ? 'だめです。アドレスを選択してね' : 'Oops! No address selected.');
        if (isNaN(amount) || amount <= 0) return showWindowError(TagG.jp ? 'だめです。数量は正の値にしてね' : 'Oops! Invalid amount.');
        const currencyAmount = TagG.qty2amount(calcTotalSatToBuy(amount), this.currencyAssetInfo.isDivisible);
        if (!this.assetInfo.isDivisible && !Number.isInteger(amount)) return showWindowError(TagG.jp ? 'だめです。数量は整数にしてね' : 'Oops! Invalid amount.');
        if (!this.currencyAssetInfo.isDivisible && !Number.isInteger(currencyAmount)) return showWindowError(TagG.jp ? 'だめです。支払い額が整数になるようにしてね' : 'Oops! "Total" must be an integer.');
        const addrData = TagG.addrsData.find(data => (data.address === fromAddr));
        const giveSat = TagG.amount2qty(currencyAmount, this.currencyAssetInfo.isDivisible);
        const getSat = TagG.amount2qty(amount, this.assetInfo.isDivisible);
        this.orderData.getAmount = amount;
        aPrepareOrder(addrData, this.currencyAssetInfo.asset, giveSat, this.assetInfo.asset, getSat);
    };
    this.purchaseConfWindow.onClickOKButton = () => { aOrder() };

    this.on('mount', async() => {
        const container = this.refs.swalwindows_container;
        while (container && container.firstChild) container.removeChild(container.firstChild);
    });
    const aPrepareOrder = async(addrData, giveAsset, giveSat, getAsset, getSat) => {
        try {
            TagG.bodySpinner.start();
            const orderParams = {
                'source': addrData.address,
                'give_asset': giveAsset,
                'give_quantity': giveSat,
                'get_asset': getAsset,
                'get_quantity': getSat,
                'expiration': 1,
                'fee_provided': 0,
                'fee_required': 0,
                'fee_per_kb': TagG.FEESAT_PERBYTE * 1000,
                'extended_tx_info': true,
            };
            if (addrData.publicKey) orderParams['pubkey'] = addrData.publicKey.toString('hex');
            if (TagG.confNum === 0) orderParams['allow_unconfirmed_inputs'] = true;
            const orderData = this.orderData;
            await TagG.aMonapartyTXCreate('create_order', orderParams, orderData);
            orderData.address = addrData.address;
            orderData.giveAsset = giveAsset;
            orderData.giveSat = giveSat;
            console.log(orderData);
            clearWindowMessages();
            Util.aOpenContentSwal(this.refs.purchase_conf_window);
        }
        catch (error) {
            console.error(error);
            showWindowError(TagG.jp?
                `トランザクションの準備に失敗しました。入力した値に問題があるか、必要な${TagG.BASECHAIN_ASSET}やトークンが足りないかもしれません。データの更新などいろいろ試してもうまくいかない場合は情報を揃えてご連絡ください。ヒント：${error}`:
                `Failed to prepare tx. hint: ${error}`);
        }
        finally { TagG.bodySpinner.stop() }
    };
    const aOrder = async() => {
        try {
            TagG.bodySpinner.start();
            const orderData = this.orderData;
            const addrData = TagG.addrsData.find(data => (data.address === orderData.address));
            const signedTXHex = await TagG.aSignTXHex(orderData.txHex, addrData, null);
            const txID = await TagG.aBroadcast(signedTXHex);
            console.log({ txID });
            reduceBalance(addrData, orderData.giveAsset, orderData.giveSat);
            if (this.refs.purchase_amount) this.refs.purchase_amount.setValue('');
            myswal({ title: 'Success', text: (TagG.jp ? '注文しました' : 'Ordered.'), icon: 'success', timer: TagG.SWAL_TIMER, buttons: {} });
        }
        catch (error) {
            console.error(error);
            showWindowError(TagG.jp?
            `トランザクションのブロードキャストに失敗しました。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}`:
            `Failed to broadcast tx. hint: ${error}`);
        }
        finally { TagG.bodySpinner.stop() }
    };
    const calcTotalSatToBuy = (amount) => {
        let totalSat = TagG.amount2qty(this.saleData.price * amount, this.currencyAssetInfo.isDivisible);
        if (this.assetInfo.isDivisible) {
            const priceSatN = BigInt(TagG.amount2qty(this.saleData.price, this.currencyAssetInfo.isDivisible));
            const amountSatN = BigInt(amount * TagG.SATOSHI_RATIO);
            if (priceSatN * amountSatN > BigInt(totalSat) * BigInt(TagG.SATOSHI_RATIO)) totalSat++;
        }
        return totalSat;
    };
    const reduceBalance = (addrData, asset, satoshi) => {
        const balanceData = addrData.assets.find(assetData => (assetData.asset === asset));
        if (balanceData) balanceData.satoshi -= satoshi;
        else if (asset === TagG.BUILTIN_ASSET) addrData.builtinSat -= satoshi;
    };
    const clearWindowMessages = () => {
        this.windowErrorText = '';
        this.update();
    };
    const showWindowError = (message) => {
        this.windowErrorText = message;
        this.update();
    };
});
riot.tag2('txmanager-dispenserpurchase', '<div ref="swalwindows_container" show="{false}"> <div ref="purchase_window" class="dispenserpurchase-swalwindow purchase-window"> <div if="{!dispenserData.assetGroup}" class="title"> <span if="{TagG.jp}">{dispenserData.assetName}を買う</span> <span if="{!TagG.jp}">Buy {dispenserData.assetName}</span> </div> <div if="{dispenserData.assetGroup}" class="title"> <span if="{TagG.jp}">{dispenserData.assetGroup} <span class="nft-tag"></span> を買う</span> <span if="{!TagG.jp}">Buy {dispenserData.assetGroup} <span class="nft-tag"></span></span> </div> <div if="{dispenserData.assetGroup}" class="basic"><i class="material-icons inline-icon">tag</i> {dispenserData.assetName}</div> <div if="{dispenserData.imageURL}" class="basic card-frame"> <img if="{!assetInfo.appData?.rakugakinft}" riot-src="{dispenserData.largeImageURL}" loading="lazy"> <img if="{assetInfo.appData?.rakugakinft}" riot-src="{dispenserData.largeImageURL + \'?\' + TagG.getRakugakiCacheID(assetInfo.asset)}" class="bg-checker" loading="lazy"> </div> <div if="{dispenserData.mainDescription}" class="basic oblique">{dispenserData.mainDescription}</div> <div class="basic">{TagG.jp ? \'発行数\' : \'Issued\'}: {commafy(assetInfo.issuedAmount)} <i if="{assetInfo.isLocked}" class="material-icons inline-icon">lock</i></div> <div if="{assetInfo.appData?.attributes}" class="basic attrs"> <div each="{assetInfo.appData.attributes}" class="attr"><div class="attr-name">{name}</div><div class="attr-value">{value}</div></div> </div> <div class="buttons-frame"> <material-button label="MPCHAIN" icon="open_in_new" is_link="true" link_href="{assetInfo.explorerURL}" link_target="_blank" reload_opts="true"></material-button> <material-button if="{dispenserData.cardData}" label="Monacard" icon="open_in_new" is_link="true" link_href="{dispenserData.cardData.siteURL}" link_target="_blank" reload_opts="true"></material-button> <material-button if="{assetInfo.appData?.monacharat}" label="{TagG.jp ? \'モナキャラット\' : \'MonaCharaT\'}" icon="open_in_new" is_link="true" link_href="{assetInfo.appData.monacharat.url}" link_target="_blank" reload_opts="true"></material-button> <material-button if="{assetInfo.appData?.monacute}" label="Monacute" icon="open_in_new" is_link="true" link_href="{assetInfo.appData.monacute.url}" link_target="_blank" reload_opts="true"></material-button> <material-button if="{referrer !== \'tokenvault_list\' && purchaseWindow.treasuresURL}" label="Token Vault" icon="search" is_link="true" link_href="{purchaseWindow.treasuresURL}" reload_opts="true"></material-button> </div> <div class="section"> <div class="basic"> <span if="{TagG.jp}">1クチあたり {dispenserData.unitBaseAmountS}&nbsp;{TagG.BASECHAIN_ASSET} &rarr; {dispenserData.unitAssetAmountS}&nbsp;{dispenserData.assetName} で {dispenserData.address} が販売中。残りの在庫は {dispenserData.remainingAmountS}&nbsp;枚（{commafy(dispenserData.zaikoNum)}&nbsp;クチ）です</span> <span if="{!TagG.jp}">Address: {dispenserData.address}<br>Unit: {dispenserData.unitBaseAmountS}&nbsp;{TagG.BASECHAIN_ASSET} &rarr; {dispenserData.unitAssetAmountS}&nbsp;{dispenserData.assetName}<br>Remaining: {dispenserData.remainingAmountS} ({commafy(dispenserData.zaikoNum)}&nbsp;units)</span> </div> <div if="{!TagG.addrsData}" class="basic">{TagG.jp ? \'ここから購入するにはWalletを開いてください\' : \'To buy from here, open your wallet.\'}</div> <div if="{TagG.addrsData}"> <div class="basic"><material-select if="{recreateFlag}" ref="purchase_addr" hint="Address" is_fullwidth="true" is_menuwidth_fixed="true" items="{TagG.addrSelectItems}" default_value="{lastSelectedAddr || TagG.addrSelectDefaultValue}" on_change="{purchaseWindow.onChangeAddrSelect}"></material-select></div> <div class="basic"><i class="material-icons inline-icon">account_balance_wallet</i> {purchaseWindow.baseBalanceStr} {TagG.BASECHAIN_ASSET}</div> <div class="basic"> <div class="inline-input unit-input"><material-textfield ref="purchase_units" hint="Units" pattern="{\'[0-9]*\'}" invalid_message="Invalid Format" on_input="{purchaseWindow.onInputUnits}"></material-textfield></div> <div class="text-with-inlineinput">{TagG.jp ? \'クチ\' : \'units\'}</div> </div> <div class="basic">{TagG.jp ? \'計\' : \'Total:\'} {commafy(purchaseWindow.baseAmount)}&nbsp;{TagG.BASECHAIN_ASSET} &rarr; {commafy(purchaseWindow.assetAmount)}&nbsp;{dispenserData.assetName}</div> </div> </div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div if="{TagG.addrsData}" class="bottombutton-frame"> <material-button label="{TagG.jp ? \'買う\' : \'BUY\'}" icon="shopping_cart" on_click="{purchaseWindow.onClickPurchaseButton}" is_unelevated="true" is_fullwidth="true"></material-button> </div> </div> <div ref="purchase_conf_window" class="dispenserpurchase-swalwindow"> <div class="title"> <span if="{TagG.jp}">{dispenserData.assetName}を買う</span> <span if="{!TagG.jp}">Buy {dispenserData.assetName}</span> </div> <div class="basic"> <span if="{TagG.jp}">{commafy(purchaseData.amount)} {TagG.BASECHAIN_ASSET} を以下のアドレスに送ります。トランザクション手数料が {commafy(purchaseData.feeAmount)} {TagG.BASECHAIN_ASSET} かかりますがOKですか？</span> <span if="{!TagG.jp}">{commafy(purchaseData.amount)} {TagG.BASECHAIN_ASSET} will be sent to the following address. You will be charged a tx fee of {commafy(purchaseData.feeAmount)} {TagG.BASECHAIN_ASSET}, OK?</span> </div> <div class="basic"> <span if="{TagG.jp}">タイミング悪くトークンが売り切れてしまった場合はトークンがもらえないのでご注意＆ご了承ください</span> <span if="{!TagG.jp}">Please note that if the tokens are sold out at an inopportune time, you will not receive any tokens.</span> </div> <div class="basic">{purchaseData.toAddr}</div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="bottombutton-frame"> <material-button label="OK" on_click="{purchaseConfWindow.onClickOKButton}" is_unelevated="true" is_fullwidth="true"></material-button> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    this.commafy = Util.commafy;
    this.purchaseWindow = {};
    this.purchaseConfWindow = {};
    this.purchaseData = {};
    this.dispenserData = {};
    this.assetInfo = {};

    this.open = (dispenserData, referrer = null) => {
        this.recreateFlag = false;
        this.update();
        this.recreateFlag = true;
        this.referrer = referrer;
        this.dispenserData = dispenserData;
        this.assetInfo = dispenserData.assetInfo;
        this.purchaseWindow.baseAmount = 0;
        this.purchaseWindow.assetAmount = 0;
        this.purchaseWindow.treasuresURL = TagG.asset2treasures[dispenserData.asset]?
           `${TagG.RELATIVE_LINK_BASE}tokenvault?${TagG.QUERYKEY_SEARCH}=${encodeURIComponent(dispenserData.asset)}` : null;
        if (TagG.addrsData) {
            const defaultAddrData = TagG.addrsData.find(data => (data.address === (this.lastSelectedAddr || TagG.addrSelectDefaultValue)));
            this.purchaseWindow.baseBalanceStr = getBalanceStr(defaultAddrData);
        }
        if (this.refs.purchase_units) {
            this.refs.purchase_units.setValue('1');
            this.purchaseWindow.onInputUnits('1');
        }
        clearWindowMessages();
        Util.aOpenContentSwal(this.refs.purchase_window);
    };
    this.purchaseWindow.onChangeAddrSelect = (address) => {
        this.lastSelectedAddr = address;
        const addrData = TagG.addrsData.find(data => (data.address === address));
        this.purchaseWindow.baseBalanceStr = getBalanceStr(addrData);
        this.update();
    };
    this.purchaseWindow.onInputUnits = (value) => {
        const units = Number(value);
        if (!Number.isInteger(units) || units < 0) return;
        this.purchaseWindow.baseAmount = (this.dispenserData.unitBaseSat * units) / TagG.SATOSHI_RATIO;
        const assetSat = this.dispenserData.unitAssetSat * units;
        this.purchaseWindow.assetAmount = TagG.qty2amount(assetSat, this.dispenserData.isDivisible);
        this.update();
    };
    this.purchaseWindow.onClickPurchaseButton = () => {
        const fromAddr = this.refs.purchase_addr.getValue();
        const units = Number(this.refs.purchase_units.getValue());
        if (!fromAddr) return showWindowError(TagG.jp ? 'だめです。アドレスを選択してね' : 'Oops! No address selected.');
        if (!Number.isInteger(units) || units < 1) return showWindowError(TagG.jp ? 'だめです。クチ数は正の整数にしてね' : 'Oops! Invalid units.');
        if (units > this.dispenserData.zaikoNum) return showWindowError(TagG.jp ? 'だめです。指定したクチ数が在庫より多いです' : 'Oops! Specified unit num exceeds stock num.');
        const addrData = TagG.addrsData.find(data => (data.address === fromAddr));
        const satoshi = this.dispenserData.unitBaseSat * units;
        if (satoshi < TagG.SENDMIN_SAT) return showWindowError(TagG.jp ? 'だめです。送金額が小さすぎます' : 'Oops! Too small amount to send.');
        aPreparePurchase(addrData, satoshi, this.dispenserData.address);
    };
    this.purchaseConfWindow.onClickOKButton = () => { aPurchase() };

    this.on('mount', async() => {
        const container = this.refs.swalwindows_container;
        while (container && container.firstChild) container.removeChild(container.firstChild);
    });

    const aPreparePurchase = async(addrData, satoshi, toAddr) => {
        try {
            TagG.bodySpinner.start();
            await TagG.aUpdateBaseAssetBalances([addrData]);
            const targets = [{ address: toAddr, value: satoshi }];
            const confirmedUTXOs = addrData.utxos.filter(utxo => (utxo.confirmations >= TagG.confNum));
            const { inputs, outputs, fee } = TagG.coinselect(confirmedUTXOs, targets, TagG.FEESAT_PERBYTE);
            if (!inputs) return showWindowError(TagG.jp ? `承認数${TagG.confNum}以上の${TagG.BASECHAIN_ASSET}が足りません` : `Insufficient ${TagG.BASECHAIN_ASSET} with confirmation >= ${TagG.confNum}`);
            if (!outputs) throw 'coinselect returns no outputs';
            for (const output of outputs) {
                if (!output.address) output.address = addrData.address;
            }
            const purchaseData = this.purchaseData;
            purchaseData.address = addrData.address;
            purchaseData.inputs = inputs;
            purchaseData.outputs = outputs;
            purchaseData.amount = satoshi / TagG.SATOSHI_RATIO;
            purchaseData.feeAmount = fee / TagG.SATOSHI_RATIO;
            purchaseData.toAddr = toAddr;
            purchaseData.sendSat = satoshi;
            purchaseData.inputsSat = inputs.reduce((sum, input) => (sum + input.value), 0);
            purchaseData.inputsUnconfSat = inputs.filter(input => input.confirmations === 0).reduce((sum, input) => (sum + input.value), 0);
            purchaseData.oturiSat = outputs.filter(output => output.address === addrData.address).reduce((sum, output) => (sum + output.value), 0);
            console.log(purchaseData);
            clearWindowMessages();
            Util.aOpenContentSwal(this.refs.purchase_conf_window);
        }
        catch (error) {
            console.error(error);
            showWindowError(TagG.jp?
                `トランザクションの準備に失敗しました。入力した値に問題があるか、必要な${TagG.BASECHAIN_ASSET}が足りないかもしれません。データの更新などいろいろ試してもうまくいかない場合は情報を揃えてご連絡ください。ヒント：${error}`:
                `Failed to prepare tx. hint: ${error}`);
        }
        finally { TagG.bodySpinner.stop() }
    };
    const aPurchase = async() => {
        try {
            TagG.bodySpinner.start();
            const purchaseData = this.purchaseData;
            const addrData = TagG.addrsData.find(data => (data.address === purchaseData.address));
            const signedTXHex = await TagG.aSignInOut(purchaseData.inputs, purchaseData.outputs, addrData);
            const isOK = await aCheckDispenserIsOK();
            if (!isOK) return;
            const txID = await TagG.aBroadcast(signedTXHex);
            console.log({ txID });
            addrData.baseSat -= purchaseData.inputsSat - purchaseData.inputsUnconfSat;
            addrData.baseUnconfSat += purchaseData.oturiSat - purchaseData.inputsUnconfSat;
            if (this.refs.purchase_units) this.refs.purchase_units.setValue('');
            myswal({ title: 'Success', text: (TagG.jp ? '送金しました' : 'Sent.'), icon: 'success', timer: TagG.SWAL_TIMER, buttons: {} });
        }
        catch (error) {
            console.error(error);
            showWindowError(TagG.jp?
            `トランザクションのブロードキャストに失敗しました。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}`:
            `Failed to broadcast tx. hint: ${error}`);
        }
        finally { TagG.bodySpinner.stop() }
    };
    const aCheckDispenserIsOK = async() => {
        try {
            const hasUnconfirmedUTXO = await aCheckHasUnconfUTXO(this.dispenserData.address);
            const getDispensersParams = { 'filters': [{ field: 'tx_hash', op: '==', value: this.dispenserData.txID }] };
            const [dispenser] = await Monaparty.aParty('get_dispensers', getDispensersParams);
            if (!dispenser || dispenser.status !== DISPENSER_STATUS.OPEN) {
                TagG.bodySpinner.stop();
                myswal(TagG.jp ? 'この自動販売は終了しました' : 'Dispenser Already Closed', (TagG.jp ? '最新のデータをチェックしたところ、売切れもしくはキャンセルによりこちらの自動販売はもう終了してしまっていたため、送金を中断しました' : `As Monapalette checked the latest data and found that this dispenser has been sold out or cancelled, ${TagG.BASECHAIN_ASSET} send has been canceled.`), 'error');
                return false;
            }
            if (Math.floor(dispenser.give_remaining / dispenser.give_quantity) * dispenser.satoshirate < this.purchaseData.sendSat) {
                TagG.bodySpinner.stop();
                myswal(TagG.jp ? '在庫不足を検出しました' : 'Stock Shortage', (TagG.jp ? '最新のデータをチェックしたところ、こちらの自動販売の在庫が現在購入しようとしている数量よりも少なくなっていたため、送金を中断しました' : `As Monapalette checked the latest data and found that this dispenser does not have enough stock, ${TagG.BASECHAIN_ASSET} send has been canceled.`), 'error');
                return false;
            }
            if (hasUnconfirmedUTXO) {
                TagG.bodySpinner.stop();
                const text = TagG.jp?
                '販売元のアドレスに関係する未承認のトランザクションを検出しました。入れ違いで自動販売が売切れになったり、自動販売がキャンセルされてしまった可能性があるので、状況を確認してから送金することを推奨します':
                `Unconfirmed tx related to the dispenser address is detected. As it is possible that the tokens has just sold out or the dispenser has just cancelled, it is recommend to check the status before sending ${TagG.BASECHAIN_ASSET}.`;
                const isOK = await myswal({
                    icon: 'warning',
                    title: (TagG.jp ? '販売元に新しいTXを検出' : 'New Tx Detected'),
                    text,
                    buttons: [(TagG.jp ? 'やめておく' : 'Cancel'), (TagG.jp ? 'それでも送る' : 'SEND ANYWAY')],
                    dangerMode: true,
                });
                return Boolean(isOK);
            }
            return true;
        }
        catch (error) {
            console.error(error);
            myswal('Error', (TagG.jp ? `データの取得に失敗しました。サーバの調子が悪いかもしれないです。しばらくたっても直らなければご連絡ください。ヒント：${error}` : `Failed to fetch data. hint: ${error}`), 'error');
            return false;
        }
    };
    const aCheckHasUnconfUTXO = async(address) => {
        const blockbookURLs = TagG.BLOCKBOOK_API_ENDPOINTS.map(endpoint => `${endpoint}v2/utxo/${address}`);
        let rawUTXOs = null;
        for (const url of blockbookURLs) {
            rawUTXOs = await Util.aGetJSON(url, false).catch(console.error);
            if (rawUTXOs) break;
        }
        if (!rawUTXOs) throw `aCheckHasUnconfUTXO failed: ${JSON.stringify(blockbookURLs)}`;
        return rawUTXOs.some(utxo => (utxo.confirmations === 0));
    };
    const getBalanceStr = (addrData) => {
        let balanceStr = `${Util.commafy(TagG.qty2amount(addrData.baseSat, true))}`;
        if (addrData.baseUnconfSat) balanceStr += `(+${Util.commafy(TagG.qty2amount(addrData.baseUnconfSat, true))})`;
        return balanceStr;
    };
    const clearWindowMessages = () => {
        this.windowErrorText = '';
        this.update();
    };
    const showWindowError = (message) => {
        this.windowErrorText = message;
        this.update();
    };
});
riot.tag2('wallet-page', '<div class="app-page"> <div class="page-header"> <i class="material-icons page-icon">account_balance_wallet</i> <h2 class="page-title">Wallet</h2> <div class="page-helpbutton-frame"> <material-iconbutton icon="help_outline" on_click="{onClickWalletHelpButton}"></material-iconbutton> </div> </div> <div class="page-content"> <div if="{!TagG.addrsData}"> <div if="{isQuickAccess}" class="limit-width"> <div class="form-frame"> <div class="title">{TagG.jp ? \'ウォレットをひらく\' : \'Open Wallet\'}</div> <div class="note">{TagG.jp ? \'クイックアクセスURLのパスワードを入力してください\' : \'Input quick-access password.\'}</div> <div class="password"> <material-textfield ref="quickaccess_password" hint="{TagG.jp ? \'パスワード\' : \'Password\'}" is_password="true" on_enter="{onEnterQuickAccessPassword}"></material-textfield> <div class="enterbutton-frame"><material-iconbutton icon="arrow_forward" on_click="{onClickQuickAccessPasswordEnterButton}"></material-iconbutton></div> </div> </div> </div> <div if="{!isQuickAccess}"> <div if="{!isMakingWalletView && !isOpeningWalletView}" class="limit-width"> <div class="large-button-flex"> <div class="large-button"><material-button label="{TagG.jp ? \'ウォレットをつくる\' : \'CREATE WALLET\'}" icon="brush" on_click="{onClickGotoMakeWalletButton}" is_outlined="true" is_fullwidth="true"></material-button></div> <div class="large-button"><material-button label="{TagG.jp ? \'ウォレットをひらく\' : \'OPEN WALLET\'}" icon="lock_open" on_click="{onClickGotoOpenWalletButton}" is_outlined="true" is_fullwidth="true"></material-button></div> </div> <div class="section-frame"> <div class="section-title">{TagG.jp ? \'モナパレットへようこそ！\' : \'Welcome to Monapalette!\'}</div> <div class="paragraph"> <span if="{TagG.jp}">お手軽に使えるモナコイン＋Monapartyの超高機能総合エンタテイメントウォレットです</span> <span if="{!TagG.jp}">Easy-to-use ultra-high-functionality entertainment wallet of Monacoin + Monaparty.</span> </div> <div if="{TagG.jp}" class="paragraph"> <span>モナパレットでモナコイン／Monaparty／モナカードを始める手順は<a href="https://opaque-toast-7ea.notion.site/fe5a2c309f3a495fa7a3399549cfe50c" target="_blank">モナパレット大全</a>を見てください</span> </div> <div if="{sampleCards}" class="sample-cards"> <div class="slider slider1"><div each="{sampleCards}" class="sample-card"><a href="{linkURL}" target="_blank"><img riot-src="{image}"></a></div></div> <div class="slider slider2"><div each="{sampleCards}" class="sample-card"><a href="{linkURL}" target="_blank"><img riot-src="{image}"></a></div></div> </div> </div> <div class="section-frame"> <div class="section-title">{TagG.jp ? \'できること\' : \'What you can do with Monapalette\'}</div> <div class="dekirukoto-subtitle"><span class="palette-red">●</span>&ensp;{TagG.jp ? \'モナコインのお手軽ウォレット\' : \'Easy-to-use Monacoin wallet\'}</div> <div class="dekirukoto-items"> <div class="dekirukoto-item">{TagG.jp ? \'ウォレット生成\' : \'Create wallet\'}</div> <div class="dekirukoto-item">{TagG.jp ? \'モナコインを送る\' : \'Send Monacoin\'}</div> <div class="dekirukoto-item">{TagG.jp ? \'モナコインを受取る\' : \'Receive Monacoin\'}</div> <div class="dekirukoto-item">{TagG.jp ? \'Trezor連携\' : \'Connect to Trezor\'}</div> <div class="dekirukoto-item">{TagG.jp ? \'Mpurse連携\' : \'Connect to Mpurse\'}</div> </div> <div class="dekirukoto-subtitle dekirukoto-subtitle-longmargin"><span class="palette-yellow">●</span>&ensp;{TagG.jp ? \'Monapartyの機能を使う\' : \'Access Monaparty features\'}</div> <div class="dekirukoto-items"> <div class="dekirukoto-item"><span class="dekirukoto-tag">Send</span>&ensp;{TagG.jp ? \'トークンの送信\' : \'Send tokens\'}</div> <div class="dekirukoto-item"><span class="dekirukoto-tag">Issuance</span>&ensp;{TagG.jp ? \'トークンの発行と設定変更\' : \'Issue and edit tokens\'}</div> <div class="dekirukoto-item"><span class="dekirukoto-tag">Order</span>&ensp;{TagG.jp ? \'分散取引所(DEX)でトークン同士を交換\' : \'Trade tokens on DEX\'}</div> <div class="dekirukoto-item"><span class="dekirukoto-tag">Dispenser</span>&ensp;{TagG.jp ? \'トークンをモナコイン建で販売\' : \'Sell tokens in Monacoin\'}</div> <div class="dekirukoto-item"><span class="dekirukoto-tag">Dividend</span>&ensp;{TagG.jp ? \'トークンの配当\' : \'Distribute tokens as dividends\'}</div> <div class="dekirukoto-item"><span class="dekirukoto-tag">Broadcast</span>&ensp;{TagG.jp ? \'公開メッセージの書込み\' : \'Write messages on Monacoin blockchain\'}</div> <div class="dekirukoto-item"><span class="dekirukoto-tag">Sweep</span>&ensp;{TagG.jp ? \'他のアドレスへの引越し\' : \'Move to another address\'}</div> </div> <div class="dekirukoto-subtitle dekirukoto-subtitle-longmargin"><span class="palette-green">●</span>&ensp;{TagG.jp ? \'Monapartyアプリで遊ぶ\' : \'Play with Monaparty apps\'}</div> <div class="dekirukoto-description"> <span if="{TagG.jp}">モナパレットは次のアプリとの特別な連携を提供しています</span> <span if="{!TagG.jp}">Monapalette offers special integration with the following apps.</span> </div> <div class="dekirukoto-items"> <div class="dekirukoto-item"><span class="dekirukoto-tag">{TagG.jp ? \'モナカード\' : \'Monacard\'}</span>&ensp;{TagG.jp ? \'トークンに画像を設定\' : \'Link images to your tokens\'}</div> <div class="dekirukoto-item"><span class="dekirukoto-tag">Token Vault</span>&ensp;{TagG.jp ? \'トークン保有者しか見れないお宝を設定\' : \'Set treasures that can only be accessed by holders of specified tokens\'}</div> <div class="dekirukoto-item"><span class="dekirukoto-tag">{TagG.jp ? \'らくがきNFT\' : \'Rakugaki NFT\'}</span>&ensp;{TagG.jp ? \'NFTに画像をペタペタ\' : \'Peta-peta images on NFTs\'}</div> <div class="dekirukoto-item"><span class="dekirukoto-tag">D-Board</span>&ensp;{TagG.jp ? \'ブロックチェーンに思い出を記録\' : \'Record your memories on the blockchain\'}</div> <div class="dekirukoto-item"><span class="dekirukoto-tag">{TagG.jp ? \'モナキャラット\' : \'MonaCharaT\'}</span>&ensp;{TagG.jp ? \'1日1枚生まれるNFTシリーズ\' : \'Lovely NFT series born one piece per day\'}</div> <div class="dekirukoto-item"><span class="dekirukoto-tag">GoriFi</span>&ensp;{TagG.jp ? \'MonapartyのDEXをもっと便利に\' : \'Use Monaparty DEX more conveniently\'}</div> <div class="dekirukoto-item"><span class="dekirukoto-tag">{TagG.jp ? \'モナレッジ\' : \'Monaledge\'}</span>&ensp;{TagG.jp ? \'モナコインのWeb3ブログ\' : \'Monacoin Web3 Blog\'}</div> <div class="dekirukoto-item"><span class="dekirukoto-tag">Ask Mona</span>&ensp;{TagG.jp ? \'モナコインのWeb3掲示板\' : \'Monacoin Web3 BBS\'}</div> </div> <div class="dekirukoto-description"> <span if="{TagG.jp}">次のアプリとは特別な連携はしませんがMonapartyウォレットとしての一般的な連携ができます</span> <span if="{!TagG.jp}">Monapalette offers general integration as a Monaparty wallet with the following apps.</span> </div> <div class="dekirukoto-items"> <div class="dekirukoto-item"><span class="dekirukoto-tag">{TagG.jp ? \'モナパちゃん\' : \'Monapachan\'}</span>&ensp;{TagG.jp ? \'Twitterでトークンをチップ\' : \'Tip Monaparty tokens on Twitter\'}</div> <div class="dekirukoto-item"><span class="dekirukoto-tag">{TagG.jp ? \'宇宙モナコイン\' : \'Space Monacoin\'}</span>&ensp;{TagG.jp ? \'インフレを楽しむ謎のオンチェーンゲーム\' : \'Mysterious on-chain game to enjoy inflation\'}</div> <div class="dekirukoto-item">and more ...</div> </div> <div class="dekirukoto-subtitle dekirukoto-subtitle-longmargin dekirukoto-subtitle-noitem"><span class="palette-blue">●</span>&ensp;{TagG.jp ? \'便利ツールを使う\' : \'Other useful tools\'} → <a href="{TagG.RELATIVE_LINK_BASE + \'tools\'}">{TagG.jp ? \'ココ\' : \'here\'}</a></div> </div> <div class="large-button-flex"> <div class="large-button"><material-button label="{TagG.jp ? \'ウォレットをつくる\' : \'CREATE WALLET\'}" icon="brush" on_click="{onClickGotoMakeWalletButton}" is_outlined="true" is_fullwidth="true"></material-button></div> <div class="large-button"><material-button label="{TagG.jp ? \'ウォレットをひらく\' : \'OPEN WALLET\'}" icon="lock_open" on_click="{onClickGotoOpenWalletButton}" is_outlined="true" is_fullwidth="true"></material-button></div> </div> </div> <div if="{isMakingWalletView}" class="limit-width"> <div class="form-frame"> <div class="title">{TagG.jp ? \'ウォレットをつくる\' : \'Create Wallet\'}</div> <div class="note"> <span if="{TagG.jp}">ウォレットの元となるニーモニックを生成します。ニーモニックを暗号化するための16文字以上のパスワードを決めてください</span> <span if="{!TagG.jp}">A mnemonic will be generated as the source of the wallet. Determine a password of at least 16 characters to encrypt the mnemonic.</span> </div> <div class="password"><material-textfield ref="new_password" hint="{TagG.jp ? \'新しいパスワード\' : \'New Password\'}" pattern="{\'.{16,}\'}" invalid_message="Too Short" is_password="true"></material-textfield></div> <div class="password"><material-textfield ref="new_password_reinput" hint="{TagG.jp ? \'もういちど\' : \'Type Again\'}" is_password="true"></material-textfield></div> <div class="switch"><material-switch label="{TagG.jp ? \'Native SegWit のアドレスを使う\' : \'Native SegWit Address\'}" on_change="{onChangeMakingSegwitSwitch}"></material-switch></div> <div class="gobutton"><material-button label="{TagG.jp ? \'ウォレットをつくる\' : \'CREATE\'}" icon="brush" on_click="{onClickMakeWalletButton}" is_unelevated="true" is_fullwidth="true"></material-button></div> <div if="{newMnemonic}"> <div class="new-mnemonic-label">{TagG.jp ? \'ニーモニック (BIP39)\' : \'Mnemonic (BIP39)\'}</div> <div class="new-mnemonic">{newMnemonic}<span if="{newMnemonicAddrType === ADDR_TYPE.P2WPKH}">{\' #segwit\'}</span></div> <div class="new-mnemonic-annotation" if="{newMnemonicAddrType === ADDR_TYPE.P2WPKH}"> <span if="{TagG.jp}">※ 末尾の「#segwit」はモナパレット専用のフラグなので、他のウォレットに入力する場合は除いてください。</span> <span if="{!TagG.jp}">※ The "#segwit" at the end is a flag for Monapalette only, so exclude it if you enter it in other wallets.</span> </div> <div class="new-mnemonic-label">{TagG.jp ? \'クイックアクセスURL\' : \'Quick-access URL\'}</div> <div class="new-url">{newQuickAccessURL}</div> <div class="new-url-copy"><material-iconbutton icon="file_copy" on_click="{onClickURLCopyButton}"></material-iconbutton></div> </div> </div> <div class="section-frame"> <div class="section-title">{TagG.jp ? \'新しいウォレットをつくったら\' : \'When you create a new wallet ...\'}</div> <div class="paragraph"> <span if="{TagG.jp}">設定したパスワードと生成されたニーモニックは間違いのないように記録して、あなたしかアクセスできない安全な場所に保管しましょう。クイックアクセスURLはブックマークなどに保存して、他人には見せびらかさないようにしましょう。</span> <span if="{!TagG.jp}">Keep the password and the generated mnemonic in a safe place where only you can access. You can save the quick-access URL in your bookmarks, but do not share it with others.</span> </div> <div class="paragraph"> <span if="{TagG.jp}">ニーモニックや秘密鍵は外部には一切送られない＝保存されないので、あなたがニーモニックを失くしてしまうと二度と残高を取り出せなくなります。また、ニーモニックを他人に知られてしまうと残高をぜんぶ盗まれます。</span> <span if="{!TagG.jp}">The mnemonic is not stored externally, so if you lose your mnemonic, you will never be able to retrieve your balance again. If someone else knows your mnemonic, your entire balance will be stolen.</span> </div> <div class="paragraph"> <span if="{TagG.jp}">クイックアクセスURLからアクセスすればパスワードを入力するだけでウォレットが使えるので、直接ニーモニックを扱う必要がなくなりいくぶん安全です。ただしクイックアクセスURLとパスワードの両方を他人に知られる／推測されるとやはり残高が盗まれます。</span> <span if="{!TagG.jp}">Using quick-access URL is relatively secure because that you don\'t need to handle the mnemonic directly. However, if someone else knows or guesses both the quick-access URL and the password, the balance will still be stolen.</span> </div> <div class="paragraph"> <span if="{TagG.jp}">利用者側の管理／知識の不足によるいかなるトラブルもモナパレットは関知しません。</span> <span if="{!TagG.jp}">Monapalette is not responsible for any problems caused by failure of management or lack of knowledge on the part of the user.</span> </div> </div> <div class="section-frame"> <div class="section-title">{TagG.jp ? \'もうすこし詳しく\' : \'A little more detail\'}</div> <div class="paragraph"> <span if="{TagG.jp}">ニーモニックはウォレットの元となるデータなので、ニーモニックだけあれば完全にウォレットを使うことができます。絶対に他人に知られてはいけないやつです。オフラインでの保管が強く推奨されます。</span> <span if="{!TagG.jp}">The mnemonic is the data that is the source of the wallet, so if you only have the mnemonic, you can control the wallet completely. It is the one that should never be known to others. Storing the mnemonic offline is highly recommended.</span> </div> <div class="paragraph"> <span if="{TagG.jp}">クイックアクセスURLにはあなたが設定したパスワードで暗号化されたニーモニックが含まれているので、クイックアクセスURLからアクセスしてパスワードを入力することで、ニーモニックを直接入力せずにウォレットを使うことができます。クイックアクセスURLを他人に知られた場合は総当りでパスワードを試行できるため、パスワードは通常のWebサービスで使うものよりも長く複雑でランダムなものにすることを強く推奨します。</span> <span if="{!TagG.jp}">Since the quick-access URL contains a mnemonic encrypted with the password you set, you can use the wallet without entering the mnemonic directly by accessing from the quick-access URL and entering the password. It is strongly recommended that passwords be longer, more complex, and more random than those used for normal web services, because if someone else knows the quick-access URL, they can attempt brute force attack.</span> </div> <div class="paragraph"> <span if="{TagG.jp}">いちどクイックアクセスURLからウォレットを開くと、暗号化した公開鍵(xpub)だけがブラウザに保存されます。次からはクイックアクセスURLにアクセスするだけでRead Onlyモードでウォレットが表示され、秘密鍵が必要になった段階ではじめてパスワードを入力する形になります。Read Onlyモードは便利なだけでなく、秘密鍵を扱わないのでセキュアです。</span> <span if="{!TagG.jp}">Once you open the wallet from the quick-access URL, encrypted public key (xpub) will be saved in your browser. The next time you access from the quick-access URL, the wallet will open in read-only mode, and you have to enter the password only when the wallet need to use private key. The read-only mode is not only convenient, but also secure since it does not handle the private key until it is necessary.</span> </div> <div class="paragraph"> <span if="{TagG.jp}">パスワードとクイックアクセスURLは<a href="{TagG.RELATIVE_LINK_BASE + \'regenerate_quickurl\'}">こちらのツール</a>で後から再設定することもできます。</span> <span if="{!TagG.jp}">You can reset your password and quick-access URL later with <a href="{TagG.RELATIVE_LINK_BASE + \'regenerate_quickurl\'}">this tool</a>.</span> </div> </div> </div> <div if="{isOpeningWalletView}" class="limit-width"> <div class="form-frame"> <div class="title">{TagG.jp ? \'ウォレットをひらく\' : \'Open Wallet\'}</div> <div class="note"> <span if="{TagG.jp}">より高いレベルのセキュリティを求めるあなたのために、モナパレットはTrezorに対応しています</span> <span if="{!TagG.jp}">For those of you who want a higher level of security, Monapalette is Trezor compatible.</span> </div> <div class="gobutton"><material-button label="{TagG.jp ? \'TREZORを使う\' : \'Use Trezor\'}" icon="security" on_click="{onClickOpenTrezorWinButton}" is_unelevated="true" is_fullwidth="true"></material-button></div> <div class="note"> <span if="{TagG.jp}">Mpurseのアドレスでもモナパレットのパワフルな機能を使いたいあなたには、Mpurseエクステをご用意しました</span> <span if="{!TagG.jp}">For those of you who want to use the powerful features of the Monapalette even with an Mpurse address, MpurseExte is here.</span> </div> <div class="gobutton"><material-button label="{TagG.jp ? \'Mpurseエクステをひらく\' : \'Open MpurseExte\'}" icon="launch" is_link="true" link_href="https://mpurse-extension.komikikaku.com/step" is_unelevated="true" is_fullwidth="true"></material-button></div> <div class="note"> <span if="{TagG.jp}">モナパレットではクイックアクセスURLの利用を推奨していますが、やんごとなき事情によりニーモニックや秘密鍵(WIF)で直接ウォレット／アドレスを開きたい場合はこちらからどうぞ</span> <span if="{!TagG.jp}">Using quick-access URLs is recommended, but if you want to open your wallet/address with a mnemonic or private key (WIF), enter here.</span> </div> <div class="note"> <span if="{TagG.jp}">とくにWIFで開く機能はMonaparty非対応ウォレットに送ってしまったトークンを取り出したい場合や、他のウォレットのアドレスで一時的にモナパレット独自の機能を使いたくなった場合にも便利です</span> <span if="{!TagG.jp}">WIF is useful when you want to retrieve tokens in a non-Monaparty wallet, or when you want to use Monapalette\'s unique features temporarily with another wallet\'s address.</span> </div> <div class="mnemonic"><material-textfield ref="existing_mnemonic" hint="{TagG.jp ? \'ニーモニック または 秘密鍵 (WIF)\' : \'Mnemonic or WIF\'}" is_password="true" on_input="{onInputMnemonic}"></material-textfield></div> <div class="switch"><material-switch ref="opening_segwit_switch" label="{TagG.jp ? \'Native SegWit アドレス\' : \'Native Segwit\'}"></material-switch></div> <div class="gobutton"><material-button label="{TagG.jp ? \'ひらく\' : \'OPEN\'}" icon="lock_open" on_click="{onClickOpenMnemonicButton}" is_unelevated="true" is_fullwidth="true"></material-button></div> </div> </div> </div> </div> <div if="{TagG.addrsData && addrsLoaded}"> <div class="addrs-frame"> <address-card each="{addrData, addrIndex in addrsDataToShow}" if="{addrIndex <= TagG.addrsDataMaxIndex}" data="{addrData}"></address-card> <div if="{TagG.addrsDataMaxIndex + 1 < addrsDataToShow.length}" class="newaddr-button"><material-button label="Add New Address" icon="add" on_click="{onClickNewAddrButton}" is_fullwidth="true" is_outlined="true"></material-button></div> </div> </div> </div> </div> <div ref="swalwindows_container" show="{false}"> <div ref="wallethelp_window" class="wallet-page-swalwindow"> <div class="basic"> <span if="{TagG.jp}">詳しいことは<a href="https://opaque-toast-7ea.notion.site/fe5a2c309f3a495fa7a3399549cfe50c" target="_blank">モナパレット大全</a>をみてね！</span> <span if="{!TagG.jp}">See <a href="https://opaque-toast-7ea.notion.site/fe5a2c309f3a495fa7a3399549cfe50c" target="_blank">Monapalette Compendium (Japanese version only)</a> for detailed instructions.</span> </div> <div class="basic"> <span if="{TagG.jp}">お役立ち情報やお得情報やどうでもいい情報を配信する<a href="https://twitter.com/monapalette" target="_blank">公式Twitter</a>もあります</span> <span if="{!TagG.jp}">We also have an <a href="https://twitter.com/monapalette" target="_blank">official Twitter feed</a> that delivers useful information, deals, and unimportant information.</span> </div> </div> <div ref="trezor_window" class="wallet-page-swalwindow"> <div class="title">{TagG.jp ? \'Trezorを使う\' : \'Open Trezor\'}</div> <div class="basic"> <span if="{TagG.jp}">HDウォレットの account を指定できます。モナパレットではお釣りアドレスを使用しないので、残高を見失わないように他で利用しているものとは account を分けたほうが無難です</span> <span if="{!TagG.jp}">You can specify an account for your HD wallet. Since Monapalette does not use change addresses, it is recomended to separate your account from the one you use elsewhere to avoid losing track of your balance.</span> </div> <div class="basic"><material-switch label="{TagG.jp ? \'Native SegWit のアドレスを使う\' : \'Native SegWit\'}" on_change="{onChangeTrezorSegwitSwitch}"></material-switch></div> <div class="basic"> <div class="text-with-inlineinput">{⁗m/⁗ + trezor.path.purpose + ⁗\'/⁗ + trezor.path.coin + ⁗\'/⁗}</div> <div class="inline-input unit-input"><material-textfield ref="trezor_account" hint="account" on_input="{onInputTrezorAccount}"></material-textfield></div> <div class="text-with-inlineinput">{⁗\'⁗}</div> </div> <div class="basic"> <span if="{TagG.jp}">以下のURLをブックマークしておけば、ブックマークを開くだけですぐに上記の path でTrezorを開けます</span> <span if="{!TagG.jp}">If you bookmark the following URL, you can immediately open Trezor with the above path.</span> </div> <div class="basic data">{trezor.directURL}</div> <div if="{windowErrorText}" class="basic warning-color">{windowErrorText}</div> <div class="bottombutton-frame"> <material-button label="{TagG.jp ? \'ひらく\' : \'OPEN\'}" icon="lock_open" on_click="{onClickOpenTrezorButton}" is_unelevated="true" is_fullwidth="true"></material-button> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    this.ADDR_TYPE = ADDR_TYPE;
    this.isMakingWalletView = false;
    this.isOpeningWalletView = false;
    this.isQuickAccess = false;
    this.newMnemonicAddrType = ADDR_TYPE.P2PKH;
    this.addrsDataToShow = [];
    this.trezor = {
        path: { purpose: TagG.addrType2purpose(ADDR_TYPE.P2PKH), coin: TagG.NETWORK.versions.bip44, account: 0 },
        addrType: ADDR_TYPE.P2PKH,
        directURL: '',
    };
    if (TagG.query[TagG.QUERYKEY_ENCMNEMONIC]) {
        this.isQuickAccess = true;
        this.encryptedMnemonic = decodeURIComponent(TagG.query[TagG.QUERYKEY_ENCMNEMONIC]);
        delete TagG.query[TagG.QUERYKEY_ENCMNEMONIC];
        if (TagG.query[TagG.QUERYKEY_ADDRTYPE]) this.quickAccessAddrType = TagG.query[TagG.QUERYKEY_ADDRTYPE];
        delete TagG.query[TagG.QUERYKEY_ADDRTYPE];
        if (TagG.query[TagG.QUERYKEY_ACCOUNT]) this.quickAccessAccount = TagG.query[TagG.QUERYKEY_ACCOUNT];
        delete TagG.query[TagG.QUERYKEY_ACCOUNT];
    }
    if (TagG.query[TagG.QUERYKEY_TREZORPATH]) {
        this.queryTrezorPath = decodeURIComponent(TagG.query[TagG.QUERYKEY_TREZORPATH]);
        delete TagG.query[TagG.QUERYKEY_TREZORPATH];
    }

    this.onClickGotoMakeWalletButton = () => {
        this.isMakingWalletView = true;
        this.update();
        window.scrollTo(0, 0);
    };
    this.onClickGotoOpenWalletButton = () => {
        this.isOpeningWalletView = true;
        this.update();
        window.scrollTo(0, 0);
    };
    this.onChangeMakingSegwitSwitch = (isChecked) => {
        this.newMnemonicAddrType = isChecked ? ADDR_TYPE.P2WPKH : ADDR_TYPE.P2PKH;
        if (this.newMnemonic) this.onClickMakeWalletButton();
    };
    this.onClickMakeWalletButton = async() => {
        const password = this.refs.new_password.getValue();
        const password2 = this.refs.new_password_reinput.getValue();
        if (password !== password2) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '2回入力したパスワードが同じではありません' : 'The password entered twice is not the same.', 'error');
        if (password.match(/\s/)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'パスワードにスペースを含めることはできません' : 'Password cannot contain spaces.', 'error');
        if (password.length < 16) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'パスワードは16文字以上にしてください' : 'Passwords must be at least 16 chars.', 'error');
        this.newMnemonic = await TagG.aGenerateStrongMnemonic(128);
        const encMnemonic = TagG.CryptoJS.AES.encrypt(this.newMnemonic, password).toString();
        const uriEncMnemonic = encodeURIComponent(encMnemonic);
        this.newQuickAccessURL = `${location.protocol}//${location.host}${location.pathname}?${TagG.QUERYKEY_ENCMNEMONIC}=${uriEncMnemonic}`;
        if (this.newMnemonicAddrType === ADDR_TYPE.P2WPKH) this.newQuickAccessURL += `&${TagG.QUERYKEY_ADDRTYPE}=${ADDR_TYPE.P2WPKH}`;
        this.update();
    };
    this.onClickURLCopyButton = () => {
        const success = Util.execCopy(this.newQuickAccessURL);
        if (success) myswal({ title: 'Copied', text: (TagG.jp ? 'クイックアクセスURLをクリップボードにコピーしました' : 'Quick-access URL copied to the clipboard.'), icon: 'success', timer: TagG.SWAL_TIMER, buttons: {} });
        else myswal('Copy Failed', TagG.jp ? 'このブラウザではコピーボタンが使えないようなので、手動でコピーしてください' : 'The copy button does not be supported by this browser.', 'error');
    };
    this.onInputMnemonic = (mnemonicOrWif) => {
        if (mnemonicOrWif.includes('#P2WPKH') || mnemonicOrWif.includes('#segwit')) this.refs.opening_segwit_switch.setChecked(true);
    };
    this.onClickOpenMnemonicButton = () => {
        const mnemonicOrWif = this.refs.existing_mnemonic.getValue().trim();
        const addrType = this.refs.opening_segwit_switch.getChecked() ? ADDR_TYPE.P2WPKH : ADDR_TYPE.P2PKH;
        if (TagG.validateWIF(mnemonicOrWif)) {
            const wif = mnemonicOrWif;
            TagG.setWIF(wif, addrType);
        }
        else {
            const mnemonic = mnemonicOrWif.split(/\s+/).slice(0, 12).join(' ');
            if (!TagG.bip39.validateMnemonic(mnemonic)) return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? '入力された値がニーモニックとしてもWIFとしても正しくありません' : 'The input value is not valid as mnemonic or WIF.', 'error');
            TagG.setMnemonic(mnemonic, addrType);
        }
        aOpenWallet();
    };
    this.onEnterQuickAccessPassword = (password) => {
        let mnemonicOrWif;
        try { mnemonicOrWif = TagG.CryptoJS.enc.Utf8.stringify(TagG.CryptoJS.AES.decrypt(this.encryptedMnemonic, password)) }
        catch (error) { return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'パスワードが正しくありません' : 'Invalid password.', 'error') }
        let addrType = ADDR_TYPE.P2PKH;
        if (this.quickAccessAddrType === ADDR_TYPE.P2WPKH) addrType = ADDR_TYPE.P2WPKH;
        if (TagG.bip39.validateMnemonic(mnemonicOrWif)) {
            let accountPath = null;
            if (this.quickAccessAccount) accountPath = TagG.addrType2defaultAccountPath(addrType, this.quickAccessAccount);
            TagG.setMnemonic(mnemonicOrWif, addrType, accountPath, this.encryptedMnemonic);
        }
        else if (TagG.validateWIF(mnemonicOrWif)) TagG.setWIF(mnemonicOrWif, addrType);
        else return myswal(TagG.jp ? 'だめ' : 'Oops!', TagG.jp ? 'パスワードが正しくありません' : 'Invalid password.', 'error');
        aOpenWallet();
    };
    this.onClickQuickAccessPasswordEnterButton = () => {
        const password = this.refs.quickaccess_password.getValue();
        this.onEnterQuickAccessPassword(password);
    };
    this.onClickWalletHelpButton = () => {
        Util.aOpenContentSwal(this.refs.wallethelp_window);
    };
    this.onClickNewAddrButton = () => {
        TagG.addrsDataMaxIndex++;
        TagG.setAddrSelectItems();
        this.update();
        const indexStoreKey = TagG.STORAGEKEY_ADDRINDEX_FORMAT.replace('{FIRST_ADDR}', TagG.addrsData[0].address);
        Util.saveStorage(indexStoreKey, TagG.addrsDataMaxIndex);
    };
    this.onClickOpenTrezorWinButton = () => {
        this.refs.trezor_account.setValue(this.trezor.path.account);
        setTrezorDirectURL();
        clearWindowMessages();
        Util.aOpenContentSwal(this.refs.trezor_window);
    };
    this.onChangeTrezorSegwitSwitch = (isChecked) => {
        this.trezor.addrType = isChecked ? ADDR_TYPE.P2WPKH : ADDR_TYPE.P2PKH;
        this.trezor.path.purpose = TagG.addrType2purpose(this.trezor.addrType);
        setTrezorDirectURL();
        this.update();
    };
    this.onInputTrezorAccount = (value) => {
        const account = Number(value);
        if (!Number.isInteger(account) || account < 0) return;
        this.trezor.path.account = account;
        setTrezorDirectURL();
        this.update();
    };
    this.onClickOpenTrezorButton = async() => {
        const path = this.trezor.path;
        const account = Number(this.refs.trezor_account.getValue());
        if (!Number.isInteger(account) || account < 0) return showWindowError(TagG.jp ? 'だめです。accountは0以上の整数にしてね' : 'Oops! Invalid account.');
        path.account = account;
        const pathStr = `m/${path.purpose}'/${path.coin}'/${path.account}'`;
        aOpenTrezor(pathStr, this.trezor.addrType);
    };

    this.on('mount', () => {
        const container = this.refs.swalwindows_container;
        while (container && container.firstChild) container.removeChild(container.firstChild);
        if (TagG.addrsData) aOpenWallet();
        else if (this.isQuickAccess) aTryOpenSavedXpub();
        else if (this.queryTrezorPath) aOpenTrezor(this.queryTrezorPath);
        else aShowSampleCards().catch(console.warn);
    });
    const aShowSampleCards = async() => {
        if (!TagG.DISPENSERS_CACHEJSON) return;
        const { dispensers } = await Util.aGetJSON(`${TagG.DISPENSERS_CACHEJSON}?${TagG.getCacheQuery(2)}`);
        let assets = dispensers.slice(0, 50).map(dispenser => dispenser.asset);
        assets = Util.uniq(assets);
        await TagG.aLoadAssets(assets, true, true);
        this.sampleCards = assets.filter(asset => TagG.asset2card[asset]).slice(0, 20).map(asset => ({
            image: TagG.getAssetImageURL('auto', asset),
            linkURL: TagG.asset2card[asset].siteURL,
        }));
        this.update();
    };
    const aOpenWallet = async() => {
        await TagG.aUpdateAddrsDataWithUI(TagG.bodySpinner);
        this.addrsLoaded = true;
        this.addrsDataToShow = [];
        for (const addrData of TagG.addrsData) {
            this.addrsDataToShow.push(addrData);
            if (addrData.assets.length > 10) {
                this.update();
                await Util.aSleep(addrData.assets.length * 10);
            }
            else if (this.addrsDataToShow.length === TagG.addrsData.length) this.update();
        }
        TagG.aLoadAssetsExtendedInfo().then(isUpdated => { if (isUpdated) this.update() });
    };
    const aTryOpenSavedXpub = async() => {
        let addrType = ADDR_TYPE.P2PKH;
        if (this.quickAccessAddrType === ADDR_TYPE.P2WPKH) addrType = ADDR_TYPE.P2WPKH;
        const accountPath = TagG.addrType2defaultAccountPath(addrType, this.quickAccessAccount || 0);
        const accountXpub = TagG.loadAccountXpub(accountPath, this.encryptedMnemonic);
        if (accountXpub) {
            TagG.setAccountXpub(accountXpub, addrType, accountPath, this.encryptedMnemonic);
            await aOpenWallet();
        }
    };
    const aOpenTrezor = async(pathStr, addrType = null) => {
        if (!addrType) {
            if (pathStr.split('/')[1] === "84'") addrType = ADDR_TYPE.P2WPKH;
            else addrType = ADDR_TYPE.P2PKH;
        }
        const trezorResult = await TrezorConnect.getPublicKey({ path: pathStr, coin: TagG.TREZOR_COIN });
        if (!trezorResult.success) {
            if (swal.getState().isOpen) return showWindowError(TagG.jp ? `エラーが発生しました: ${trezorResult.payload.error}` : `Failed. hint: ${trezorResult.payload.error}`);
            else return myswal('Error', TagG.jp ? `Trezorへのアクセスに失敗しました: ${trezorResult.payload.error}` : `Failed to access Trezor. hint: ${trezorResult.payload.error}`, 'error');
        }
        TagG.isTrezor = true;
        TagG.setAccountXpub(trezorResult.payload.xpub, addrType, pathStr);
        if (swal.getState().isOpen) swal.close();
        await aOpenWallet();
    };
    const setTrezorDirectURL = () => {
        const path = this.trezor.path;
        const encodedPathStr = encodeURIComponent(`m/${path.purpose}'/${path.coin}'/${path.account}'`).replace(/'/g, '%27');
        this.trezor.directURL = `${location.protocol}//${location.host}${location.pathname}?${TagG.QUERYKEY_TREZORPATH}=${encodedPathStr}`;
    };
    const clearWindowMessages = () => {
        this.windowErrorText = '';
        this.update();
    };
    const showWindowError = (message) => {
        this.windowErrorText = message;
        this.update();
    };
});
riot.tag2('wallet-settings', '<div class="app-page"> <div class="page-header"> <i class="material-icons page-icon">settings</i> <h2 class="page-title">{TagG.jp ? \'ウォレット設定\' : \'Wallet Settings\'}</h2> </div> <div class="page-content"> <div class="section-frame"> <div class="paragraph page-description"> <span if="{TagG.jp}">設定はブラウザに保存されます。よくわからなければ変更しないほうが無難です</span> <span if="{!TagG.jp}">The settings will be saved in your browser. If you are not sure, it is safer not to change them.</span> </div> </div> <div class="section-frame"> <div class="section-title">Monapalette in English</div> <div class="paragraph"><material-switch ref="english_switch" label="ON" on_change="{onChangeEnglish}"></material-switch></div> </div> <div class="section-frame"> <div class="section-title"> <span if="{TagG.jp}">ゼロ承認の{TagG.BASECHAIN_ASSET}を使う</span> <span if="{!TagG.jp}">Use zero-conf {TagG.BASECHAIN_ASSET}</span> </div> <div class="paragraph"><material-switch ref="zeroconf_switch" label="ON" on_change="{onChangeZeroconf}"></material-switch></div> </div> <div class="section-frame"> <div class="section-title">{TagG.jp ? \'Monapartyサーバを指定する\' : \'Specify Monaparty servers\'}</div> <div class="paragraph"> <span if="{TagG.jp}">Counterblock API のエンドポイントを入力してください。カンマ区切りで複数指定すると冗長構成で使用します。</span> <span if="{!TagG.jp}">Enter Counterblock API endpoint. If you specify multiple endpoints separated by commas, they will be used in a redundant configuration.</span> </div> <div class="paragraph"> <span if="{TagG.jp}">ここに変な値や利用できないエンドポイントが入っているとウォレットが動かなくなるのでご注意ください。</span> <span if="{!TagG.jp}">Please note that the wallet will not work if there is strange value or unavailable endpoint here.</span> </div> <div class="paragraph"><material-textfield ref="endpoints_input" hint="Counterblock Endpoints" on_change="{onChangeEndpoints}"></material-textfield></div> </div> </div> </div>', '', '', function(opts) {
    this.TagG = TagG;
    const ENDPOINTS_STRKEY = TagG.STORAGEKEY_ENDPOINTS_FORMAT.replace('{COIN}', TagG.COIN);
    this.onChangeZeroconf = (checked) => {
        if (checked) TagG.confNum = 0;
        else TagG.confNum = 1;
        Util.saveStorage(TagG.STORAGEKEY_CONFNUM, TagG.confNum);
    };
    this.onChangeEnglish = (checked) => {
        if (checked) {
            TagG.jp = false;
            Util.saveStorage(TagG.STORAGEKEY_LANG, 'EN');
        }
        else {
            TagG.jp = true;
            Util.saveStorage(TagG.STORAGEKEY_LANG, 'JA');
        }
        TagG.body.update();
    };
    this.onChangeEndpoints = (endpoints) => {
        endpoints = endpoints.split(',').map(end => end.trim()).filter(end => end);
        if (endpoints.length > 0) {
            Monaparty.serversFile = null;
            Monaparty.fullEndpoints = endpoints || TagG.MONAPARTY_ENDPOINTS;
            Monaparty.nocoindEndpoints = [];
            Util.saveStorage(ENDPOINTS_STRKEY, endpoints);
        }
        else {
            Monaparty.serversFile = Monaparty.defaults.serversFile;
            Monaparty.fullEndpoints = TagG.MONAPARTY_ENDPOINTS || Monaparty.defaults.fullEndpoints;
            Monaparty.nocoindEndpoints = Monaparty.defaults.nocoindEndpoints;
            Util.saveStorage(ENDPOINTS_STRKEY, null);
        }
    };
    this.on('mount', async() => {
        this.refs.zeroconf_switch.setChecked(TagG.confNum === 0);
        this.refs.english_switch.setChecked(TagG.jp === false);
        this.refs.endpoints_input.setValue((Util.loadStorage(ENDPOINTS_STRKEY) || []).join(', '));
    });
});
riot.tag2('base-body', '<div ref="body" class="body-frame"> <div class="body-content"> <base-pagecontroller on_changepage="{onChangePage}"></base-pagecontroller> </div> <div class="body-header"> <h1 class="body-title"><img if="{!subscript}" riot-src="{TagG.RELATIVE_URL_BASE + \'img/logoicon_80.png\'}" onclick="{onClickTitle}" class="body-title-logo body-clickable" loading="lazy"><span onclick="{onClickTitle}" class="body-clickable">{title}</span></h1> <div if="{subscript}" class="body-poweredby">{subscript}</div> </div> <addrs-window ref="addrs_window"></addrs-window> <base-menu ref="menu" items="{menuItems}"></base-menu> <base-swalcontents></base-swalcontents> <txmanager-dispenserpurchase ref="dispenser_purchase"></txmanager-dispenserpurchase> <txmanager-dexpurchase ref="dex_purchase"></txmanager-dexpurchase> <util-spinner ref="spinner" is_fulldisplay="true"></util-spinner> </div>', '', '', function(opts) {
    this.TagG = TagG;
    TagG.body = this;
    switch (TagG.COIN) {
        case COINS.MONA_TESTNET:
            this.title = TagG.jp ? 'モナパレット' : 'Monapalette';
            this.subscript = '※※※ TESTNET ※※※';
            this.menuItems = [
                { title: 'Wallet', onClick: () => { route('/wallet'); } },
                { title: 'Markets', onClick: () => { route('/shops'); } },
                { title: 'D-Board', onClick: () => { route('/dboard'); } },
                { title: 'AppConnect', onClick: () => { route('/appconnect'); } },
                { title: 'Tools', onClick: () => { route('/tools'); } },
                { title: 'About', onClick: () => { route('/about'); } },
            ];
            break;
        case COINS.MONA:
            this.title = TagG.jp ? 'モナパレット' : 'Monapalette';
            this.menuItems = [
                { title: 'Wallet', onClick: () => { route('/wallet'); } },
                { title: 'Markets', onClick: () => { route('/shops'); } },
                { title: 'Token Vault', onClick: () => { route('/tokenvault'); } },
                { title: (TagG.jp ? 'らくがきNFT' : 'Rakugaki NFT'), onClick: () => { route('/rakugaki'); } },
                { title: 'D-Board', onClick: () => { route('/dboard'); } },
                { title: 'AppConnect', onClick: () => { route('/appconnect'); } },
                { title: 'Tools', onClick: () => { route('/tools'); } },
                { title: 'About', onClick: () => { route('/about'); } },
            ];
            break;
        case COINS.BTC_TESTNET:
            this.title = 'Counterpalette';
            this.subscript = '※※※ TESTNET ※※※';
            this.menuItems = [
                { title: 'Wallet', onClick: () => { route('/wallet'); } },
                { title: 'Tools', onClick: () => { route('/tools'); } },
            ];
            break;
        case COINS.BTC:
            this.title = 'Counterpalette';
            this.menuItems = [
                { title: 'Wallet', onClick: () => { route('/wallet'); } },
                { title: 'Tools', onClick: () => { route('/tools'); } },
            ];
            break;
    }
    if (TagG.IS_MPURSEEX) {
        this.title = TagG.jp ? 'Mpurseエクステ' : 'MpurseExte';
        this.subscript = TagG.jp ? 'powered by モナパレット' : 'powered by Monapalette';
        this.menuItems = [
            { title: 'Wallet', onClick: () => { route('/addr_from_mpurse'); } },
            { title: 'Markets', onClick: () => { route('/shops'); } },
            { title: 'Token Vault', onClick: () => { route('/tokenvault'); } },
            { title: (TagG.jp ? 'らくがきNFT' : 'Rakugaki NFT'), onClick: () => { route('/rakugaki'); } },
            { title: 'D-Board', onClick: () => { route('/dboard'); } },
            { title: 'Tools', onClick: () => { route('/tools'); } },
            { title: 'About', onClick: () => { route('/about'); } },
        ];
    }
    this.onClickTitle = () => { route('/'); };
    this.onChangePage = () => {
        this.refs.menu.closePanel();
    };
    this.on('mount', () => {
        TagG.bodyMenu = this.refs.menu;
        TagG.bodySpinner = this.refs.spinner;
        TagG.bodyDispenserPurchase = this.refs.dispenser_purchase;
        TagG.bodyDexPurchase = this.refs.dex_purchase;
        TagG.bodyAddrsWin = this.refs.addrs_window;
    });
});
riot.tag2('base-menu', '<material-iconbutton icon="menu" class="menu-button" on_click="{onMenuButtonClick}"></material-iconbutton> <div if="{notificationSum}" class="menu-button-notification">{notificationSum}</div> <div ref="panel" if="{isShown}" class="menu-panel"> <material-iconbutton icon="close" class="close-button" on_click="{onCloseButtonClick}"></material-iconbutton> <div class="menu-items"> <div each="{items}" class="menu-item"> <material-button label="{title}" on_click="{onClickAndClose}" is_fullwidth="true"></material-button> <div if="{title2count[title]}" class="menu-item-notification">{title2count[title]}</div> </div> </div> </div>', '', '', function(opts) {
    this.items = opts.items || [{ title: 'sample', onClick: () => console.log('sample') }];
    for (const item of this.items) {
        item.onClickAndClose = () => {
            item.onClick();
            this.closePanel();
        };
    }
    this.isShown = false;
    this.title2count = {};
    let showAnimationWaiting = false;

    this.onMenuButtonClick = () => {
        this.isShown = true;
        showAnimationWaiting = true;
        this.update();
    };
    this.onCloseButtonClick = () => {
        this.closePanel();
    };
    this.on('updated', () => {
        if (showAnimationWaiting) {
            TweenLite.to(this.refs.panel, 0.5, {
                opacity: '1.0',
                onComplete: () => {
                    this.refs.panel.style.pointerEvents = 'auto';
                },
            });
            showAnimationWaiting = false;
        }
    });
    this.closePanel = () => {
        if (!this.refs.panel) return;
        this.refs.panel.style.pointerEvents = 'none';
        TweenLite.to(this.refs.panel, 0.5, {
            opacity: '0.0',
            onComplete: () => {
                this.isShown = false;
                this.update();
            },
        });
    };
    this.setNotifications = (title2count = {}) => {
        this.notificationSum = 0;
        for (const title in title2count) this.notificationSum += title2count[title];
        this.title2count = title2count;
        this.update();
    };
});
riot.tag2('base-pagecontroller', '<div ref="frame" class="pagecontroller-frame"> <wallet-page if="{page === \'wallet\'}"></wallet-page> <markets-page if="{page === \'markets\'}" tab="{pageParams[0]}" param1="{pageParams[1]}" param2="{pageParams[2]}"></markets-page> <tokenvault-page if="{page === \'tokenvault\'}"></tokenvault-page> <rakugaki-page if="{page === \'rakugaki\'}"></rakugaki-page> <dboard-page if="{page === \'dboard\'}"></dboard-page> <appconnect-page if="{page === \'appconnect\'}"></appconnect-page> <tools-page if="{page === \'tools\'}"></tools-page> <about-page if="{page === \'about\'}"></about-page> <wallet-settings if="{page === \'wallet_settings\'}"></wallet-settings> <monaparty-api-page if="{page === \'monaparty_api\'}"></monaparty-api-page> <regenerate-quickurl-page if="{page === \'regenerate_quickurl\'}"></regenerate-quickurl-page> <generate-kirakiraaddr-page if="{page === \'generate_kirakiraaddr\'}"></generate-kirakiraaddr-page> <addrbook-page if="{page === \'addrbook\'}"></addrbook-page> <addr-from-mpurse-page if="{page === \'addr_from_mpurse\'}"></addr-from-mpurse-page> <step-page if="{page === \'step\'}"></step-page> <appconnect-sample if="{page === \'appconnect_sample\'}"></appconnect-sample> </div>', '', '', function(opts) {
    if (TagG.IS_MPURSEEX) route('/', () => { this.changePage('addr_from_mpurse') });
    else route('/', () => { this.changePage('wallet') });
    route('/wallet', () => { this.changePage('wallet') });
    route('/dispensers', () => { if (this.page !== 'markets') this.changePage('markets', 'DISPENSERS') });
    route('/dex', () => { if (this.page !== 'markets') this.changePage('markets', 'DEX') });
    route('/dex/*/*', (param1, param2) => { if (this.page !== 'markets') this.changePage('markets', 'DEX', decodeURIComponent(param1), decodeURIComponent(param2)) });
    route('/shops', () => { if (this.page !== 'markets') this.changePage('markets', 'SHOPS') });
    route('/shops/*', (param1) => { if (this.page !== 'markets') this.changePage('markets', 'SHOPS', decodeURIComponent(param1)) });
    route('/tokenvault', () => { this.changePage('tokenvault') });
    route('/rakugaki', () => { this.changePage('rakugaki') });
    route('/dboard', () => { this.changePage('dboard') });
    route('/appconnect', () => { this.changePage('appconnect') });
    route('/tools', () => { this.changePage('tools') });
    route('/about', () => { this.changePage('about') });
    route('/wallet_settings', () => { this.changePage('wallet_settings') });
    route('/monaparty_api', () => { this.changePage('monaparty_api') });
    route('/regenerate_quickurl', () => { this.changePage('regenerate_quickurl') });
    route('/generate_kirakiraaddr', () => { this.changePage('generate_kirakiraaddr') });
    route('/addrbook', () => { this.changePage('addrbook') });
    route('/addr_from_mpurse', () => { this.changePage('addr_from_mpurse') });
    route('/step', () => { this.changePage('step') });
    route('/appconnect_sample', () => { this.changePage('appconnect_sample') });
    route('/regenerate_quickurl?..', () => { route('/') });
    route('/*?..', (page) => {
        Object.assign(TagG.query, route.query() || {});
        route(`/${page}`);
    });
    route('/*/*?..', (page, param1) => {
        Object.assign(TagG.query, route.query() || {});
        route(`/${page}/${param1}`);
    });
    route('/..', () => { route('/') });
    route.start(true);

    const _onChangePage = opts.on_changepage;
    this.page = opts.page || '';

    this.changePage = (page, ...pageParams) => {
        if (swal.getState().isOpen) swal.close();
        const frame = this.refs.frame;
        if (_onChangePage) _onChangePage();
        frame.style.pointerEvents = 'none';
        frame.style.opacity = '0.0';
        window.scrollTo(0, 0);
        this.page = page;
        this.pageParams = pageParams;
        this.update();
        TweenLite.to(frame, 0.5, {
            opacity: '1.0',
            onComplete: () => { frame.style.pointerEvents = 'auto'; },
        });
    };
});
riot.tag2('base-swalcontents', '<div ref="container" show="{false}"> <div ref="password_win" class="base-swalcontent"> <div class="title">{passwordWin.title || \'Password\'}</div> <div if="{passwordWin.text}" class="basic">{passwordWin.text}</div> <div class="basic"><material-textfield ref="password_win_input" hint="Password" is_password="true" on_enter="{passwordWin.onClickOK}"></material-textfield></div> <div class="bottombutton-frame"><material-button label="OK" on_click="{passwordWin.onClickOK}" is_unelevated="true" is_fullwidth="true"></div> </div> </div>', '', '', function(opts) {
    this.passwordWin = {};
    this.passwordWin.onClickOK = () => {
        const ref = this.refs.password_win;
        ref.isOK = true;
        ref.value = this.refs.password_win_input.getValue();
        swal.close();
    };
    this.on('mount', () => {
        const container = this.refs.container;
        while (container && container.firstChild) container.removeChild(container.firstChild);
        setup();
    });
    const setup = () => {
        const swalContents = {};
        swalContents.getPasswordWin = ({ title = null, text = null } = {}) => {
            this.passwordWin.title = title;
            this.passwordWin.text = text;
            this.update();
            const ref = this.refs.password_win;
            [ref.isOK, ref.value] = [false, null];
            return ref;
        };
        TagG.swalContents = swalContents;
    };
});
riot.tag2('material-button', '<button if="{!isLink}" ref="button" onclick="{onClick}" class="mdc-button {mdcOptClass} {mdccustom-button-white: isWhite, mdccustom-button-fullwidth: isFullWidth}"> <div class="mdc-button__ripple"></div> <i if="{icon}" class="material-icons mdc-button__icon" aria-hidden="true">{icon}</i> <span class="mdc-button__label">{label}</span> </button> <a if="{isLink}" ref="button" href="{linkHref}" target="{linkTarget}" class="mdc-button {mdcOptClass} {mdccustom-button-white: isWhite, mdccustom-button-fullwidth: isFullWidth}"> <div class="mdc-button__ripple"></div> <i if="{icon}" class="material-icons mdc-button__icon" aria-hidden="true">{icon}</i> <span class="mdc-button__label">{label}</span> </a>', '', '', function(opts) {
    const loadOpts = () => {
        this.label = opts.label || '';
        this.icon = opts.icon || null;
        if (opts.is_outlined) this.mdcOptClass = 'mdc-button--outlined';
        if (opts.is_raised) this.mdcOptClass = 'mdc-button--raised';
        if (opts.is_unelevated) this.mdcOptClass = 'mdc-button--unelevated';
        this.isWhite = Boolean(opts.is_white);
        this.isFullWidth = Boolean(opts.is_fullwidth);
        this.isLink = Boolean(opts.is_link);
        this.onClick = opts.on_click;
        this.linkHref = opts.link_href;
        this.linkTarget = opts.link_target;
        this.isDisabled = Boolean(opts.is_disabled);
    };
    this.setDisabled = (isDisabled) => {
        this.refs.button.disabled = isDisabled;
        this.isDisabled = isDisabled;
    };
    this.setLabel = (label, icon = 'NO_CHANGE') => {
        this.label = label;
        if (icon !== 'NO_CHANGE') this.icon = icon;
        this.update();
    };
    this.on('mount', () => {
        this.refs.button.disabled = this.isDisabled;
        new mdc.ripple.MDCRipple(this.refs.button);
    });
    this.on('update', () => {
        if (opts.reload_opts) {
            loadOpts();
            this.refs.button.disabled = this.isDisabled;
        }
    });
    loadOpts();
});
riot.tag2('material-checkbox', '<div ref="formfield" class="mdc-form-field"> <div ref="checkbox" class="mdc-checkbox"> <input type="checkbox" class="mdc-checkbox__native-control" id="{random + \'_checkbox\'}" onchange="{onChange}"> <div class="mdc-checkbox__background"> <svg class="mdc-checkbox__checkmark" viewbox="0 0 24 24"> <path class="mdc-checkbox__checkmark-path" fill="none" d="M1.73,12.91 8.1,19.28 22.79,4.59"></path> </svg> <div class="mdc-checkbox__mixedmark"></div> </div> <div class="mdc-checkbox__ripple"></div> </div> <label for="{random + \'_checkbox\'}" class="mdccustom-label">{label}</label> </div>', '', '', function(opts) {
    const main = () => {
        this.label = opts.label || '';
        const isDefaultChecked = Boolean(opts.is_checked);
        const _onChange = opts.on_change || (checked => { checked });
        this.random = generateID(16);
        let mdcCheckbox = null;

        this.getChecked = () => {
            if (mdcCheckbox) return mdcCheckbox.checked;
            else return null;
        };
        this.setChecked = (checked) => {
            if (mdcCheckbox) mdcCheckbox.checked = checked;
            else setTimeout(() => {
                if (mdcCheckbox) mdcCheckbox.checked = checked;
            }, 0);
        };

        this.onChange = () => { _onChange(mdcCheckbox.checked) };
        this.on('mount', () => {
            mdcCheckbox = new mdc.checkbox.MDCCheckbox(this.refs.checkbox);
            const mdcFormField = new mdc.formField.MDCFormField(this.refs.formfield);
            mdcFormField.input = mdcCheckbox;
            mdcCheckbox.checked = isDefaultChecked;
        });
    };
    const generateID = (length) => {
        const CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        const CHARS_LENGTH = CHARS.length;
        let code = '';
        for (let i = 0; i < length; i++) code += CHARS[Math.floor(Math.random() * CHARS_LENGTH)];
        return code;
    };
    main();
});
riot.tag2('material-chip', '<div ref="chip" class="mdc-chip" onclick="{onClick}"> <div class="mdc-chip__ripple"></div> <i if="{icon}" class="material-icons mdc-chip__icon mdc-chip__icon--leading">{icon}</i> <span class="mdc-chip__primary-action"> <span class="mdc-chip__text">{label}</span> </span> </div>', '', '', function(opts) {
    this.label = opts.label || '';
    this.icon = opts.icon || null;
    this.onClick = opts.on_click;

    this.setLabel = (label, icon = 'NO_CHANGE') => {
        this.label = label;
        if (icon !== 'NO_CHANGE') this.icon = icon;
        this.update();
    };

    this.on('mount', () => {
        new mdc.chips.MDCChip(this.refs.chip);
    });
});
riot.tag2('material-iconbutton', '<button if="{!isLink}" ref="button" onclick="{onClick}" class="mdc-icon-button material-icons {mdcOptClass} {mdccustom-iconbutton-white: isWhite}">{icon}</button> <a if="{isLink}" ref="button" href="{linkHref}" target="{linkTarget}" class="mdc-icon-button material-icons {mdcOptClass} {mdccustom-iconbutton-white: isWhite}">{icon}</a>', '', '', function(opts) {
    const loadOpts = () => {
        this.icon = opts.icon || null;
        this.isWhite = Boolean(opts.is_white);
        this.isLink = Boolean(opts.is_link);
        this.onClick = opts.on_click;
        this.linkHref = opts.link_href;
        this.linkTarget = opts.link_target;
        this.isDisabled = Boolean(opts.is_disabled);
    };
    this.setDisabled = (isDisabled) => {
        this.refs.button.disabled = isDisabled;
        this.isDisabled = isDisabled;
    };
    this.setIcon = (icon) => { this.icon = icon; this.update() };
    this.on('mount', () => {
        this.refs.button.disabled = this.isDisabled;
        const ripple = new mdc.ripple.MDCRipple(this.refs.button);
        ripple.unbounded = true;
    });
    this.on('update', () => {
        if (opts.reload_opts) {
            loadOpts();
            this.refs.button.disabled = this.isDisabled;
        }
    });
    loadOpts();
});
riot.tag2('material-radiobuttons', '<div each="{items}" ref="{formFieldRef}" class="mdc-form-field {mdccustom-formfield-block: isBlock}"> <div ref="{radioRef}" class="mdc-radio"> <input ref="{radioInputRef}" class="mdc-radio__native-control" type="radio" id="{id}" riot-value="{value}" name="{name}" onchange="{onChange}"> <div class="mdc-radio__background"> <div class="mdc-radio__outer-circle"></div> <div class="mdc-radio__inner-circle"></div> </div> <div class="mdc-radio__ripple"></div> </div> <label for="{id}" class="mdccustom-label">{label}</label> </div>', '', '', function(opts) {
    const main = () => {
        this.items = deepCopy(opts.items || [{ value: 'value1', label: 'item1' }]);
        const defaultValue = opts.default_value || null;
        this.isBlock = Boolean(opts.is_block);
        const _onChange = opts.on_change || (value => { value });
        for (const item of this.items) {
            const random = generateID(16);
            item.id = random;
            item.formFieldRef = `${random}_formfield`;
            item.radioRef = `${random}_radio`;
            item.radioInputRef = `${random}_radioinput`;
        }
        this.value = this.items[0] ? this.items[0].value : null;
        this.name = generateID(16);

        this.getValue = () => this.value;
        this.setValue = (value) => {
            const item = this.items.find(item => (item.value === value));
            if (!item) return;
            if (item.mdcRadio) {
                item.mdcRadio.checked = true;
                this.value = value;
            }
            else setTimeout(() => {
                if (item.mdcRadio) {
                    item.mdcRadio.checked = true;
                    this.value = value;
                }
            }, 0);
        };

        this.onChange = (event) => {
            const sender = event.target || event.srcElement;
            this.value = sender.value;
            _onChange(this.value);
        };
        this.on('mount', () => {
            for (const item of this.items) {
                item.mdcRadio = new mdc.radio.MDCRadio(this.refs[item.radioRef]);
                const formField = new mdc.formField.MDCFormField(this.refs[item.formFieldRef]);
                formField.input = item.mdcRadio;
            }
            if (defaultValue) this.setValue(defaultValue);
            else if (this.items[0]) this.items[0].mdcRadio.checked = true;
        });
    };
    const generateID = (length) => {
        const CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        const CHARS_LENGTH = CHARS.length;
        let code = '';
        for (let i = 0; i < length; i++) code += CHARS[Math.floor(Math.random() * CHARS_LENGTH)];
        return code;
    };
    const deepCopy = (array) => JSON.parse(JSON.stringify(array));
    main();
});
riot.tag2('material-select', '<div ref="select" class="mdc-select mdc-select--filled {mdccustom-select-fullwidth: isFullWidth}"> <div class="mdc-select__anchor" role="button" aria-haspopup="listbox" aria-expanded="false" aria-labelledby="{random + \'_label\'} {random + \'_selectedtext\'}"> <span class="mdc-select__ripple"></span> <span id="{random + \'_label\'}" class="mdc-floating-label">{hint}</span> <span class="mdc-select__selected-text-container"> <span id="{random + \'_selectedtext\'}" class="mdc-select__selected-text"></span> </span> <span class="mdc-select__dropdown-icon"> <svg class="mdc-select__dropdown-icon-graphic" viewbox="7 10 10 5" focusable="false"> <polygon class="mdc-select__dropdown-icon-inactive" stroke="none" fill-rule="evenodd" points="7 10 12 15 17 10"></polygon> <polygon class="mdc-select__dropdown-icon-active" stroke="none" fill-rule="evenodd" points="7 15 12 10 17 15"></polygon> </svg> </span> <span class="mdc-line-ripple"></span> </div> <div class="mdc-select__menu mdc-menu mdc-menu-surface {mdc-menu-surface--fullwidth: isMenuWidthFixed}"> <ul class="mdc-list" role="listbox" aria-label="Food picker listbox"> <li if="{withEmptyItem}" class="mdc-list-item mdc-list-item--selected" aria-selected="true" data-value="" role="option"> <span class="mdc-list-item__ripple"></span> </li> <li each="{items}" class="mdc-list-item" aria-selected="false" data-value="{value}" role="option"> <span class="mdc-list-item__ripple"></span> <span class="mdc-list-item__text">{label}</span> </li> </ul> </div> </div>', '', '', function(opts) {
    const main = () => {
        this.hint = opts.hint || '';
        this.items = copyItems(opts.items || [{ value: 'value1', label: 'item1' }]);
        const defaultValue = opts.default_value || null;
        const width = opts.width || null;
        this.isFullWidth = Boolean(opts.is_fullwidth);
        this.withEmptyItem = Boolean(opts.with_empty_item);
        this.isMenuWidthFixed = Boolean(opts.is_menuwidth_fixed);
        const _onChange = opts.on_change || (value => { value });
        this.random = generateID(16);
        let mdcSelect = null;

        this.getValue = () => {
            if (mdcSelect) return mdcSelect.value;
            else return null;
        };
        this.setValue = (value) => {
            if (mdcSelect) mdcSelect.value = value;
            else setTimeout(() => {
                if (mdcSelect) mdcSelect.value = value;
            }, 0);
        };

        const onChange = () => { _onChange(mdcSelect.value) };
        this.on('mount', () => {
            this.refs.select.style.width = width;
            mdcSelect = new mdc.select.MDCSelect(this.refs.select);
            if (defaultValue) mdcSelect.value = defaultValue;
            mdcSelect.listen('MDCSelect:change', onChange);
        });
    };
    const generateID = (length) => {
        const CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        const CHARS_LENGTH = CHARS.length;
        let code = '';
        for (let i = 0; i < length; i++) code += CHARS[Math.floor(Math.random() * CHARS_LENGTH)];
        return code;
    };
    const copyItems = (items) => JSON.parse(JSON.stringify(items.map(({ value, label }) => ({ value, label }))));
    main();
});
riot.tag2('material-switch', '<div ref="switch" class="mdc-switch"> <div class="mdc-switch__track"></div> <div class="mdc-switch__thumb-underlay"> <div class="mdc-switch__thumb"></div> <input type="checkbox" id="{random + \'_switch\'}" class="mdc-switch__native-control" role="switch" aria-checked="false" onchange="{onChange}"> </div> </div> <label for="{random + \'_switch\'}" class="mdccustom-label">{label}</label>', '', '', function(opts) {
    const main = () => {
        this.label = opts.label || '';
        const isDefaultChecked = Boolean(opts.is_checked);
        const _onChange = opts.on_change || (checked => { checked });
        this.random = generateID(16);
        let mdcSwitch = null;

        this.getChecked = () => {
            if (mdcSwitch) return mdcSwitch.checked;
            else return null;
        };
        this.setChecked = (checked) => {
            if (mdcSwitch) mdcSwitch.checked = checked;
            else setTimeout(() => {
                if (mdcSwitch) mdcSwitch.checked = checked;
            }, 0);
        };

        this.onChange = () => { _onChange(mdcSwitch.checked) };
        this.on('mount', () => {
            mdcSwitch = new mdc.switchControl.MDCSwitch(this.refs.switch);
            mdcSwitch.checked = isDefaultChecked;
        });
    };
    const generateID = (length) => {
        const CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        const CHARS_LENGTH = CHARS.length;
        let code = '';
        for (let i = 0; i < length; i++) code += CHARS[Math.floor(Math.random() * CHARS_LENGTH)];
        return code;
    };
    main();
});
riot.tag2('material-tabbar', '<div ref="tabbar" class="mdc-tab-bar" role="tablist"> <div class="mdc-tab-scroller"> <div class="mdc-tab-scroller__scroll-area"> <div class="mdc-tab-scroller__scroll-content"> <button each="{items}" class="mdc-tab" role="tab" aria-selected="false" tabindex="{index}"> <span class="mdc-tab__content"> <span if="{icon}" class="mdc-tab__icon material-icons" aria-hidden="true">{icon}</span> <span class="mdc-tab__text-label">{label}</span> </span> <span class="mdc-tab-indicator"> <span class="mdc-tab-indicator__content mdc-tab-indicator__content--underline"></span> </span> <span class="mdc-tab__ripple"></span> </button> </div> </div> </div> </div>', '', '', function(opts) {
    const main = () => {
        this.items = deepCopy(opts.items || [{ value: 'value1', label: 'item1', icon: 'favorite' }]);
        const defaultValue = opts.default_value || null;
        const onChange = opts.on_change || (value => { value });
        this.items.forEach((item, index) => { item.index = index });
        this.value = this.items[0] ? this.items[0].value : null;
        let mdcTabbar = null;

        this.getValue = () => this.value;
        this.setValue = (value) => {
            const index = value2index(value);
            if (index === -1) return;
            if (mdcTabbar) activateIndex(index);
            else setTimeout(() => {
                if (mdcTabbar) activateIndex(index);
            }, 0);
        };

        const onActivated = ({ detail }) => {
            this.value = this.items[detail.index].value;
            onChange(this.value);
        };
        this.on('mount', () => {
            mdcTabbar = new mdc.tabBar.MDCTabBar(this.refs.tabbar);
            const index = defaultValue ? value2index(defaultValue) : 0;
            activateIndex(index);
            mdcTabbar.listen('MDCTabBar:activated', onActivated);
        });
        const value2index = (value) => this.items.findIndex(item => (item.value === value));
        const activateIndex = (index) => {
            mdcTabbar.activateTab(index);
            mdcTabbar.scrollIntoView(index);
            this.value = this.items[index].value;
        };
    };
    const deepCopy = (array) => JSON.parse(JSON.stringify(array));
    main();
});
riot.tag2('material-textfield', '<label if="{!isOutlined}" ref="textfield" class="mdc-text-field mdc-text-field--filled {mdc-text-field--textarea: isTextarea}"> <span class="mdc-text-field__ripple"></span> <span if="{hint}" class="mdc-floating-label" id="{random + \'_label\'}">{hint}</span> <input if="{!isTextarea}" ref="input" class="mdc-text-field__input" type="{isPassword ? \'password\' : \'text\'}" maxlength="{maxLength}" aria-labelledby="{random + \'_label\'}" aria-controls="{random + \'_helper\'}" aria-describedby="{random + \'_helper\'}" onchange="{onChange}" oninput="{onInput}" onkeypress="{onKeyPress}"> <textarea if="{isTextarea}" ref="input" class="mdc-text-field__input" rows="{textareaRows}" maxlength="{maxLength}" aria-labelledby="{random + \'_label\'}" aria-controls="{random + \'_helper\'}" aria-describedby="{random + \'_helper\'}" onchange="{onChange}" oninput="{onInput}" onkeypress="{onKeyPress}"></textarea> <span class="mdc-line-ripple"></span> </label> <label if="{isOutlined}" ref="textfield" class="mdc-text-field mdc-text-field--outlined {mdc-text-field--textarea: isTextarea}"> <span class="mdc-notched-outline"> <span class="mdc-notched-outline__leading"></span> <span if="{hint}" class="mdc-notched-outline__notch"> <span class="mdc-floating-label" id="{random + \'_label\'}">{hint}</span> </span> <span class="mdc-notched-outline__trailing"></span> </span> <input if="{!isTextarea}" ref="input" class="mdc-text-field__input" type="{isPassword ? \'password\' : \'text\'}" maxlength="{maxLength}" aria-labelledby="{random + \'_label\'}" aria-controls="{random + \'_helper\'}" aria-describedby="{random + \'_helper\'}" onchange="{onChange}" oninput="{onInput}" onkeypress="{onKeyPress}"> <textarea if="{isTextarea}" ref="input" class="mdc-text-field__input" rows="{textareaRows}" maxlength="{maxLength}" aria-labelledby="{random + \'_label\'}" aria-controls="{random + \'_helper\'}" aria-describedby="{random + \'_helper\'}" onchange="{onChange}" oninput="{onInput}" onkeypress="{onKeyPress}"></textarea> </label> <div class="mdc-text-field-helper-line"> <div class="mdc-text-field-helper-text {mdc-text-field-helper-text--persistent: isHelperPersistent || !isValid, mdc-text-field-helper-text--validation-msg: !isValid}" id="{random + \'_helper\'}" aria-hidden="true"></div> <div if="{maxLength}" class="mdc-text-field-character-counter">0 / {maxLength}</div> </div>', '', '', function(opts) {
    const main = () => {
        this.hint = opts.hint || '';
        let helperMessage = opts.helper || '';
        const invalidMessage = opts.invalid_message || 'invalid input';
        const patternReg = opts.pattern ? new RegExp(`^${opts.pattern}\$`) : null;
        const checkFunc = opts.check_func;
        this.maxLength = opts.max_length || null;
        this.isOutlined = Boolean(opts.is_outlined);
        this.isHelperPersistent = Boolean(opts.is_helper_persistent);
        this.isPassword = Boolean(opts.is_password);
        this.isTextarea = Boolean(opts.is_textarea);
        this.textareaRows = opts.textarea_rows || '4';
        const onEnter = opts.on_enter || (value => { value });
        const _onChange = opts.on_change || (value => { value });
        const _onInput = opts.on_input || (value => { value });
        this.random = generateID(16);
        this.isValid = true;
        let mdcTextField = null;

        this.getValue = () => {
            if (mdcTextField) return mdcTextField.value;
            else return null;
        };
        this.setValue = (value) => {
            if (mdcTextField) mdcTextField.value = value;
            else setTimeout(() => {
                if (mdcTextField) mdcTextField.value = value;
            }, 0);
        };
        this.setHelperMessage = (message) => {
            helperMessage = message;
            if (mdcTextField.valid) mdcTextField.helperTextContent = helperMessage;
        };
        this.focus = () => { mdcTextField.focus() };

        this.onChange = () => {
            const inputValue = mdcTextField.value;
            const lastValid = this.isValid;
            if (checkFunc) this.isValid = checkFunc(inputValue);
            else if (patternReg) this.isValid = Boolean(inputValue.match(patternReg));
            if (this.isValid !== lastValid) {
                this.update();
                mdcTextField.helperTextContent = this.isValid ? helperMessage : invalidMessage;
                mdcTextField.valid = this.isValid;
            }
            _onChange(inputValue);
        };
        this.onInput = () => { _onInput(mdcTextField.value) };
        this.onKeyPress = (event) => {
            if (event.code === 'Enter' || event.key === 'Enter' || event.keyCode === 13) {
                onEnter(mdcTextField.value);
            }
        };
        this.on('mount', () => {
            mdcTextField = new mdc.textField.MDCTextField(this.refs.textfield);
            mdcTextField.useNativeValidation = false;
            mdcTextField.helperTextContent = helperMessage;
        });
    };
    const generateID = (length) => {
        const CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        const CHARS_LENGTH = CHARS.length;
        let code = '';
        for (let i = 0; i < length; i++) code += CHARS[Math.floor(Math.random() * CHARS_LENGTH)];
        return code;
    };
    main();
});
riot.tag2('util-hashcolors', '<svg xmlns="http://www.w3.org/2000/svg" ref="svg" class="svg-frame" version="1.1" riot-width="{width}" riot-height="{height}" riot-viewbox="{viewBox}"></svg>', '', '', function(opts) {
    const INTERVAL = 10;
    const RADIUS = 4;
    const NUM = 8;
    this.width = `${INTERVAL * NUM}`;
    this.height = `${RADIUS * 2}`;
    this.viewBox = `0 0 ${INTERVAL * NUM} ${RADIUS * 2}`;
    this.on('mount', () => { aUpdateValue(opts.seed_value) });
    this.on('update', () => { aUpdateValue(opts.seed_value) });
    const aUpdateValue = async(value) => {
        if (value === this.value) return;
        this.value = value;
        const hash = await aSha256(value);
        const svg = this.refs.svg;
        if (!svg) return;
        while (svg.firstChild) svg.removeChild(svg.lastChild);
        for (let i = 0; i < NUM; i++) {
            const color = `#${hash.slice(i * 6, (i + 1) * 6)}`;
            const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
            circle.setAttribute('cx', `${INTERVAL / 2 + INTERVAL * i}`);
            circle.setAttribute('cy', `${RADIUS}`);
            circle.setAttribute('r', `${RADIUS}`);
            circle.setAttribute('fill', color);
            svg.appendChild(circle);
        }
    };
    const aSha256 = async(text) => {
        const uint8array  = new TextEncoder().encode(text);
        const arrayBuffer = await crypto.subtle.digest('SHA-256', uint8array);
        const hex = Array.from(new Uint8Array(arrayBuffer)).map(num => num.toString(16).padStart(2, '0')).join('');
        return hex;
    };
});
riot.tag2('util-qrcode', '<svg xmlns="http://www.w3.org/2000/svg" class="svg-frame"> <g ref="qrcode"></g> </svg>', '', '', function(opts) {
    this.value = opts.qr_value;

    this.getValue = () => {
        return this.value;
    };
    this.setValue = value => {
        this.value = value;
        this.updateQR();
    };
    this.updateQR = () => {
        if (this.value) this.qrcode.makeCode(this.value);
    };
    this.on('mount', () => {
        this.qrcode = new QRCode(this.refs.qrcode, {
            width: 400,
            height: 400,
            useSVG: true,
        });
        this.updateQR();
    });
});
riot.tag2('util-spinner', '<div if="{isSpinning}" ref="frame" class="spinner-frame {spinner-frame-fulldisplay: isFullDisplay}"></div>', '', '', function(opts) {
    this.isFullDisplay = Boolean(opts.is_fulldisplay);
    const SPINNER_PARAMS = {
        lines: 9,
        length: 12,
        width: 4,
        radius: 16,
        scale: 1,
        corners: 0.5,
        color: '#222222',
        opacity: 0.1,
        rotate: 0,
        direction: 1,
        speed: 1.2,
        trail: 60,
        fps: 20,
        zIndex: 2e9,
        className: 'spinner',
        top: '50%',
        left: '50%',
        shadow: false,
        hwaccel: false,
        position: 'absolute'
    };
    this.isSpinning = false;
    this.spinner = new Spinner(SPINNER_PARAMS);
    this.start = () => {
        if (this.isSpinning) return;
        this.isSpinning = true;
        this.update();
        if (this.refs.frame) this.spinner.spin(this.refs.frame);
    };
    this.stop = () => {
        if (!this.isSpinning) return;
        if (this.refs.frame) this.spinner.stop();
        this.isSpinning = false;
        this.update();
    };
});
