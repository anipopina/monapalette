const BUCKET = process.env['BUCKET'];
const FILEKEY = process.env['FILEKEY'];
const SNS_ARN = process.env['SNS_ARN'];
const aws = require('aws-sdk');
const sns = new aws.SNS();
const S3json = require('./S3json.js');
const Monaparty = require('./Monaparty.js');
Monaparty.timeout = 30 * 1e3;
const common = require('./common.js');

const MAIL_PREFIX = 'monapalette-cachedboard: ';
const DBOARD_PREFIX = '@b.';
const DMESSAGE_PREFIX = '@b.m ';
const DUSER_PREFIX = '@b.u ';
let count = 0;

exports.handler = () => {
const global = async() => {
    const dboardFile = new S3json({ bucketName: BUCKET, fileKey: FILEKEY, useTmp: true, isTestMode: false });
    const main = async() => {
        let { messages, users, assetsInfo, monacards, invalidAssets, noCards, lastBlock } = await dboardFile.aGet();
        const params = {
            'filters': [{ field: 'text', op: 'LIKE', value: `${DBOARD_PREFIX}%` }],
            'start_block': lastBlock + 1,
        };
        const broadcasts = await Monaparty.aCounterPartyRequest('get_broadcasts', params);
        const maxTimestamp = Math.floor(new Date().getTime() / 1000) - 900; // 15分前
        while (broadcasts.length && broadcasts[broadcasts.length - 1].timestamp > maxTimestamp) broadcasts.pop(); // reorg対策
        if (broadcasts.length) lastBlock = broadcasts[broadcasts.length - 1].block_index;
        const newMessages = broadcasts.filter(bro => bro.text.indexOf(DMESSAGE_PREFIX) === 0).sort(sortByTimeDesc);
        const newUsers = broadcasts.filter(bro => bro.text.indexOf(DUSER_PREFIX) === 0).sort(sortByTimeAsc);
        messages.unshift(...newMessages);
        users.push(...newUsers);
        if (count++ % 3 === 0) { // 10分おきの実行(30分おきのtrue)を想定
            const assets = parseAssetsToLoad(messages); // longnameの場合もある
            [assetsInfo, monacards, noCards] = await common.aGetAssets(assets, assetsInfo, monacards);
            const asset2valid = {};
            for (const info of assetsInfo) {
                asset2valid[info.asset] = true;
                if (info.asset_longname) asset2valid[info.asset_longname] = true;
            }
            invalidAssets = assets.filter(ast => !asset2valid[ast]);
        }
        await dboardFile.aPut({ messages, users, assetsInfo, monacards, invalidAssets, noCards, lastBlock }, { useGzip: true });
    };
    const parseAssetsToLoad = (rawMessages) => {
        const assetsToLoad = [];
        const isDeletedIndexAddr = [];
        for (const rawMessage of rawMessages) {
            try {
                const address = rawMessage.source;
                const index = rawMessage.tx_index;
                const rawText = rawMessage.text.slice(DMESSAGE_PREFIX.length);
                const parsedText = JSON.parse(rawText);
                if (isDeletedIndexAddr[`${index}.${address}`]) continue;
                if (typeof parsedText === 'string') {}
                else if (typeof parsedText.d === 'number') isDeletedIndexAddr[`${parsedText.d}.${address}`] = true;
                else {
                    if (typeof parsedText.m !== 'string' && parsedText.m !== undefined) continue;
                    if (typeof parsedText.p === 'string') {
                        const formatCode = parsedText.p.slice(0, 1);
                        const imageIDs = parsedText.p.slice(1).split(',');
                        if (imageIDs.length === 0) continue;
                        switch (formatCode) {
                            case '2': {
                                for (const asset of imageIDs) assetsToLoad.push(asset);
                                break;
                            }
                            default:
                                continue;
                        }
                    }
                }
            }
            catch (error) {}
        }
        return Array.from(new Set(assetsToLoad));
    };
    const sortByTimeAsc = (a, b) => {
        if (a.tx_index > b.tx_index) return 1;
        if (a.tx_index < b.tx_index) return -1;
        return (a.timestamp > b.timestamp ? 1 : -1);
    };
    const sortByTimeDesc = (a, b) => {
        if (a.tx_index < b.tx_index) return 1;
        if (a.tx_index > b.tx_index) return -1;
        return (a.timestamp < b.timestamp ? 1 : -1);
    };
    const onCatch = (error) => {
        console.error(error);
        aNotify(MAIL_PREFIX + 'error catched', stringifyError(error));
    };
    const aNotify = async(subject, message) => {
        const params = { Subject: subject, Message: message || subject, TopicArn: SNS_ARN };
        await sns.publish(params).promise().catch(console.error);
    };
    const stringifyError = (error) => {
        if (error instanceof Error) return `message: ${error.message}\nstack: ${error.stack}`;
        return JSON.stringify(error);
    };
    await main().catch(onCatch);
};
global().catch(console.error);
};
