const fetch = require('node-fetch');
const Monaparty = require('./Monaparty.js');
Monaparty.timeout = 30 * 1e3;

const common = {};
common.aGetAssets = async(assets, lastAssetsInfo = [], lastMonacards = []) => {
    assets = Array.from(new Set(assets));
    const assetsInfo = (await aGetAssetsInfo(assets)) || lastAssetsInfo;
    const assetNames = assetsInfo.map(info => (info.asset_longname || info.asset));
    const monacards = (await aGetMonacards(assetNames)) || lastMonacards;
    const assetName2isCard = {};
    for (const card of monacards) assetName2isCard[card.asset_common_name] = true;
    const noCards = assetNames.filter(name => !assetName2isCard[name]);
    return [assetsInfo, monacards, noCards];
};
module.exports = common;

const aGetAssetsInfo = async(assets) => {
    const ONCE = 250;
    try {
        const promises = [];
        for (let i = 0; i < Math.ceil(assets.length / ONCE); i++) {
            const slicedAssets = assets.slice(ONCE * i, ONCE * (i + 1));
            promises.push(Monaparty.aCounterBlockRequest('get_assets_info', { assetsList: slicedAssets }));
        }
        const assetsInfoArray = await Promise.all(promises);
        const assetsInfo = assetsInfoArray.reduce((joined, nextList) => ([...joined, ...nextList]), []);
        return assetsInfo;
    }
    catch (error) {
        console.error(error);
        return null;
    }
};
const aGetMonacards = async(assetNames) => {
    const URL = 'https://card.mona.jp/api/card_detail_post';
    if (assetNames.length === 0) return [];
    try {
        const fetchParams = {
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/json' },
            body: `assets=${encodeURIComponent(assetNames.join(','))}`,
        };
        let { details: monacards } = await fetch(URL, fetchParams).then(res => res.json());
        return monacards || [];
    }
    catch (error) {
        console.error(error);
        return null;
    }
};
