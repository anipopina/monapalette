const BUCKET = process.env['BUCKET'];
const FILEKEY = process.env['FILEKEY'];
const SNS_ARN = process.env['SNS_ARN'];
const ASSETS = process.env['ASSETS'].split(','); // nameも可
const MAIL_PREFIX = 'monapalette-cacheholders: ';
const aws = require('aws-sdk');
const sns = new aws.SNS();
const S3json = require('./S3json.js');
const Monaparty = require('./Monaparty.js');
Monaparty.timeout = 30 * 1e3;

exports.handler = () => {
const global = async() => {
    const holdersFile = new S3json({ bucketName: BUCKET, fileKey: FILEKEY, useTmp: true, isTestMode: false });
    const main = async() => {
        const asset2addr2qty = {};
        for (const asset of ASSETS) {
            const addr2qty = {};
            // dispenserやdexの分も返ってくる // dex分は別レコード(escrowにtxidが入るけどaddressは販売元)
            const holders = await Monaparty.aCounterPartyRequest('get_holders', { 'asset': asset });
            for (const { address, address_quantity: qty } of holders) {
                if (!addr2qty[address]) addr2qty[address] = 0;
                addr2qty[address] += qty;
            }
            asset2addr2qty[asset] = addr2qty;
        }
        await holdersFile.aPut({ asset2addr2qty }, { useGzip: true });
    };
    const onCatch = (error) => {
        console.error(error);
        aNotify(MAIL_PREFIX + 'error catched', stringifyError(error));
    };
    const aNotify = async(subject, message) => {
        const params = { Subject: subject, Message: message || subject, TopicArn: SNS_ARN };
        await sns.publish(params).promise().catch(console.error);
    };
    const stringifyError = (error) => {
        if (error instanceof Error) return `message: ${error.message}\nstack: ${error.stack}`;
        return JSON.stringify(error);
    };
    await main().catch(onCatch);
};
global().catch(console.error);
};
