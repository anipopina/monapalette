const BUCKET = process.env['BUCKET'];
const FILEKEY = process.env['FILEKEY'];
const SNS_ARN = process.env['SNS_ARN'];
const MAIL_PREFIX = 'monapalette-cachedexassets: ';
const aws = require('aws-sdk');
const sns = new aws.SNS();
const S3json = require('./S3json.js');
const Monaparty = require('./Monaparty.js');
Monaparty.timeout = 30 * 1e3;
const common = require('./common.js');

exports.handler = () => {
const global = async() => {
    const dexAssetsFile = new S3json({ bucketName: BUCKET, fileKey: FILEKEY, useTmp: true, isTestMode: false });
    const main = async() => {
        let { assetsInfo, monacards, noCards } = await dexAssetsFile.aGet();
        const orders = await Monaparty.aCounterPartyRequestTableAll('get_orders', { 'status': 'open' }, {}, { maxPage: 10 });
        const assets = [...orders.map(order => order.give_asset), ...orders.map(order => order.get_asset)];
        [assetsInfo, monacards, noCards] = await common.aGetAssets(assets, assetsInfo, monacards);
        await dexAssetsFile.aPut({ assetsInfo, monacards, noCards }, { useGzip: true });
    };
    const onCatch = (error) => {
        console.error(error);
        aNotify(MAIL_PREFIX + 'error catched', stringifyError(error));
    };
    const aNotify = async(subject, message) => {
        const params = { Subject: subject, Message: message || subject, TopicArn: SNS_ARN };
        await sns.publish(params).promise().catch(console.error);
    };
    const stringifyError = (error) => {
        if (error instanceof Error) return `message: ${error.message}\nstack: ${error.stack}`;
        return JSON.stringify(error);
    };
    await main().catch(onCatch);
};
global().catch(console.error);
};
