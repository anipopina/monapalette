const BUCKET = process.env['BUCKET'];
const FILEKEY = process.env['FILEKEY'];
const SNS_ARN = process.env['SNS_ARN'];
const MAIL_PREFIX = 'monapalette-cachedispensers: ';
const aws = require('aws-sdk');
const sns = new aws.SNS();
const S3json = require('./S3json.js');
const Monaparty = require('./Monaparty.js');
Monaparty.timeout = 30 * 1e3;
const common = require('./common.js');

const DISPENSER_STATUS = { OPEN: 0, CLOSE: 10 };
let count = 0;

exports.handler = () => {
const global = async() => {
    const dispensersFile = new S3json({ bucketName: BUCKET, fileKey: FILEKEY, useTmp: true, isTestMode: false });
    const main = async() => {
        if (count++ % 10 === 0) await aUpdateAll(); // 2分おきの実行(20分おきのtrue)を想定
        else await aUpdateDiff();
    };
    const aUpdateAll = async() => {
        let { dispensers, assetsInfo, monacards, noCards } = await dispensersFile.aGet();
        dispensers = await aGetDispensers()
        const assets = dispensers.map(dispenser => dispenser.asset);
        [assetsInfo, monacards, noCards] = await common.aGetAssets(assets, assetsInfo, monacards);
        await dispensersFile.aPut({ dispensers, assetsInfo, monacards, noCards }, { useGzip: true });
    };
    const aUpdateDiff = async() => {
        let { dispensers, assetsInfo, monacards, noCards } = await dispensersFile.aGet();
        const params = { // status指定なし
            'limit': 100,
            'order_by': 'tx_index',
            'order_dir': 'DESC',
        };
        const dispensers100 = await Monaparty.aCounterPartyRequest('get_dispensers', params);
        const index2dispenser = dictizeListWithKey(dispensers, 'tx_index');
        const asset2info = dictizeListWithKey(assetsInfo, 'asset');
        dispensers100.reverse();
        let newAssets = [];
        for (const newItem of dispensers100) {
            const oldItem = index2dispenser[newItem.tx_index];
            if (oldItem) {
                Object.assign(oldItem, newItem);
            }
            else {
                if (newItem.status === DISPENSER_STATUS.OPEN) {
                    dispensers.unshift(newItem);
                    if (!asset2info[newItem.asset]) newAssets.push(newItem.asset);
                }
            }
        }
        dispensers = dispensers.filter(disp => disp.status === DISPENSER_STATUS.OPEN);
        const [newAssetsInfo, newMonacards, newNoCards] = await common.aGetAssets(newAssets);
        assetsInfo.push(...newAssetsInfo);
        monacards.push(...newMonacards);
        noCards = Array.from(new Set([...noCards, ...newNoCards]));
        await dispensersFile.aPut({ dispensers, assetsInfo, monacards, noCards }, { useGzip: true });
    };
    const aGetDispensers = async() => {
        const params = { // get_dispensersはstatusパラメータが効かないのでフィルタでstatus指定
            'filters': [{ field: 'status', op: '==', value: DISPENSER_STATUS.OPEN }],
        };
        const dispensers = await Monaparty.aCounterPartyRequestTableAll('get_dispensers', params, {}, { maxPage: 10 });
        dispensers.reverse();
        return dispensers;
    };
    const dictizeListWithKey = (list, key) => {
        const dict = {};
        for (const item of list) item[key] && (dict[item[key]] = item);
        return dict;
    };
    const onCatch = (error) => {
        console.error(error);
        aNotify(MAIL_PREFIX + 'error catched', stringifyError(error));
    };
    const aNotify = async(subject, message) => {
        const params = { Subject: subject, Message: message || subject, TopicArn: SNS_ARN };
        await sns.publish(params).promise().catch(console.error);
    };
    const stringifyError = (error) => {
        if (error instanceof Error) return `message: ${error.message}\nstack: ${error.stack}`;
        return JSON.stringify(error);
    };
    await main().catch(onCatch);
};
global().catch(console.error);
};
