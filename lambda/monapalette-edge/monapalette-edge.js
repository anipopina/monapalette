const aws = require('aws-sdk');
const s3 = new aws.S3();
let baseHTML = null;
let twID2shopsData = null;
const shopID2customHTML = {};

exports.handler = (event, context, callback) => {
    const request = event.Records[0].cf.request;
    const customHeaders = request.origin.s3.customHeaders;
    const BUCKET = customHeaders['x-env-bucket'][0].value;
    const FILEKEY = customHeaders['x-env-filekey'][0].value;
    const SHOPSJSON_BUCKET = customHeaders['x-env-shops-bucket'][0].value;
    const SHOPSJSON_FILEKEY = customHeaders['x-env-shops-filekey'][0].value;
    const SHOPS_IMGROOT = customHeaders['x-env-shops-imgroot'][0].value;
    const HOST_URL = customHeaders['x-env-host-url'][0].value;

    const main = async() => {
        if (!baseHTML) {
            const data = await s3.getObject({ Bucket: BUCKET, Key: FILEKEY }).promise();
            baseHTML = data.Body.toString();
            console.log('Loaded baseHTML from S3');
        }
        const path = request.uri;
        const pathSplit = path.split('/');
        const shopID = pathSplit[pathSplit.length - 1];
        let html = shopID2customHTML[shopID];
        if (!html) {
            html = baseHTML;
            try {
                if (shopID.match(/^\d+_[0-9A-F]{8}$/)) {
                    if (!twID2shopsData) {
                        const data = await s3.getObject({ Bucket: SHOPSJSON_BUCKET, Key: SHOPSJSON_FILEKEY }).promise();
                        twID2shopsData = JSON.parse(data.Body.toString()).twID2shopsData;
                        console.log('shops.json from S3');
                    }
                    const [userID, hex] = shopID.split('_');
                    const userShops = twID2shopsData[userID]?.shops || [];
                    const shop = userShops.find(_shop => _shop.hex === hex);
                    if (shop) {
                        const url = HOST_URL + path;
                        const title = escapeHTML(shop.name);
                        const description = escapeHTML(shop.desc);
                        html = html.
                        replace(/<meta\s+property="og:title"\s+content="[^"]*"/, `<meta property="og:title" content="${title}"`).
                        replace(/<meta\s+property="og:description"\s+content="[^"]*"/, `<meta property="og:description" content="${description}"`).
                        replace(/<meta\s+property="og:url"\s+content="[^"]*"/, `<meta property="og:url" content="${url}"`).
                        replace(/<meta\s+name="twitter:card"\s+content="[^"]*"/, `<meta name="twitter:card" content="summary_large_image"`).
                        replace(/<meta\s+name="twitter:title"\s+content="[^"]*"/, `<meta name="twitter:title" content="${title}"`).
                        replace(/<meta\s+name="twitter:description"\s+content="[^"]*"/, `<meta name="twitter:description" content="${description}"`);
                        if (shop.img) {
                            const image = `${SHOPS_IMGROOT}${shopID}`;
                            html = html.
                            replace(/<meta\s+property="og:image"\s+content="[^"]*"/, `<meta property="og:image" content="${image}"`).
                            replace(/<meta\s+name="twitter:image"\s+content="[^"]*"/, `<meta name="twitter:image" content="${image}"`);
                        }
                    }
                }
            }
            catch (error) { console.error(error) }
        }
        const response = {
            status: '200',
            statusDescription: 'OK',
            headers: {
                'content-type': [{ key: 'Content-Type', value: 'text/html' }],
                'content-encoding': [{ key: 'Content-Encoding', value: 'UTF-8' }],
            },
            body: html,
        };
        callback(null, response);
    };
    const escapeHTML = string => {
        return string.replace(/[&'`"<>\n]/g, (match) => {
            return {
                '&': '&amp;',
                "'": '&#x27;',
                '`': '&#x60;',
                '"': '&quot;',
                '<': '&lt;',
                '>': '&gt;',
                '\n': ' ',
            }[match];
        });
    };
    main();
};
