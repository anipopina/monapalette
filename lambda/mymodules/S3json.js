// version: 2022-03-22

const aws = require('aws-sdk');
const s3 = new aws.S3();
const fs = require('fs');
const zlib = require('zlib');

class S3json {

    constructor({ bucketName, fileKey, useTmp, isTestMode }) {
        this.bucketName = bucketName;
        this.fileKey = fileKey;
        this.useTmp = useTmp;
        this.isTestMode = isTestMode;
        this.cachedBody = null;
        this.tmpFilePath = `/tmp/s3jsoncache.${bucketName}.${encodeURIComponent(fileKey)}`;
    }

    async aGet() {
        if (!this.cachedBody) {
            if (this.useTmp && fs.existsSync(this.tmpFilePath)) {
                this.cachedBody = fs.readFileSync(this.tmpFilePath, 'utf8');
            }
            else {
                const params = { Bucket: this.bucketName, Key: this.fileKey };
                this.cachedBody = await s3.getObject(params).promise().
                then(data => {
                    if (data['ContentEncoding'] === 'gzip') return zlib.gunzipSync(data.Body).toString();
                    return data.Body.toString();
                }).
                catch(error => {
                    console.error(`S3json get error bucket: ${this.bucketName}, file: ${this.fileKey}`);
                    throw error;
                });
                if (this.useTmp) fs.writeFileSync(this.tmpFilePath, this.cachedBody);
            }
        }
        return JSON.parse(this.cachedBody);
    }

    async aPut(dict, { indent = 0, useGzip = false } = {}) {
        const body = JSON.stringify(dict, null, indent);
        if (this.useTmp && !this.cachedBody && fs.existsSync(this.tmpFilePath)) { // aGet未使用の場合
            this.cachedBody = fs.readFileSync(this.tmpFilePath, 'utf8');
        }
        if (body === this.cachedBody) return;
        this.cachedBody = body;
        const params = {
            Bucket: this.bucketName,
            Key: this.fileKey,
            Body: useGzip ? zlib.gzipSync(body) : body,
            ContentType: 'application/json',
        };
        if (useGzip) params['ContentEncoding'] = 'gzip';
        if (this.isTestMode) {
            console.log(`TEST: S3json put ${JSON.stringify(params).slice(0, 1000)}`);
            return;
        }
        if (this.useTmp) fs.writeFileSync(this.tmpFilePath, body);
        await s3.upload(params).promise().
        catch(error => {
            console.error(`S3json put error params: ${JSON.stringify(params).slice(0, 1000)}`);
            throw error;
        });
    }
}

module.exports = S3json;
